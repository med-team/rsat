# rbind.fill

# $1 positive.bed.tab
# $2 negative.bed.tab
# $3 output_matrix.tab

Rscript -e "library(data.table);\
dt1=fread('$1', header=T);\
dt2=fread('$2', header=T);\
dt_out=rbind(dt1, dt2, fill=TRUE);\
dt_out[is.na(dt_out)] = 0;\
write.table(data.frame(dt_out, row.names=1), file='$3', quote=F, sep='\t', row.names=T, col.names=NA)
"

