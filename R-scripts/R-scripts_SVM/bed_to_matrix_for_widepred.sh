#!/bin/bash
 
# Execute getopt on the arguments passed to this program, identified by the special character $@
PARSED_OPTIONS=$(getopt -n "$0" -o hm: --long "help,positive_bed:,negative_bed:,genome:,background_freqs:,shuffling:,background_seqs:,genome_info:,ngs:,outdir:"  -- "$@")
 
#Bad arguments, something has gone wrong with the getopt command.
if [ $? -ne 0 ];
then
  exit 1
fi
 
# A little magic, necessary when using getopt.
eval set -- "$PARSED_OPTIONS"

# Now goes through all the options with a case and using shift to analyse 1 argument at a time.
#$1 identifies the first argument, and when we use shift we discard the first argument, so $2 becomes $1 and goes again through the case.
while true;
do
  case "$1" in
 
    -h|--help)
      echo "usage: $0 --help --positive_bed test_small/positive_2seqs.bed --negative_bed test_small/negative_5seqs.bed -m test_small/mfile2.tf --background_freqs data/2nt_intergenic_Drosophila_melanogaster.freq --genome dm3 --outdir test_small/outdir
"
     shift;;
 
    --positive_bed)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        positive_bed=$2
      fi
      shift 2;;
 
    -m)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        matrix_file=$2
      fi
      shift 2;;
 
    --genome)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        genome=$2
      fi
      shift 2;;
      
    --negative_bed)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        negative_bed=$2
      fi
      shift 2;; 
      
    --background_freqs)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        background_freqs=$2
      fi
      shift 2;; 
 
    --shuffling)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        shuffling=$2
      fi
      shift 2;;
 
    --background_seqs)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        background_seqs=$2
      fi
      shift 2;;
 
    --genome_info)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        genome_info=$2
      fi
      shift 2;;

    --ngs)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        ngs=$2
      fi
      shift 2;;
 
    --outdir)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        outdir=$2
      fi
      shift 2;;
 
    --)
      shift
      break;;
  esac
done

## Input
output_matrix=${outdir}/feature_matrix.tab

this_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Positive ---------------------------------------------------
pos_seqs_bed=${positive_bed}
seqs_fasta=${outdir}/positive.bed.fasta
seqs_ft=${outdir}/positive.bed.ft
data_tab_pos=${outdir}/positive.bed.tab
class=1

# returns $data_tab_pos=${outdir}/crms.bed.tab
cmd="bash $this_dir/create_matrix.sh --bed $pos_seqs_bed --fasta $seqs_fasta --rsat_ft $seqs_ft  --background_freqs $background_freqs -m $matrix_file --class $class --genome $genome --out_matrix $data_tab_pos";
#echo $cmd
eval $cmd &

wait


if [ -n "$ngs" ];
	#processing of wig files if specified
	then
	IFS=',' read -a array <<< "${ngs}"
	bedArray=()
	wigArray=()
	for ((i=0; i < ${#array[@]}; i++))
		do
  		filename=$(basename "${array[$i]}")
#		echo Long name, short extension
#		echo name: "${filename%.*}"
#		echo extension: "${filename##*.}"
		if [ ${filename##*.} == "wig" ];
			then
			wigArray+=(${array[$i]})
		elif [ ${filename##*.} == "bw" ];
			then
			wigArray+=(${array[$i]})
		elif [ ${filename##*.} == "bigwig" ];
			then
			wigArray+=(${array[$i]})
		elif [ ${filename##*.} == "bed" ];
			then
			bedArray+=(${array[$i]})
		fi
	done
	positive_chipseq_signal=${outdir}/positive_wigCov.csv

	positive_matrix="positive_matrix.csv"


	####dealing with wig files if any#########
	if [ ${#wigArray[@]} -eq 0 ];
	then
		echo "non empty"
	else
		####positive regions#########
		$this_dir/java_genomics_toolkit/toolRunner.sh ngs.IntervalStats -l $pos_seqs_bed -o ${positive_chipseq_signal} -s mean "${wigArray[@]}"
		ls -lthr | awk -v genome=${genome} '{out=$7; for(i=8;i<=NF;i++){out=out"\t"$i}; print genome"_"$1"_"$2"_"$3"_"$6"\t"out}' ${positive_chipseq_signal} > ${outdir}/tmp_pos2.csv
		join <(sort ${data_tab_pos}) <(sort ${outdir}/tmp_pos2.csv)  -t $'\t' > ${outdir}/${positive_matrix} # vertical merge of motifs and chipseq data

		wait

		for ((i=0; i < ${#wigArray[@]}; i++))
			do
			filename=$(basename "${wigArray[$i]}")
			if [ $i == 0 ];
				then
					wigHeader=$filename
				else	
					wigHeader=$wigHeader"\t"$filename
				fi
			done

		## Merge positive and negative seqs in one table with one header--------------------------------
		cat ${outdir}/${positive_matrix} > ${output_matrix}
		header="Regions"$(head ${data_tab_pos} -n1)"\t"$wigHeader # create header of new table
		sed -i "1i${header}" ${output_matrix} # insert header at top
		
		

	fi

	if [ ${#bedArray[@]} -eq 0 ];
	then
		echo "bed empty"
	else
		for ((i=0; i < ${#bedArray[@]}; i++))
			do
				####positive regions#########
				/data/rsat/bin/intersectBed -a $pos_seqs_bed -b ${bedArray[$i]} -c > ${outdir}/tmp_bedpos$i.csv
				awk -v genome=${genome} '{ if ( $4 > 1 ) { $4 = 1 }; print genome"_"$1"_"$2"_"$3"_+\t"$4}' ${outdir}/tmp_bedpos$i.csv > ${outdir}/tmp_bedposBin$i.csv
	
				filename=$(basename "${bedArray[$i]}")
				if [ $i == 0 ];
				then
					bedHeader=$filename
				else	
					bedHeader=$bedHeader"\t"$filename
				fi
			done
	

	if [ ${#wigArray[@]} -eq 0 ];
	then
		##positive##
		awk 'END { for (K in k) print K, k[K] }{ k[$1] = k[$1] ? k[$1] FS $2 : $2 }' ${outdir}/tmp_bedposBin* > ${outdir}/positiveBedChip.tab
		awk -v OFS="\t" '$1=$1' ${outdir}/positiveBedChip.tab > ${outdir}/positiveBedChip2.tab
		join <(sort ${data_tab_pos}) <(sort ${outdir}/positiveBedChip2.tab)  -t $'\t' > ${outdir}/postivieBedChip.tab
		
		##headering###
		cat ${outdir}/postivieBedChip.tab > ${output_matrix}
		header="Regions"$(head ${data_tab_pos} -n1)"\t"$bedHeader # create header of new table
		sed -i "1i${header}" ${output_matrix} # insert header at top

	else
		##positive##
		awk 'END { for (K in k) print K, k[K] }{ k[$1] = k[$1] ? k[$1] FS $2 : $2 }' ${outdir}/tmp_bedposBin* > ${outdir}/positiveBedChip.tab
		awk -v OFS="\t" '$1=$1' ${outdir}/positiveBedChip.tab > ${outdir}/positiveBedChip2.tab
		join <(sort ${outdir}/${positive_matrix}) <(sort ${outdir}/positiveBedChip2.tab)  -t $'\t' > ${outdir}/postivieBedChip.tab
	
		cat ${outdir}/postivieBedChip.tab > ${output_matrix}
		header=$header"\t"$bedHeader # create header of new table
		sed -i "1i${header}" ${output_matrix}
	fi
	fi
else
	#no chip files are provided
	cp $data_tab_pos ${output_matrix}

fi
