#!/bin/bash
 
# Execute getopt on the arguments passed to this program, identified by the special character $@
PARSED_OPTIONS=$(getopt -n "$0" -o hm: --long "help,matrix_file:,mlist:,outdir:"  -- "$@")
 
#Bad arguments, something has gone wrong with the getopt command.
if [ $? -ne 0 ];
then
  exit 1
fi
 
# A little magic, necessary when using getopt.
eval set -- "$PARSED_OPTIONS"

# Now goes through all the options with a case and using shift to analyse 1 argument at a time.
#$1 identifies the first argument, and when we use shift we discard the first argument, so $2 becomes $1 and goes again through the case.
while true;
do
  case "$1" in
 
    -h|--help)
      echo "usage: $0 --help --matrix_file demo/259_matrices.tf --mlist demo/mlist.txt --outdir tmp/splitMatrices"
     shift;;
 
    --matrix_file)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        matrix_file=$2
      fi
      shift 2;;
 
    --mlist)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        mlist=$2
      fi
      shift 2;;
     
    --outdir)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        outdir=$2
      fi
      shift 2;;
    
    --)
      shift
      break;;
  esac
done

## Input
rm ${outdir}/splitMatrices/
mkdir -p ${outdir}/splitMatrices/
cp ${mlist} ${outdir}/splitMatrices/
filtered_matrix=./filtered_matrices.tab

$RSAT/perl-scripts/convert-matrix -i ${matrix_file} -from transfac -to transfac -split -o ${outdir}/splitMatrices/m

cd ${outdir}/splitMatrices/
$RSAT/perl-scripts/convert-matrix -mlist ${mlist} -from transfac -to transfac -o ${filtered_matrix}

rename 's/\.tab$/\.tf/' ${filtered_matrix}


