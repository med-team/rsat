#!/bin/bash
 
# Execute getopt on the arguments passed to this program, identified by the special character $@
PARSED_OPTIONS=$(getopt -n "$0" -o hm: --long "help,bed:,fasta:,rsat_ft:,background_freqs:,class:,genome:,out_matrix:"  -- "$@")
 
#Bad arguments, something has gone wrong with the getopt command.
if [ $? -ne 0 ];
then
  exit 1
fi
 
# A little magic, necessary when using getopt.
eval set -- "$PARSED_OPTIONS"


# Now goes through all the options with a case and using shift to analyse 1 argument at a time.
#$1 identifies the first argument, and when we use shift we discard the first argument, so $2 becomes $1 and goes again through the case.
while true;
do
  case "$1" in
 
    -h|--help)
      echo "usage: $0 --help --bed test_small/positive_2seqs.bed --fasta test_small/outdir/positive.bed.fasta --rsat_ft test_small/outdir/positive.bed.ft --background_freqs data/2nt_intergenic_Drosophila_melanogaster.freq -m test_small/mfile2.tf --class 1 --genome dm3 --out_matrix test_small/outdir/positive.bed.tab"
     shift;;
 
    --bed)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        seqs_bed=$2
      fi
      shift 2;;
 
    --fasta)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        seqs_fasta=$2
      fi
      shift 2;;
 
    --rsat_ft)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        seqs_ft=$2
      fi
      shift 2;;
 
    --background_freqs)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        background_freqs=$2
      fi
      shift 2;;
 
    -m)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        matrix_file=$2
      fi
      shift 2;;
 
    --class)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        class=$2
      fi
      shift 2;;
 
    --genome)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        genome=$2
      fi
      shift 2;;
 
    --out_matrix)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        data_tab=$2
      fi
      shift 2;;
 
    --)
      shift
      break;;
  esac
done

this_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
    
# Extract sequences : generate multiFasta
#echo "$RSAT/perl-scripts/fetch-sequences -genome $genome -i $seqs_bed -o $seqs_fasta"
cmd="$RSAT/perl-scripts/fetch-sequences -genome ${genome} -i ${seqs_bed} -o ${seqs_fasta}"
#echo $cmd
eval $cmd

# Matrix-scan (about 10min)
#echo $matrix_file
cmd="$RSAT/perl-scripts/matrix-scan -v 1 -quick -matrix_format transfac -pseudo 1 -decimals 1 -2str -origin end -bgfile ${background_freqs} -bg_pseudo 0.01 -return sites,pval -uth pval 0.001 -n skip -m $matrix_file -i ${seqs_fasta} -o ${seqs_ft}"
#echo $cmd
eval $cmd

# Transform matrix-scan file to matrix # 10min
python $this_dir/rsat_features_file_to_matrix.py --ft ${seqs_ft} --fasta ${seqs_fasta} --tab ${data_tab}
#cmd="Rscript $this_dir/rsat_ft_file_to_matrix_cli.R  ${seqs_fasta} ${seqs_ft} ${data_tab}"
#echo $cmd
#eval $cmd

# Prepare matrix for SVM
sed -i "s/\t/\t$class\t/1" ${data_tab} # add class field
sed -i "s/^\t$class\t/\tcl\t/1" ${data_tab} # replace class field in first line by cl label

