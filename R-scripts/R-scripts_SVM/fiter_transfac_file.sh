#!/bin/bash
 
# Execute getopt on the arguments passed to this program, identified by the special character $@
PARSED_OPTIONS=$(getopt -n "$0" -o h --long "help,tf_matrices:,tf_list:,out_tf_matrices:"  -- "$@")
 
#Bad arguments, something has gone wrong with the getopt command.
if [ $? -ne 0 ];
then
  exit 1
fi
 
# A little magic, necessary when using getopt.
eval set -- "$PARSED_OPTIONS"

# Now goes through all the options with a case and using shift to analyse 1 argument at a time.
#$1 identifies the first argument, and when we use shift we discard the first argument, so $2 becomes $1 and goes again through the case.
while true;
do
  case "$1" in
 
    -h|--help)
      echo "usage: $0 --help --tf_matrices demo/259_matrices.tf --tf_list demo/mlist.txt --out_tf_matrices tmp/splitMatrices
"
     shift;;
 
    --tf_matrices)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        tf_matrices=$2
      fi
      shift 2;;
 
    --tf_list)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        tf_list=$2
      fi
      shift 2;;
 
    --out_tf_matrices)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        out_tf_matrices=$2
      fi
      shift 2;;
 
    --)
      shift
      break;;
  esac
done

#tf_matrices=test_filter_matrix/mfile10.tf
#tf_list=test_filter_matrix/mlist5.txt
#out_tf_matrices=test_filter_matrix/mfile5.tf

tf_matrices_fn=`basename $tf_matrices`
tf_list_fn=`basename $tf_list`
out_tf_matrices_fn=`basename $out_tf_matrices`
tmpdir=`mktemp -d` # create temp dir
currentdir=$PWD

cp $tf_matrices $tmpdir
cp $tf_list $tmpdir
cd $tmpdir
mkdir splitted_matrices

$RSAT/perl-scripts/convert-matrix -i ${tf_matrices_fn} -from transfac -to transfac -split -o ${tmpdir}/splitted_matrices/m
#$RSAT/perl-scripts/convert-matrix -i ${matrix_file} -from transfac -to transfac -split -o ${outdir}/splitMatrices/m

rename 's/\.transfac$//' splitted_matrices/* # strip transfac suffix
rename 's/m\_//' splitted_matrices/m_* # strip m_ prefix

cd splitted_matrices
$RSAT/perl-scripts/convert-matrix -mlist ../${tf_list_fn} -from transfac -to transfac -o ../${out_tf_matrices_fn}

cd $currentdir
cp $tmpdir/$out_tf_matrices_fn $out_tf_matrices


