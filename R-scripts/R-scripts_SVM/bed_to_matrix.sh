#!/bin/bash


RSAT=/data/rsat 
# Execute getopt on the arguments passed to this program, identified by the special character $@
PARSED_OPTIONS=$(getopt -n "$0" -o hm: --long "help,positive_bed:,negative_bed:,genome:,background_freqs:,shuffling:,background_seqs:,genome_info:,ngs:,outdir:"  -- "$@")
 
#Bad arguments, something has gone wrong with the getopt command.
if [ $? -ne 0 ];
then
  exit 1
fi
 
# A little magic, necessary when using getopt.
eval set -- "$PARSED_OPTIONS"

# Now goes through all the options with a case and using shift to analyse 1 argument at a time.
#$1 identifies the first argument, and when we use shift we discard the first argument, so $2 becomes $1 and goes again through the case.
while true;
do
  case "$1" in
 
    -h|--help)
      echo "usage: $0 --help --positive_bed test_small/positive_2seqs.bed --negative_bed test_small/negative_5seqs.bed -m test_small/mfile2.tf --background_freqs data/2nt_intergenic_Drosophila_melanogaster.freq --genome dm3 --outdir test_small/outdir
"
     shift;;
 
    --positive_bed)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        positive_bed=$2
      fi
      shift 2;;
 
    -m)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        matrix_file=$2
      fi
      shift 2;;
 
    --genome)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        genome=$2
      fi
      shift 2;;
      
    --negative_bed)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        negative_bed=$2
      fi
      shift 2;; 
      
    --background_freqs)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        background_freqs=$2
      fi
      shift 2;; 
 
    --shuffling)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        shuffling=$2
      fi
      shift 2;;
 
    --background_seqs)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        background_seqs=$2
      fi
      shift 2;;
 
    --genome_info)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        genome_info=$2
      fi
      shift 2;;

    --ngs)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        ngs=$2
      fi
      shift 2;;
 
    --outdir)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        outdir=$2
      fi
      shift 2;;
 
    --)
      shift
      break;;
  esac
done

## Input
output_matrix=${outdir}/feature_matrix.tab
chromInfo=$RSAT/data/svmrsat/genomes/$genome.genome
this_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

## Other inputs and variables
#background_seqs=$this_dir"/../data/dmel-all-intergenic_and_introns-promoters+-500bp_r5.57.bed"
#genomeInfo=$this_dir"/../data/dm3ChromInfo.txt"
#genome='dm3'
#negative_seqs_bed=$outdir/"background_"$(basename ${positive_bed}) # negative set
#ls -lh $negative_seqs_bed
#
#background_freqs=$this_dir"/../data/2nt_intergenic_Drosophila_melanogaster.freq"
#matrix_file=$this_dir"/../data/259_matrices.tf"

if [ -n "$shuffling" ];
# Create negative set by shuffling background seqs with positive bed statistics ------------
#echo ${negative_seqs_bed}
then
    rm -f ${negative_seqs_bed}
	negative_seqs_bed=$outdir/"negative_"$(basename ${positive_bed}) # negative set
	for ((i = 1; i <= $shuffling; i += 1))
		do
			shuffleBed -seed $i -incl ${background_seqs} -chrom -i ${positive_bed} -g $chromInfo >> ${negative_seqs_bed}
	done
else
	negative_seqs_bed=$negative_bed
fi


# Positive ---------------------------------------------------
pos_seqs_bed=${positive_bed}
seqs_fasta=${outdir}/positive.bed.fasta
seqs_ft=${outdir}/positive.bed.ft
data_tab_pos=${outdir}/positive.bed.tab
class=1

# returns $data_tab_pos=${outdir}/crms.bed.tab
cmd="bash $this_dir/create_matrix_RSAT_R.sh --bed $pos_seqs_bed --fasta $seqs_fasta --rsat_ft $seqs_ft  --background_freqs $background_freqs -m $matrix_file --class $class --genome $genome --out_matrix $data_tab_pos --outdir ${outdir}";

eval $cmd &


# Negative ---------------------------------------------------
neg_seqs_bed=${negative_seqs_bed}
seqs_fasta=${outdir}/negative.bed.fasta
seqs_ft=${outdir}/negative.bed.ft
data_tab_neg=${outdir}/negative.bed.tab
class=-1

# returns $data_tab_pos=${outdir}/crms.bed.tab
cmd="bash $this_dir/create_matrix_RSAT_R.sh --bed $neg_seqs_bed --fasta $seqs_fasta --rsat_ft $seqs_ft  --background_freqs $background_freqs -m $matrix_file --class $class --genome $genome --out_matrix $data_tab_neg --outdir ${outdir}";

#echo $cmd
eval $cmd &

wait

bash ${this_dir}/rbind_fill_matrix.sh ${data_tab_pos} ${data_tab_neg} ${output_matrix}
header=$(head -n1 ${output_matrix})
tail -n +2 ${output_matrix} > ${outdir}/feature_matrix2.tab

if [ -n "$ngs" ];


cat $pos_seqs_bed $neg_seqs_bed > ${outdir}/posNeg_seqs_bed.bed

	#processing of wig files if specified
	then
	IFS=',' read -a array <<< "${ngs}"
	bedArray=()
	wigArray=()
	for ((i=0; i < ${#array[@]}; i++))
		do
  		filename=$(basename "${array[$i]}")
#		echo Long name, short extension
#		echo name: "${filename%.*}"
#		echo extension: "${filename##*.}"
		if [ ${filename##*.} == "wig" ];
			then
			wigArray+=(${array[$i]})
		elif [ ${filename##*.} == "bw" ];
			then
			wigArray+=(${array[$i]})
		elif [ ${filename##*.} == "bigwig" ];
			then
			wigArray+=(${array[$i]})
		elif [ ${filename##*.} == "bed" ];
			then
			bedArray+=(${array[$i]})
		fi
	done

	chip_seq_signal=${outdir}/posNeg_seqs_wigcov.tab
	ngs_matrix=${outdir}/posNeg_seqs_matrix.tab

	####dealing with wig files if any#########
	if [ ${#wigArray[@]} -eq 0 ];
	then
		echo "no wig"
	else
		for ((i=0; i < ${#wigArray[@]}; i++))
			do
			filename=$(basename "${wigArray[$i]}")
			if [ $i == 0 ];
				then
					wigHeader=":"$filename
				else	
					wigHeader=$wigHeader"\t"$filename
				fi
			done

		$this_dir/java_genomics_toolkit/toolRunner.sh ngs.IntervalStats -l ${outdir}/posNeg_seqs_bed.bed -o ${chip_seq_signal} -s mean "${wigArray[@]}"
		ls -lthr | awk -v genome=${genome} '{out=$7; for(i=8;i<=NF;i++){out=out"\t"$i}; print genome"_"$1"_"$2"_"$3"_"$6"\t"out}' ${chip_seq_signal} > ${outdir}/tmp_chipSignal.tab
		tail -n +2 ${outdir}/tmp_chipSignal.tab > ${outdir}/tmp_chipSignal2.tab
	if [ ${#bedArray[@]} -eq 0 ];  #if there is a no bed but a wig
	then
		join <(sort ${outdir}/feature_matrix2.tab) <(sort ${outdir}/tmp_chipSignal2.tab)  -t $'\t' > ${outdir}/output.tab
		sed -i "1i${header}${wigHeader}" ${outdir}/output.tab
	fi
	
	fi

	if [ ${#bedArray[@]} -eq 0 ];
	then
		echo "no bed"
	else
		for ((i=0; i < ${#bedArray[@]}; i++))
			do
				filename=$(basename "${bedArray[$i]}")
				if [ $i == 0 ];
				then
					bedHeader=":"$filename
				else	
					bedHeader=$bedHeader"\t"$filename
				fi

				/data/rsat/bin/intersectBed -a ${outdir}/posNeg_seqs_bed.bed -b ${bedArray[$i]} -c > ${outdir}/tmp_bed$i.tab
				awk -v genome=${genome} '{ if ( $4 > 1 ) { $4 = 1 }; print genome"_"$1"_"$2"_"$3"_+\t"$4}' ${outdir}/tmp_bed$i.tab > ${outdir}/tmp_bedBin$i.tab

			done
		awk 'NF > 0 { a[$1] = a[$1] " " $2 } END { for (i in a) { print i a[i]; } }' ${outdir}/tmp_bedBin* > ${outdir}/BedChip.tab
		awk -v OFS="\t" '$1=$1' ${outdir}/BedChip.tab > ${outdir}/BedChip2.tab

	if [ ${#wigArray[@]} -eq 0 ];  #if there is a bed but not a wig
	then
		join <(sort ${outdir}/feature_matrix2.tab) <(sort ${outdir}/BedChip2.tab)  -t $'\t' > ${outdir}/output.tab
		sed -i "1i${header}${bedHeader}" ${outdir}/output.tab
		
	else		#if there is a bed and a wig
		join <(sort ${outdir}/feature_matrix2.tab) <(sort ${outdir}/tmp_chipSignal2.tab)  -t $'\t' > ${outdir}/output0.tab
		join <(sort ${outdir}/output0.tab) <(sort ${outdir}/BedChip2.tab)  -t $'\t' > ${outdir}/output.tab
		sed -i "1i${header}${wigHeader}${bedHeader}" ${outdir}/output.tab
		
	fi
		
	fi
	sed -i "s/:/\t/g" ${outdir}/output.tab
	head ${outdir}/output.tab
else	#there is no NGS data
	sed -i "1i${header}" ${outdir}/feature_matrix2.tab
	head ${outdir}/feature_matrix2.tab

fi


