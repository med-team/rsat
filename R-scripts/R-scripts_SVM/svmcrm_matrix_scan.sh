#!/bin/bash
 

RSAT=/data/rsat
# Execute getopt on the arguments passed to this program, identified by the special character $@
PARSED_OPTIONS=$(getopt -n "$0" -o hm: --long "help,fasta:,bgfile:,output:,outdir:"  -- "$@")
 
#Bad arguments, something has gone wrong with the getopt command.
if [ $? -ne 0 ];
then
  exit 1
fi
 
# A little magic, necessary when using getopt.
eval set -- "$PARSED_OPTIONS"

# Now goes through all the options with a case and using shift to analyse 1 argument at a time.
#$1 identifies the first argument, and when we use shift we discard the first argument, so $2 becomes $1 and goes again through the case.
while true;
do
  case "$1" in
 
    -h|--help)
      echo "usage: $0 --help --fasta seq1.fasta --matrix_file dTCF_tin.tf --bgfile 2nt_intergenic_Drosophila_melanogaster.freq --output out_dTCF_tin_mc.ft
"
     shift;;
 
    --fasta)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        fasta=$2
      fi
      shift 2;;
 
    -m)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        matrix_file=$2
      fi
      shift 2;;
      
    --bgfile)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        bgfile=$2
      fi
      shift 2;; 
      
    --output)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        output=$2
      fi
      shift 2;; 
   
    --outdir)
 
      # We need to take the option of this argument
      if [ -n "$2" ];
      then
        outdir=$2
      fi
      shift 2;;
 
    --)
      shift
      break;;
  esac
done


# Usage: time bash matrix_scan_mc.sh seq1.fasta dTCF_tin.tf 2nt_intergenic_Drosophila_melanogaster.freq out_dTCF_tin_mc.ft

# Input vars
#fasta=$1
#matrix_file=$2
#bgfile=$3
#outft=$4

#tmpdir=${outdir}
#mkdir -p ${tmpdir}
cmd="$RSAT/perl-scripts/convert-matrix -i ${matrix_file} -from transfac -to transfac -split -o ${outdir}/m"
eval $cmd

mkdir ${outdir}/output_split
output_split=${outdir}/output_split

for tf in $(ls ${outdir}/*.transfac); 
do 

tf_name=$(basename $tf);

cmd="$RSAT/perl-scripts/matrix-scan -v 1 -quick -matrix_format transfac -decimals 1 -2str -origin end -bgfile ${bgfile} -return sites,pval -uth pval 0.001 -n skip -m ${tf} -i ${fasta} -o ${output_split}/${tf_name}_out.ft; sed -i '/^;/d' ${output_split}/${tf_name}_out.ft"
#echo $cmd
eval $cmd &
done 

wait

cat <(cat ${output_split}/* | head -n1) <(cat ${output_split}/* |sort |sed '/^#/ d') >$output

