package MyTypes::OligoDiffRequest;
use strict;
use warnings;


__PACKAGE__->_set_element_form_qualified(0);

sub get_xmlns { 'urn:RSATWS' };

our $XML_ATTRIBUTE_CLASS;
undef $XML_ATTRIBUTE_CLASS;

sub __get_attr_class {
    return $XML_ATTRIBUTE_CLASS;
}

use Class::Std::Fast::Storable constructor => 'none';
use base qw(SOAP::WSDL::XSD::Typelib::ComplexType);

Class::Std::initialize();

{ # BLOCK to scope variables

my %output_of :ATTR(:get<output>);
my %verbosity_of :ATTR(:get<verbosity>);
my %test_of :ATTR(:get<test>);
my %tmp_test_infile_of :ATTR(:get<tmp_test_infile>);
my %control_of :ATTR(:get<control>);
my %tmp_control_infile_of :ATTR(:get<tmp_control_infile>);
my %side_of :ATTR(:get<side>);
my %nopurge_of :ATTR(:get<nopurge>);
my %length_of :ATTR(:get<length>);
my %noov_of :ATTR(:get<noov>);
my %str_of :ATTR(:get<str>);
my %lth_of :ATTR(:get<lth>);
my %uth_of :ATTR(:get<uth>);

__PACKAGE__->_factory(
    [ qw(        output
        verbosity
        test
        tmp_test_infile
        control
        tmp_control_infile
        side
        nopurge
        length
        noov
        str
        lth
        uth

    ) ],
    {
        'output' => \%output_of,
        'verbosity' => \%verbosity_of,
        'test' => \%test_of,
        'tmp_test_infile' => \%tmp_test_infile_of,
        'control' => \%control_of,
        'tmp_control_infile' => \%tmp_control_infile_of,
        'side' => \%side_of,
        'nopurge' => \%nopurge_of,
        'length' => \%length_of,
        'noov' => \%noov_of,
        'str' => \%str_of,
        'lth' => \%lth_of,
        'uth' => \%uth_of,
    },
    {
        'output' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'verbosity' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
        'test' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'tmp_test_infile' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'control' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'tmp_control_infile' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'side' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'nopurge' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
        'length' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
        'noov' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
        'str' => 'SOAP::WSDL::XSD::Typelib::Builtin::int',
        'lth' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'uth' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
    },
    {

        'output' => 'output',
        'verbosity' => 'verbosity',
        'test' => 'test',
        'tmp_test_infile' => 'tmp_test_infile',
        'control' => 'control',
        'tmp_control_infile' => 'tmp_control_infile',
        'side' => 'side',
        'nopurge' => 'nopurge',
        'length' => 'length',
        'noov' => 'noov',
        'str' => 'str',
        'lth' => 'lth',
        'uth' => 'uth',
    }
);

} # end BLOCK







1;


=pod

=head1 NAME

MyTypes::OligoDiffRequest

=head1 DESCRIPTION

Perl data type class for the XML Schema defined complexType
OligoDiffRequest from the namespace urn:RSATWS.

Parameters for the operation oligo_diff.




=head2 PROPERTIES

The following properties may be accessed using get_PROPERTY / set_PROPERTY
methods:

=over

=item * output


=item * verbosity


=item * test


=item * tmp_test_infile


=item * control


=item * tmp_control_infile


=item * side


=item * nopurge


=item * length


=item * noov


=item * str


=item * lth


=item * uth




=back


=head1 METHODS

=head2 new

Constructor. The following data structure may be passed to new():

 { # MyTypes::OligoDiffRequest
   output =>  $some_value, # string
   verbosity =>  $some_value, # int
   test =>  $some_value, # string
   tmp_test_infile =>  $some_value, # string
   control =>  $some_value, # string
   tmp_control_infile =>  $some_value, # string
   side =>  $some_value, # string
   nopurge =>  $some_value, # int
   length =>  $some_value, # int
   noov =>  $some_value, # int
   str =>  $some_value, # int
   lth =>  $some_value, # string
   uth =>  $some_value, # string
 },




=head1 AUTHOR

Generated by SOAP::WSDL

=cut

