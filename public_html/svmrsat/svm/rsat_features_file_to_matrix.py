#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import sys

import yaml
from Bio import SeqIO
from collections import defaultdict
from pandas import DataFrame
import math

import pdb

def readFastaSeqs(fninfpath):
    #################################
    ## read seq lengths in fasta file
    seqlengths = {}
    handle = open(fninfpath, "r")
    for record in SeqIO.parse(handle, "fasta"):
        seqlengths[record.id] = len(record.seq)
    handle.close()
    return seqlengths

def readFtFile(fninfpath2):
  #################################
  ## read pvals as log10
  pwmdic={}
  pvallog10dict = {}
  ##
  for l in open(fninfpath2).readlines():
    if not (l.startswith(";") or l.startswith("#")):
      l = l.strip().split("\t")
      seq = l[0]
      pwm = l[2]
      pvallog10 = math.log10(float(l[8]))
      pwmdic[pwm] = ""
      if not seq in pvallog10dict:
        pvallog10dict[seq] = {pwm : [pvallog10]}
      else:
        #pwmdict = pvallog10dict[seq]
        if not pwm in pvallog10dict[seq]:
          pvallog10dict[seq][pwm] = [pvallog10]
        else:
          pvallog10dict[seq][pwm].append(pvallog10)
  return pvallog10dict, pwmdic

def makeMatrix(pvallog10dict, pwmdic, seqlengths):
  #################################
  ## make matrix
  matrixdic = {}
  for seq in seqlengths:
    # need to remove one from start coordinate that rsat adds w/out apparent raison
    seq_split=seq.split("_")
    seq_split[2]=str(int(seq_split[2])-1)   
    seq2="_".join(seq_split)
    # need to remove one from start coordinate that rsat adds w/out apparent raison
    #pdb.set_trace()
    matrixdic[seq2] = {}
    for pwm in pwmdic:
      #print seq,pwm
      matrixdic[seq2][pwm] = 0
      if seq in pvallog10dict:
        if pwm in pvallog10dict[seq]:
          matrixdic[seq2][pwm] = -sum(pvallog10dict[seq][pwm])/seqlengths[seq]
  #print matrixdic
  outdf = DataFrame.from_dict(matrixdic,"index")
  outdf = outdf.reindex_axis(sorted(outdf.columns), axis=1) # reorder columns
  return outdf

def entry_function(input, ft_path, fasta_path, csv_path):
    seqlengths = readFastaSeqs(fasta_path)
    pvallog10dict, pwmdic = readFtFile(ft_path)
    outdf = makeMatrix(pvallog10dict, pwmdic, seqlengths)
    outdf.to_csv(csv_path,sep=',')

def create_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', dest='input', help="Prints it to stdout.")
    parser.add_argument('--ft', help="RSAT feature files")
    parser.add_argument('--fasta', help="Multi sequence fasta file with sequence used for fasta analysis.")
    parser.add_argument('--csv', help="CSV file with matrix (rows: seqs, columns: matrices)")
    return parser

def main():
    parser = create_parser()
    args = parser.parse_args()
    entry_function(args.input, args.ft, args.fasta, args.csv)

if __name__=='__main__':
    main()

