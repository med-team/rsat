seqs_bed=$1
seqs_fasta=$2
seqs_ft=$3
background_freqs=$4
matrix_file=$5
data_csv=$6
class=$7
maindir=$8

this_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Extract sequences : generate multiFasta
/data/rsat/perl-scripts/fetch-sequences -genome dm3 -i ${seqs_bed} -o ${seqs_fasta}

# Matrix-scan (about 10min)
# /data/rsat/perl-scripts/matrix-scan -v 1 -quick -matrix_format transfac -pseudo 1 -decimals 1 -2str -origin end -bgfile ${background_freqs} -bg_pseudo 0.01 -return sites,pval -uth pval 0.001 -n skip -m $matrix_file -i ${seqs_fasta} -o ${seqs_ft}

#ls -lh ${seqs_ft}
#ls -lh ${seqs_fasta}

# Transform matrix-scan file to matrix # 10min
python $this_dir/rsat_features_file_to_matrix.py --ft ${seqs_ft} --fasta ${seqs_fasta} --csv ${data_csv}

#ls -lh ${data_csv}

# Prepare matrix for SVM
sed -i "s/,/,$class,/1" ${data_csv} # add class field
sed -i "s/^,$class,/,cl,/1" ${data_csv} # replace class field in first line by cl label

