<?php

require('functions.php');

$cmd="bash /data/rsat/public_html/svm/svm/bed_to_matrix.sh"; # will store command

$workingdir = "/tmp/rsatsvm_".date("Ymd_His")."_".randchar(6);

print "Working dir: " . $workingdir . "<br>";

$matrixtmpfn = $_FILES["feature_matrix"]["tmp_name"];
$matrixfn = $_FILES["feature_matrix"]["name"];
move_uploaded_file($matrixtmpfn, $workingdir . "/" . $matrixfn);

$cmd .= " ".$workingdir . "/" . $matrixfn ; 

// Check that kernel has been specified
$genome = $_REQUEST["genome"];
$cmd .= " ".$genome ; 

// Shuffling for generatcp svm/demo/*.fting negative set
$shuffling = $_REQUEST["shuffling"];
$cmd .= " ".$shuffling ; 

$cmd .= " ".$workingdir ; 

print $cmd;

## copy ft files to skip
#$output = shell_exec("/bin/cp /data/rsat/public_html/svm/svm/demo/*.ft $workingdir");
## bed_to_matrix
#$output = shell_exec($cmd);
## files in output
#$output = shell_exec("ls -lh $workingdir");
#echo "<pre>$output</pre>";

?>

