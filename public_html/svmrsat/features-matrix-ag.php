<html>
<head>
<title>RSAT - Features matrix</title>
<link rel="stylesheet" type="text/css" href = "main_grat.css" media="screen">
   </head>
   <body class="results"> 

<?php
// Load RSAT configuration
   require('functions.php');
 //print_r($properties);
UpdateLogFile("rsat","","");

// print_r($_POST);
// print_r($_FILES);

// Initialize variables
//$cmd = '/data/rsat/public_html/data/Temporary_stored_data_for_SVM/svmcrmrsat/bed_to_matrix.sh';
$cmd = 'data/svm_rsat/bed_to_matrix.sh';
$dir="/tmp/svm_rsat";

$working_dirTemp = "_".date("Ymd_His")."_".randchar(3);

$working_dir=$dir."/".$expName.$working_dirTemp;
mkdir($working_dir,0700);
#print $working_dir;

#print $cmd;
$exp_bed_file = "/^[\w\-\+\s,\.\#; \/]+$/";

//Fill buffer
echo str_repeat(" ", 1024), "\n";

////////////////////////////////////////////////////////////////
//Print <h3>
echo "<H3><a href='".$properties['rsat_www']."'>RSAT</a> - Features matrix - results</H3>";

////////////////////////////////////////////////////////////////
// Check arguments
$errors = false;

// Check that genome has been specified
$genome = $_REQUEST['genome'];

if ($genome == "none" or $genome == "" ) {
  error( "You forgot to specify the genome.");
  $errors = true;
 } //else {
  //$argument .= " -genome $genome";
// }

$expName=$_POST['expName'];
if ($expName == "none" or $expName == "" ) {
  error( "You forgot to specify an experiment name.");
  $errors = true;


//Check syntax of email address (ensure the texte netered in email box was an email address)
$output = $_REQUEST['output'];
$user_email = $_REQUEST['user_email'];
if($output =="email") {
  if (!preg_match("#^[^@\.]+(\.[^@]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$#", $user_email)) {
     error( "Email not valid");
     $errors=true;
  }
 }
}

print_r($_FILES);
#print $_FILES["bedfile"];

?>
 
  </body>
</html>
