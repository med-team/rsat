<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>RSAT - Features matrix Results</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="http://pedagogix-tagc.univ-mrs.fr/rsat/main.css" media="screen,projection,print"/>
	 <link rel="stylesheet" type="text/css" href="http://pedagogix-tagc.univ-mrs.fr/rsat/tabs.css" media="screen,projection,print"/> 
    <link rel="stylesheet" type="text/css" href = "http://pedagogix-tagc.univ-mrs.fr/rsat/main_grat.css" media="screen">
    </head>
    
    <body class="results"> 
    
      <h3 align='center'><a href="http://pedagogix-tagc.univ-mrs.fr/rsat/">RSAT</a> - Features matrix</h3>
      <br/> 
      <fieldset>  
         <legend><b>Result files</b></legend>    
<?php
$filename="./files_created.txt";
$lines = count(file($filename));

if ($lines < 13){
	$page = $_SERVER['PHP_SELF'];
	$sec = "10";
	header("Refresh: $sec; url=$page");
	}
	
	$handle = @fopen($filename, "r");
	if ($handle) {
    while (($buffer = fgets($handle, 4096)) !== false) {
        echo $buffer;
    }
    fclose($handle);
}

$fp = @fopen("./variables.txt", 'r'); 

// Add each line to an array
if ($fp) {
   $array = explode("\n", fread($fp, filesize($filename)));
   $csv=$array[0];
echo "
</fieldset>
<table class = 'Nextstep'>
    <tr><th colspan=4>next step</th></tr>
    <tr>
      <td align=center>
	    <form method='post' action='../../tune-svm_form.php'>
	    <input type='hidden' name='ftfile' value='".$csv."'>
	    <input type='submit' value='SVM tuning'>
	    </form>
      </td>
    </tr>

  </table>

  
</body>
</html>
";
}
fclose($fp);
?>