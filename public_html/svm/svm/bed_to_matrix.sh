## Input
#positive_seqs_bed=${workingdir}/CRM_heart_13-16_orthologues.bed
positive_seqs_bed=$1
genome=$2
shuffling=$3
workingdir=$4
output_matrix=${workingdir}/out_matrix.csv
rand_dir=$5
matrices=$6
matrixFormat=$7

## Other inputs and variables
background_seqs="data/dmel-all-intergenic_and_introns-promoters+-500bp_r5.57.bed"
genomeInfo="data/dm3ChromInfo.txt"

background_freqs="data/2nt_intergenic_Drosophila_melanogaster.freq"
#matrix_file="data/259_matrices.tf"

this_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

#Test if shuffling value is numeric or not, e.g. if a negative bed file is submitted or not
if [ -n "`echo $shuffling | sed 's/[0-9]//g'`" ]
	then     
		negative_seqs_bed=$shuffling
	else     
# Create negative set by shuffling background seqs with positive bed statistics ------------
#echo ${negative_seqs_bed}
negative_seqs_bed=$workingdir/"background_"$(basename ${positive_seqs_bed}) # negative set

for ((i = 1; i <= $shuffling; i += 1))
	do
	shuffleBed -seed $i -incl $this_dir/${background_seqs} -chrom -i ${positive_seqs_bed} -g $this_dir/$genomeInfo >> ${negative_seqs_bed}
done

fi

# Positive ---------------------------------------------------
seqs_bed=${positive_seqs_bed}
seqs_fasta=${workingdir}/crms.bed.fasta
seqs_ft=${workingdir}/crms.bed.ft
data_csv_pos=${workingdir}/crms.bed.csv
class=1


echo "Positives<br>" >> ${workingdir}/files_created.txt
echo "Input bed file: <a href=http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir/"$(basename ${seqs_bed})">http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir"/"$(basename ${seqs_bed})</a><br>" >> ${workingdir}/files_created.txt

# returns $data_csv_pos=${workingdir}/crms.bed.csv
bash $this_dir/create_matrix.sh $seqs_bed $seqs_fasta $seqs_ft $background_freqs $matrices $data_csv_pos $class $workingdir $rand_dir $matrixFormat &

echo "Matrix (csv) file: <a href=http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir/"$(basename ${data_csv_pos})">http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir"/"$(basename ${data_csv_pos})</a><br>" >> ${workingdir}/files_created.txt

# Negative ---------------------------------------------------
seqs_bed=${negative_seqs_bed}
seqs_fasta=${workingdir}/background.bed.fasta
seqs_ft=${workingdir}/background.bed.ft
data_csv_neg=${workingdir}/background.bed.csv
class=-1
echo "Negatives<br>" >> ${workingdir}/files_created.txt
echo "Input bed file: <a href=http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir/"$(basename ${seqs_bed})">http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir"/"$(basename ${seqs_bed})</a><br>" >> ${workingdir}/files_created.txt

# returns $data_csv_pos=${workingdir}/crms.bed.csv
bash $this_dir/create_matrix.sh $seqs_bed $seqs_fasta $seqs_ft $background_freqs $matrices $data_csv_neg $class $workingdir $rand_dir $matrixFormat &


echo "Matrix (csv) file: <a href=http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir/"$(basename ${data_csv_neg})">http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir"/"$(basename ${data_csv_neg})</a><br>" >> ${workingdir}/files_created.txt

wait

# Merge positive and negative seqs in one table with one header--------------------------------
cp $data_csv_pos ${output_matrix}
tail -n+2 $data_csv_neg >> ${output_matrix}
echo "Final matrix<br>" >> ${workingdir}/files_created.txt
echo "Output matrix (csv) file: <a href=http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir/"$(basename ${output_matrix})">http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir"/"$(basename ${output_matrix})</a><br>" >> ${workingdir}/files_created.txt

echo "http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir/"$(basename ${output_matrix}) >> ${workingdir}/variables.txt
