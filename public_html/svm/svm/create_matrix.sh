seqs_bed=$1
seqs_fasta=$2
seqs_ft=$3
background_freqs=$4
matrix_file=$5
data_csv=$6
class=$7
workingdir=$8
rand_dir=$9
matrixFormat=$10


this_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Extract sequences : generate multiFasta
/data/rsat/perl-scripts/fetch-sequences -genome dm3 -i ${seqs_bed} -o ${seqs_fasta}
echo "Fasta file: <a href=http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir/"$(basename ${seqs_fasta})">http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir"/"$(basename ${seqs_fasta})</a><br>" >> ${workingdir}/files_created.txt

# Matrix-scan (about 10min)
#matrixscan="/data/rsat/perl-scripts/matrix-scan -v 1 -quick -matrix_format transfac -pseudo 1 -decimals 1 -2str -origin end -bgfile $this_dir/${background_freqs} -bg_pseudo 0.01 -return sites,pval -uth pval 0.001 -n skip -m $this_dir/$matrix_file -i ${seqs_fasta} -o ${seqs_ft}"
#echo "$matrixscan" >> ${workingdir}/files_created.txt

/data/rsat/perl-scripts/matrix-scan -v 1 -quick -matrix_format transfac -pseudo 1 -decimals 1 -2str -origin end -bgfile $this_dir/${background_freqs} -bg_pseudo 0.01 -return sites,pval -uth pval 0.001 -n skip -m $matrix_file -i ${seqs_fasta} -o ${seqs_ft}

echo "Feature (ft) file (matrix-scan output): <a href=http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir/"$(basename ${seqs_ft})">http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/$rand_dir"/"$(basename ${seqs_ft})</a><br>" >> ${workingdir}/files_created.txt

#ls -lh ${seqs_ft}
#ls -lh ${seqs_fasta}

# Transform matrix-scan file to matrix # 10min
python $this_dir/rsat_features_file_to_matrix.py --ft ${seqs_ft} --fasta ${seqs_fasta} --csv ${data_csv}

#ls -lh ${data_csv}

# Prepare matrix for SVM
sed -i "s/,/,$class,/1" ${data_csv} # add class field
sed -i "s/^,$class,/,cl,/1" ${data_csv} # replace class field in first line by cl label

