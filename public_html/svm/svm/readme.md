# Test php command
./svm_rsat/bed_to_matrix.sh /data/rsat/public_html/svm/svm_rsat/test_ag/CRM_13-16.bed dm3 20 /data/rsat/public_html/svm/svm_rsat/test_ag

# Performance (Pedagogix)
## Without matrix-scan (comment l13 in create_matrix)
time bash bed_to_matrix.sh

real    0m12.520s
user    0m9.761s
sys     0m0.948s

## With matrix-scan (ft files in test)
time bash bed_to_matrix.sh

real    9m22.000s
user    15m56.584s
sys     0m20.921s

## SVM using out_matrix.csv from test folder
 system.time(source("run_svm.R"))
    user   system  elapsed 
1263.003    9.253   68.366 


