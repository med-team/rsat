<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>RSAT - Plot of performances of SVM model.</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="main.css" media="screen,projection,print"/>
    <link rel="stylesheet" type="text/css" href="tabs.css" media="screen,projection,print"/>
    <script src="RSAT_menu.js" type="text/javascript"></script>
    <script src="RSAT_tabs.js" type="text/javascript"></script>
    <!-- <script type="text/javascript">
			function add_demo() {
				document.getElementById('genome').value = 'dm3';
				document.forms[0].bed.value="";
				document.getElementById('bedfile').value='/data/rsat/public_html/data/svm_rsat/data/CRM_13-16.bed'; 				
				document.forms[0].header[0].checked=true;			
				document.forms[0].output[0].checked=true;
				document.getElementById('xtime').value="5";
				document.getElementById('expName').value='HeartCRM_13-16h';									
 				document.forms[0].sequence_url.value = '/data/rsat/public_html/data/svm_rsat/data/CRM_13-16.bed';
}
		</script> -->
  </head>

  <body class="form">
    <div>
      <h3 align='center'><a href="http://pedagogix-tagc.univ-mrs.fr/rsat/">RSAT</a> - Plot of performances of SVM model.</h3>
      <br/>
       <form method='post' action='plot-performance.php' enctype='multipart/form-data'>
 		<fieldset>
 		<legend><b>Kappa score</b></legend> 
 		<br/><br/><br/>
 		</fieldset>
 		<fieldset>
 		<legend><b>Model performances</b></legend> 
 		<fieldset>
 		<legend><b>All features</b></legend> 
 		</fieldset>
 		<fieldset>
 		<legend><b>Top 50 features</b></legend> 
 		</fieldset>
 		<fieldset>
 		<legend><b>Top 100 features</b></legend> 
 		</fieldset>
 		<fieldset>
 		<legend><b>Top 150 features</b></legend> 
 		</fieldset>
 		<br/><br/><br/>
 		</fieldset>
      <fieldset>
 		<legend><b>Features</b></legend> 
		Select number of features for creating SVM model: <font style='color:orange'>(mandatory)</font>&nbsp;&nbsp;<input type="text" name='feature_n' id='feature_n' placeholder="50"/>		
 		</fieldset>
      <ul><table class='formbutton'>
        <tr valign=middle>
          <td><input type="submit" name="submit" value="Create model" /></td>
          <td><input type="reset"  name="reset" /></td> 
          <td><input type="button" name="demo" value="Demo" onclick="add_demo()"/></td>  
          <td><b><a href='help.fetch-sequences.html'>[MANUAL]</a></b></td>
          <td><b><a href='http://www.bigre.ulb.ac.be/forums/' target='_top'>[ASK A QUESTION]</a></b></td>
        </tr></table>
      </ul>
    </div>
  </body>
</html>

