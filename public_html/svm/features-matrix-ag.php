<html>
<head>
<title>RSAT - Features matrix</title>
<link rel="stylesheet" type="text/css" href = "main_grat.css" media="screen">
   </head>
   <body class="results"> 

<?php
// Load RSAT configuration
   require('functions.php');
 //print_r($properties);UpdateLogFile("rsat","","");
UpdateLogFile("rsat","","");

$exp_bed_file = "/^[\w\-\+\s,\.\#; \/]+$/";

$cmd="/bin/bash /data/rsat/public_html/svm/svm/bed_to_matrix.sh"; # will store command
$rand_dir="rsatsvm_".date("Ymd_His")."_".randchar(6);
$workingdir = "/data/rsat/public_html/svm/tmp/".$rand_dir;
$shuffling = $_REQUEST["xtime"];
shell_exec("/bin/mkdir $workingdir");
//Fill buffer
echo str_repeat(" ", 1024), "\n";

////////////////////////////////////////////////////////////////
//Print <h3>
echo "<H3><a href='".$properties['rsat_www']."'>RSAT</a> - Features matrix - results</H3>";


// Check that genome has been specified
$genome = $_REQUEST['genome'];

if ($genome == "none" or $genome == "" ) {
  error( "You forgot to specify the genome.");
  $errors = true;
 } 
 
 ////////////////////////////////////////////////////////////////
//matrices

	// Check that the custom_motifs has been specified 0 or once.
$custom_motifs_specifications = 0;

// Bed data pasted in text area
if ($_REQUEST['matrix'] != "") {
	$custom_motifs_specifications++;
}
// Local bed file on client machine
if ($_FILES["matrix_file"]['name'] != "") {
	$custom_motifs_specifications++;
}

// Report error if no BED has been specified
if ($custom_motifs_specifications > 1) {
	error("The Custom_Motifs file can be specified only in 1 way (paste, upload)");
	$errors = true;
}
$matrix_format = $_REQUEST['matrix_format'];
//Custom_Motifs data provided in text area
  if ($_REQUEST['matrix'] != "") {
    $array_line = explode("\n",$_REQUEST['matrix']);
    // $custom_motifs_file = $properties['rsat_tmp']."/"."userbed".$suffix.".transfac";
    $custom_motifs_file = $workingdir . "/matrix.".$matrix_format;
    $file = fopen ($custom_motifs_file, "w");
    $no_custom_motifs_line = true;
	
    foreach($array_line as $line) {
      if (preg_match("/^[\w\-\+\s,\.\#; \/]+$/",$line)) {
	$no_custom_motifs_line = false;
	fwrite($file, $line."\n");
      } /*else {
	warning (htmlspecialchars($line)." is not a matrix line.\n");
      }*/
    }
    fclose($file);
	
    if ($no_custom_motifs_line) {
      error("All your line are not custom_motifs format");
      $errors = true;
    } 
  }	
	
  // Upload custom_motifs file from client machine
  if ($_FILES["matrix_file"]['name'] != "") {
    $custom_motifs_file_name = basename($_FILES['matrix_file']['name']);
    $extension = end( explode( ".", $custom_motifs_file_name));
		
    // Move uploaded custom_motifs file in tmp
    // $custom_motifs_file = $properties['rsat_tmp']."/".$custom_motifs_file_name;
    $custom_motifs_file = $workingdir."/".$custom_motifs_file_name;
		
    if(move_uploaded_file($_FILES['matrix_file']['tmp_name'], $custom_motifs_file)) {
      $argument .= " --custo_db_file_path $custom_motifs_file"; 
    } else {
      error('File upload failed');
      $errors = true;
    }
  }
  
  
/////////////////POSITIVE FILES/////////////////////////////////
////////////////////////////////////////////////////////////////
// Check that the positive BED has been specified once and only once
$bed_specifications = 0;

// Bed data pasted in text area
$pf_bed = $_REQUEST['bed'];
if ($pf_bed != "") {
  $bed_specifications++;
 }
// Local bed file on client machine
if ($_FILES["bedfile"]['name'] != "") {
  $bed_specifications++;
 }

// Bed specified by URL of remote server
$sequence_url = $_REQUEST['sequence_url'];
if ( $sequence_url != "") {
  $bed_specifications++;
 }
 // Report error if no BED has been specified
if ($bed_specifications == 0) {
  error("You forgot to specify the genomic coordinates (BED file)");
  $errors=true;
 } else if ($bed_specifications > 1) {
  error("The BED file can be specified only in 1 way (paste, upload or URL of external server)");
  $errors = true;
 }

/////////////////NEGATIVES FILES/////////////////////////////////    
////////////////////////////////////////////////////////////////
// Check that the positive BED has been specified once and only once
$Negbed_specifications = 0;

// Bed data pasted in text area
$pf_Negbed = $_REQUEST['Negbed'];
if ($pf_Negbed != "") {
  $Negbed_specifications++;
 }
// Local bed file on client machine
if ($_FILES["Negbedfile"]['name'] != "") {
  $Negbed_specifications++;
 }

// Bed specified by URL of remote server
$Negequence_url = $_REQUEST['Negsequence_url'];
if ( $Negequence_url != "") {
  $Negbed_specifications++;
 }
 
// error if no BED has been specified
if ($Negbed_specifications == 0) {
  warning("No negative bed was specified ".$shuffling." times random sequence set will be generated");
  $Negbed_file = 2;
 } else if ($Negbed_specifications > 1) {
  error("The negative BED file can be specified only in 1 way (paste, upload or URL of external server)");
  $errors = true;
 }
 
//////////////////////////////////////////////////
// Write bed file in temporary directory
if (!$errors) {
	
	// positive Bed data provided in text area
	
  if ($pf_bed != "") {
    $array_line = explode("\n",$pf_bed);
    // $bed_file = $properties['rsat_tmp']."/"."userbed".$suffix.".bed";
    $bed_file = $workingdir . "/userbed.bed";
    $file = fopen ($bed_file, "w");
    $no_bed_line = true;
    $warnings = "";
    
    foreach($array_line as $line) {
      if (preg_match("/^[\w\-\+\s,\.\#; \/]+$/",$line)) {
	$no_bed_line = false;
	fwrite($file, $line."\n");
      } else {
	$warnings .= htmlspecialchars($line)."<br/>\n";
      }
    }
    fclose($file);
    
    if ($warnings != "") {
      warning("Not bed line :<br/>\n".$warnings);
    }
    
    if ($no_bed_line) {
      error("All your line are not bed format");
      $errors = true;
    } 
  }
  
  // Upload positive bed file from url
  if ($sequence_url != "") {
    $url = explode("/",$sequence_url);
    $bedfn = end($url);
    
    if ($url[0]=="http:" or $url[0]=='ftp:') {
      $web_bed = file_get_contents($sequence_url);
      
      // $bed_file = $properties['rsat_tmp']."/".$bed_file;
    	$bed_file = $workingdir . "/" . $bedfn;
      $extension = end(explode( ".", $bed_file));

      $file = fopen ($bed_file, "w");
      fwrite($file, $web_bed);
      fclose($file);
      //$argument .= " --input_peaks $bed_file";

    } else {
      error(htmlentities($sequence_url)." is not a valid URL (should start with http: or ftp:.");
      $errors = true;
    }
  }
  
  // Upload positive bed file from client machine
  if ($_FILES["bedfile"]['name'] != "") {
    $bedfn = basename($_FILES['bedfile']['name']);
    
    // Upload bed file to the directory for this query
    // $bed_file = $properties['rsat_tmp']."/".$bed_file_name;
    $bed_file = $workingdir . "/" . $bedfn;
    $extension = end( explode( ".", $bed_file));
    
    if ($extension == "bed") {
      $bed_file = str_replace(".bed",$suffix.".bed",$bed_file);
    } else {
      $bed_file = $bed_file.$suffix.".bed";
    }
    
    if(move_uploaded_file($_FILES['bedfile']['tmp_name'], $bed_file)) {
      //$argument .= " --input_peaks $bed_file";
    } else {
      error('File upload failed');
      $errors = true;
    }
  }


	// Negative Bed data provided in text area
	
  if ($pf_Negbed != "") {
    $array_line = explode("\n",$pf_Negbed);
    // $bed_file = $properties['rsat_tmp']."/"."userbed".$suffix.".bed";
    $Negbed_file = $workingdir . "/Negative_userbed.bed";
    $Negfile = fopen ($Negbed_file, "w");
    $no_bed_line = true;
    $warnings = "";
    
    foreach($array_line as $line) {
      if (preg_match("/^[\w\-\+\s,\.\#; \/]+$/",$line)) {
	$no_bed_line = false;
	fwrite($Negfile, $line."\n");
      } else {
	$warnings .= htmlspecialchars($line)."<br/>\n";
      }
    }
    fclose($Negfile);
    
    if ($warnings != "") {
      warning("Not bed line :<br/>\n".$warnings);
    }
    
    if ($no_bed_line) {
      error("All your line are not bed format");
      $errors = true;
    } 
  }
  
  // Upload Negative bed file from url
  if ($Negequence_url != "") {
    $Negurl = explode("/",$Negequence_url);
    $Negbedfn = end($Negurl);
    
    if ($Negurl[0]=="http:" or $Negurl[0]=='ftp:') {
      $Negweb_bed = file_get_contents($Negequence_url);
      
      // $bed_file = $properties['rsat_tmp']."/".$bed_file;
    	$Negbed_file = $workingdir . "/" . $Negbedfn;
      $Negextension = end(explode( ".", $bed_file));

      $Negfile = fopen ($Negbed_file, "w");
      fwrite($Negfile, $Negweb_bed);
      fclose($Negfile);

    } else {
      error(htmlentities($Negequence_url)." is not a valid URL (should start with http: or ftp:.");
      $errors = true;
    }
  }
  
  // Upload Negative bed file from client machine
  if ($_FILES["Negbedfile"]['name'] != "") {
    $Negbedfn = basename($_FILES['Negbedfile']['name']);
    
    // Upload Negative bed file to the directory for this query
    // $bed_file = $properties['rsat_tmp']."/".$bed_file_name;
    $Negbed_file = $workingdir . "/" . $Negbedfn;
    $Negextension = end( explode( ".", $Negbed_file));
    
    if(move_uploaded_file($_FILES['Negbedfile']['tmp_name'], $Negbed_file)) {
      $argument .= " --input_peaks $bed_file";
    } else {
      error('File upload failed');
      $errors = true;
    }
  }
////////////////////////////////////////////////////////////////
//command building

$cmd .= " ".$bed_file ;
$cmd .= " ".$genome ; 
$cmd .= " ".$Negbed_file ; 
$cmd .= " ".$workingdir ; 
$cmd .= " ".$rand_dir ;
$cmd .= " ".$custom_motifs_file ;
$cmd .= " ".$matrix_format ;

//print "su - www-data -c '/bin/cp /data/rsat/public_html/svm/svm/demo/CRM_13-16.bed $workingdir'<br >";
shell_exec("/bin/cp /data/rsat/public_html/svm/svm/$bedfn /data/rsat/public_html/svm/tmp/$rand_dir");
//print "su - www-data -c '/bin/cp /data/rsat/public_html/svm/results.php $workingdir'<br >";
shell_exec("/bin/cp /data/rsat/public_html/svm/results.php $workingdir");
//print "su - www-data -c '/bin/cp /data/rsat/public_html/svm/svm/demo/*.ft $workingdir'<br >";
//shell_exec("/bin/cp /data/rsat/public_html/svm/svm/demo/*.ft $workingdir");
# bed_to_matrix
//print "su - www-data -c '$cmd'<br>";
print "Genome :".$genome."<br>" ;
print "Shuffling :".$Negbed_file."<br>" ;
print "workingdir :".$workingdir."<br>" ;
print "rand_dir :".$rand_dir."<br>" ;
print "matrix :".$custom_motifs_file."<br>" ;
print "Bed file :". $bed_file." <br>";
print "Matrix format :".$matrix_format." <br>";

shell_exec($cmd);
print "Command :".$cmd."<br ><br ><br >";
print "Your task has been submitted to the RSAT server.<br><br>";
print "Results will be available at:<br>";
print "<a href='http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/".$rand_dir."/results.php' >http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/".$rand_dir."/results.php</a>";

}
?>
 
  </body>
</html>
