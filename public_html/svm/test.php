<?php

require('functions.php');

$cmd="bash /data/rsat/public_html/svm/svm/bed_to_matrix.sh"; # will store command

$workingdir = "/tmp/rsatsvm_".date("Ymd_His")."_".randchar(6);

print "Working dir: " . $workingdir . "<br>";


print "Bedfile moved" . (int)mkdir($workingdir, 0700) . " ------------ <br>";

$bedtmpfn = $_FILES["bedfile"]["tmp_name"];
$bedfn = $_FILES["bedfile"]["name"];
move_uploaded_file($bedtmpfn, $workingdir . "/" . $bedfn);

$cmd .= " ".$workingdir . "/" . $bedfn ; 

# Print files in working dir
#$dir = $workingdir;
#$files1 = scandir($dir);
#print_r($files1."<br>");

print "Genome --------------------- <br>";
// Check that genome has been specified
$genome = $_REQUEST["genome"];
$cmd .= " ".$genome ; 


// Shuffling for generatcp svm/demo/*.fting negative set
$shuffling = $_REQUEST["shuffling"];
$cmd .= " ".$shuffling ; 

$cmd .= " ".$workingdir ; 

print $cmd;

# copy ft files to skip
$output = shell_exec("/bin/cp /data/rsat/public_html/svm/svm/demo/*.ft $workingdir");
# bed_to_matrix
$output = shell_exec($cmd);
# files in output
$output = shell_exec("ls -lh $workingdir");
echo "<pre>$output</pre>";
print "</br>Command: ".$cmd;
print "</br>Your tasks has been successfully submitted to RSAT server. </br>" ;
print "</br>When finished, you will receive a mail and your results will be available at:</br>";
print "<a href='https://pedagogix-tagc.univ-mrs.fr/".$workingdir."/features-to-matrix.php'>https://pedagogix-tagc.univ-mrs.fr/".$workingdir."/features-to-matrix.php</a><br /><br />";

$lines = file('./svm/filesCreated.txt');

echo "The next line will be in the previous linked page.</br>";
echo "Input positive bed :<a href='file://" . htmlspecialchars($lines[0]) . "'> file://" . htmlspecialchars($lines[0]) . "</a><br />\n";



?>

