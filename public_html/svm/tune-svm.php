<html>
<head>
<title>RSAT - Tune SVM</title>
<link rel="stylesheet" type="text/css" href = "main_grat.css" media="screen">
   </head>
   <body class="results"> 

<?php
// Load RSAT configuration
   require('functions.php');
 //print_r($properties);
UpdateLogFile("rsat","","");

////////////////////////////////////////////////////////////////
//Print <h3>
echo "<H3><a href='".$properties['rsat_www']."'>RSAT</a> - Tune SVM - results</H3><br ><br ><br >";

////////////////////////////////////////////////////////////////
//Rscript tune_svm.R script_data/crm_feature_matrix.csv radial "c(10, 80)" "c(1, 2)" 2 F script_data
$cmd="Rscript tune_svm_script.R"; # will store command
$url = $_POST['sequence_url'];
$pieces = explode("/", $url);
$rand_dir = $pieces[6];
$working_dir = $pieces[3]."/".$pieces[4]."/".$pieces[5]."/".$pieces[6];

$kernel = $_REQUEST['kernel'];
$gamma_vector = $_REQUEST['gamma_vector'];
$gammas = "c(".$gamma_vector.")";
$cost_vector = $_REQUEST['cost_vector'];
$costs = "c(".$cost_vector.")";
$cross = $_REQUEST['cross'];
$scale = $_REQUEST['scale'];

$cmd .= " ".$url." ".$kernel." ".$gammas." ".$costs." ".$cross." ".$scale." ".$working_dir ; 

print "su - www-data -c '/bin/cp /data/rsat/public_html/svm/resultsSVM.php /data/rsat/public_html/svm/tmp/$rand_dir'<br >";
shell_exec("/bin/cp /data/rsat/public_html/svm/resultsSVM.php /data/rsat/public_html/svm/tmp/$rand_dir");
print "su - www-data -c '/bin/cp /data/rsat/public_html/svm/exec/script_data/c.g.plot.png /data/rsat/public_html/svm/tmp/$rand_dir'<br >";
shell_exec("/bin/cp /data/rsat/public_html/svm/exec/script_data/c.g.plot.png /data/rsat/public_html/svm/tmp/$rand_dir");

print "Command :".$cmd."<br ><br ><br >";
print "Your task has been submitted to the RSAT server.<br><br>";

print "Results will be available at:<br>";
print "<a href='http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/".$rand_dir."/resultsSVM.php' >http://pedagogix-tagc.univ-mrs.fr/rsat/svm/tmp/".$rand_dir."/resultsSVM.php</a>";
//shell_exec($cmd);

?>
 
  </body>
</html>
