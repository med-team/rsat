<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>RSAT - Tune SVM Results</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="http://pedagogix-tagc.univ-mrs.fr/rsat/main.css" media="screen,projection,print"/>
	 <link rel="stylesheet" type="text/css" href="http://pedagogix-tagc.univ-mrs.fr/rsat/tabs.css" media="screen,projection,print"/> 
    <link rel="stylesheet" type="text/css" href = "http://pedagogix-tagc.univ-mrs.fr/rsat/main_grat.css" media="screen">
    </head>
    
    <body class="results"> 
    
      <h3 align='center'><a href="http://pedagogix-tagc.univ-mrs.fr/rsat/">RSAT</a> - Tune SVM</h3>
      <br/> 
      <fieldset>  
         <legend><b>Result files</b></legend>   
         <img src="./c.g.plot.png" border="0" align="center"/> 
         After tuning, best SVM parameters are (they will be used for following steps (features ranking)): <br>
          
<?php
$filename = "./files_created.txt";
$fp = @fopen("./variables.txt", 'r'); 
$fpCostGamma = @fopen("./costGamma.txt", 'r'); 
if ($fpCostGamma) {
   $array = explode("\n", fread($fpCostGamma, filesize($filename)));
   $cost=$array[0];
   $gamma=$array[1];
// Add each line to an array
echo "Cost: ".$cost."<br>";
echo "Gamma: ".$gamma."<br>";
}
if ($fp) {
   $array = explode("\n", fread($fp, filesize($filename)));
   $csv=$array[0];
echo "
</fieldset>
<table class = 'Nextstep'>
    <tr><th colspan=4>next step</th></tr>
    <tr>
      <td align=center>
	    <form method='post' action='../../rank-features_form.php'>
	    <input type='hidden' name='ftfile' value='".$csv."'>
	    <input type='hidden' name='cost' value='".$cost."'>
	    <input type='hidden' name='gamma' value='".$gamma."'>
	    <input type='submit' value='Feature ranking'>
	    </form>
      </td>
    </tr>

  </table>

  
</body>
</html>
";
}
fclose($fp);
?>