<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>RSAT - Feature ranking</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="main.css" media="screen,projection,print"/>
    <link rel="stylesheet" type="text/css" href="tabs.css" media="screen,projection,print"/>
    <script src="RSAT_menu.js" type="text/javascript"></script>
    <script src="RSAT_tabs.js" type="text/javascript"></script>
    <!-- <script type="text/javascript">
			function add_demo() {
				document.getElementById('genome').value = 'dm3';
				document.forms[0].bed.value="";
				document.getElementById('bedfile').value='/data/rsat/public_html/data/svm_rsat/data/CRM_13-16.bed'; 				
				document.forms[0].header[0].checked=true;			
				document.forms[0].output[0].checked=true;
				document.getElementById('xtime').value="5";
				document.getElementById('expName').value='HeartCRM_13-16h';									
 				document.forms[0].sequence_url.value = '/data/rsat/public_html/data/svm_rsat/data/CRM_13-16.bed';
}
		</script> -->
  </head>

  <body class="form">
    <div>
      <h3 align='center'><a href="http://pedagogix-tagc.univ-mrs.fr/rsat/">RSAT</a> - Rank features</h3>
      <br/>
 
      <form method='post' action='rank-features.php' enctype='multipart/form-data'>
 		
        <fieldset>  
         <legend><b>Matrix of features </b><font style='color:orange'>(mandatory)</font> </legend>    
			Upload matrix of features (csv file), in any of the three following ways: (<a href='help.feature-matrix.html#io_format'>How to generate a matrix of features? </a>) 
			       <ul type='square'>
	    <li>Paste feature file<br/>
	      
              <textarea name='ftfile' rows='6' cols='45'></textarea></li>
	   <li>Specify the URL of ft file on remote server (e.g. Galaxy)<br/>
              <?php
              print "<input type='text' name='sequence_url' size='62' value='".$_REQUEST['ftfile']."'/><br/></li>";
              ?>
            <li>Upload a file from your computer<br/>
	      <input type='file' name='ftfile' id='ftfile' size='40' /></li>
	  </ul>
	  </p>
	   <br/><br/>
	    </fieldset> 
   <p>
    
	<fieldset> 
	<legend><b>Options <font style='color:green'>(optionnal)</font></b></legend> 
	Kernel: 	<select name='kernel' id='kernel'>
           	<option value ='radial'> radial </option>
				<option value = 'linear'>linear</option>
        		</select> 
        		&nbsp;&nbsp;&nbsp;
   Scale data: <select name='scale' id='scale'>
           	<option value ='F'> No </option>
				<option value = 'T'> Yes </option>
        		</select>
        		&nbsp;&nbsp;&nbsp;
   Internal folds: <input type="text" name="k" size="3" placeholder="2"/>
   &nbsp;&nbsp;&nbsp;
   Halve-above: <select name='halve.above' id='halve.above'>
           	<option value ='5'> 5 (fast) </option>
				<option value = '10'> 10 (slow) </option>
        		</select>
        		<br/><br/> 

      </fieldset><br/>
      
<fieldset> 
	<legend><b>Advanced options <font style='color:green'>(very optionnal)</font></b></legend> 
	<?php
              echo "
	Gamma value:&nbsp;&nbsp;<input type='text' name='gamma' id='gamma' value='".$_REQUEST['gamma']."'/>
   Cost value:&nbsp;&nbsp;<input type='text' name='cost' id='cost' value='".$_REQUEST['cost']."'/>";
   ?>   	
        		<br/><br/>       
      </fieldset><br/>
<p>

      <b>Output</b>&nbsp;<input type="radio" name="output" value="display"  checked="checked"/>display <input type="radio" name="output" value="email"/>email <input type="text" name="user_email"  size="30" />

      <ul><table class='formbutton'>
        <tr valign=middle>
          <td><input type="submit" name="submit" value="GO" /></td>
          <td><input type="reset"  name="reset" /></td> 
          <td><input type="button" name="demo" value="Demo" onclick="add_demo()"/></td>  
          <td><b><a href='help.fetch-sequences.html'>[MANUAL]</a></b></td>
          <td><b><a href='http://www.bigre.ulb.ac.be/forums/' target='_top'>[ASK A QUESTION]</a></b></td>
        </tr></table>
      </ul>
    </div>
  </body>
</html>

