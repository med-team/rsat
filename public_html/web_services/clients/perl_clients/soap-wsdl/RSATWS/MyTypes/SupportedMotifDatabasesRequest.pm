package MyTypes::SupportedMotifDatabasesRequest;
use strict;
use warnings;


__PACKAGE__->_set_element_form_qualified(0);

sub get_xmlns { 'urn:RSATWS' };

our $XML_ATTRIBUTE_CLASS;
undef $XML_ATTRIBUTE_CLASS;

sub __get_attr_class {
    return $XML_ATTRIBUTE_CLASS;
}

use Class::Std::Fast::Storable constructor => 'none';
use base qw(SOAP::WSDL::XSD::Typelib::ComplexType);

Class::Std::initialize();

{ # BLOCK to scope variables

my %output_of :ATTR(:get<output>);
my %return_of :ATTR(:get<return>);

__PACKAGE__->_factory(
    [ qw(        output
        return

    ) ],
    {
        'output' => \%output_of,
        'return' => \%return_of,
    },
    {
        'output' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
        'return' => 'SOAP::WSDL::XSD::Typelib::Builtin::string',
    },
    {

        'output' => 'output',
        'return' => 'return',
    }
);

} # end BLOCK







1;


=pod

=head1 NAME

MyTypes::SupportedMotifDatabasesRequest

=head1 DESCRIPTION

Perl data type class for the XML Schema defined complexType
SupportedMotifDatabasesRequest from the namespace urn:RSATWS.

Parameters for the operation supported_organisms.




=head2 PROPERTIES

The following properties may be accessed using get_PROPERTY / set_PROPERTY
methods:

=over

=item * output


=item * return




=back


=head1 METHODS

=head2 new

Constructor. The following data structure may be passed to new():

 { # MyTypes::SupportedMotifDatabasesRequest
   output =>  $some_value, # string
   return =>  $some_value, # string
 },




=head1 AUTHOR

Generated by SOAP::WSDL

=cut

