AC  cluster_1
XX
ID  cluster_1
XX
DE  wgtAACwGt
P0       A     C     G     T
1       10     4     0     7
2        1     4    14     2
3        4     2     2    13
4       20     0     0     1
5       21     0     0     0
6        0    21     0     0
7        7     5     3     6
8        0     0    21     0
9        5     4     2    10
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_1
CC  AC: cluster_1
CC  id: cluster_1
CC  name: cluster_1
CC  version: 
CC  name: cluster_1
CC  description: wgtAACwGt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: agtAACaGt
CC  consensus.strict.rc: ACTGTTACT
CC  consensus.IUPAC: wgtAACwGt
CC  consensus.IUPAC.rc: ACWGTTACW
CC  consensus.regexp: [at]gtAAC[at]Gt
CC  consensus.regexp.rc: AC[AT]GTTAC[AT]
CC  merged_ID: ovo,prd
CC  merge_nb: 2
CC  merged_AC: MA0126.1,MA0239.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_10
XX
ID  cluster_10
XX
DE  tTTAATTAG
P0       A     C     G     T
1     3.75  2.25  3.25   9.5
2     0.75   4.5     0  13.5
3        0   1.5  1.75  15.5
4    16.75     0   0.5   1.5
5    18.75     0     0     0
6        0  0.75  0.75 17.25
7     1.75     0  1.75 15.25
8    14.25     0   4.5     0
9     0.25   0.5   2.5     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_10
CC  AC: cluster_10
CC  id: cluster_10
CC  name: cluster_10
CC  version: 
CC  name: cluster_10
CC  description: tTTAATTAG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tTTAATTAg
CC  consensus.strict.rc: CTAATTAAA
CC  consensus.IUPAC: tTTAATTAg
CC  consensus.IUPAC.rc: CTAATTAAA
CC  consensus.regexp: tTTAATTAg
CC  consensus.regexp.rc: CTAATTAAA
CC  merged_ID: Vsx2,inv,Ubx,HHEX
CC  merge_nb: 4
CC  merged_AC: MA0180.1,MA0229.1,MA0094.2,MA0183.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_11
XX
ID  cluster_11
XX
DE  CCGGAArY
P0       A     C     G     T
1      1.5   504   1.5     1
2        3 504.5   0.5   0.5
3      0.5   0.5   507   0.5
4      0.5   0.5   507   0.5
5      507   0.5   0.5   0.5
6      457   0.5   0.5  50.5
7      169     1 337.5     1
8      0.5   143   0.5   356
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_11
CC  AC: cluster_11
CC  id: cluster_11
CC  name: cluster_11
CC  version: 
CC  name: cluster_11
CC  description: CCGGAArY
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: CCGGAAgT
CC  consensus.strict.rc: ACTTCCGG
CC  consensus.IUPAC: CCGGAArY
CC  consensus.IUPAC.rc: RYTTCCGG
CC  consensus.regexp: CCGGAA[ag][CT]
CC  consensus.regexp.rc: [AG][CT]TTCCGG
CC  merged_ID: Eip74EF,Ets21C
CC  merge_nb: 2
CC  merged_AC: MA0026.1,MA0916.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_12
XX
ID  cluster_12
XX
DE  wbTCrAGTGs
P0       A     C     G     T
1       70    27    41   129
2      0.5    98    69  99.5
3        0  25.5     0 241.5
4        0   220    18    29
5    172.5     0  94.5     0
6      267     0     0     0
7        0     0   267     0
8        1     0     0   266
9      4.5     0 262.5     0
10     6.5  78.5 150.5    22
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_12
CC  AC: cluster_12
CC  id: cluster_12
CC  name: cluster_12
CC  version: 
CC  name: cluster_12
CC  description: wbTCrAGTGs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ttTCaAGTGg
CC  consensus.strict.rc: CCACTTGAAA
CC  consensus.IUPAC: wbTCrAGTGs
CC  consensus.IUPAC.rc: SCACTYGAVW
CC  consensus.regexp: [at][cgt]TC[ag]AGTG[cg]
CC  consensus.regexp.rc: [CG]CACT[CT]GA[ACG][AT]
CC  merged_ID: tin,vnd
CC  merge_nb: 2
CC  merged_AC: MA0247.2,MA0253.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_13
XX
ID  cluster_13
XX
DE  kTGTTTryrywa
P0       A     C     G     T
1        4   1.5     7     8
2      1.5     0     0  32.5
3        4     0    30     0
4        0     0     0    34
5        0     0   0.5  33.5
6        0     0   1.5  32.5
7       20     0     9     5
8      2.5  17.5   4.5   9.5
9     11.5   7.5     9     6
10       2    12   2.5  17.5
11      19   1.5   2.5    11
12     7.5     2     2     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_13
CC  AC: cluster_13
CC  id: cluster_13
CC  name: cluster_13
CC  version: 
CC  name: cluster_13
CC  description: kTGTTTryrywa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tTGTTTacataa
CC  consensus.strict.rc: TTATGTAAACAA
CC  consensus.IUPAC: kTGTTTryrywa
CC  consensus.IUPAC.rc: TWRYRYAAACAM
CC  consensus.regexp: [gt]TGTTT[ag][ct][ag][ct][at]a
CC  consensus.regexp.rc: T[AT][AG][CT][AG][CT]AAACA[AC]
CC  merged_ID: fkh,slp1
CC  merge_nb: 2
CC  merged_AC: MA0446.1,MA0458.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_14
XX
ID  cluster_14
XX
DE  kcrmarmtATCGATa
P0       A     C     G     T
1      373   436   566 617.5
2    479.5 718.5 423.5   371
3      628 423.5 654.5 286.5
4    935.5   679   378     0
5    1188.5   227   472 539.5
6    1267.5   259 645.5   255
7    881.5 1185.5 298.5  61.5
8    473.5   361     0 1592.5
9    2388.5     0  38.5     0
10       0     0     0  2427
11       0  2427     0     0
12    38.5     0 2388.5     0
13   2224.5   0.5   202     0
14   247.5    85     0 2094.5
15    1382   2.5   484 558.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_14
CC  AC: cluster_14
CC  id: cluster_14
CC  name: cluster_14
CC  version: 
CC  name: cluster_14
CC  description: kcrmarmtATCGATa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tcgaaactATCGATa
CC  consensus.strict.rc: TATCGATAGTTTCGA
CC  consensus.IUPAC: kcrmarmtATCGATa
CC  consensus.IUPAC.rc: TATCGATAKYTKYGM
CC  consensus.regexp: [gt]c[ag][ac]a[ag][ac]tATCGATa
CC  consensus.regexp.rc: TATCGATA[GT][CT]T[GT][CT]G[AC]
CC  merged_ID: BEAF-32,pnr
CC  merge_nb: 2
CC  merged_AC: MA0529.1,MA0536.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_15
XX
ID  cluster_15
XX
DE  MA0022.1 dl; from JASPAR
P0       A     C     G     T
1        0     5     6     2
2        0     0    12     1
3        0     1    11     1
4        0     0    10     3
5        1     1     3     8
6        1     0     2    10
7        1     0     0    12
8        0     0     0    13
9        0     3     0    10
10       1     9     0     3
11       1     9     1     2
12       3     5     5     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_15
CC  AC: MA0022.1
CC  id: MA0022.1
CC  name: cluster_15
CC  version: 
CC  name: cluster_15
CC  description: MA0022.1 dl; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: dl
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0022.1
XX
//
AC  cluster_17
XX
ID  cluster_17
XX
DE  MA0173.1 CG11617; from JASPAR
P0       A     C     G     T
1        1     0     0    16
2        0     0     0    17
3       10     0     3     4
4       17     0     0     0
5        0    17     0     0
6       17     0     0     0
7        0     0     0     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_17
CC  AC: MA0173.1
CC  id: MA0173.1
CC  name: cluster_17
CC  version: 
CC  name: cluster_17
CC  description: MA0173.1 CG11617; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: CG11617
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0173.1
XX
//
AC  cluster_18
XX
ID  cluster_18
XX
DE  MA0235.1 onecut; from JASPAR
P0       A     C     G     T
1        1     2     0    12
2        0     0     0    15
3        0     0    15     0
4       15     0     0     0
5        0     0     0    15
6        0     0     0    15
7        5     0     4     6
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_18
CC  AC: MA0235.1
CC  id: MA0235.1
CC  name: cluster_18
CC  version: 
CC  name: cluster_18
CC  description: MA0235.1 onecut; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: onecut
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0235.1
XX
//
AC  cluster_19
XX
ID  cluster_19
XX
DE  MA0211.1 bap; from JASPAR
P0       A     C     G     T
1        0     0     1    22
2        0     0     0    23
3       23     0     0     0
4       23     0     0     0
5        0     0    23     0
6        1     0     0    22
7        7     0    16     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_19
CC  AC: MA0211.1
CC  id: MA0211.1
CC  name: cluster_19
CC  version: 
CC  name: cluster_19
CC  description: MA0211.1 bap; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: bap
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0211.1
XX
//
AC  cluster_2
XX
ID  cluster_2
XX
DE  yTAATTr
P0       A     C     G     T
1     2.14  5.92  1.56 12.32
2     0.44   0.2     0  21.3
3    21.16     0  0.38   0.4
4    21.88     0  0.04  0.02
5      0.6  0.14  0.24 20.96
6     0.74  0.56  5.04  15.6
7     14.5  0.08  7.04  0.32
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_2
CC  AC: cluster_2
CC  id: cluster_2
CC  name: cluster_2
CC  version: 
CC  name: cluster_2
CC  description: yTAATTr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tTAATTa
CC  consensus.strict.rc: TAATTAA
CC  consensus.IUPAC: yTAATTr
CC  consensus.IUPAC.rc: YAATTAR
CC  consensus.regexp: [ct]TAATT[ag]
CC  consensus.regexp.rc: [CT]AATTA[AG]
CC  merged_ID: zen,E5,zen2,ap,ind,CG18599,pb,ems,eve,Awh,Lim3,HGTX,Vsx1,otp,btn,Dfd,Scr,Antp,ftz,abd-A,lab,CG15696-RA,Lim1,CG11294,al,CG32105,repo,exex,ro,Pph13,Rx,CG9876,OdsH,PHDP,hbn,unpg,CG32532,en,C15,B-H1,B-H2,Hmx,unc-4,CG11085,CG34031,tup,NK7.1,bsh,lms,slou
CC  merge_nb: 50
CC  merged_AC: MA0256.1,MA0189.1,MA0257.1,MA0209.1,MA0228.1,MA0177.1,MA0238.1,MA0219.1,MA0221.1,MA0167.1,MA0195.1,MA0191.1,MA0181.1,MA0236.1,MA0215.1,MA0186.1,MA0203.1,MA0166.1,MA0225.1,MA0206.1,MA0230.1,MA0176.1,MA0194.1,MA0172.1,MA0208.1,MA0178.1,MA0240.1,MA0224.1,MA0241.1,MA0200.1,MA0202.1,MA0184.1,MA0198.1,MA0457.1,MA0226.1,MA0251.1,MA0179.1,MA0220.1,MA0170.1,MA0168.1,MA0169.1,MA0192.1,MA0250.1,MA0171.1,MA0444.1,MA0248.1,MA0196.1,MA0214.1,MA0175.1,MA0245.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_20
XX
ID  cluster_20
XX
DE  MA0205.1 Trl; from JASPAR
P0       A     C     G     T
1        4    22     9    36
2        6    23    11    31
3       12     6    30    23
4        0    64     3     4
5        3     0    14    54
6        1    70     0     0
7       13     0     2    56
8        5    54     7     5
9        7    13    11    40
10       9    33    10    19
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_20
CC  AC: MA0205.1
CC  id: MA0205.1
CC  name: cluster_20
CC  version: 
CC  name: cluster_20
CC  description: MA0205.1 Trl; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: Trl
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0205.1
XX
//
AC  cluster_21
XX
ID  cluster_21
XX
DE  MA0255.1 z; from JASPAR
P0       A     C     G     T
1       11     3     5    22
2        1     7     3    30
3        1     0    40     0
4       41     0     0     0
5        1     0    40     0
6        8    10     0    23
7        2     6    29     4
8       18     7    11     5
9       10     5    12    14
10      14     6     2    19
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_21
CC  AC: MA0255.1
CC  id: MA0255.1
CC  name: cluster_21
CC  version: 
CC  name: cluster_21
CC  description: MA0255.1 z; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: z
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0255.1
XX
//
AC  cluster_23
XX
ID  cluster_23
XX
DE  MA0445.1 D; from JASPAR
P0       A     C     G     T
1        1     8     7    13
2        0    25     0     4
3        0    17     0    12
4       20     0     0     9
5        0     0     0    29
6        0     0     3    26
7        2     0    27     0
8        0     0     0    29
9        1     2     4    22
10       4    10     6     9
11       6     1     1    21
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_23
CC  AC: MA0445.1
CC  id: MA0445.1
CC  name: cluster_23
CC  version: 
CC  name: cluster_23
CC  description: MA0445.1 D; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0445.1
XX
//
AC  cluster_24
XX
ID  cluster_24
XX
DE  MA0049.1 hb; from JASPAR
P0       A     C     G     T
1        1     5     8     2
2        6     8     2     0
3        9     3     4     0
4        4     3     1     8
5       13     1     0     2
6       16     0     0     0
7       16     0     0     0
8       14     0     2     0
9       15     1     0     0
10       9     2     2     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_24
CC  AC: MA0049.1
CC  id: MA0049.1
CC  name: cluster_24
CC  version: 
CC  name: cluster_24
CC  description: MA0049.1 hb; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: hb
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0049.1
XX
//
AC  cluster_25
XX
ID  cluster_25
XX
DE  MA0216.2 cad; from JASPAR
P0       A     C     G     T
1      854     0  1143   306
2      575   341  1387     0
3        0  1481     0   822
4      745  1475     0    83
5     2117     0   186     0
6        0    95     0  2208
7     2236     0     0    67
8     2303     0     0     0
9     2303     0     0     0
10    1637   115   147   404
11    1046   755     0   502
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_25
CC  AC: MA0216.2
CC  id: MA0216.2
CC  name: cluster_25
CC  version: 
CC  name: cluster_25
CC  description: MA0216.2 cad; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: cad
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0216.2
XX
//
AC  cluster_26
XX
ID  cluster_26
XX
DE  MA0443.1 btd; from JASPAR
P0       A     C     G     T
1       14     3     7     6
2       10     0    20     0
3        4     0    18     8
4        2     0    28     0
5        0     0    30     0
6        0     0    30     0
7        2    28     0     0
8        0     0    30     0
9        0     0    20    10
10      15     1    10     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_26
CC  AC: MA0443.1
CC  id: MA0443.1
CC  name: cluster_26
CC  version: 
CC  name: cluster_26
CC  description: MA0443.1 btd; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: btd
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0443.1
XX
//
AC  cluster_27
XX
ID  cluster_27
XX
DE  MA0450.1 hkb; from JASPAR
P0       A     C     G     T
1        3     0    18    11
2        8     0    24     0
3        0     0    32     0
4        0     0    32     0
5        0    32     0     0
6        0     0    32     0
7        0     0     3    29
8        0     0    31     1
9       24     3     2     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_27
CC  AC: MA0450.1
CC  id: MA0450.1
CC  name: cluster_27
CC  version: 
CC  name: cluster_27
CC  description: MA0450.1 hkb; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: hkb
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0450.1
XX
//
AC  cluster_28
XX
ID  cluster_28
XX
DE  MA0010.1 br; from JASPAR
P0       A     C     G     T
1        3     1     4     1
2        1     1     1     6
3        5     2     1     1
4        7     0     1     1
5        3     0     0     6
6        6     0     0     3
7        4     1     4     0
8        7     0     1     1
9        1     8     0     0
10       9     0     0     0
11       8     0     1     0
12       5     0     3     1
13       4     0     0     5
14       2     3     2     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_28
CC  AC: MA0010.1
CC  id: MA0010.1
CC  name: cluster_28
CC  version: 
CC  name: cluster_28
CC  description: MA0010.1 br; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: br
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0010.1
XX
//
AC  cluster_3
XX
ID  cluster_3
XX
DE  mTAATCCr
P0       A     C     G     T
1     55.2  76.8  27.8  44.2
2        0     0     0 216.4
3      216     0     0   0.4
4    216.4     0     0     0
5        0     0   0.6 215.8
6        0 216.4     0     0
7        0 208.4   0.6   7.6
8     69.6  43.4  69.6  17.4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_3
CC  AC: cluster_3
CC  id: cluster_3
CC  name: cluster_3
CC  version: 
CC  name: cluster_3
CC  description: mTAATCCr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cTAATCCa
CC  consensus.strict.rc: TGGATTAG
CC  consensus.IUPAC: mTAATCCr
CC  consensus.IUPAC.rc: YGGATTAK
CC  consensus.regexp: [ac]TAATCC[ag]
CC  consensus.regexp.rc: [CT]GGATTA[GT]
CC  merged_ID: dve,Ptx1,Gsc,bcd,oc
CC  merge_nb: 5
CC  merged_AC: MA0915.1,MA0201.1,MA0190.1,MA0212.1,MA0234.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_30
XX
ID  cluster_30
XX
DE  MA0460.1 ttk; from JASPAR
P0       A     C     G     T
1       10     8     3     1
2       22     0     0     0
3        0     0    22     0
4        0     0    22     0
5       22     0     0     0
6        0     5     0    17
7       21     0     0     1
8       22     0     0     0
9        1     5     3    13
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_30
CC  AC: MA0460.1
CC  id: MA0460.1
CC  name: cluster_30
CC  version: 
CC  name: cluster_30
CC  description: MA0460.1 ttk; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ttk
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0460.1
XX
//
AC  cluster_31
XX
ID  cluster_31
XX
DE  MA0242.1 Bgb::run; from JASPAR
P0       A     C     G     T
1       12     2     0    15
2       27     0     1     1
3       28     0     1     0
4        0    29     0     0
5        0    28     1     0
6        5     1    23     0
7        0    29     0     0
8       28     0     0     1
9       20     0     9     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_31
CC  AC: MA0242.1
CC  id: MA0242.1
CC  name: cluster_31
CC  version: 
CC  name: cluster_31
CC  description: MA0242.1 Bgb::run; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: Bgb::run
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0242.1
XX
//
AC  cluster_32
XX
ID  cluster_32
XX
DE  MA0917.1 gcm2; from JASPAR
P0       A     C     G     T
1      730   111   159     0
2        0    12     0   988
3      235    24   741     0
4       47   800   141    12
5       71     0   753   177
6        0     0  1000     0
7        0     0  1000     0
8       56   352    93   500
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_32
CC  AC: MA0917.1
CC  id: MA0917.1
CC  name: cluster_32
CC  version: 
CC  name: cluster_32
CC  description: MA0917.1 gcm2; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: gcm2
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0917.1
XX
//
AC  cluster_33
XX
ID  cluster_33
XX
DE  MA0185.1 Deaf1; from JASPAR
P0       A     C     G     T
1        0     3     0     7
2        0     0     0    10
3        0    10     0     0
4        0     0    10     0
5        0     0     5     5
6        1     3     4     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_33
CC  AC: MA0185.1
CC  id: MA0185.1
CC  name: cluster_33
CC  version: 
CC  name: cluster_33
CC  description: MA0185.1 Deaf1; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: Deaf1
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0185.1
XX
//
AC  cluster_34
XX
ID  cluster_34
XX
DE  MA0193.1 schlank; from JASPAR
P0       A     C     G     T
1        0    19     0     0
2        0     9     0    10
3       17     2     0     0
4        1    18     0     0
5        0    12     0     7
6       18     0     1     0
7       12     0     4     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_34
CC  AC: MA0193.1
CC  id: MA0193.1
CC  name: cluster_34
CC  version: 
CC  name: cluster_34
CC  description: MA0193.1 schlank; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: schlank
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0193.1
XX
//
AC  cluster_35
XX
ID  cluster_35
XX
DE  MA0222.1 exd; from JASPAR
P0       A     C     G     T
1        4     4     4     5
2        1     0     2    14
3        0     0     0    17
4        0     0     0    17
5        0     0    17     0
6       17     0     0     0
7        0    12     0     5
8       11     0     6     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_35
CC  AC: MA0222.1
CC  id: MA0222.1
CC  name: cluster_35
CC  version: 
CC  name: cluster_35
CC  description: MA0222.1 exd; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: exd
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0222.1
XX
//
AC  cluster_36
XX
ID  cluster_36
XX
DE  MA0459.1 tll; from JASPAR
P0       A     C     G     T
1       20     4     4     5
2       29     0     4     0
3       33     0     0     0
4       31     1     1     0
5        0     0    33     0
6        0     2     0    31
7        0    33     0     0
8       33     0     0     0
9       31     0     1     1
10      17    10     2     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_36
CC  AC: MA0459.1
CC  id: MA0459.1
CC  name: cluster_36
CC  version: 
CC  name: cluster_36
CC  description: MA0459.1 tll; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: tll
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0459.1
XX
//
AC  cluster_38
XX
ID  cluster_38
XX
DE  MA0451.1 kni; from JASPAR
P0       A     C     G     T
1       19     1     2     4
2       25     1     0     0
3       16     0     0    10
4        5     9     6     6
5        0     4     1    21
6       21     0     5     0
7        0     0    26     0
8       17     0     8     1
9        1     3    18     4
10       0    26     0     0
11      25     0     1     0
12       5    12     7     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_38
CC  AC: MA0451.1
CC  id: MA0451.1
CC  name: cluster_38
CC  version: 
CC  name: cluster_38
CC  description: MA0451.1 kni; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: kni
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0451.1
XX
//
AC  cluster_39
XX
ID  cluster_39
XX
DE  MA0244.1 slbo; from JASPAR
P0       A     C     G     T
1        9     1     1     1
2        0     1     0    11
3        0     0     3     9
4        3     2     7     0
5        2    10     0     0
6        7     2     2     1
7        6     5     1     0
8       11     1     0     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_39
CC  AC: MA0244.1
CC  id: MA0244.1
CC  name: cluster_39
CC  version: 
CC  name: cluster_39
CC  description: MA0244.1 slbo; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: slbo
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0244.1
XX
//
AC  cluster_4
XX
ID  cluster_4
XX
DE  wWACA
P0       A     C     G     T
1    12.6667 0.666667 2.66667 15.3333
2       22     0 1.33333     8
3    31.3333     0     0     0
4        0 31.3333     0     0
5    31.3333     0     0     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_4
CC  AC: cluster_4
CC  id: cluster_4
CC  name: cluster_4
CC  version: 
CC  name: cluster_4
CC  description: wWACA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tAACA
CC  consensus.strict.rc: TGTTA
CC  consensus.IUPAC: wWACA
CC  consensus.IUPAC.rc: TGTWW
CC  consensus.regexp: [at][AT]ACA
CC  consensus.regexp.rc: TGT[AT][AT]
CC  merged_ID: caup,ara,mirr
CC  merge_nb: 3
CC  merged_AC: MA0217.1,MA0210.1,MA0233.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_40
XX
ID  cluster_40
XX
DE  MA0447.1 gt; from JASPAR
P0       A     C     G     T
1       28     5    25     2
2        0     0     0    60
3        1     1     3    55
4       54     0     6     0
5        0    53     0     7
6        7     0    53     0
7        0     6     0    54
8       55     3     1     1
9       60     0     0     0
10       2    25     5    28
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_40
CC  AC: MA0447.1
CC  id: MA0447.1
CC  name: cluster_40
CC  version: 
CC  name: cluster_40
CC  description: MA0447.1 gt; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: gt
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0447.1
XX
//
AC  cluster_41
XX
ID  cluster_41
XX
DE  MA0213.1 brk; from JASPAR
P0       A     C     G     T
1        1     5     4     0
2        0     4     0     6
3        0     0    10     0
4        0     0    10     0
5        0    10     0     0
6        1     0     9     0
7        1     8     0     1
8        0     5     1     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_41
CC  AC: MA0213.1
CC  id: MA0213.1
CC  name: cluster_41
CC  version: 
CC  name: cluster_41
CC  description: MA0213.1 brk; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: brk
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0213.1
XX
//
AC  cluster_42
XX
ID  cluster_42
XX
DE  MA0531.1 CTCF; from JASPAR
P0       A     C     G     T
1      306   876   403   317
2      313  1147   219   223
3      457   383   826   236
4      676   784   350    92
5      257   714    87   844
6     1534     1   192   175
7      202     0  1700     0
8      987     0   912     3
9        2     4   311  1585
10       0     0  1902     0
11       2     0  1652   248
12     124  1645     3   130
13       1     0  1807    94
14      79  1514     8   301
15     231   773   144   754
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_42
CC  AC: MA0531.1
CC  id: MA0531.1
CC  name: cluster_42
CC  version: 
CC  name: cluster_42
CC  description: MA0531.1 CTCF; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: CTCF
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0531.1
XX
//
AC  cluster_43
XX
ID  cluster_43
XX
DE  MA0086.1 sna; from JASPAR
P0       A     C     G     T
1        0    39     1     0
2       39     0     0     1
3        3     0    37     0
4        2     0    38     0
5        0     0     0    40
6        0     0    38     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_43
CC  AC: MA0086.1
CC  id: MA0086.1
CC  name: cluster_43
CC  version: 
CC  name: cluster_43
CC  description: MA0086.1 sna; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: sna
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0086.1
XX
//
AC  cluster_44
XX
ID  cluster_44
XX
DE  MA0254.1 vvl; from JASPAR
P0       A     C     G     T
1        0     2     0     9
2       11     0     0     0
3        2     2     0     7
4        0     0     6     5
5        3     8     0     0
6       11     0     0     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_44
CC  AC: MA0254.1
CC  id: MA0254.1
CC  name: cluster_44
CC  version: 
CC  name: cluster_44
CC  description: MA0254.1 vvl; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: vvl
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0254.1
XX
//
AC  cluster_45
XX
ID  cluster_45
XX
DE  MA0456.1 opa; from JASPAR
P0       A     C     G     T
1        0     1    15     2
2       10     7     1     0
3        1    16     0     1
4        1    15     1     1
5        0    16     1     1
6        0    18     0     0
7        0    18     0     0
8        0    14     0     4
9        4     0    13     1
10       0    14     2     2
11       3     1     4    10
12       2     0    16     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_45
CC  AC: MA0456.1
CC  id: MA0456.1
CC  name: cluster_45
CC  version: 
CC  name: cluster_45
CC  description: MA0456.1 opa; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: opa
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0456.1
XX
//
AC  cluster_46
XX
ID  cluster_46
XX
DE  MA0454.1 odd; from JASPAR
P0       A     C     G     T
1        7     5     0     3
2       10     5     0     0
3        0    14     1     0
4       12     0     3     0
5        0     0    15     0
6        0     0     0    15
7       15     0     0     0
8        0     0    15     0
9        0    15     0     0
10      11     3     1     0
11       5     4     6     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_46
CC  AC: MA0454.1
CC  id: MA0454.1
CC  name: cluster_46
CC  version: 
CC  name: cluster_46
CC  description: MA0454.1 odd; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: odd
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0454.1
XX
//
AC  cluster_47
XX
ID  cluster_47
XX
DE  MA0243.1 sd; from JASPAR
P0       A     C     G     T
1        3     0     8     3
2        8     1     1     4
3        4     8     1     1
4       14     0     0     0
5        1     1     0    12
6        3     0     1    10
7        0     8     0     6
8        3     5     3     3
9        4     1     2     7
10       5     6     3     0
11       5     1     7     1
12       4     3     4     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_47
CC  AC: MA0243.1
CC  id: MA0243.1
CC  name: cluster_47
CC  version: 
CC  name: cluster_47
CC  description: MA0243.1 sd; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: sd
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0243.1
XX
//
AC  cluster_48
XX
ID  cluster_48
XX
DE  MA0532.1 Stat92E; from JASPAR
P0       A     C     G     T
1       15    50    24    29
2       24    22    59    13
3       27    35    41    15
4       91     1    17     9
5       47     0    27    44
6        0     1     1   116
7        0     6     0   112
8        0   106     0    12
9        0    67    14    37
10      38    21    30    29
11      41     2    70     5
12       0     1   117     0
13     113     0     2     3
14     118     0     0     0
15      63     9    10    36
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_48
CC  AC: MA0532.1
CC  id: MA0532.1
CC  name: cluster_48
CC  version: 
CC  name: cluster_48
CC  description: MA0532.1 Stat92E; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: Stat92E
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0532.1
XX
//
AC  cluster_49
XX
ID  cluster_49
XX
DE  MA0535.1 Mad; from JASPAR
P0       A     C     G     T
1        0    48    47     7
2       35    26    13    28
3        0    14    88     0
4       35     0    56    11
5        0   102     0     0
6        0     0   102     0
7       23    65     0    14
8        0    71    31     0
9       29     0    73     0
10      29    39    34     0
11      11    53    25    13
12      29     0    73     0
13      12    48    27    15
14      32    34    16    20
15      11    28    63     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_49
CC  AC: MA0535.1
CC  id: MA0535.1
CC  name: cluster_49
CC  version: 
CC  name: cluster_49
CC  description: MA0535.1 Mad; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: Mad
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0535.1
XX
//
AC  cluster_5
XX
ID  cluster_5
XX
DE  TGACAg
P0       A     C     G     T
1        0     0     0 20.6667
2        0     0 20.6667     0
3    20.6667     0     0     0
4        0 20.3333     0 0.333333
5    19.6667     0     1     0
6    1.66667 1.33333 9.33333 2.66667
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_5
CC  AC: cluster_5
CC  id: cluster_5
CC  name: cluster_5
CC  version: 
CC  name: cluster_5
CC  description: TGACAg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: TGACAg
CC  consensus.strict.rc: CTGTCA
CC  consensus.IUPAC: TGACAg
CC  consensus.IUPAC.rc: CTGTCA
CC  consensus.regexp: TGACAg
CC  consensus.regexp.rc: CTGTCA
CC  merged_ID: hth,achi,vis
CC  merge_nb: 3
CC  merged_AC: MA0227.1,MA0207.1,MA0252.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_50
XX
ID  cluster_50
XX
DE  MA0015.1 Cf2; from JASPAR
P0       A     C     G     T
1       25    13    40     2
2        1     2     1    76
3       74     2     4     0
4        0     9     4    67
5       78     2     0     0
6        1     4     1    74
7       41     2    36     1
8        2     9     1    68
9       53     3    20     4
10      12    29    15    24
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_50
CC  AC: MA0015.1
CC  id: MA0015.1
CC  name: cluster_50
CC  version: 
CC  name: cluster_50
CC  description: MA0015.1 Cf2; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: Cf2
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0015.1
XX
//
AC  cluster_51
XX
ID  cluster_51
XX
DE  MA0249.1 twi; from JASPAR
P0       A     C     G     T
1        1     4     3     7
2        3     7     3     2
3        1     3    10     1
4        0    14     1     0
5       11     3     1     0
6        0     4     1    10
7        7     1     5     2
8        0     0     0    15
9        0     0    12     3
10       1     0     2    12
11       1     2     2    10
12       2     3     8     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_51
CC  AC: MA0249.1
CC  id: MA0249.1
CC  name: cluster_51
CC  version: 
CC  name: cluster_51
CC  description: MA0249.1 twi; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: twi
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0249.1
XX
//
AC  cluster_52
XX
ID  cluster_52
XX
DE  MA0449.1 h; from JASPAR
P0       A     C     G     T
1        1     2    30     1
2        3     7    17     7
3        1    31     1     1
4       22     1    10     1
5        1    30     1     2
6        2     1    30     1
7        1    10     1    22
8        1     1    31     1
9        7    17     7     3
10       1    30     2     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_52
CC  AC: MA0449.1
CC  id: MA0449.1
CC  name: cluster_52
CC  version: 
CC  name: cluster_52
CC  description: MA0449.1 h; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: h
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0449.1
XX
//
AC  cluster_53
XX
ID  cluster_53
XX
DE  MA0197.2 nub; from JASPAR
P0       A     C     G     T
1        2     1     1    25
2       29     0     0     0
3        0     0     0    29
4        0     0    25     4
5        0    19     1     9
6       24     0     0     5
7       28     0     1     0
8       28     0     0     1
9        4     1     0    24
10       6     5     8    10
11      19     5     1     4
12       4     3    15     7
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_53
CC  AC: MA0197.2
CC  id: MA0197.2
CC  name: cluster_53
CC  version: 
CC  name: cluster_53
CC  description: MA0197.2 nub; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: nub
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0197.2
XX
//
AC  cluster_54
XX
ID  cluster_54
XX
DE  MA0218.1 ct; from JASPAR
P0       A     C     G     T
1        0     6     0    14
2        1     2     0    17
3        9     0    11     0
4       20     0     0     0
5       17     0     1     2
6        0    19     1     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_54
CC  AC: MA0218.1
CC  id: MA0218.1
CC  name: cluster_54
CC  version: 
CC  name: cluster_54
CC  description: MA0218.1 ct; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ct
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0218.1
XX
//
AC  cluster_55
XX
ID  cluster_55
XX
DE  MA0016.1 usp; from JASPAR
P0       A     C     G     T
1        0     1    37     0
2        1     0    36     1
3        0     0    38     0
4        0     0    38     0
5        0     0     0    38
6        0    36     1     1
7       35     0     3     0
8        5    25     3     5
9        5     8    22     3
10       6    10    16     6
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_55
CC  AC: MA0016.1
CC  id: MA0016.1
CC  name: cluster_55
CC  version: 
CC  name: cluster_55
CC  description: MA0016.1 usp; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: usp
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0016.1
XX
//
AC  cluster_56
XX
ID  cluster_56
XX
DE  MA0237.2 pan; from JASPAR
P0       A     C     G     T
1        0     0    34    37
2        0    71     0     0
3        0     0    71     0
4        0    20    40    11
5        7    43     3    18
6       11    10    19    31
7        7    40     0    24
8        0    30    15    26
9        0     0     0    71
10       0     0     0    71
11       0     0    11    60
12      11     9    41    10
13      34     0    21    16
14      22     0     0    49
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_56
CC  AC: MA0237.2
CC  id: MA0237.2
CC  name: cluster_56
CC  version: 
CC  name: cluster_56
CC  description: MA0237.2 pan; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: pan
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0237.2
XX
//
AC  cluster_58
XX
ID  cluster_58
XX
DE  MA0534.1 EcR::usp; from JASPAR
P0       A     C     G     T
1       29    36    39     0
2       60    12    32     0
3       19    10    75     0
4        5     3    35    61
5        9     0     0    95
6       12    66     0    26
7       74     0    30     0
8       34    11     5    54
9        0     0     0   104
10       0     0   104     0
11      66    33     0     5
12      49    55     0     0
13      21    57     0    26
14       0    34     0    70
15      24    28     0    52
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_58
CC  AC: MA0534.1
CC  id: MA0534.1
CC  name: cluster_58
CC  version: 
CC  name: cluster_58
CC  description: MA0534.1 EcR::usp; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: EcR::usp
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0534.1
XX
//
AC  cluster_59
XX
ID  cluster_59
XX
DE  MA0530.1 cnc::maf-S; from JASPAR
P0       A     C     G     T
1      142   113   159    60
2      340    20   114     0
3        0     0     0   474
4        0     0   474     0
5      407    46    21     0
6        0   292    80   102
7       80     0   169   225
8       78   215    90    91
9      136    33   163   142
10      24     0   422    28
11       0   474     0     0
12     368     0   106     0
13     133   128    99   114
14     221    63     0   190
15     214    12     0   248
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_59
CC  AC: MA0530.1
CC  id: MA0530.1
CC  name: cluster_59
CC  version: 
CC  name: cluster_59
CC  description: MA0530.1 cnc::maf-S; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: cnc::maf-S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0530.1
XX
//
AC  cluster_6
XX
ID  cluster_6
XX
DE  tTwATkr
P0       A     C     G     T
1     5.25  1.25  1.75  16.5
2     2.75   0.5     0  21.5
3       10  0.25  0.75 13.75
4     24.5     0     0  0.25
5        0  0.75     0    24
6     2.75   0.5     8  13.5
7    16.25     0   7.5     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_6
CC  AC: cluster_6
CC  id: cluster_6
CC  name: cluster_6
CC  version: 
CC  name: cluster_6
CC  description: tTwATkr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tTtATta
CC  consensus.strict.rc: TAATAAA
CC  consensus.IUPAC: tTwATkr
CC  consensus.IUPAC.rc: YMATWAA
CC  consensus.regexp: tT[at]AT[gt][ag]
CC  consensus.regexp.rc: [CT][AC]AT[AT]AA
CC  merged_ID: CG4328-RA,Abd-B,Dbx,H2.0
CC  merge_nb: 4
CC  merged_AC: MA0182.1,MA0165.1,MA0174.1,MA0448.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_61
XX
ID  cluster_61
XX
DE  MA0452.2 Kr; from JASPAR
P0       A     C     G     T
1      179   422   238   457
2      205   189   218   684
3      151   386    73   686
4     1248     0     0    48
5     1080   216     0     0
6       11  1170     0   115
7        0  1177     0   119
8        0  1296     0     0
9        0   202     0  1094
10       0     0     0  1296
11       0   113     0  1183
12     113   399   231   553
13     318   322   285   371
14     211   434   219   432
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_61
CC  AC: MA0452.2
CC  id: MA0452.2
CC  name: cluster_61
CC  version: 
CC  name: cluster_61
CC  description: MA0452.2 Kr; from JASPAR
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: Kr
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: MA0452.2
XX
//
AC  cluster_7
XX
ID  cluster_7
XX
DE  TAAytA
P0       A     C     G     T
1        0     0     0  22.5
2     22.5     0     0     0
3     22.5     0     0     0
4      0.5   7.5     1  13.5
5      2.5     4     5    11
6       18     0   4.5     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_7
CC  AC: cluster_7
CC  id: cluster_7
CC  name: cluster_7
CC  version: 
CC  name: cluster_7
CC  description: TAAytA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: TAAttA
CC  consensus.strict.rc: TAATTA
CC  consensus.IUPAC: TAAytA
CC  consensus.IUPAC.rc: TARTTA
CC  consensus.regexp: TAA[ct]tA
CC  consensus.regexp.rc: TA[AG]TTA
CC  merged_ID: lbe,lbl
CC  merge_nb: 2
CC  merged_AC: MA0231.1,MA0232.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_8
XX
ID  cluster_8
XX
DE  TGATAC
P0       A     C     G     T
1        0 1.66667 0.333333 22.6667
2        0     0 24.6667     0
3    24.6667     0     0     0
4        0 2.33333 1.66667 20.6667
5    24.3333     0 0.333333     0
6    0.333333 9.66667     0     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_8
CC  AC: cluster_8
CC  id: cluster_8
CC  name: cluster_8
CC  version: 
CC  name: cluster_8
CC  description: TGATAC
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: TGATAC
CC  consensus.strict.rc: GTATCA
CC  consensus.IUPAC: TGATAC
CC  consensus.IUPAC.rc: GTATCA
CC  consensus.regexp: TGATAC
CC  consensus.regexp.rc: GTATCA
CC  merged_ID: Optix,Six4,so
CC  merge_nb: 3
CC  merged_AC: MA0199.1,MA0204.1,MA0246.1
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_9
XX
ID  cluster_9
XX
DE  TAATTrs
P0       A     C     G     T
1      0.5     0     0  21.5
2     21.5     0   0.5     0
3       22     0     0     0
4        0     0     0    22
5      1.5     0   1.5    19
6      7.5     0  12.5     2
7        1     9   8.5   3.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_9
CC  AC: cluster_9
CC  id: cluster_9
CC  name: cluster_9
CC  version: 
CC  name: cluster_9
CC  description: TAATTrs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: TAATTgc
CC  consensus.strict.rc: GCAATTA
CC  consensus.IUPAC: TAATTrs
CC  consensus.IUPAC.rc: SYAATTA
CC  consensus.regexp: TAATT[ag][cg]
CC  consensus.regexp.rc: [CG][CT]AATTA
CC  merged_ID: Dll,Dr
CC  merge_nb: 2
CC  merged_AC: MA0187.1,MA0188.1
CC  merge_type: mean
CC  type: merge
XX
//
