AC  cluster_1
XX
ID  cluster_1
XX
DE  yTAATyYAATTAr
P0       A     C     G     T
1    10.7778 19.4444 10.6667 28.3333
2    1270.22 781.778 699.778 14400.4
3    13752.3 563.778  1102 1734.11
4    15975.6 375.889   465 335.778
5    1378.11 1471.67 886.111 13416.3
6    224.667 5445.11  1286 10196.4
7    1.11111 5145.33 101.222 11904.6
8    15281.6 200.111 1595.78 74.7778
9    17050.9 26.7778 51.3333 23.2222
10   16.7778 17.5556 15.8889 17102
11   426.667 343.222 182.111 16200.2
12   15466.1 345.556 554.111 786.444
13   2131.67 949.333 2242.11 1060.56
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_1
CC  AC: cluster_1
CC  id: cluster_1
CC  name: cluster_1
CC  version: 
CC  name: cluster_1
CC  description: yTAATyYAATTAr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tTAATtTAATTAg
CC  consensus.strict.rc: CTAATTAAATTAA
CC  consensus.IUPAC: yTAATyYAATTAr
CC  consensus.IUPAC.rc: YTAATTRRATTAR
CC  consensus.regexp: [ct]TAAT[ct][CT]AATTA[ag]
CC  consensus.regexp.rc: [CT]TAATT[AG][AG]ATTA[AG]
CC  merged_ID: ARX_D,ALX3_D,UNC4_D,PHX2B_D,PHX2A_D,DRGX_D,ALX1_B,RAX2_D,ALX4_D
CC  merge_nb: 9
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m16_M01016,HOCOMOCO_2015_11_23_Human_m4_M01004,HOCOMOCO_2015_11_23_Human_m591_M01591,HOCOMOCO_2015_11_23_Human_m434_M01434,HOCOMOCO_2015_11_23_Human_m433_M01433,HOCOMOCO_2015_11_23_Human_m84_M01084,HOCOMOCO_2015_11_23_Human_m3_M01003,HOCOMOCO_2015_11_23_Human_m478_M01478,HOCOMOCO_2015_11_23_Human_m5_M01005
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_10
XX
ID  cluster_10
XX
DE  aACAmTrmMATTGtty
P0       A     C     G     T
1    2616.33 967.333   898   517
2    3464.33 192.667    97 1244.67
3    133.333 3589.67 931.667   344
4     4737 117.667    54    90
5    2794.33  2088 43.6667 72.6667
6    40.6667    90 38.3333 4829.67
7     2919 76.3333 1856.33   147
8     1913  2224 362.667   499
9    1420.67 3519.33 7.33333 51.3333
10    4900 78.3333     8 12.3333
11      90 58.3333     4 4846.33
12   17.3333    65     9 4907.33
13     307 971.667  3585   135
14   1216.33   551   162 3069.33
15   455.667 838.333 641.333 3063.33
16   817.333  1691 660.667 1829.67
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_10
CC  AC: cluster_10
CC  id: cluster_10
CC  name: cluster_10
CC  version: 
CC  name: cluster_10
CC  description: aACAmTrmMATTGtty
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aACAaTacCATTGttt
CC  consensus.strict.rc: AAACAATGGTATTGTT
CC  consensus.IUPAC: aACAmTrmMATTGtty
CC  consensus.IUPAC.rc: RAACAATKKYAKTGTT
CC  consensus.regexp: aACA[ac]T[ag][ac][AC]ATTGtt[ct]
CC  consensus.regexp.rc: [AG]AACAAT[GT][GT][CT]A[GT]TGTT
CC  merged_ID: SOX21_D,SOX11_D,SOX7_D
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m516_M01516,HOCOMOCO_2015_11_23_Human_m510_M01510,HOCOMOCO_2015_11_23_Human_m521_M01521
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_100
XX
ID  cluster_100
XX
DE  gwTAATyGATTAw
P0       A     C     G     T
1      473 390.5 1370.5 297.5
2    1076.5   420   278   757
3     1187   253   267  8397
4    10058   9.5    19  17.5
5     9796   9.5     9 289.5
6       19    21  23.5 10040.5
7       50 4581.5  77.5  5395
8     81.5   9.5 10009     4
9    10056.5  10.5  32.5   4.5
10    32.5   7.5   6.5 10057.5
11    15.5    18   9.5 10061
12   8905.5   267 282.5   649
13   3587.5  1319  1304 3893.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_100
CC  AC: cluster_100
CC  id: cluster_100
CC  name: cluster_100
CC  version: 
CC  name: cluster_100
CC  description: gwTAATyGATTAw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gaTAATtGATTAt
CC  consensus.strict.rc: ATAATCAATTATC
CC  consensus.IUPAC: gwTAATyGATTAw
CC  consensus.IUPAC.rc: WTAATCRATTAWC
CC  consensus.regexp: g[at]TAAT[ct]GATTA[at]
CC  consensus.regexp.rc: [AT]TAATC[AG]ATTA[AT]C
CC  merged_ID: PAX3_D,PAX7_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m421_M01421,HOCOMOCO_2015_11_23_Human_m426_M01426
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_101
XX
ID  cluster_101
XX
DE  rAswTCAAAGrra
P0       A     C     G     T
1       89  60.5    76    24
2    184.5  14.5  30.5    28
3       15 112.5 116.5  13.5
4      163     6     4  84.5
5     26.5   4.5     3 223.5
6     15.5 189.5    46   6.5
7      253   2.5     0     2
8      246     2   9.5     0
9      251   0.5   4.5   1.5
10    26.5  12.5   209   9.5
11    73.5    52 107.5  24.5
12      97    64  68.5    28
13   111.5    59  42.5  44.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_101
CC  AC: cluster_101
CC  id: cluster_101
CC  name: cluster_101
CC  version: 
CC  name: cluster_101
CC  description: rAswTCAAAGrra
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aAgaTCAAAGgaa
CC  consensus.strict.rc: TTCCTTTGATCTT
CC  consensus.IUPAC: rAswTCAAAGrra
CC  consensus.IUPAC.rc: TYYCTTTGAWSTY
CC  consensus.regexp: [ag]A[cg][at]TCAAAG[ag][ag]a
CC  consensus.regexp.rc: T[CT][CT]CTTTGA[AT][CG]T[CT]
CC  merged_ID: TCF7_C,TF7L2_A
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m561_M01561,HOCOMOCO_2015_11_23_Human_m569_M01569
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_102
XX
ID  cluster_102
XX
DE  rssrTRKGgts
P0       A     C     G     T
1       31    13    50    27
2       18    41    39    23
3        8    69    38     6
4       60     6    52     3
5       28     1     8    84
6       89     0    32     0
7        0     1    33    87
8       11     4    94    12
9       25    19    47    30
10      16    18    17    70
11      23    32    50    16
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_102
CC  AC: HOCOMOCO_2015_11_23_Human_m353_M01353
CC  id: HOCOMOCO_2015_11_23_Human_m353_M01353
CC  name: cluster_102
CC  version: 
CC  name: cluster_102
CC  description: rssrTRKGgts
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NDF2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m353_M01353
XX
//
AC  cluster_103
XX
ID  cluster_103
XX
DE  tTAATkAykww
P0       A     C     G     T
1     6933  7571  4945 19699
2     1420  2299  1095 34334
3    34395   389  3892   472
4    38747   121   169   111
5       78   366   249 38455
6        5     5 14222 24916
7    37178    36  1881    53
8     2307 15279  6120 15442
9     6256  8646 13314 10932
10   17846  4055  4135 13112
11    9935  4079  2950 22184
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_103
CC  AC: HOCOMOCO_2015_11_23_Human_m328_M01328
CC  id: HOCOMOCO_2015_11_23_Human_m328_M01328
CC  name: cluster_103
CC  version: 
CC  name: cluster_103
CC  description: tTAATkAykww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MEOX2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m328_M01328
XX
//
AC  cluster_104
XX
ID  cluster_104
XX
DE  ggtrrTKarsg
P0       A     C     G     T
1     2325  1998  3118  2077
2     2122  2021  3433  1942
3     1427  1454  1782  4855
4     4979   277  2902  1360
5     5967   535  3015     1
6        1     1   227  9289
7        1     1  2921  6595
8     5624   508  2038  1348
9     2487  1345  4468  1218
10    1535  2726  3055  2202
11    1689  2079  3647  2103
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_104
CC  AC: HOCOMOCO_2015_11_23_Human_m218_M01218
CC  id: HOCOMOCO_2015_11_23_Human_m218_M01218
CC  name: cluster_104
CC  version: 
CC  name: cluster_104
CC  description: ggtrrTKarsg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HME2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m218_M01218
XX
//
AC  cluster_105
XX
ID  cluster_105
XX
DE  rGdACAbksTGTtCytk
P0       A     C     G     T
1      200   8.5 179.5 106.5
2       26   1.5 403.5  63.5
3    130.5   107 131.5 125.5
4      412    22  20.5    40
5      0.5 458.5  21.5    14
6    343.5  22.5    36  92.5
7     40.5   134   151   169
8       70    47   145 232.5
9       80   216   128  70.5
10    47.5    30     4   413
11    12.5    11 467.5   3.5
12      13    16    12 453.5
13    82.5    93    60   259
14    15.5 429.5   3.5    46
15      44 218.5    12   220
16    60.5  55.5  31.5    97
17      44    27    78  95.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_105
CC  AC: cluster_105
CC  id: cluster_105
CC  name: cluster_105
CC  version: 
CC  name: cluster_105
CC  description: rGdACAbksTGTtCytk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aGgACAttcTGTtCttt
CC  consensus.strict.rc: AAAGAACAGAATGTCCT
CC  consensus.IUPAC: rGdACAbksTGTtCytk
CC  consensus.IUPAC.rc: MARGAACASMVTGTHCY
CC  consensus.regexp: [ag]G[agt]ACA[cgt][gt][cg]TGTtC[ct]t[gt]
CC  consensus.regexp.rc: [AC]A[AG]GAACA[CG][AC][ACG]TGT[ACT]C[CT]
CC  merged_ID: GCR_S,ANDR_A
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m185_M01185,HOCOMOCO_2015_11_23_Human_m6_M01006
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_106
XX
ID  cluster_106
XX
DE  wwmmGGAwrTw
P0       A     C     G     T
1       25     1     2    11
2       12     9     3    15
3       19    11     7     2
4       24    11     3     1
5        0     0    39     0
6        0     0    39     0
7       39     0     0     0
8       26     0     0    13
9       16     4    19     0
10       3     7     1    28
11      19     3     6    11
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_106
CC  AC: HOCOMOCO_2015_11_23_Human_m105_M01105
CC  id: HOCOMOCO_2015_11_23_Human_m105_M01105
CC  name: cluster_106
CC  version: 
CC  name: cluster_106
CC  description: wwmmGGAwrTw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ELF5_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m105_M01105
XX
//
AC  cluster_107
XX
ID  cluster_107
XX
DE  wmsCGGAAGTr
P0       A     C     G     T
1      935   242   341   521
2      810   564   406   259
3      183  1142   553   161
4      159  1725    99    56
5        1     0  2038     0
6        1     1  2032     5
7     2032     0     5     2
8     2025     1     4     9
9      350    11  1674     4
10      25   151    44  1819
11     710   174   828   327
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_107
CC  AC: HOCOMOCO_2015_11_23_Human_m131_M01131
CC  id: HOCOMOCO_2015_11_23_Human_m131_M01131
CC  name: cluster_107
CC  version: 
CC  name: cluster_107
CC  description: wmsCGGAAGTr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ETV6_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m131_M01131
XX
//
AC  cluster_108
XX
ID  cluster_108
XX
DE  wTGACAGcTGTcm
P0       A     C     G     T
1     7916 3631.5 4421.5  7213
2    597.5   593 579.5 21412
3      539   540 21566.5 536.5
4    22805    47 150.5 179.5
5       51 23022.5    59  49.5
6    22402.5    93 593.5    93
7      568  3316 16398.5 2899.5
8    3645.5 11873 5614.5  2049
9     1370 2224.5   982 18605.5
10   2391.5 1852.5 16806.5 2131.5
11    2375 2438.5 2468.5 15900
12      12 128.5  39.5  50.5
13   104.5    78    25    23
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_108
CC  AC: cluster_108
CC  id: cluster_108
CC  name: cluster_108
CC  version: 
CC  name: cluster_108
CC  description: wTGACAGcTGTcm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aTGACAGcTGTca
CC  consensus.strict.rc: TGACAGCTGTCAT
CC  consensus.IUPAC: wTGACAGcTGTcm
CC  consensus.IUPAC.rc: KGACAGCTGTCAW
CC  consensus.regexp: [at]TGACAGcTGTc[ac]
CC  consensus.regexp.rc: [GT]GACAGCTGTCA[AT]
CC  merged_ID: MEIS2_B,TGIF2_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m325_M01325,HOCOMOCO_2015_11_23_Human_m579_M01579
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_109
XX
ID  cluster_109
XX
DE  wrmGTGGGyGt
P0       A     C     G     T
1        5     0     0     3
2        2     0     5     1
3        5     2     0     1
4        1     0     7     0
5        1     0     1     6
6        0     0     8     0
7        0     0     7     1
8        0     0     8     0
9        0     4     0     4
10       0     0     7     1
11       1     1     1     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_109
CC  AC: HOCOMOCO_2015_11_23_Human_m98_M01098
CC  id: HOCOMOCO_2015_11_23_Human_m98_M01098
CC  name: cluster_109
CC  version: 
CC  name: cluster_109
CC  description: wrmGTGGGyGt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: EGR3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m98_M01098
XX
//
AC  cluster_11
XX
ID  cluster_11
XX
DE  aAmCATATGgy
P0       A     C     G     T
1    667.75 474.5 422.25 380.5
2     1497 83.25   239 125.75
3    901.25   862 155.75    26
4     20.5 1903.5 10.25 10.75
5    1874.5 16.25 40.25    14
6      5.5  29.5 24.25 1885.75
7    1871.25    59  3.25  11.5
8    16.75 52.75  9.25 1866.25
9       35    47 1576.5 286.5
10   58.25 280.25 1167.5   439
11     162 520.25 122.5 1140.25
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_11
CC  AC: cluster_11
CC  id: cluster_11
CC  name: cluster_11
CC  version: 
CC  name: cluster_11
CC  description: aAmCATATGgy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aAaCATATGgt
CC  consensus.strict.rc: ACCATATGTTT
CC  consensus.IUPAC: aAmCATATGgy
CC  consensus.IUPAC.rc: RCCATATGKTT
CC  consensus.regexp: aA[ac]CATATGg[ct]
CC  consensus.regexp.rc: [AG]CCATATG[GT]TT
CC  merged_ID: OLIG2_D,OLIG3_D,ATOH1_D,BHA15_D
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m405_M01405,HOCOMOCO_2015_11_23_Human_m406_M01406,HOCOMOCO_2015_11_23_Human_m23_M01023,HOCOMOCO_2015_11_23_Human_m35_M01035
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_110
XX
ID  cluster_110
XX
DE  cAArATGGCGGc
P0       A     C     G     T
1    177.5 436.5 114.5  71.5
2      976  41.5  88.5  99.5
3      905  67.5    52   181
4      467   179   452 107.5
5    1188.5   6.5     4   6.5
6      5.5     0   0.5 1199.5
7        1     0 1204.5     0
8       23   0.5  1167    15
9    107.5 1000.5  14.5    83
10    93.5 162.5 850.5    99
11     131 114.5 832.5 127.5
12     229   656 144.5   176
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_110
CC  AC: cluster_110
CC  id: cluster_110
CC  name: cluster_110
CC  version: 
CC  name: cluster_110
CC  description: cAArATGGCGGc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cAAaATGGCGGc
CC  consensus.strict.rc: GCCGCCATTTTG
CC  consensus.IUPAC: cAArATGGCGGc
CC  consensus.IUPAC.rc: GCCGCCATYTTG
CC  consensus.regexp: cAA[ag]ATGGCGGc
CC  consensus.regexp.rc: GCCGCCAT[CT]TTG
CC  merged_ID: TYY1_A,TYY2_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m588_M01588,HOCOMOCO_2015_11_23_Human_m589_M01589
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_111
XX
ID  cluster_111
XX
DE  atTkcGmAATmty
P0       A     C     G     T
1      6.5     2   1.5     1
2     45.5  43.5    82   246
3        0   0.5  43.5   373
4     94.5     0 193.5   129
5       21 233.5    91  71.5
6       35     2   380     0
7    131.5   188   0.5    97
8      355  61.5   0.5     0
9      417     0     0     0
10       0    63     5   349
11     109 202.5    19  86.5
12    87.5  29.5    76   224
13      47   129  87.5 153.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_111
CC  AC: cluster_111
CC  id: cluster_111
CC  name: cluster_111
CC  version: 
CC  name: cluster_111
CC  description: atTkcGmAATmty
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: atTgcGcAATctt
CC  consensus.strict.rc: AAGATTGCGCAAT
CC  consensus.IUPAC: atTkcGmAATmty
CC  consensus.IUPAC.rc: RAKATTKCGMAAT
CC  consensus.regexp: atT[gt]cG[ac]AAT[ac]t[ct]
CC  consensus.regexp.rc: [AG]A[GT]ATT[GT]CG[AC]AAT
CC  merged_ID: CEBPE_A,CEBPG_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m51_M01051,HOCOMOCO_2015_11_23_Human_m52_M01052
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_112
XX
ID  cluster_112
XX
DE  TAATTggtwttTAATTrs
P0       A     C     G     T
1    2127.5 1718.5 1225.5 11859
2    13125.5 716.5 821.5  2267
3    15472.5 184.5 228.5  1045
4    2003.5 196.5   445 14285.5
5    1078.5  1423   995 13434
6    3794.5   292 10297  2547
7     2503 4031.5 7453.5 2942.5
8    3865.5  2216  2683  8166
9     4949  2878 2024.5  7079
10    2701 2635.5  3810  7784
11    2197  2863 1765.5 10105
12    57.5 158.5    10 16704.5
13   16235    15 403.5   277
14   16922     1     6   1.5
15    86.5   165   254 16425
16     459 750.5  1184 14537
17   4912.5   503 10784.5 730.5
18    1887  5325 6080.5  2075
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_112
CC  AC: cluster_112
CC  id: cluster_112
CC  name: cluster_112
CC  version: 
CC  name: cluster_112
CC  description: TAATTggtwttTAATTrs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: TAATTggttttTAATTgg
CC  consensus.strict.rc: CCAATTAAAAACCAATTA
CC  consensus.IUPAC: TAATTggtwttTAATTrs
CC  consensus.IUPAC.rc: SYAATTAAAWACCAATTA
CC  consensus.regexp: TAATTggt[at]ttTAATT[ag][cg]
CC  consensus.regexp.rc: [CG][CT]AATTAAA[AT]ACCAATTA
CC  merged_ID: BARX1_D,MSX1_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m27_M01027,HOCOMOCO_2015_11_23_Human_m337_M01337
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_113
XX
ID  cluster_113
XX
DE  rrGkGGGCGTGgy
P0       A     C     G     T
1      772   239   707   506
2      822   152   883   367
3      285    33  1863    43
4       38    14  1378   794
5       19     0  2205     0
6        0     0  2224     0
7        0     0  2222     2
8       17  2127    17    63
9        3     2  2216     3
10       3     9   205  2007
11     182    44  1946    52
12     349   203  1289   383
13     281   989   320   634
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_113
CC  AC: HOCOMOCO_2015_11_23_Human_m289_M01289
CC  id: HOCOMOCO_2015_11_23_Human_m289_M01289
CC  name: cluster_113
CC  version: 
CC  name: cluster_113
CC  description: rrGkGGGCGTGgy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: KLF14_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m289_M01289
XX
//
AC  cluster_114
XX
ID  cluster_114
XX
DE  GmGgGGGcGkGgg
P0       A     C     G     T
1        1     7    32     4
2       11    22     4     7
3        1     3    40     0
4        6     9    21     8
5        4     0    40     0
6        1     0    39     4
7        0     1    42     1
8        7    26     1    10
9        0     0    44     0
10       3     5    23    13
11       8     3    31     2
12       3     6    27     8
13       2     9    30     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_114
CC  AC: HOCOMOCO_2015_11_23_Human_m601_M01601
CC  id: HOCOMOCO_2015_11_23_Human_m601_M01601
CC  name: cluster_114
CC  version: 
CC  name: cluster_114
CC  description: GmGgGGGcGkGgg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: WT1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m601_M01601
XX
//
AC  cluster_115
XX
ID  cluster_115
XX
DE  wTTAAkTGktt
P0       A     C     G     T
1      315   197   282   427
2      131    88    52   950
3       99    32    23  1067
4     1044     2    98    77
5     1215     0     5     1
6      132    26   378   685
7        6   211     8   996
8       39    16  1137    29
9      217   261   430   313
10     161   224   167   669
11     244   225   190   562
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_115
CC  AC: HOCOMOCO_2015_11_23_Human_m222_M01222
CC  id: HOCOMOCO_2015_11_23_Human_m222_M01222
CC  name: cluster_115
CC  version: 
CC  name: cluster_115
CC  description: wTTAAkTGktt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HMX2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m222_M01222
XX
//
AC  cluster_116
XX
ID  cluster_116
XX
DE  btyAAGTGGkt
P0       A     C     G     T
1      174   317   380   376
2      191   150   160   746
3      107   378   165   597
4     1142     8    75    22
5     1173     6    63     5
6        7     7  1225     8
7       32     4    36  1175
8       27     7  1204     9
9       32   133   923   159
10     156   246   350   495
11     274   247   225   501
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_116
CC  AC: HOCOMOCO_2015_11_23_Human_m375_M01375
CC  id: HOCOMOCO_2015_11_23_Human_m375_M01375
CC  name: cluster_116
CC  version: 
CC  name: cluster_116
CC  description: btyAAGTGGkt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NKX23_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m375_M01375
XX
//
AC  cluster_117
XX
ID  cluster_117
XX
DE  gsymrgAGGTCAb
P0       A     C     G     T
1        4     2    11     1
2        1     6     8     3
3        2    10     1     5
4        5     8     4     1
5       12     0     6     0
6        4     3    10     1
7       17     0     1     0
8        0     0    18     0
9        0     0    14     4
10       0     0     0    18
11       0    18     0     0
12      17     0     0     1
13       2     5     6     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_117
CC  AC: HOCOMOCO_2015_11_23_Human_m392_M01392
CC  id: HOCOMOCO_2015_11_23_Human_m392_M01392
CC  name: cluster_117
CC  version: 
CC  name: cluster_117
CC  description: gsymrgAGGTCAb
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NR2C1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m392_M01392
XX
//
AC  cluster_118
XX
ID  cluster_118
XX
DE  tTGTTTATkkTTw
P0       A     C     G     T
1      122    92   103   309
2        0     0     0   626
3       52     0   572     2
4        1     0     0   625
5        0     0     0   626
6        6     0   110   510
7      512     0    23    91
8        0    45     0   581
9       71     0   269   286
10      28     0   204   394
11      33     0    18   575
12       4     6    11   605
13     225    44    87   270
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_118
CC  AC: HOCOMOCO_2015_11_23_Human_m158_M01158
CC  id: HOCOMOCO_2015_11_23_Human_m158_M01158
CC  name: cluster_118
CC  version: 
CC  name: cluster_118
CC  description: tTGTTTATkkTTw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXJ3_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m158_M01158
XX
//
AC  cluster_119
XX
ID  cluster_119
XX
DE  tkkTTwwTGTTTa
P0       A     C     G     T
1       77    61    75   400
2       21     0   240   352
3       18     0   317   278
4        0     0     0   613
5       34     0    71   508
6      243     0    71   299
7      219    48     0   346
8        0     0     0   613
9      100     0   513     0
10       0     0     0   613
11       0     0     0   613
12       0     0     8   605
13     396    64    72    81
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_119
CC  AC: HOCOMOCO_2015_11_23_Human_m159_M01159
CC  id: HOCOMOCO_2015_11_23_Human_m159_M01159
CC  name: cluster_119
CC  version: 
CC  name: cluster_119
CC  description: tkkTTwwTGTTTa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXJ3_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m159_M01159
XX
//
AC  cluster_12
XX
ID  cluster_12
XX
DE  rRGkTCAb
P0       A     C     G     T
1       19     2    13     5
2       29     0    10     0
3        0     0    39     0
4      2.5     0    11  25.5
5        0   0.5     0  38.5
6        0    36   2.5   0.5
7     38.5     0   0.5     0
8      3.5  12.5    11    12
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_12
CC  AC: cluster_12
CC  id: cluster_12
CC  name: cluster_12
CC  version: 
CC  name: cluster_12
CC  description: rRGkTCAb
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aAGtTCAc
CC  consensus.strict.rc: GTGAACTT
CC  consensus.IUPAC: rRGkTCAb
CC  consensus.IUPAC.rc: VTGAMCYY
CC  consensus.regexp: [ag][AG]G[gt]TCA[cgt]
CC  consensus.regexp.rc: [ACG]TGA[AC]C[CT][CT]
CC  merged_ID: NR1I2_S,NR1I3_S
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m389_M01389,HOCOMOCO_2015_11_23_Human_m391_M01391
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_120
XX
ID  cluster_120
XX
DE  ywawwwwwassYmATTAawt
P0       A     C     G     T
1      2.5     4     2     6
2    6724.5 4937.5  2075 9646.5
3    13018 4199.5 1158.5 5007.5
4    12299 1525.5 649.5 8909.5
5     7207   337  1106 14733.5
6    10689.5   425 425.5 11843.5
7    14700.5 337.5 1315.5  7030
8    14295.5   821 2405.5 5861.5
9    12798 1512.5  5608  3465
10   4816.5 6649.5 8485.5  3432
11   2518.5  7113 12375.5 1376.5
12   136.5  6995 226.5 16025.5
13   14131  8055 295.5   902
14   22456.5  95.5  43.5   788
15   759.5    36  11.5 22576.5
16   303.5  1295 188.5 21596.5
17   20782.5 749.5  1061 790.5
18   15376.5  2118 1647.5 4241.5
19    5973 3880.5 3781.5 9748.5
20   5664.5  3692  4656  9371
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_120
CC  AC: cluster_120
CC  id: cluster_120
CC  name: cluster_120
CC  version: 
CC  name: cluster_120
CC  description: ywawwwwwassYmATTAawt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ttaattaaaggTaATTAatt
CC  consensus.strict.rc: AATTAATTACCTTTAATTAA
CC  consensus.IUPAC: ywawwwwwassYmATTAawt
CC  consensus.IUPAC.rc: AWTTAATKRSSTWWWWWTWR
CC  consensus.regexp: [ct][at]a[at][at][at][at][at]a[cg][cg][CT][ac]ATTAa[at]t
CC  consensus.regexp.rc: A[AT]TTAAT[GT][AG][CG][CG]T[AT][AT][AT][AT][AT]T[AT][AG]
CC  merged_ID: NKX61_D,DMBX1_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m380_M01380,HOCOMOCO_2015_11_23_Human_m82_M01082
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_121
XX
ID  cluster_121
XX
DE  rkcGGrTkrgk
P0       A     C     G     T
1      260    39   253    58
2       72    35   283   220
3      149   285   117    59
4       15    61   530     4
5       23     4   581     2
6      355     0   255     0
7       20    39     1   550
8       19    15   181   395
9      352    13   217    28
10     122    35   396    57
11      87   145   218   160
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_121
CC  AC: HOCOMOCO_2015_11_23_Human_m197_M01197
CC  id: HOCOMOCO_2015_11_23_Human_m197_M01197
CC  name: cluster_121
CC  version: 
CC  name: cluster_121
CC  description: rkcGGrTkrgk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: GSC2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m197_M01197
XX
//
AC  cluster_122
XX
ID  cluster_122
XX
DE  grggtsabttsAGGwCAk
P0       A     C     G     T
1        4   3.5   8.5   1.5
2     24.5     3    21     6
3        6     8    35   5.5
4        6     8    33   7.5
5      6.5    10     7    31
6      7.5  28.5  14.5     4
7       33     5   8.5     8
8        7    19  14.5    14
9     12.5  13.5    12  16.5
10       4     9     8  33.5
11     9.5  24.5  17.5     3
12      48     0   4.5     2
13     0.5     3  47.5   3.5
14       0   4.5    46     4
15    15.5     1     4    34
16     2.5    40     9     3
17    42.5   7.5     3   1.5
18     4.5   6.5  16.5   9.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_122
CC  AC: cluster_122
CC  id: cluster_122
CC  name: cluster_122
CC  version: 
CC  name: cluster_122
CC  description: grggtsabttsAGGwCAk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gaggtcacttcAGGtCAg
CC  consensus.strict.rc: CTGACCTGAAGTGACCTC
CC  consensus.IUPAC: grggtsabttsAGGwCAk
CC  consensus.IUPAC.rc: MTGWCCTSAAVTSACCYC
CC  consensus.regexp: g[ag]ggt[cg]a[cgt]tt[cg]AGG[at]CA[gt]
CC  consensus.regexp.rc: [AC]TG[AT]CCT[CG]AA[ACG]T[CG]ACC[CT]C
CC  merged_ID: THA_S,THB_S
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m581_M01581,HOCOMOCO_2015_11_23_Human_m583_M01583
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_123
XX
ID  cluster_123
XX
DE  yrMATTCCwggsat
P0       A     C     G     T
1        1     7     3     4
2    122.5   3.5 126.5  12.5
3     71.5   181   8.5     4
4      260     1     2     2
5      0.5   2.5     0   262
6     15.5     1     6 242.5
7        1 245.5  11.5     7
8        3   226     1    35
9    102.5   8.5  10.5 143.5
10      31  37.5   151  45.5
11    61.5  61.5 105.5  36.5
12    38.5   106  85.5    35
13     103  62.5  52.5    47
14    35.5    55    56 118.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_123
CC  AC: cluster_123
CC  id: cluster_123
CC  name: cluster_123
CC  version: 
CC  name: cluster_123
CC  description: yrMATTCCwggsat
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cgCATTCCtggcat
CC  consensus.strict.rc: ATGCCAGGAATGCG
CC  consensus.IUPAC: yrMATTCCwggsat
CC  consensus.IUPAC.rc: ATSCCWGGAATKYR
CC  consensus.regexp: [ct][ag][AC]ATTCC[at]gg[cg]at
CC  consensus.regexp.rc: AT[CG]CC[AT]GGAAT[GT][CT][AG]
CC  merged_ID: TEAD1_D,TEAD4_A
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m562_M01562,HOCOMOCO_2015_11_23_Human_m564_M01564
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_124
XX
ID  cluster_124
XX
DE  srsyGCGCmTGCGCagkgss
P0       A     C     G     T
1       19    23  40.5     7
2     37.5   9.5    32  10.5
3     48.5   138   114    39
4       27  85.5    58   169
5       33     2   302   2.5
6     34.5   269  26.5   9.5
7     15.5     6   316     2
8        5 319.5    14     1
9    135.5 131.5    48  24.5
10       6  10.5    22   301
11       4     1 326.5     8
12    11.5   318     5     5
13       7     6   318   8.5
14     3.5   299  12.5  24.5
15   189.5  44.5  82.5    23
16      50  66.5 179.5  43.5
17      63    75  94.5   107
18    67.5    77   172    23
19      53   108   136  42.5
20    41.5   120 134.5  43.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_124
CC  AC: cluster_124
CC  id: cluster_124
CC  name: cluster_124
CC  version: 
CC  name: cluster_124
CC  description: srsyGCGCmTGCGCagkgss
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gactGCGCaTGCGCagtggg
CC  consensus.strict.rc: CCCACTGCGCATGCGCAGTC
CC  consensus.IUPAC: srsyGCGCmTGCGCagkgss
CC  consensus.IUPAC.rc: SSCMCTGCGCAKGCGCRSYS
CC  consensus.regexp: [cg][ag][cg][ct]GCGC[ac]TGCGCag[gt]g[cg][cg]
CC  consensus.regexp.rc: [CG][CG]C[AC]CTGCGCA[GT]GCGC[AG][CG][CT][CG]
CC  merged_ID: NRF1_A,ZN639_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m402_M01402,HOCOMOCO_2015_11_23_Human_m635_M01635
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_125
XX
ID  cluster_125
XX
DE  carRGGTCArrgk
P0       A     C     G     T
1       47 117.5  46.5    39
2    166.5  37.5  21.5  24.5
3      135    22  90.5    13
4    191.5   0.5    67   1.5
5        2     0 255.5     3
6        9     2 235.5    14
7        2  12.5  20.5 225.5
8      0.5   242   5.5  12.5
9    257.5     0     2     1
10      96    32  98.5    34
11   105.5    31   100    24
12    56.5  17.5 145.5    41
13    60.5  35.5  98.5    66
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_125
CC  AC: cluster_125
CC  id: cluster_125
CC  name: cluster_125
CC  version: 
CC  name: cluster_125
CC  description: carRGGTCArrgk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: caaAGGTCAgagg
CC  consensus.strict.rc: CCTCTGACCTTTG
CC  consensus.IUPAC: carRGGTCArrgk
CC  consensus.IUPAC.rc: MCYYTGACCYYTG
CC  consensus.regexp: ca[ag][AG]GGTCA[ag][ag]g[gt]
CC  consensus.regexp.rc: [AC]C[CT][CT]TGACC[CT][CT]TG
CC  merged_ID: RARB_D,COT2_S
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m475_M01475,HOCOMOCO_2015_11_23_Human_m59_M01059
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_126
XX
ID  cluster_126
XX
DE  tsAGGTCAsr
P0       A     C     G     T
1        4     2     4    16
2        3    10    13     0
3       22     2     2     0
4        1     0    23     2
5        0     2    22     2
6        1     0     1    24
7        0    23     2     1
8       25     1     0     0
9        2    13     8     3
10      10     4     8     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_126
CC  AC: HOCOMOCO_2015_11_23_Human_m495_M01495
CC  id: HOCOMOCO_2015_11_23_Human_m495_M01495
CC  name: cluster_126
CC  version: 
CC  name: cluster_126
CC  description: tsAGGTCAsr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: RXRB_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m495_M01495
XX
//
AC  cluster_127
XX
ID  cluster_127
XX
DE  yTsAGGTCAs
P0       A     C     G     T
1        8    26    14    26
2        6    11     6    51
3       10    37    21     6
4       58     3    12     1
5        0     1    67     6
6        0     0    69     5
7        5     0     1    68
8        1    68     5     0
9       69     4     1     0
10      11    22    30    11
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_127
CC  AC: HOCOMOCO_2015_11_23_Human_m582_M01582
CC  id: HOCOMOCO_2015_11_23_Human_m582_M01582
CC  name: cluster_127
CC  version: 
CC  name: cluster_127
CC  description: yTsAGGTCAs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: THA_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m582_M01582
XX
//
AC  cluster_128
XX
ID  cluster_128
XX
DE  wawwTGATTGATgdg
P0       A     C     G     T
1       57    20    29    39
2       65    22    31    27
3       43    24    17    61
4       39    10     8    88
5       22     2    11   110
6        5     2   134     4
7      123     7    12     3
8       21     7     0   117
9        5     8    22   110
10      10     2   127     6
11     124     2    13     6
12       7    13     4   121
13      20    19    71    35
14      37    12    51    45
15      22    30    58    35
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_128
CC  AC: HOCOMOCO_2015_11_23_Human_m428_M01428
CC  id: HOCOMOCO_2015_11_23_Human_m428_M01428
CC  name: cluster_128
CC  version: 
CC  name: cluster_128
CC  description: wawwTGATTGATgdg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PBX1_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m428_M01428
XX
//
AC  cluster_129
XX
ID  cluster_129
XX
DE  krawkTGATTGawkg
P0       A     C     G     T
1        1     3     7     8
2        8     2     7     2
3       10     4     4     1
4       10     3     1     5
5        1     1     6    11
6        1     1     0    17
7        1     0    15     3
8       15     0     3     1
9        0     3     0    16
10       2     0     0    17
11       0     0    15     4
12      13     1     2     3
13       5     2     0    12
14       3     0     8     8
15       3     4     8     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_129
CC  AC: HOCOMOCO_2015_11_23_Human_m429_M01429
CC  id: HOCOMOCO_2015_11_23_Human_m429_M01429
CC  name: cluster_129
CC  version: 
CC  name: cluster_129
CC  description: krawkTGATTGawkg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PBX2_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m429_M01429
XX
//
AC  cluster_13
XX
ID  cluster_13
XX
DE  dkCTATTTTTrgm
P0       A     C     G     T
1    89.3333 17.6667 87.3333 117.333
2    34.3333 25.6667 118.333   137
3    10.6667 233.333 15.3333    56
4    1.33333    15     0   299
5    296.667 3.66667 6.33333 8.66667
6    21.3333     3 0.333333 290.667
7       23     2     2 288.333
8    8.33333 9.66667 1.66667 295.667
9    55.3333 33.3333 4.33333 222.333
10   35.6667 10.3333 32.3333   237
11   178.333 0.666667 135.333     1
12   46.3333 18.6667 212.333    38
13   123.333 106.667    37 48.3333
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_13
CC  AC: cluster_13
CC  id: cluster_13
CC  name: cluster_13
CC  version: 
CC  name: cluster_13
CC  description: dkCTATTTTTrgm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ttCTATTTTTaga
CC  consensus.strict.rc: TCTAAAAATAGAA
CC  consensus.IUPAC: dkCTATTTTTrgm
CC  consensus.IUPAC.rc: KCYAAAAATAGMH
CC  consensus.regexp: [agt][gt]CTATTTTT[ag]g[ac]
CC  consensus.regexp.rc: [GT]C[CT]AAAAATAG[AC][ACT]
CC  merged_ID: MEF2D_C,MEF2A_A,MEF2C_C
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m323_M01323,HOCOMOCO_2015_11_23_Human_m320_M01320,HOCOMOCO_2015_11_23_Human_m322_M01322
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_130
XX
ID  cluster_130
XX
DE  TGTTTgTTT
P0       A     C     G     T
1        6     3     3    63
2        8     1    66     0
3        0     0     0    75
4        0     0     0    75
5        3     0    15    57
6       17     0    45    13
7        0     6     0    69
8        2     0     1    72
9        2    12     0    61
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_130
CC  AC: HOCOMOCO_2015_11_23_Human_m151_M01151
CC  id: HOCOMOCO_2015_11_23_Human_m151_M01151
CC  name: cluster_130
CC  version: 
CC  name: cluster_130
CC  description: TGTTTgTTT
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXD3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m151_M01151
XX
//
AC  cluster_131
XX
ID  cluster_131
XX
DE  kCACGTGAs
P0       A     C     G     T
1      224    11  1200   602
2      169  1866     1     1
3     1995    42     0     0
4        4  1923    68    42
5      102     4  1927     4
6       92    55    24  1866
7        0     0  2037     0
8     1398   303   173   163
9      152  1041   616   228
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_131
CC  AC: HOCOMOCO_2015_11_23_Human_m38_M01038
CC  id: HOCOMOCO_2015_11_23_Human_m38_M01038
CC  name: cluster_131
CC  version: 
CC  name: cluster_131
CC  description: kCACGTGAs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BHE40_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m38_M01038
XX
//
AC  cluster_132
XX
ID  cluster_132
XX
DE  yTAATTr
P0       A     C     G     T
1       10    18     5    26
2        0     0     0    59
3       59     0     0     0
4       58     0     1     0
5        1     1     0    57
6        0     3     3    53
7       27     1    29     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_132
CC  AC: HOCOMOCO_2015_11_23_Human_m470_M01470
CC  id: HOCOMOCO_2015_11_23_Human_m470_M01470
CC  name: cluster_132
CC  version: 
CC  name: cluster_132
CC  description: yTAATTr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PRRX2_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m470_M01470
XX
//
AC  cluster_133
XX
ID  cluster_133
XX
DE  rgsrtrTGktkk
P0       A     C     G     T
1      161    39   129    63
2       94    76   156    66
3       47   195   104    46
4      226    14   113    39
5       44    11    94   243
6      223     7   114    48
7       41     1     0   350
8        0     0   392     0
9        5    22   241   124
10      39    48    57   248
11      80    45   157   110
12      57    76   149   110
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_133
CC  AC: HOCOMOCO_2015_11_23_Human_m37_M01037
CC  id: HOCOMOCO_2015_11_23_Human_m37_M01037
CC  name: cluster_133
CC  version: 
CC  name: cluster_133
CC  description: rgsrtrTGktkk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BHE23_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m37_M01037
XX
//
AC  cluster_134
XX
ID  cluster_134
XX
DE  tTGGCacrgwGCCAr
P0       A     C     G     T
1       34    50    25   124
2        8     7    16   202
3       11     2   204    16
4       15     7   199    12
5       26   168    27    12
6       97    58    29    49
7       50    76    49    58
8       86     0   147     0
9       54    51    80    48
10      70    30    25   108
11      18    16   171    28
12      14   189    15    15
13      13   195    13    12
14     182    15    16    20
15     102    21    83    27
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_134
CC  AC: HOCOMOCO_2015_11_23_Human_m363_M01363
CC  id: HOCOMOCO_2015_11_23_Human_m363_M01363
CC  name: cluster_134
CC  version: 
CC  name: cluster_134
CC  description: tTGGCacrgwGCCAr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NFIA_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m363_M01363
XX
//
AC  cluster_135
XX
ID  cluster_135
XX
DE  tyyTGGCwsmswkCCmr
P0       A     C     G     T
1      106   110    77   207
2       21   280    52   147
3        9   193     7   291
4       12     6    14   468
5        3     2   492     3
6        2     6   488     4
7       41   443    12     4
8      261    48    20   171
9       77   139   204    80
10     145   146    99   110
11     113   144   166    77
12     167    51   110   172
13      26    78   241   155
14      43   378    10    69
15      21   443    13    23
16     256   176    17    51
17     223    66   142    69
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_135
CC  AC: HOCOMOCO_2015_11_23_Human_m365_M01365
CC  id: HOCOMOCO_2015_11_23_Human_m365_M01365
CC  name: cluster_135
CC  version: 
CC  name: cluster_135
CC  description: tyyTGGCwsmswkCCmr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NFIC_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m365_M01365
XX
//
AC  cluster_136
XX
ID  cluster_136
XX
DE  gAAAsyrAAw
P0       A     C     G     T
1        9     3    26     2
2       36     4     0     0
3       40     0     0     0
4       36     1     0     3
5        4    14    17     5
6        1    16     6    17
7       10     2    27     1
8       37     0     1     2
9       32     8     0     0
10      27     0     3    10
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_136
CC  AC: HOCOMOCO_2015_11_23_Human_m272_M01272
CC  id: HOCOMOCO_2015_11_23_Human_m272_M01272
CC  name: cluster_136
CC  version: 
CC  name: cluster_136
CC  description: gAAAsyrAAw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: IRF7_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m272_M01272
XX
//
AC  cluster_137
XX
ID  cluster_137
XX
DE  GAAAgcGAAAyT
P0       A     C     G     T
1        1     0     4     0
2        5     0     0     0
3        5     0     0     0
4        5     0     0     0
5        0     1     3     1
6        1     3     0     1
7        0     0     4     1
8        5     0     0     0
9        5     0     0     0
10       5     0     0     0
11       0     3     0     2
12       0     0     1     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_137
CC  AC: HOCOMOCO_2015_11_23_Human_m274_M01274
CC  id: HOCOMOCO_2015_11_23_Human_m274_M01274
CC  name: cluster_137
CC  version: 
CC  name: cluster_137
CC  description: GAAAgcGAAAyT
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: IRF9_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m274_M01274
XX
//
AC  cluster_138
XX
ID  cluster_138
XX
DE  CktyGYGGGGGGTm
P0       A     C     G     T
1      132  1253   202    94
2      325   199   432   725
3      344   395    86   856
4       60  1057    59   505
5       68    18  1587     8
6       42  1177    15   447
7      118     3  1504    56
8        1     0  1637    43
9        1     0  1677     3
10       2     1  1673     5
11       3     1  1676     1
12      10     3  1660     8
13      14   210    90  1367
14     487   874   193   127
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_138
CC  AC: HOCOMOCO_2015_11_23_Human_m193_M01193
CC  id: HOCOMOCO_2015_11_23_Human_m193_M01193
CC  name: cluster_138
CC  version: 
CC  name: cluster_138
CC  description: CktyGYGGGGGGTm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: GLIS2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m193_M01193
XX
//
AC  cluster_139
XX
ID  cluster_139
XX
DE  kCrCaGCGGGGGGtc
P0       A     C     G     T
1      882   424  1654  1353
2      587  2935   434   357
3     1751   320  1564   678
4       16  4240    14    43
5     2869   685   346   413
6      948    33  3329     3
7        3  3821     1   488
8      705     0  3604     4
9        0     0  4298    15
10       0     0  4313     0
11      71    32  3994   216
12      25    30  4026   232
13     302   295  3340   376
14     330   755   549  2679
15     983  2130   687   513
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_139
CC  AC: HOCOMOCO_2015_11_23_Human_m620_M01620
CC  id: HOCOMOCO_2015_11_23_Human_m620_M01620
CC  name: cluster_139
CC  version: 
CC  name: cluster_139
CC  description: kCrCaGCGGGGGGtc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZIC4_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m620_M01620
XX
//
AC  cluster_14
XX
ID  cluster_14
XX
DE  taATTTGCATAwtywww
P0       A     C     G     T
1    962.75 567.75 588.5 1777.5
2    1836.5   918 503.75 638.25
3     3178 149.25   165 404.25
4    162.25 191.75    83 3459.5
5      658  80.5  84.5 3073.5
6    882.75  71.5  8.25  2934
7    310.25    66  3475 45.25
8    301.75  3582   6.5  6.25
9    3813.75 40.75  28.5  13.5
10   14.25    15  9.75 3857.5
11   3629.75 32.25  95.5   139
12   1851.5   244 57.75 1743.25
13   851.25 231.5 514.25 2299.5
14   559.75 1154.5 315.25  1867
15   2009.25 335.25 320.25 1231.75
16    1589 430.75 526.25 1350.5
17   432.25   277 297.75 650.25
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_14
CC  AC: cluster_14
CC  id: cluster_14
CC  name: cluster_14
CC  version: 
CC  name: cluster_14
CC  description: taATTTGCATAwtywww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: taATTTGCATAattaat
CC  consensus.strict.rc: ATTAATTATGCAAATTA
CC  consensus.IUPAC: taATTTGCATAwtywww
CC  consensus.IUPAC.rc: WWWRAWTATGCAAATTA
CC  consensus.regexp: taATTTGCATA[at]t[ct][at][at][at]
CC  consensus.regexp.rc: [AT][AT][AT][AG]A[AT]TATGCAAATTA
CC  merged_ID: P5F1B_D,PO2F2_D,PO3F3_D,PO3F4_D
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m413_M01413,HOCOMOCO_2015_11_23_Human_m445_M01445,HOCOMOCO_2015_11_23_Human_m449_M01449,HOCOMOCO_2015_11_23_Human_m450_M01450
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_140
XX
ID  cluster_140
XX
DE  vCAGGTGkksd
P0       A     C     G     T
1       17    17    20     6
2        5    50     3     2
3       50     3     3     4
4        2     1    53     4
5        0     0    59     1
6        1     0     3    56
7        2     2    55     1
8        7     7    22    24
9       10     8    24    18
10       9    19    18    14
11      19     9    17    15
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_140
CC  AC: HOCOMOCO_2015_11_23_Human_m329_M01329
CC  id: HOCOMOCO_2015_11_23_Human_m329_M01329
CC  name: cluster_140
CC  version: 
CC  name: cluster_140
CC  description: vCAGGTGkksd
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MESP1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m329_M01329
XX
//
AC  cluster_141
XX
ID  cluster_141
XX
DE  wkGATKGATg
P0       A     C     G     T
1        2     1     1     4
2        0     0     3     5
3        1     0     7     0
4        8     0     0     0
5        0     0     0     8
6        0     0     6     2
7        0     0     8     0
8        7     1     0     0
9        0     0     0     8
10       1     1     5     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_141
CC  AC: HOCOMOCO_2015_11_23_Human_m238_M01238
CC  id: HOCOMOCO_2015_11_23_Human_m238_M01238
CC  name: cluster_141
CC  version: 
CC  name: cluster_141
CC  description: wkGATKGATg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXA1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m238_M01238
XX
//
AC  cluster_142
XX
ID  cluster_142
XX
DE  wGATkGATGG
P0       A     C     G     T
1        5     0     1     9
2        0     0    15     0
3       15     0     0     0
4        0     0     0    15
5        0     0     9     6
6        0     0    15     0
7       15     0     0     0
8        0     0     0    15
9        0     3    12     0
10       1     1    13     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_142
CC  AC: HOCOMOCO_2015_11_23_Human_m244_M01244
CC  id: HOCOMOCO_2015_11_23_Human_m244_M01244
CC  name: cluster_142
CC  version: 
CC  name: cluster_142
CC  description: wGATkGATGG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXB1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m244_M01244
XX
//
AC  cluster_143
XX
ID  cluster_143
XX
DE  gGGggGGGggGGGtGGGrg
P0       A     C     G     T
1      701   874  4198  1179
2      490   497  4969   996
3      489   526  5136   801
4      618   681  4460  1193
5      823   813  4023  1293
6      726   602  4730   894
7      503   439  5200   810
8      587   481  4977   907
9     1019   699  4103  1131
10    1473   804  3322  1353
11    1021   289  5221   421
12      40    43  6844    25
13     133   105  6663    51
14    1167  1418   340  4027
15      81    59  6744    68
16      70    72  6107   703
17     168   295  5999   490
18    1758  1021  2686  1487
19    1646   893  3135  1278
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_143
CC  AC: HOCOMOCO_2015_11_23_Human_m552_M01552
CC  id: HOCOMOCO_2015_11_23_Human_m552_M01552
CC  name: cluster_143
CC  version: 
CC  name: cluster_143
CC  description: gGGggGGGggGGGtGGGrg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TBX15_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m552_M01552
XX
//
AC  cluster_144
XX
ID  cluster_144
XX
DE  gggrkGyGkGGGhGGgrg
P0       A     C     G     T
1       95    96   219    90
2      104   103   223    70
3      106    72   226    96
4      135    70   234    61
5      111    37   190   162
6       58    41   378    23
7       84   242    25   149
8       21    21   404    54
9       17     4   226   253
10      80     3   402    15
11       4     0   488     8
12      16    19   457     8
13     131   226     3   140
14      90     6   375    29
15      36     6   387    71
16     118    46   267    69
17     139    49   229    83
18     109    92   245    54
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_144
CC  AC: HOCOMOCO_2015_11_23_Human_m95_M01095
CC  id: HOCOMOCO_2015_11_23_Human_m95_M01095
CC  name: cluster_144
CC  version: 
CC  name: cluster_144
CC  description: gggrkGyGkGGGhGGgrg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: EGR1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m95_M01095
XX
//
AC  cluster_145
XX
ID  cluster_145
XX
DE  twayTAATTmrh
P0       A     C     G     T
1      197   107   100   530
2      401   118   168   247
3      481   177   125   151
4      198   415    80   241
5        0     9     0   925
6      790    20     3   121
7      870     5     0    59
8        0    36     1   897
9       60    37   167   670
10     404   411    62    57
11     358    80   414    82
12     239   364    82   249
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_145
CC  AC: HOCOMOCO_2015_11_23_Human_m422_M01422
CC  id: HOCOMOCO_2015_11_23_Human_m422_M01422
CC  name: cluster_145
CC  version: 
CC  name: cluster_145
CC  description: twayTAATTmrh
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PAX4_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m422_M01422
XX
//
AC  cluster_146
XX
ID  cluster_146
XX
DE  grTwTTATtGg
P0       A     C     G     T
1        3     3     9     2
2        6     1     6     4
3        0     0     3    14
4        5     0     2    10
5        1     0     0    16
6        4     0     0    13
7       17     0     0     0
8        0     3     0    14
9        2     1     4    10
10       0     1    13     3
11       2     3    11     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_146
CC  AC: HOCOMOCO_2015_11_23_Human_m237_M01237
CC  id: HOCOMOCO_2015_11_23_Human_m237_M01237
CC  name: cluster_146
CC  version: 
CC  name: cluster_146
CC  description: grTwTTATtGg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXA13_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m237_M01237
XX
//
AC  cluster_147
XX
ID  cluster_147
XX
DE  ywTTGTTww
P0       A     C     G     T
1       11    31    15    51
2       40     4     5    59
3        8     1     4    95
4       10     1     3    94
5        0     8    98     2
6        6     0     4    98
7       21     4     4    79
8       28    10     8    62
9       37    26     3    42
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_147
CC  AC: HOCOMOCO_2015_11_23_Human_m537_M01537
CC  id: HOCOMOCO_2015_11_23_Human_m537_M01537
CC  name: cluster_147
CC  version: 
CC  name: cluster_147
CC  description: ywTTGTTww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SRY_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m537_M01537
XX
//
AC  cluster_148
XX
ID  cluster_148
XX
DE  crrCAGrTGkcc
P0       A     C     G     T
1        5    11     5     5
2        7     1    17     1
3        7     3    15     1
4        0    26     0     0
5       24     0     0     2
6        1     4    21     0
7       11     5    10     0
8        0     1     0    25
9        0     0    26     0
10       3     2    13     8
11       3    12     6     5
12       6    14     5     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_148
CC  AC: HOCOMOCO_2015_11_23_Human_m352_M01352
CC  id: HOCOMOCO_2015_11_23_Human_m352_M01352
CC  name: cluster_148
CC  version: 
CC  name: cluster_148
CC  description: crrCAGrTGkcc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NDF1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m352_M01352
XX
//
AC  cluster_149
XX
ID  cluster_149
XX
DE  wGcTGAcks
P0       A     C     G     T
1       15     2     2    28
2        1     0    45     1
3       11    31     4     1
4        1     0     0    46
5        0     4    39     4
6       41     3     0     3
7        3    31     7     6
8       11     5    16    15
9        7    13    19     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_149
CC  AC: HOCOMOCO_2015_11_23_Human_m308_M01308
CC  id: HOCOMOCO_2015_11_23_Human_m308_M01308
CC  name: cluster_149
CC  version: 
CC  name: cluster_149
CC  description: wGcTGAcks
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MAFB_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m308_M01308
XX
//
AC  cluster_15
XX
ID  cluster_15
XX
DE  mATGACt
P0       A     C     G     T
1       27    29  18.5    15
2     68.5     5    15     1
3      1.5   0.5     1  86.5
4      4.5     0  83.5   1.5
5       89   0.5     0     0
6      1.5  71.5   2.5    14
7       19     7  21.5    42
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_15
CC  AC: cluster_15
CC  id: cluster_15
CC  name: cluster_15
CC  version: 
CC  name: cluster_15
CC  description: mATGACt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cATGACt
CC  consensus.strict.rc: AGTCATG
CC  consensus.IUPAC: mATGACt
CC  consensus.IUPAC.rc: AGTCATK
CC  consensus.regexp: [ac]ATGACt
CC  consensus.regexp.rc: AGTCAT[GT]
CC  merged_ID: MAFG_S,NF2L1_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m311_M01311,HOCOMOCO_2015_11_23_Human_m354_M01354
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_150
XX
ID  cluster_150
XX
DE  tGCTGAckyar
P0       A     C     G     T
1       81   130    52   542
2       18    13   766     8
3       64   706     6    29
4       24    30     3   748
5       16     4   689    96
6      791     2     3     9
7       14   535   110   146
8      126   105   344   230
9      131   299   100   275
10     347   136   140   182
11     214    96   333   162
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_150
CC  AC: HOCOMOCO_2015_11_23_Human_m403_M01403
CC  id: HOCOMOCO_2015_11_23_Human_m403_M01403
CC  name: cluster_150
CC  version: 
CC  name: cluster_150
CC  description: tGCTGAckyar
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NRL_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m403_M01403
XX
//
AC  cluster_151
XX
ID  cluster_151
XX
DE  tkGGATTAmw
P0       A     C     G     T
1        1     4     3     9
2        2     1     8     6
3        2     0    14     1
4        0     0    17     0
5       14     3     0     0
6        0     0     0    17
7        0     0     1    16
8       12     1     0     4
9        7     5     4     1
10       6     3     3     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_151
CC  AC: HOCOMOCO_2015_11_23_Human_m437_M01437
CC  id: HOCOMOCO_2015_11_23_Human_m437_M01437
CC  name: cluster_151
CC  version: 
CC  name: cluster_151
CC  description: tkGGATTAmw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PITX2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m437_M01437
XX
//
AC  cluster_152
XX
ID  cluster_152
XX
DE  rsGGATTAdcs
P0       A     C     G     T
1    14959 11482 13572  6548
2     7104 21006 13734  4717
3     3119   809 42027   606
4      290   240 45628   403
5    46494    19    31    17
6        5     7     3 46546
7        6    58    75 46422
8    46142   130   148   141
9    13924   665 13320 18652
10    2258 29757  9024  5522
11    7008 17200 12237 10116
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_152
CC  AC: HOCOMOCO_2015_11_23_Human_m83_M01083
CC  id: HOCOMOCO_2015_11_23_Human_m83_M01083
CC  name: cluster_152
CC  version: 
CC  name: cluster_152
CC  description: rsGGATTAdcs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: DPRX_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m83_M01083
XX
//
AC  cluster_153
XX
ID  cluster_153
XX
DE  ggTgrssTGAk
P0       A     C     G     T
1      428   462   989   291
2      502   462  1168    38
3       46   121    54  1949
4      143   539  1374   114
5      606     7  1440   117
6      146   591  1243   190
7       84   682  1398     6
8      213   177   108  1672
9        7    52  2096    15
10    1720    61   113   276
11      74   505   933   658
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_153
CC  AC: HOCOMOCO_2015_11_23_Human_m534_M01534
CC  id: HOCOMOCO_2015_11_23_Human_m534_M01534
CC  name: cluster_153
CC  version: 
CC  name: cluster_153
CC  description: ggTgrssTGAk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SRBP1_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m534_M01534
XX
//
AC  cluster_154
XX
ID  cluster_154
XX
DE  rsgTGGggwGAkg
P0       A     C     G     T
1      610   578   878   276
2      406   707   965   264
3      494   387  1332   129
4      350   114   144  1734
5       50    78  2129    85
6      122    13  2202     5
7      411   517  1149   265
8      260   538  1515    29
9      678   351    26  1287
10      59   184  2042    57
11    1638    47   526   131
12     369   440   865   668
13     371   491  1055   425
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_154
CC  AC: HOCOMOCO_2015_11_23_Human_m535_M01535
CC  id: HOCOMOCO_2015_11_23_Human_m535_M01535
CC  name: cluster_154
CC  version: 
CC  name: cluster_154
CC  description: rsgTGGggwGAkg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SRBP2_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m535_M01535
XX
//
AC  cluster_155
XX
ID  cluster_155
XX
DE  yraCAGCTGcwGc
P0       A     C     G     T
1        3     7     3    14
2        8     4    14     1
3       15     4     5     3
4        1    25     1     0
5       27     0     0     0
6        1     3    23     0
7        0    26     1     0
8        1     0     0    26
9        0     0    27     0
10       2    15     6     4
11       9     1     2    15
12       1     4    21     1
13       4    15     5     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_155
CC  AC: HOCOMOCO_2015_11_23_Human_m348_M01348
CC  id: HOCOMOCO_2015_11_23_Human_m348_M01348
CC  name: cluster_155
CC  version: 
CC  name: cluster_155
CC  description: yraCAGCTGcwGc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MYOG_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m348_M01348
XX
//
AC  cluster_156
XX
ID  cluster_156
XX
DE  ryCAGCTGygG
P0       A     C     G     T
1        4     2     7     1
2        3     7     0     4
3        0    14     0     0
4       14     0     0     0
5        0     0    14     0
6        0    14     0     0
7        0     0     1    13
8        0     0    14     0
9        0     7     1     6
10       3     2     7     2
11       0     2    12     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_156
CC  AC: HOCOMOCO_2015_11_23_Human_m570_M01570
CC  id: HOCOMOCO_2015_11_23_Human_m570_M01570
CC  name: cluster_156
CC  version: 
CC  name: cluster_156
CC  description: ryCAGCTGygG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TFAP4_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m570_M01570
XX
//
AC  cluster_157
XX
ID  cluster_157
XX
DE  gyTAAwyGgysywwwwwg
P0       A     C     G     T
1     9476  9834 17729  5786
2     7154 16490  5197 13984
3      386   266   260 41913
4    42328    28    63   406
5    42751    33    28    13
6    20392    77  1323 21033
7      709 19177   723 22216
8      903    72 41600   250
9     8745 10606 14842  8632
10    2423 11796  2673 25933
11    6192 16422 12607  7604
12    9366 16247  5721 11491
13   11501  5239  4330 21755
14   22378   892  2134 17421
15   21000   791   588 20446
16   17983  1380  2127 21335
17   14410  7798  5054 15563
18    9772  6668 18649  7736
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_157
CC  AC: HOCOMOCO_2015_11_23_Human_m26_M01026
CC  id: HOCOMOCO_2015_11_23_Human_m26_M01026
CC  name: cluster_157
CC  version: 
CC  name: cluster_157
CC  description: gyTAAwyGgysywwwwwg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BARH2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m26_M01026
XX
//
AC  cluster_158
XX
ID  cluster_158
XX
DE  ysyTAATyrsytyyhwwtt
P0       A     C     G     T
1     1475  4050  1808  2610
2     2366  2653  3941   983
3      650  5321   454  3518
4       87   255    72  9529
5     9452   339    20   132
6     9608   285     9    41
7       18   196    26  9703
8       71  3826  1477  4569
9     2882   391  6206   464
10    1059  3408  3717  1759
11    1010  2593   456  5884
12    1584  2449   932  4978
13    2303  2851  1029  3760
14    2035  3698   825  3385
15    2636  3211   529  3567
16    4623  1500  1128  2692
17    5446   599   436  3462
18    2076   749   715  6403
19    2384  1314  1455  4790
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_158
CC  AC: HOCOMOCO_2015_11_23_Human_m598_M01598
CC  id: HOCOMOCO_2015_11_23_Human_m598_M01598
CC  name: cluster_158
CC  version: 
CC  name: cluster_158
CC  description: ysyTAATyrsytyyhwwtt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: VENTX_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m598_M01598
XX
//
AC  cluster_159
XX
ID  cluster_159
XX
DE  GmCACGTGGCA
P0       A     C     G     T
1     2216  2359 21260  5147
2     8228 18002   106  4646
3      872 24424  4940   746
4    30982     0     0     0
5        0 30982     0     0
6        0     0 30982     0
7        0     0     0 30982
8        0     0 30982     0
9        0     0 30982     0
10       0 30982     0     0
11   30439   181   181   181
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_159
CC  AC: HOCOMOCO_2015_11_23_Human_m62_M01062
CC  id: HOCOMOCO_2015_11_23_Human_m62_M01062
CC  name: cluster_159
CC  version: 
CC  name: cluster_159
CC  description: GmCACGTGGCA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: CR3L1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m62_M01062
XX
//
AC  cluster_16
XX
ID  cluster_16
XX
DE  CaAAGGTCAs
P0       A     C     G     T
1    0.333333 1.33333     0     0
2    11.6667 2.66667 4.66667 0.666667
3       18     0     1 0.666667
4    18.6667 0.333333 0.333333 0.333333
5        0     0    19 0.666667
6    0.666667 0.333333 18.3333 0.333333
7    0.666667 2.66667     0 16.3333
8        0    18 0.333333 1.33333
9    19.3333     0     0 0.333333
10   3.33333 8.33333 6.33333 1.66667
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_16
CC  AC: cluster_16
CC  id: cluster_16
CC  name: cluster_16
CC  version: 
CC  name: cluster_16
CC  description: CaAAGGTCAs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: caAAGGTCAc
CC  consensus.strict.rc: GTGACCTTTG
CC  consensus.IUPAC: caAAGGTCAs
CC  consensus.IUPAC.rc: STGACCTTTG
CC  consensus.regexp: caAAGGTCA[cg]
CC  consensus.regexp.rc: [CG]TGACCTTTG
CC  merged_ID: NR4A3_D,NR4A1_C,NR4A2_C
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m399_M01399,HOCOMOCO_2015_11_23_Human_m397_M01397,HOCOMOCO_2015_11_23_Human_m398_M01398
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_160
XX
ID  cluster_160
XX
DE  ggAAAsyGAAAsbrrra
P0       A     C     G     T
1        6     6    29     3
2       10     2    30     2
3       39     1     4     0
4       43     0     0     1
5       40     0     2     2
6        7    11    25     1
7        6    16     9    13
8        4     1    38     1
9       43     0     1     0
10      43     0     0     1
11      42     2     0     0
12       9    21    11     3
13       1    15    12    16
14      13     3    20     8
15      19     7    15     3
16      22     4    13     5
17      22     7     7     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_160
CC  AC: HOCOMOCO_2015_11_23_Human_m269_M01269
CC  id: HOCOMOCO_2015_11_23_Human_m269_M01269
CC  name: cluster_160
CC  version: 
CC  name: cluster_160
CC  description: ggAAAsyGAAAsbrrra
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: IRF3_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m269_M01269
XX
//
AC  cluster_161
XX
ID  cluster_161
XX
DE  grTTryGTAAyms
P0       A     C     G     T
1        7     6    15     7
2       19     3    10     3
3        0     0     0    35
4        1     2     6    26
5       19     2    14     0
6        0    16     3    16
7        2     0    33     0
8        0     6     0    29
9       35     0     0     0
10      34     0     0     1
11       1    20     0    14
12      14    14     4     3
13       5    13    13     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_161
CC  AC: HOCOMOCO_2015_11_23_Human_m214_M01214
CC  id: HOCOMOCO_2015_11_23_Human_m214_M01214
CC  name: cluster_161
CC  version: 
CC  name: cluster_161
CC  description: grTTryGTAAyms
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HLF_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m214_M01214
XX
//
AC  cluster_162
XX
ID  cluster_162
XX
DE  rTTaTGyAAya
P0       A     C     G     T
1        8     6    10     1
2        2     0     1    22
3        2     2     2    19
4       16     2     4     3
5        1     3     2    19
6        2     1    21     1
7        0    11     0    14
8       23     1     0     1
9       24     0     0     1
10       3     8     6     8
11      10     6     5     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_162
CC  AC: HOCOMOCO_2015_11_23_Human_m74_M01074
CC  id: HOCOMOCO_2015_11_23_Human_m74_M01074
CC  name: cluster_162
CC  version: 
CC  name: cluster_162
CC  description: rTTaTGyAAya
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: DBP_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m74_M01074
XX
//
AC  cluster_163
XX
ID  cluster_163
XX
DE  GGGmGGGGssggggggGgGgGg
P0       A     C     G     T
1       31    30   430     9
2        6    14   475     5
3        4    11   484     1
4      209   261     4    26
5        3     9   487     1
6        6    10   481     3
7       66    35   380    19
8       25    23   448     4
9       55   194   230    21
10      66   216   187    31
11      73    58   324    45
12      89   101   298    12
13      91    62   323    24
14      70    81   335    14
15      70    72   333    25
16      94   108   270    28
17      62    68   351    19
18      84    98   306    12
19      39    88   358    15
20      76   112   292    20
21      37    77   366    20
22      30   117   315    38
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_163
CC  AC: HOCOMOCO_2015_11_23_Human_m316_M01316
CC  id: HOCOMOCO_2015_11_23_Human_m316_M01316
CC  name: cluster_163
CC  version: 
CC  name: cluster_163
CC  description: GGGmGGGGssggggggGgGgGg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MAZ_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m316_M01316
XX
//
AC  cluster_164
XX
ID  cluster_164
XX
DE  gGGGGCGGGGCcgGgsssgggs
P0       A     C     G     T
1       89    58   336    12
2       52    25   365    53
3       61    41   389     4
4       12    25   448    10
5       12     8   472     3
6       32   436     8    19
7        4    18   461    12
8       20    31   403    41
9       64    23   379    29
10      34    33   414    14
11      43   363    66    23
12      39   326    83    47
13     100    71   205   119
14      26    87   355    27
15      60   112   302    21
16      75   124   259    37
17      41   169   243    42
18      40   155   263    37
19      82   104   275    34
20      85   106   256    48
21      45   100   321    29
22      58   162   235    40
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_164
CC  AC: HOCOMOCO_2015_11_23_Human_m528_M01528
CC  id: HOCOMOCO_2015_11_23_Human_m528_M01528
CC  name: cluster_164
CC  version: 
CC  name: cluster_164
CC  description: gGGGGCGGGGCcgGgsssgggs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SP4_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m528_M01528
XX
//
AC  cluster_165
XX
ID  cluster_165
XX
DE  vmChGGAarTsc
P0       A     C     G     T
1        2     3     2     0
2        3     2     1     1
3        1     6     0     0
4        3     2     0     2
5        0     0     7     0
6        0     0     7     0
7        7     0     0     0
8        5     1     0     1
9        3     0     4     0
10       0     1     0     6
11       0     3     4     0
12       1     5     1     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_165
CC  AC: HOCOMOCO_2015_11_23_Human_m107_M01107
CC  id: HOCOMOCO_2015_11_23_Human_m107_M01107
CC  name: cluster_165
CC  version: 
CC  name: cluster_165
CC  description: vmChGGAarTsc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ELK3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m107_M01107
XX
//
AC  cluster_166
XX
ID  cluster_166
XX
DE  kayrrTTTATGw
P0       A     C     G     T
1        2     1     4     3
2        5     2     2     1
3        0     3     1     6
4        4     1     4     1
5        5     1     3     1
6        1     0     0     9
7        0     0     0    10
8        1     0     0     9
9       10     0     0     0
10       0     0     0    10
11       1     0     8     1
12       4     2     1     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_166
CC  AC: HOCOMOCO_2015_11_23_Human_m235_M01235
CC  id: HOCOMOCO_2015_11_23_Human_m235_M01235
CC  name: cluster_166
CC  version: 
CC  name: cluster_166
CC  description: kayrrTTTATGw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXA10_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m235_M01235
XX
//
AC  cluster_167
XX
ID  cluster_167
XX
DE  rwTGATTkATGmm
P0       A     C     G     T
1        3     1     2     1
2        3     1     0     3
3        0     1     0     6
4        1     0     6     0
5        7     0     0     0
6        0     0     0     7
7        0     0     0     7
8        1     0     3     3
9        6     0     1     0
10       0     0     0     7
11       0     0     6     1
12       3     4     0     0
13       3     2     1     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_167
CC  AC: HOCOMOCO_2015_11_23_Human_m247_M01247
CC  id: HOCOMOCO_2015_11_23_Human_m247_M01247
CC  name: cluster_167
CC  version: 
CC  name: cluster_167
CC  description: rwTGATTkATGmm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXB6_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m247_M01247
XX
//
AC  cluster_168
XX
ID  cluster_168
XX
DE  rsAwwrTgACACCTAGGTGTsAAAw
P0       A     C     G     T
1       12     3     9     2
2        4    13     8     1
3       21     1     1     3
4        9     1     1    15
5       15     0     0    11
6        8     0    17     1
7        0     0     0    26
8        3     5    18     0
9       26     0     0     0
10       0    26     0     0
11      26     0     0     0
12       0    25     0     1
13       0    24     1     1
14       0     0     0    26
15      26     0     0     0
16       0     2    24     0
17       0     1    25     0
18       0     1     0    25
19       0     0    26     0
20       1     1     0    24
21       0     7    17     2
22      25     0     0     1
23      22     2     1     1
24      22     0     3     1
25       7     3     3    13
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_168
CC  AC: HOCOMOCO_2015_11_23_Human_m42_M01042
CC  id: HOCOMOCO_2015_11_23_Human_m42_M01042
CC  name: cluster_168
CC  version: 
CC  name: cluster_168
CC  description: rsAwwrTgACACCTAGGTGTsAAAw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BRAC_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m42_M01042
XX
//
AC  cluster_169
XX
ID  cluster_169
XX
DE  wttTmrCAcrTAGGTGTGAaww
P0       A     C     G     T
1     6995  2238  2448  4904
2     3969  2409  3127  7080
3     2643  1971  3168  8803
4      458   696   416 15015
5     4363  9329  2266   627
6     9951   275  5451   908
7      177 16070   145   193
8    14942   136  1369   138
9     1355 10894   558  3778
10    5041  3504  5568  2472
11    1553   760   767 13505
12   14303   122  1057  1103
13    1161  1020 12513  1891
14     221    50 16215    99
15      73   146    80 16286
16      86    79 16339    81
17     449  3125   145 12866
18     307  1228 12434  2616
19   15610   284   402   289
20    9894  2606  1539  2546
21    7514  2441  2258  4372
22    4923  1984  2126  7552
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_169
CC  AC: HOCOMOCO_2015_11_23_Human_m553_M01553
CC  id: HOCOMOCO_2015_11_23_Human_m553_M01553
CC  name: cluster_169
CC  version: 
CC  name: cluster_169
CC  description: wttTmrCAcrTAGGTGTGAaww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TBX19_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m553_M01553
XX
//
AC  cluster_17
XX
ID  cluster_17
XX
DE  adwkGGCGsGAAar
P0       A     C     G     T
1    376.75 180.75 235.75   261
2      346   126 301.75 281.75
3    312.75 136.5 221.25   385
4      222  75.5 288.25 469.75
5    76.75 133.75 834.75 10.25
6    24.75 35.75   984    11
7    57.25 965.75 10.75 21.75
8     34.5  10.5 950.75 59.75
9    29.75 374.75 640.5  10.5
10    49.5   116 875.5  14.5
11   894.25 50.75 95.25 15.25
12   816.5 26.25 189.25  23.5
13   652.5    81   235    87
14   331.5 221.5 305.25 197.25
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_17
CC  AC: cluster_17
CC  id: cluster_17
CC  name: cluster_17
CC  version: 
CC  name: cluster_17
CC  description: adwkGGCGsGAAar
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aattGGCGgGAAaa
CC  consensus.strict.rc: TTTTCCCGCCAATT
CC  consensus.IUPAC: adwkGGCGsGAAar
CC  consensus.IUPAC.rc: YTTTCSCGCCMWHT
CC  consensus.regexp: a[agt][at][gt]GGCG[cg]GAAa[ag]
CC  consensus.regexp.rc: [CT]TTTC[CG]CGCC[AC][AT][ACT]T
CC  merged_ID: E2F7_D,TFDP1_S,E2F1_A,E2F4_A
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m92_M01092,HOCOMOCO_2015_11_23_Human_m573_M01573,HOCOMOCO_2015_11_23_Human_m86_M01086,HOCOMOCO_2015_11_23_Human_m89_M01089
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_170
XX
ID  cluster_170
XX
DE  daWsAATGkTATTsWt
P0       A     C     G     T
1       12     4    14    10
2       26     5     8     1
3       11     1     0    28
4        1    13    25     1
5       40     0     0     0
6       38     0     1     1
7        0     0     1    39
8        0     1    36     3
9        0     1    12    27
10       2     5     1    32
11      40     0     0     0
12       0     0     6    34
13       0     0     1    39
14       0    25    15     0
15      30     0     0    10
16       2     4     8    26
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_170
CC  AC: HOCOMOCO_2015_11_23_Human_m515_M01515
CC  id: HOCOMOCO_2015_11_23_Human_m515_M01515
CC  name: cluster_170
CC  version: 
CC  name: cluster_170
CC  description: daWsAATGkTATTsWt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SOX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m515_M01515
XX
//
AC  cluster_171
XX
ID  cluster_171
XX
DE  wsAATGkwATTsw
P0       A     C     G     T
1      968   187   187  1406
2      282  1293  1013   160
3     2645    15    66    22
4     2514   151    49    34
5        4     1    27  2716
6       22     0  2724     2
7      475   538   722  1013
8      926   378    65  1379
9     2718     7    17     6
10      43    15   246  2444
11      26    12    79  2631
12     170   908  1387   283
13    1402   162   213   971
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_171
CC  AC: HOCOMOCO_2015_11_23_Human_m518_M01518
CC  id: HOCOMOCO_2015_11_23_Human_m518_M01518
CC  name: cluster_171
CC  version: 
CC  name: cluster_171
CC  description: wsAATGkwATTsw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SOX3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m518_M01518
XX
//
AC  cluster_172
XX
ID  cluster_172
XX
DE  kwyAAGTrsTTd
P0       A     C     G     T
1        2     3     8    11
2        6     0     2    16
3        0     8     3    13
4       24     0     0     0
5       24     0     0     0
6        0     0    24     0
7        1     0     0    23
8        9     1    14     0
9        0     8    11     5
10       3     1     1    19
11       2     0     2    20
12       7     3     6     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_172
CC  AC: HOCOMOCO_2015_11_23_Human_m374_M01374
CC  id: HOCOMOCO_2015_11_23_Human_m374_M01374
CC  name: cluster_172
CC  version: 
CC  name: cluster_172
CC  description: kwyAAGTrsTTd
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NKX22_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m374_M01374
XX
//
AC  cluster_173
XX
ID  cluster_173
XX
DE  yTGTGGTtdgk
P0       A     C     G     T
1       11    25    13    24
2        7     3     3    60
3        0     5    67     1
4        5     9     1    58
5        0     0    73     0
6        0     0    72     1
7        1     6     8    58
8        5     9    12    47
9       21     8    19    25
10      12    15    31    15
11       6     9    24    34
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_173
CC  AC: HOCOMOCO_2015_11_23_Human_m492_M01492
CC  id: HOCOMOCO_2015_11_23_Human_m492_M01492
CC  name: cluster_173
CC  version: 
CC  name: cluster_173
CC  description: yTGTGGTtdgk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: RUNX2_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m492_M01492
XX
//
AC  cluster_174
XX
ID  cluster_174
XX
DE  grrCwTGTCys
P0       A     C     G     T
1        4     1    13     4
2       10     1    10     1
3       11     0    10     1
4        0    22     0     0
5       11     1     2     8
6        3     0     0    19
7        1     0    21     0
8        0     2     3    17
9        0    18     1     3
10       1     8     0    13
11       3     8     8     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_174
CC  AC: HOCOMOCO_2015_11_23_Human_m415_M01415
CC  id: HOCOMOCO_2015_11_23_Human_m415_M01415
CC  name: cluster_174
CC  version: 
CC  name: cluster_174
CC  description: grrCwTGTCys
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: P63_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m415_M01415
XX
//
AC  cluster_175
XX
ID  cluster_175
XX
DE  kgGrCAdGyyyk
P0       A     C     G     T
1        2     2     7     4
2        2     1    10     2
3        1     0    11     3
4        6     0     9     0
5        1    14     0     0
6       15     0     0     0
7        4     1     5     5
8        0     1    14     0
9        0     7     0     8
10       0     8     1     6
11       1     5     3     6
12       2     3     6     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_175
CC  AC: HOCOMOCO_2015_11_23_Human_m417_M01417
CC  id: HOCOMOCO_2015_11_23_Human_m417_M01417
CC  name: cluster_175
CC  version: 
CC  name: cluster_175
CC  description: kgGrCAdGyyyk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: P73_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m417_M01417
XX
//
AC  cluster_176
XX
ID  cluster_176
XX
DE  TGATTArrTya
P0       A     C     G     T
1       68    54    49  2514
2       52     5  2620     8
3     2667     7     8     3
4        4     1     2  2678
5        3    13    15  2654
6     2315    14   355     1
7     1435   268   962    20
8     1407   281   835   162
9       35     5    43  2602
10     166   803   154  1562
11    1770   198   366   351
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_176
CC  AC: HOCOMOCO_2015_11_23_Human_m85_M01085
CC  id: HOCOMOCO_2015_11_23_Human_m85_M01085
CC  name: cluster_176
CC  version: 
CC  name: cluster_176
CC  description: TGATTArrTya
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: DUXA_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m85_M01085
XX
//
AC  cluster_177
XX
ID  cluster_177
XX
DE  stcaAGkGcy
P0       A     C     G     T
1        9    40    23    13
2       10    20     3    52
3        0    53    19    13
4       58     5    20     2
5       85     0     0     0
6        1     0    84     0
7       12     3    32    38
8       14     0    59    12
9        5    45    16    19
10      17    24    11    33
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_177
CC  AC: HOCOMOCO_2015_11_23_Human_m373_M01373
CC  id: HOCOMOCO_2015_11_23_Human_m373_M01373
CC  name: cluster_177
CC  version: 
CC  name: cluster_177
CC  description: stcaAGkGcy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NKX21_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m373_M01373
XX
//
AC  cluster_178
XX
ID  cluster_178
XX
DE  tTCAAGkrc
P0       A     C     G     T
1        1     1     1     3
2        0     0     0     6
3        0     5     1     0
4        6     0     0     0
5        6     0     0     0
6        0     0     6     0
7        0     0     4     2
8        4     0     2     0
9        1     3     1     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_178
CC  AC: HOCOMOCO_2015_11_23_Human_m377_M01377
CC  id: HOCOMOCO_2015_11_23_Human_m377_M01377
CC  name: cluster_178
CC  version: 
CC  name: cluster_178
CC  description: tTCAAGkrc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NKX28_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m377_M01377
XX
//
AC  cluster_179
XX
ID  cluster_179
XX
DE  rrmtTGAytGAtk
P0       A     C     G     T
1        4     2     3     0
2        3     1     4     1
3        3     3     2     1
4        0     2     2     5
5        0     0     0     9
6        0     0     9     0
7        9     0     0     0
8        0     4     0     5
9        2     1     1     5
10       0     0     8     1
11       7     2     0     0
12       1     2     1     5
13       2     0     4     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_179
CC  AC: HOCOMOCO_2015_11_23_Human_m439_M01439
CC  id: HOCOMOCO_2015_11_23_Human_m439_M01439
CC  name: cluster_179
CC  version: 
CC  name: cluster_179
CC  description: rrmtTGAytGAtk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PKNX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m439_M01439
XX
//
AC  cluster_18
XX
ID  cluster_18
XX
DE  RGAACAk
P0       A     C     G     T
1    1454.5   4.5 629.5  43.5
2       61     7 2044.5  19.5
3    1520.5    80 396.5   135
4     1990    13  88.5  40.5
5     62.5  1802   263   4.5
6    1982.5    23  99.5    27
7      349   170  1048   565
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_18
CC  AC: cluster_18
CC  id: cluster_18
CC  name: cluster_18
CC  version: 
CC  name: cluster_18
CC  description: RGAACAk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: AGAACAg
CC  consensus.strict.rc: CTGTTCT
CC  consensus.IUPAC: RGAACAk
CC  consensus.IUPAC.rc: MTGTTCY
CC  consensus.regexp: [AG]GAACA[gt]
CC  consensus.regexp.rc: [AC]TGTTC[CT]
CC  merged_ID: GCR_S,PRGR_S
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m186_M01186,HOCOMOCO_2015_11_23_Human_m466_M01466
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_180
XX
ID  cluster_180
XX
DE  tGCTGAGTCA
P0       A     C     G     T
1        2     3     0    11
2        0     0    16     0
3        0    16     0     0
4        0     3     0    13
5        1     0    14     1
6       15     0     0     1
7        0     3    13     0
8        0     0     0    16
9        0    16     0     0
10      16     0     0     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_180
CC  AC: HOCOMOCO_2015_11_23_Human_m313_M01313
CC  id: HOCOMOCO_2015_11_23_Human_m313_M01313
CC  name: cluster_180
CC  version: 
CC  name: cluster_180
CC  description: tGCTGAGTCA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MAFK_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m313_M01313
XX
//
AC  cluster_181
XX
ID  cluster_181
XX
DE  ssstACGTGcss
P0       A     C     G     T
1       49   151   253    46
2       55   178   173    93
3       68   182   232    17
4       38    99   107   255
5      423     1    73     2
6        0   498     0     1
7        0     1   497     1
8        0     0     0   499
9        0     0   499     0
10     104   295    52    48
11      68   132   232    67
12      31   154   195   119
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_181
CC  AC: HOCOMOCO_2015_11_23_Human_m212_M01212
CC  id: HOCOMOCO_2015_11_23_Human_m212_M01212
CC  name: cluster_181
CC  version: 
CC  name: cluster_181
CC  description: ssstACGTGcss
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HIF1A_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m212_M01212
XX
//
AC  cluster_182
XX
ID  cluster_182
XX
DE  drGkGGGCGtg
P0       A     C     G     T
1     2701   641  2396  1933
2     2954   129  3309  1279
3      133     4  7531     3
4       11     0  4170  3490
5        0     0  7671     0
6        0     0  7671     0
7        0     0  7671     0
8        0  7582     0    89
9        0     0  7671     0
10    1433  1432  1670  3136
11    1559  1497  3045  1570
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_182
CC  AC: HOCOMOCO_2015_11_23_Human_m288_M01288
CC  id: HOCOMOCO_2015_11_23_Human_m288_M01288
CC  name: cluster_182
CC  version: 
CC  name: cluster_182
CC  description: drGkGGGCGtg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: KLF13_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m288_M01288
XX
//
AC  cluster_183
XX
ID  cluster_183
XX
DE  kkGGCGGGArrg
P0       A     C     G     T
1      412   314   553   585
2      376   120   858   510
3      275   220  1341    28
4       47    53  1752    12
5      216  1599     4    45
6      108    54  1657    45
7       16   121  1712    15
8       19    19  1807    19
9     1506     6   348     4
10    1086    97   669    12
11     484   318   968    94
12     347   415   901   201
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_183
CC  AC: HOCOMOCO_2015_11_23_Human_m91_M01091
CC  id: HOCOMOCO_2015_11_23_Human_m91_M01091
CC  name: cluster_183
CC  version: 
CC  name: cluster_183
CC  description: kkGGCGGGArrg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: E2F6_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m91_M01091
XX
//
AC  cluster_184
XX
ID  cluster_184
XX
DE  mwAcMmGGAArTr
P0       A     C     G     T
1      160   109    64    67
2      213    37    17   133
3      308     0    85     7
4       71   232    93     4
5      110   290     0     0
6      169   231     0     0
7        0     0   400     0
8        0     0   376    24
9      400     0     0     0
10     400     0     0     0
11     131     3   261     5
12       3     7    61   329
13     235    23   106    36
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_184
CC  AC: HOCOMOCO_2015_11_23_Human_m101_M01101
CC  id: HOCOMOCO_2015_11_23_Human_m101_M01101
CC  name: cluster_184
CC  version: 
CC  name: cluster_184
CC  description: mwAcMmGGAArTr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: EHF_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m101_M01101
XX
//
AC  cluster_185
XX
ID  cluster_185
XX
DE  ktTAATTGhTttTTAAt
P0       A     C     G     T
1        7     5    11    11
2        4     6     3    21
3        0     6     0    28
4       34     0     0     0
5       34     0     0     0
6        0     0     5    29
7        0     0     0    34
8        5     0    29     0
9       10    11     2    11
10       2     4     2    26
11       7     7     2    18
12       5     6     6    17
13       4     3     1    26
14       3     1     0    30
15      24     1     4     5
16      27     1     1     5
17       2     1     8    23
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_185
CC  AC: HOCOMOCO_2015_11_23_Human_m221_M01221
CC  id: HOCOMOCO_2015_11_23_Human_m221_M01221
CC  name: cluster_185
CC  version: 
CC  name: cluster_185
CC  description: ktTAATTGhTttTTAAt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HMX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m221_M01221
XX
//
AC  cluster_186
XX
ID  cluster_186
XX
DE  GyGGGGGGTm
P0       A     C     G     T
1        2     7    28     1
2        1    15     4    18
3        4     0    32     2
4        0     0    30     8
5        1     1    36     0
6        2     0    29     7
7        0     0    38     0
8        2     1    32     3
9        1     9     0    28
10      16    16     4     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_186
CC  AC: HOCOMOCO_2015_11_23_Human_m194_M01194
CC  id: HOCOMOCO_2015_11_23_Human_m194_M01194
CC  name: cluster_186
CC  version: 
CC  name: cluster_186
CC  description: GyGGGGGGTm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: GLIS3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m194_M01194
XX
//
AC  cluster_187
XX
ID  cluster_187
XX
DE  kkGrcrGGtGksa
P0       A     C     G     T
1     1036   529  1679  1364
2      513   273  1508  2314
3      313   208  3804   283
4     2692   126  1350   440
5      576  2683   556   793
6     2964     9  1577    58
7        9    15  4000   584
8      310   698  3243   357
9      328    91  1098  3091
10     411   146  3559   492
11     658   255  1598  2097
12     594  1847  1550   617
13    2164   548  1019   877
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_187
CC  AC: HOCOMOCO_2015_11_23_Human_m440_M01440
CC  id: HOCOMOCO_2015_11_23_Human_m440_M01440
CC  name: cluster_187
CC  version: 
CC  name: cluster_187
CC  description: kkGrcrGGtGksa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PKNX2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m440_M01440
XX
//
AC  cluster_188
XX
ID  cluster_188
XX
DE  kgggkGGGGGaGGGG
P0       A     C     G     T
1        5     4    16     9
2        3     7    20     4
3        8     7    15     4
4        4     3    20     7
5        6     6     9    13
6        1     3    29     1
7        3     0    31     0
8        0     0    34     0
9        2     0    30     2
10       0     0    34     0
11      16     7     5     6
12       7     0    26     1
13       1     1    32     0
14       7     0    26     1
15       4     3    25     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_188
CC  AC: HOCOMOCO_2015_11_23_Human_m624_M01624
CC  id: HOCOMOCO_2015_11_23_Human_m624_M01624
CC  name: cluster_188
CC  version: 
CC  name: cluster_188
CC  description: kgggkGGGGGaGGGG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN148_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m624_M01624
XX
//
AC  cluster_189
XX
ID  cluster_189
XX
DE  kkkGGGGGGGyrgk
P0       A     C     G     T
1      541   315   912   643
2      331   186  1002   892
3      190   115  1228   878
4       44    27  2234   106
5       26     8  2320    57
6       29     5  2353    24
7       28    11  2325    47
8       21     4  2328    58
9       31     4  2016   360
10       1     1  2409     0
11     562   754    42  1053
12     777   207   991   436
13     518   305  1025   563
14     439   275  1038   659
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_189
CC  AC: HOCOMOCO_2015_11_23_Human_m638_M01638
CC  id: HOCOMOCO_2015_11_23_Human_m638_M01638
CC  name: cluster_189
CC  version: 
CC  name: cluster_189
CC  description: kkkGGGGGGGyrgk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN740_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m638_M01638
XX
//
AC  cluster_19
XX
ID  cluster_19
XX
DE  rrGkyCAAAGkyCA
P0       A     C     G     T
1      153   100 198.5  48.5
2    238.5    19   186  56.5
3       46  21.5 384.5    48
4     91.5    39   164 205.5
5       73 173.5 117.5   136
6        8 445.5    10  36.5
7    479.5   6.5  10.5   3.5
8    425.5     4    62   8.5
9    447.5     2  46.5     4
10       2     4   489     5
11    14.5     4   154 327.5
12    19.5   249    61 170.5
13    10.5   434  11.5    44
14   349.5    40    51  59.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_19
CC  AC: cluster_19
CC  id: cluster_19
CC  name: cluster_19
CC  version: 
CC  name: cluster_19
CC  description: rrGkyCAAAGkyCA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gaGtcCAAAGtcCA
CC  consensus.strict.rc: TGGACTTTGGACTC
CC  consensus.IUPAC: rrGkyCAAAGkyCA
CC  consensus.IUPAC.rc: TGRMCTTTGRMCYY
CC  consensus.regexp: [ag][ag]G[gt][ct]CAAAG[gt][ct]CA
CC  consensus.regexp.rc: TG[AG][AC]CTTTG[AG][AC]C[CT][CT]
CC  merged_ID: HNF4A_A,HNF4G_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m226_M01226,HOCOMOCO_2015_11_23_Human_m227_M01227
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_190
XX
ID  cluster_190
XX
DE  cATAAwTTATGCr
P0       A     C     G     T
1        2     7     3     2
2       13     1     0     0
3        0     0     0    14
4       12     0     0     2
5       14     0     0     0
6        5     0     0     9
7        0     2     0    12
8        0     0     0    14
9       13     0     0     1
10       0     0     0    14
11       0     0    14     0
12       0    12     0     2
13       7     2     5     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_190
CC  AC: HOCOMOCO_2015_11_23_Human_m455_M01455
CC  id: HOCOMOCO_2015_11_23_Human_m455_M01455
CC  name: cluster_190
CC  version: 
CC  name: cluster_190
CC  description: cATAAwTTATGCr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PO6F1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m455_M01455
XX
//
AC  cluster_191
XX
ID  cluster_191
XX
DE  rCmGGAAGyggsg
P0       A     C     G     T
1      219    31   199    51
2       14   390    93     3
3      270   228     2     0
4        0     0   499     1
5        1     0   499     0
6      498     0     2     0
7      464     0     2    34
8       94     2   404     0
9        7   152    54   287
10     114    70   280    36
11     120    99   241    40
12      71   185   167    77
13      74    98   207   121
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_191
CC  AC: HOCOMOCO_2015_11_23_Human_m138_M01138
CC  id: HOCOMOCO_2015_11_23_Human_m138_M01138
CC  name: cluster_191
CC  version: 
CC  name: cluster_191
CC  description: rCmGGAAGyggsg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FLI1_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m138_M01138
XX
//
AC  cluster_192
XX
ID  cluster_192
XX
DE  tGrrrArTCCCc
P0       A     C     G     T
1        1     1     2     6
2        0     0    10     0
3        3     0     7     0
4        3     0     7     0
5        7     0     3     0
6       10     0     0     0
7        7     0     3     0
8        1     0     0     9
9        0     8     1     1
10       0    10     0     0
11       0     9     0     1
12       2     4     2     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_192
CC  AC: HOCOMOCO_2015_11_23_Human_m613_M01613
CC  id: HOCOMOCO_2015_11_23_Human_m613_M01613
CC  name: cluster_192
CC  version: 
CC  name: cluster_192
CC  description: tGrrrArTCCCc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZEP1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m613_M01613
XX
//
AC  cluster_193
XX
ID  cluster_193
XX
DE  gkssTGACGTGG
P0       A     C     G     T
1        4     4    11     1
2        1     4     6     9
3        0     5    12     3
4        2     8     9     1
5        0     1     0    19
6        0     1    19     0
7       18     1     0     1
8        0    20     0     0
9        0     0    20     0
10       0     0     0    20
11       0     2    18     0
12       2     0    18     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_193
CC  AC: HOCOMOCO_2015_11_23_Human_m21_M01021
CC  id: HOCOMOCO_2015_11_23_Human_m21_M01021
CC  name: cluster_193
CC  version: 
CC  name: cluster_193
CC  description: gkssTGACGTGG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ATF6A_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m21_M01021
XX
//
AC  cluster_194
XX
ID  cluster_194
XX
DE  rGrACAkycTGtw
P0       A     C     G     T
1       12     0     9     4
2        0     1    24     0
3       17     0     8     0
4       21     0     0     4
5        1    23     1     0
6       21     2     1     1
7        5     5     8     7
8        3     9     3    10
9        2    12     5     6
10       6     0     0    19
11       0     0    21     4
12       0     4     4    17
13      10     6     0     9
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_194
CC  AC: HOCOMOCO_2015_11_23_Human_m465_M01465
CC  id: HOCOMOCO_2015_11_23_Human_m465_M01465
CC  name: cluster_194
CC  version: 
CC  name: cluster_194
CC  description: rGrACAkycTGtw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PRGR_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m465_M01465
XX
//
AC  cluster_195
XX
ID  cluster_195
XX
DE  ksmhCAGGAAGTrrcdh
P0       A     C     G     T
1        1     1     3     3
2        0     5     2     1
3        3     3     1     1
4        3     2     1     2
5        1     7     0     0
6        7     1     0     0
7        0     0     8     0
8        0     0     8     0
9        8     0     0     0
10       7     1     0     0
11       0     0     8     0
12       0     0     0     8
13       4     1     3     0
14       5     0     2     1
15       1     5     1     1
16       3     0     2     3
17       2     3     1     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_195
CC  AC: HOCOMOCO_2015_11_23_Human_m132_M01132
CC  id: HOCOMOCO_2015_11_23_Human_m132_M01132
CC  name: cluster_195
CC  version: 
CC  name: cluster_195
CC  description: ksmhCAGGAAGTrrcdh
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ETV7_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m132_M01132
XX
//
AC  cluster_196
XX
ID  cluster_196
XX
DE  rdyTTGTTTwbwwttt
P0       A     C     G     T
1       18     7    19    14
2       16     5    20    17
3        4    18    14    22
4        4     9     3    42
5        0     1     0    57
6        4     0    54     0
7        1     0     0    57
8        1     0     0    57
9        0     0     4    54
10      28     2     4    24
11       1    25    17    15
12      16     8    11    23
13      16     6     5    31
14      12    11    11    24
15      10     3    13    32
16       9     8     9    32
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_196
CC  AC: HOCOMOCO_2015_11_23_Human_m163_M01163
CC  id: HOCOMOCO_2015_11_23_Human_m163_M01163
CC  name: cluster_196
CC  version: 
CC  name: cluster_196
CC  description: rdyTTGTTTwbwwttt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXO1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m163_M01163
XX
//
AC  cluster_197
XX
ID  cluster_197
XX
DE  CsCTTTGTTyKm
P0       A     C     G     T
1        1     6     1     0
2        0     3     4     1
3        0     7     0     1
4        1     1     0     6
5        0     1     0     7
6        0     1     0     7
7        0     0     8     0
8        0     1     0     7
9        0     1     0     7
10       1     5     0     2
11       0     0     2     6
12       2     4     1     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_197
CC  AC: HOCOMOCO_2015_11_23_Human_m519_M01519
CC  id: HOCOMOCO_2015_11_23_Human_m519_M01519
CC  name: cluster_197
CC  version: 
CC  name: cluster_197
CC  description: CsCTTTGTTyKm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SOX4_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m519_M01519
XX
//
AC  cluster_198
XX
ID  cluster_198
XX
DE  yCyATTGTTyt
P0       A     C     G     T
1       21    60    24    55
2        4   115    17    24
3        6   102     2    50
4      144     0     0    16
5        2     1     2   155
6        1     3     1   155
7        1     0   159     0
8        5     1     0   154
9       11     5     9   135
10      15    88     7    50
11      27    19    19    95
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_198
CC  AC: HOCOMOCO_2015_11_23_Human_m523_M01523
CC  id: HOCOMOCO_2015_11_23_Human_m523_M01523
CC  name: cluster_198
CC  version: 
CC  name: cluster_198
CC  description: yCyATTGTTyt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SOX9_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m523_M01523
XX
//
AC  cluster_199
XX
ID  cluster_199
XX
DE  ddGGGyGTGGym
P0       A     C     G     T
1       22     6    20    16
2       21     3    16    24
3        2     2    56     4
4        4     2    52     6
5        2     0    58     4
6        3    41     0    20
7        2     1    58     3
8        1     2     0    61
9        2     1    59     2
10       4     1    56     3
11       4    18     8    34
12      19    23     9    13
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_199
CC  AC: HOCOMOCO_2015_11_23_Human_m287_M01287
CC  id: HOCOMOCO_2015_11_23_Human_m287_M01287
CC  name: cluster_199
CC  version: 
CC  name: cluster_199
CC  description: ddGGGyGTGGym
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: KLF12_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m287_M01287
XX
//
AC  cluster_2
XX
ID  cluster_2
XX
DE  yTTCykrGAAr
P0       A     C     G     T
1       66 150.5  76.5 203.75
2     6.25 16.25    13 461.25
3    20.75     4 63.25 408.75
4    39.75 433.25     9 14.75
5    38.25 212.75 14.75   231
6    101.5     0 269.75 125.5
7    177.25  7.25 283.25    29
8       10   1.5 464.25    21
9    446.25  22.5   8.5  19.5
10   481.25  2.25    10  3.25
11   210.5 72.25 132.75 81.25
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_2
CC  AC: cluster_2
CC  id: cluster_2
CC  name: cluster_2
CC  version: 
CC  name: cluster_2
CC  description: yTTCykrGAAr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tTTCtggGAAa
CC  consensus.strict.rc: TTTCCCAGAAA
CC  consensus.IUPAC: yTTCykrGAAr
CC  consensus.IUPAC.rc: YTTCYMRGAAR
CC  consensus.regexp: [ct]TTC[ct][gt][ag]GAA[ag]
CC  consensus.regexp.rc: [CT]TTC[CT][AC][AG]GAA[AG]
CC  merged_ID: STA5A_B,STA5B_C,STAT3_A,STAT4_D
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m538_M01538,HOCOMOCO_2015_11_23_Human_m539_M01539,HOCOMOCO_2015_11_23_Human_m543_M01543,HOCOMOCO_2015_11_23_Human_m544_M01544
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_20
XX
ID  cluster_20
XX
DE  gTGGGTGGTCy
P0       A     C     G     T
1       36 60.3333   196 33.3333
2        0 0.333333     0 325.333
3        0     0   324 1.66667
4    0.333333 0.333333   276    49
5    0.333333     0 325.333     0
6        1 0.333333     1 323.333
7        0     0 325.333 0.333333
8    0.333333     0 323.667 1.66667
9    0.666667 0.666667     1 323.333
10   0.333333 322.667 1.33333 1.33333
11   26.3333 119.667 31.6667   148
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_20
CC  AC: cluster_20
CC  id: cluster_20
CC  name: cluster_20
CC  version: 
CC  name: cluster_20
CC  description: gTGGGTGGTCy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gTGGGTGGTCt
CC  consensus.strict.rc: AGACCACCCAC
CC  consensus.IUPAC: gTGGGTGGTCy
CC  consensus.IUPAC.rc: RGACCACCCAC
CC  consensus.regexp: gTGGGTGGTC[ct]
CC  consensus.regexp.rc: [AG]GACCACCCAC
CC  merged_ID: GLI2_B,GLI1_C,GLI3_B
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m190_M01190,HOCOMOCO_2015_11_23_Human_m189_M01189,HOCOMOCO_2015_11_23_Human_m191_M01191
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_200
XX
ID  cluster_200
XX
DE  tyrwTAATkr
P0       A     C     G     T
1        2     2     1     9
2        0     6     2     6
3        6     2     4     2
4        4     2     0     8
5        0     0     0    14
6       13     0     1     0
7       14     0     0     0
8        0     0     0    14
9        0     1     6     7
10       5     0     7     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_200
CC  AC: HOCOMOCO_2015_11_23_Human_m28_M01028
CC  id: HOCOMOCO_2015_11_23_Human_m28_M01028
CC  name: cluster_200
CC  version: 
CC  name: cluster_200
CC  description: tyrwTAATkr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BARX2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m28_M01028
XX
//
AC  cluster_201
XX
ID  cluster_201
XX
DE  arytAAGTGgv
P0       A     C     G     T
1        8     4     4     4
2        5     1    10     4
3        1     6     3    10
4        4     2     3    11
5       15     0     3     2
6       20     0     0     0
7        0     0    20     0
8        0     0     0    20
9        1     0    19     0
10       1     3    14     2
11       8     5     7     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_201
CC  AC: HOCOMOCO_2015_11_23_Human_m379_M01379
CC  id: HOCOMOCO_2015_11_23_Human_m379_M01379
CC  name: cluster_201
CC  version: 
CC  name: cluster_201
CC  description: arytAAGTGgv
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NKX32_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m379_M01379
XX
//
AC  cluster_202
XX
ID  cluster_202
XX
DE  wtTGTTTAydtwTdTtt
P0       A     C     G     T
1      111    48    57    94
2       47    34    38   191
3       10    11    10   279
4       32     6   266     6
5        0     1     3   306
6        0     0    72   238
7        1     0    29   280
8      213    51    34    12
9       12   109     9   180
10      96    30    90    94
11      59    22    37   192
12     113    19    55   123
13      19    17     7   267
14      97    13    98   102
15      16    23    27   244
16      74    18    24   194
17      26    42    61   181
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_202
CC  AC: HOCOMOCO_2015_11_23_Human_m154_M01154
CC  id: HOCOMOCO_2015_11_23_Human_m154_M01154
CC  name: cluster_202
CC  version: 
CC  name: cluster_202
CC  description: wtTGTTTAydtwTdTtt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXG1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m154_M01154
XX
//
AC  cluster_203
XX
ID  cluster_203
XX
DE  tgtTTGTTTrTTTAywTTt
P0       A     C     G     T
1      194   198   269   546
2      170   228   536   273
3      231    49   160   767
4       76    29    50  1052
5        1     1     1  1204
6      106     1  1091     9
7       10     0     0  1197
8        0     0     0  1207
9        0     0     0  1207
10     568     0   639     0
11       0     0     0  1207
12       0     0     0  1207
13       0     0     0  1207
14    1190     0    14     3
15       1   618     1   587
16     501    23    26   657
17     126    18    23  1040
18     273    21    18   895
19     233   151   102   721
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_203
CC  AC: HOCOMOCO_2015_11_23_Human_m161_M01161
CC  id: HOCOMOCO_2015_11_23_Human_m161_M01161
CC  name: cluster_203
CC  version: 
CC  name: cluster_203
CC  description: tgtTTGTTTrTTTAywTTt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXL1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m161_M01161
XX
//
AC  cluster_204
XX
ID  cluster_204
XX
DE  aGGTGTGAawTTTCaC
P0       A     C     G     T
1     4018   289  1204   665
2      721   534  4585   336
3       88   127  5867    94
4       63   606    63  5444
5        1     1  6173     1
6      150   467    70  5489
7       77   352  4702  1045
8     5923    15   204    34
9     3504   568   568  1536
10    2718   220   368  2870
11     867   211   158  4940
12     611   498   238  4829
13     207   211    34  5724
14     779  4764   416   217
15    4043   168  1336   629
16      52  6007    43    74
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_204
CC  AC: HOCOMOCO_2015_11_23_Human_m330_M01330
CC  id: HOCOMOCO_2015_11_23_Human_m330_M01330
CC  name: cluster_204
CC  version: 
CC  name: cluster_204
CC  description: aGGTGTGAawTTTCaC
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MGAP_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m330_M01330
XX
//
AC  cluster_205
XX
ID  cluster_205
XX
DE  rAGGTGTgAawwttmaCaCc
P0       A     C     G     T
1     6793  1708  4267  2946
2    13760   523   803   628
3     1115  1036 13049   514
4      246   270 14955   243
5      396  1164   397 13757
6      374   353 14635   352
7     1237  1580   463 12434
8      734  2494  9715  2771
9    13562   451  1197   504
10    6727  2759  2453  3775
11    9033  1333  1336  4012
12    5468  1452  1215  7579
13    3917  3467  3431  4899
14    1926  3481  1142  9165
15    4874  5834  3875  1131
16    9696  2117  2076  1825
17    1748 12343   873   750
18   10175  2299  2260   980
19    1060 12011  1305  1338
20    1784  7580  2595  3755
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_205
CC  AC: HOCOMOCO_2015_11_23_Human_m559_M01559
CC  id: HOCOMOCO_2015_11_23_Human_m559_M01559
CC  name: cluster_205
CC  version: 
CC  name: cluster_205
CC  description: rAGGTGTgAawwttmaCaCc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TBX4_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m559_M01559
XX
//
AC  cluster_206
XX
ID  cluster_206
XX
DE  raAAGtGAAAGTga
P0       A     C     G     T
1      183    37   225    53
2      316    48    72    62
3      359    58    50    31
4      440     1    50     7
5       34     3   450    11
6       84    12    81   321
7       16     2   479     1
8      494     1     1     2
9      477     0    20     1
10     486     1     4     7
11      11     6   469    12
12       8    38    24   428
13     114    37   240   107
14     216   100    87    95
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_206
CC  AC: HOCOMOCO_2015_11_23_Human_m463_M01463
CC  id: HOCOMOCO_2015_11_23_Human_m463_M01463
CC  name: cluster_206
CC  version: 
CC  name: cluster_206
CC  description: raAAGtGAAAGTga
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PRDM1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m463_M01463
XX
//
AC  cluster_207
XX
ID  cluster_207
XX
DE  hwvATGckGGymbk
P0       A     C     G     T
1        4     4     1     3
2        5     1     1     5
3        3     4     3     2
4       10     2     0     0
5        0     0     0    12
6        0     0    12     0
7        2     8     0     2
8        0     1     7     4
9        0     0    12     0
10       0     0    12     0
11       1     4     1     6
12       5     3     2     2
13       1     4     3     4
14       1     2     4     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_207
CC  AC: HOCOMOCO_2015_11_23_Human_m183_M01183
CC  id: HOCOMOCO_2015_11_23_Human_m183_M01183
CC  name: cluster_207
CC  version: 
CC  name: cluster_207
CC  description: hwvATGckGGymbk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: GCM1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m183_M01183
XX
//
AC  cluster_208
XX
ID  cluster_208
XX
DE  bATGCGGGTab
P0       A     C     G     T
1       40    88   114   110
2      268    24    36    24
3        0     0     0   352
4       16     1   335     0
5        1   349     0     2
6        0     0   349     3
7        0     0   352     0
8        1     1   349     1
9       10    51     3   288
10     194    24    81    53
11      47    88   100   117
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_208
CC  AC: HOCOMOCO_2015_11_23_Human_m184_M01184
CC  id: HOCOMOCO_2015_11_23_Human_m184_M01184
CC  name: cluster_208
CC  version: 
CC  name: cluster_208
CC  description: bATGCGGGTab
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: GCM2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m184_M01184
XX
//
AC  cluster_209
XX
ID  cluster_209
XX
DE  rGkTCAysrrgaGgtCA
P0       A     C     G     T
1       67     2    47     5
2        0     0   119     2
3        6     4    77    34
4        6     1    10   104
5        1    88    22    10
6      106     0    13     2
7       27    37    26    31
8       21    46    40    14
9       34    23    48    16
10      54    12    39    16
11      30    30    52     9
12      72    16    22    11
13       9    14    93     5
14      12     7    73    29
15      17    13    18    73
16      12    84    16     9
17      88    10    14     9
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_209
CC  AC: HOCOMOCO_2015_11_23_Human_m473_M01473
CC  id: HOCOMOCO_2015_11_23_Human_m473_M01473
CC  name: cluster_209
CC  version: 
CC  name: cluster_209
CC  description: rGkTCAysrrgaGgtCA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: RARA_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m473_M01473
XX
//
AC  cluster_21
XX
ID  cluster_21
XX
DE  ctgkkrsgsrswGATAAgr
P0       A     C     G     T
1       45   199    83  58.5
2       31  59.5  70.5 224.5
3     14.5  21.5 258.5    91
4       37    82 165.5   101
5       67    90   111 117.5
6       98    93   119  75.5
7       79  96.5   139    71
8       92  91.5   125    77
9       71   108 120.5    86
10   135.5    58   116    76
11    43.5 171.5 118.5    52
12     247  10.5     4   124
13       1     0 384.5     0
14   384.5   0.5     0   0.5
15     1.5     1     1   382
16     367   1.5     2    15
17   321.5   7.5    46  10.5
18      56    66 227.5    36
19   110.5    80   157    38
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_21
CC  AC: cluster_21
CC  id: cluster_21
CC  name: cluster_21
CC  version: 
CC  name: cluster_21
CC  description: ctgkkrsgsrswGATAAgr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ctggtggggacaGATAAgg
CC  consensus.strict.rc: CCTTATCTGTCCCCACCAG
CC  consensus.IUPAC: ctgkkrsgsrswGATAAgr
CC  consensus.IUPAC.rc: YCTTATCWSYSCSYMMCAG
CC  consensus.regexp: ctg[gt][gt][ag][cg]g[cg][ag][cg][at]GATAAg[ag]
CC  consensus.regexp.rc: [CT]CTTATC[AT][CG][CT][CG]C[CG][CT][AC][AC]CAG
CC  merged_ID: GATA1_S,TAL1_S
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m173_M01173,HOCOMOCO_2015_11_23_Human_m548_M01548
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_210
XX
ID  cluster_210
XX
DE  ggrGktcrscvrvAGKTCAv
P0       A     C     G     T
1        5     3    12     4
2        5     4    11     4
3        8     1    11     4
4        0     1    18     5
5        1     5    10     8
6        5     2     5    12
7        3    12     5     4
8       13     3     7     1
9        5     8     8     3
10       4    15     3     2
11       6     7     7     4
12      12     3     6     3
13       9     6     9     0
14      20     0     3     1
15       0     0    24     0
16       0     0    17     7
17       4     0     0    20
18       2    20     2     0
19      23     1     0     0
20       7     8     8     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_210
CC  AC: HOCOMOCO_2015_11_23_Human_m476_M01476
CC  id: HOCOMOCO_2015_11_23_Human_m476_M01476
CC  name: cluster_210
CC  version: 
CC  name: cluster_210
CC  description: ggrGktcrscvrvAGKTCAv
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: RARG_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m476_M01476
XX
//
AC  cluster_211
XX
ID  cluster_211
XX
DE  cryTrATTrsmattaatt
P0       A     C     G     T
1     1694  3637  1623  2089
2     2779  1838  3942   484
3      249  4797   639  3358
4        5     9     4  9025
5     5380     4  3659     0
6     9036     3     4     0
7        1     1     0  9041
8        1    30    34  8978
9     4614     1  4424     4
10     496  5665  2688   194
11    3713  2328  1320  1682
12    4820  1755  1314  1154
13    1980  1374   588  5101
14    1203  1742   470  5628
15    5450   653  1077  1863
16    5539   551   977  1976
17    2148   872  1124  4899
18    2045  1136  1450  4412
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_211
CC  AC: HOCOMOCO_2015_11_23_Human_m303_M01303
CC  id: HOCOMOCO_2015_11_23_Human_m303_M01303
CC  name: cluster_211
CC  version: 
CC  name: cluster_211
CC  description: cryTrATTrsmattaatt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: LHX8_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m303_M01303
XX
//
AC  cluster_212
XX
ID  cluster_212
XX
DE  kCAACAGGTGgtw
P0       A     C     G     T
1        9     8    99    63
2        6   163     5     5
3      166     5     4     4
4      172     2     3     2
5        3   171     2     3
6      178     0     0     1
7       29     0   150     0
8        1     1   176     1
9        3     2     1   173
10      15     4   130    30
11      20    20    95    44
12      34    37    35    73
13      52    36    22    69
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_212
CC  AC: HOCOMOCO_2015_11_23_Human_m498_M01498
CC  id: HOCOMOCO_2015_11_23_Human_m498_M01498
CC  name: cluster_212
CC  version: 
CC  name: cluster_212
CC  description: kCAACAGGTGgtw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SCRT1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m498_M01498
XX
//
AC  cluster_213
XX
ID  cluster_213
XX
DE  rwgCAACAGGTGg
P0       A     C     G     T
1      279   105   211   124
2      215   144    69   291
3       64    67   416   172
4        0   719     0     0
5      717     2     0     0
6      718     0     0     1
7        0   719     0     0
8      718     0     1     0
9       41     0   677     1
10       1     0   718     0
11       1     1     1   716
12      61    15   515   128
13     107   148   304   160
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_213
CC  AC: HOCOMOCO_2015_11_23_Human_m499_M01499
CC  id: HOCOMOCO_2015_11_23_Human_m499_M01499
CC  name: cluster_213
CC  version: 
CC  name: cluster_213
CC  description: rwgCAACAGGTGg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SCRT2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m499_M01499
XX
//
AC  cluster_214
XX
ID  cluster_214
XX
DE  rGGTCAs
P0       A     C     G     T
1      209     6   146    10
2       12     0   336    23
3        1    11   350     9
4        3    27    30   311
5        0   370     0     1
6      359     8     0     4
7       49   168   114    40
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_214
CC  AC: HOCOMOCO_2015_11_23_Human_m122_M01122
CC  id: HOCOMOCO_2015_11_23_Human_m122_M01122
CC  name: cluster_214
CC  version: 
CC  name: cluster_214
CC  description: rGGTCAs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ESR2_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m122_M01122
XX
//
AC  cluster_215
XX
ID  cluster_215
XX
DE  wwTATTGAYTtwdw
P0       A     C     G     T
1        8     2     4    10
2        7     4     0    13
3        2     4     1    17
4       24     0     0     0
5        0     0     0    24
6        0     1     3    20
7        0     0    24     0
8       23     0     1     0
9        0     7     0    17
10       2     1     2    19
11       5     3     0    16
12       8     3     2    11
13       6     2     7     9
14       7     5     3     9
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_215
CC  AC: HOCOMOCO_2015_11_23_Human_m228_M01228
CC  id: HOCOMOCO_2015_11_23_Human_m228_M01228
CC  name: cluster_215
CC  version: 
CC  name: cluster_215
CC  description: wwTATTGAYTtwdw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HNF6_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m228_M01228
XX
//
AC  cluster_216
XX
ID  cluster_216
XX
DE  tATyGATTTTTt
P0       A     C     G     T
1     1870  1779  1782  4911
2     7190  1049  1051  1052
3     1050  1048  1049  7195
4     1049  3359  1050  4884
5        2     0 10340     0
6    10341     0     1     0
7        0     1     0 10341
8        4     6     0 10332
9      867    19    27  9429
10     494   151    81  9616
11     624  1174   219  8325
12     987  1936  1399  6020
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_216
CC  AC: HOCOMOCO_2015_11_23_Human_m408_M01408
CC  id: HOCOMOCO_2015_11_23_Human_m408_M01408
CC  name: cluster_216
CC  version: 
CC  name: cluster_216
CC  description: tATyGATTTTTt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ONEC3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m408_M01408
XX
//
AC  cluster_217
XX
ID  cluster_217
XX
DE  syTAATr
P0       A     C     G     T
1        0     6     7     0
2        0     5     2     6
3        1     0     0    12
4       13     0     0     0
5       13     0     0     0
6        0     0     0    13
7        5     0     8     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_217
CC  AC: HOCOMOCO_2015_11_23_Human_m277_M01277
CC  id: HOCOMOCO_2015_11_23_Human_m277_M01277
CC  name: cluster_217
CC  version: 
CC  name: cluster_217
CC  description: syTAATr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ISL1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m277_M01277
XX
//
AC  cluster_218
XX
ID  cluster_218
XX
DE  kTTAATGg
P0       A     C     G     T
1        0     0     2     3
2        0     0     0     5
3        0     0     1     4
4        5     0     0     0
5        5     0     0     0
6        0     0     0     5
7        0     0     5     0
8        0     1     3     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_218
CC  AC: HOCOMOCO_2015_11_23_Human_m351_M01351
CC  id: HOCOMOCO_2015_11_23_Human_m351_M01351
CC  name: cluster_218
CC  version: 
CC  name: cluster_218
CC  description: kTTAATGg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NANOG_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m351_M01351
XX
//
AC  cluster_219
XX
ID  cluster_219
XX
DE  wsACTGmAATTswTky
P0       A     C     G     T
1     1436   328   293  2344
2      730  2169  1199   303
3     3609   351   218   223
4      443  3569   183   206
5       47    64    34  4256
6      666   148  3550    37
7     1264  2008   538   591
8     3519   850     2    30
9     4268   101     4    28
10      51   138     3  4209
11       4    93     3  4301
12      69  1187  2507   638
13    2830   110    55  1406
14     102   597   277  3425
15     693   803  1624  1281
16     776  1655   590  1380
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_219
CC  AC: HOCOMOCO_2015_11_23_Human_m522_M01522
CC  id: HOCOMOCO_2015_11_23_Human_m522_M01522
CC  name: cluster_219
CC  version: 
CC  name: cluster_219
CC  description: wsACTGmAATTswTky
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SOX8_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m522_M01522
XX
//
AC  cluster_22
XX
ID  cluster_22
XX
DE  rrGGtcArAGGtCAa
P0       A     C     G     T
1      8.5     6  6.25   3.5
2    276.5    45 214.75 30.25
3    105.75    23 398.25  39.5
4    53.75 62.75 400.75 49.25
5    59.75    62   125 319.75
6     66.5 370.25    89 40.75
7      463 29.25 67.75   6.5
8      320    36 199.25 11.25
9    477.5  3.75 64.25    21
10      29     7 503.25 27.25
11      20  22.5   460    64
12    36.5 58.75   118 353.25
13    74.5 407.25 57.25  27.5
14   441.5  20.5    72  32.5
15    8.25  4.75  3.75     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_22
CC  AC: cluster_22
CC  id: cluster_22
CC  name: cluster_22
CC  version: 
CC  name: cluster_22
CC  description: rrGGtcArAGGtCAa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aaGGtcAaAGGtCAa
CC  consensus.strict.rc: TTGACCTTTGACCTT
CC  consensus.IUPAC: rrGGtcArAGGtCAa
CC  consensus.IUPAC.rc: TTGACCTYTGACCYY
CC  consensus.regexp: [ag][ag]GGtcA[ag]AGGtCAa
CC  consensus.regexp.rc: TTGACCT[CT]TGACC[CT][CT]
CC  merged_ID: COT2_S,PPARD_D,NR2C2_A,PPARA_S
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m60_M01060,HOCOMOCO_2015_11_23_Human_m459_M01459,HOCOMOCO_2015_11_23_Human_m393_M01393,HOCOMOCO_2015_11_23_Human_m457_M01457
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_220
XX
ID  cluster_220
XX
DE  cyGgyAmbddgcyaagr
P0       A     C     G     T
1        9    21     8    12
2        2    18     3    27
3        1     2    36    11
4        7     4    30     9
5        1    28     1    20
6       42     2     1     5
7       19    18     8     5
8        5    16    16    13
9       14     7    13    16
10      15     5    13    17
11       8     8    34     0
12      11    31     1     7
13       2    34     0    14
14      25     7     9     9
15      33     1     9     7
16       4     5    30    11
17      14     7    20     9
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_220
CC  AC: HOCOMOCO_2015_11_23_Human_m585_M01585
CC  id: HOCOMOCO_2015_11_23_Human_m585_M01585
CC  name: cluster_220
CC  version: 
CC  name: cluster_220
CC  description: cyGgyAmbddgcyaagr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TLX1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m585_M01585
XX
//
AC  cluster_221
XX
ID  cluster_221
XX
DE  waATwWGCatw
P0       A     C     G     T
1       82    43    28   125
2      153    55    22    48
3      202     8     7    61
4        1     6     7   264
5       95     0     6   177
6       86     1     0   191
7       35     3   213    27
8        4   268     3     3
9      123    53    51    51
10      54    60    53   111
11      94    57    55    72
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_221
CC  AC: HOCOMOCO_2015_11_23_Human_m262_M01262
CC  id: HOCOMOCO_2015_11_23_Human_m262_M01262
CC  name: cluster_221
CC  version: 
CC  name: cluster_221
CC  description: waATwWGCatw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXD8_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m262_M01262
XX
//
AC  cluster_222
XX
ID  cluster_222
XX
DE  wATGcAAAt
P0       A     C     G     T
1       63    41    26    80
2      188     5    12     5
3       10     6    13   181
4        7     3   182    18
5       23   141    16    30
6      165     8     1    36
7      176     2    16    16
8      184     3    14     9
9       31    10    29   140
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_222
CC  AC: HOCOMOCO_2015_11_23_Human_m444_M01444
CC  id: HOCOMOCO_2015_11_23_Human_m444_M01444
CC  name: cluster_222
CC  version: 
CC  name: cluster_222
CC  description: wATGcAAAt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PO2F1_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m444_M01444
XX
//
AC  cluster_223
XX
ID  cluster_223
XX
DE  sCAsCTGty
P0       A     C     G     T
1        4    27    32    14
2        1    76     0     0
3       75     0     2     0
4        4    27    38     8
5        4    62     9     2
6        0     2     0    75
7        0     2    72     3
8        5    18     7    47
9        3    42     2    30
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_223
CC  AC: HOCOMOCO_2015_11_23_Human_m347_M01347
CC  id: HOCOMOCO_2015_11_23_Human_m347_M01347
CC  name: cluster_223
CC  version: 
CC  name: cluster_223
CC  description: sCAsCTGty
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MYOD1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m347_M01347
XX
//
AC  cluster_224
XX
ID  cluster_224
XX
DE  srsCAyGTGksca
P0       A     C     G     T
1      451   916   511   120
2      931   113   671   283
3       93   911   843   151
4      391  1599     6     2
5     1625     3   349    21
6        9  1268    29   692
7       39    24  1918    17
8       40    54     7  1897
9        2     7  1574   415
10     226   258   813   701
11     126   520  1015   337
12     490   849   181   478
13     859   345   481   313
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_224
CC  AC: HOCOMOCO_2015_11_23_Human_m111_M01111
CC  id: HOCOMOCO_2015_11_23_Human_m111_M01111
CC  name: cluster_224
CC  version: 
CC  name: cluster_224
CC  description: srsCAyGTGksca
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ENOA_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m111_M01111
XX
//
AC  cluster_225
XX
ID  cluster_225
XX
DE  kGrwCACGTGRCys
P0       A     C     G     T
1        3     2     4     6
2        1     0    13     1
3        5     2     7     1
4        9     0     2     4
5        0    14     0     1
6       15     0     0     0
7        0    15     0     0
8        0     0    15     0
9        0     0     0    15
10       0     0    15     0
11      11     0     4     0
12       1    12     1     1
13       2     7     1     5
14       1    10     4     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_225
CC  AC: HOCOMOCO_2015_11_23_Human_m40_M01040
CC  id: HOCOMOCO_2015_11_23_Human_m40_M01040
CC  name: cluster_225
CC  version: 
CC  name: cluster_225
CC  description: kGrwCACGTGRCys
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BMAL1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m40_M01040
XX
//
AC  cluster_226
XX
ID  cluster_226
XX
DE  gtmAATrTTKACATArs
P0       A     C     G     T
1     1543  1145  2974  1376
2      628  1013  1589  3808
3     2491  2703  1134   710
4     5652   387   398   601
5     5096   580   211  1151
6       24    24   113  6877
7     4551     7  2463    17
8       94     6    65  6873
9       94     2   189  6753
10      21     4  1966  5047
11    6757     3   270     8
12      22  6315    76   625
13    5453   100   236  1249
14     217  1012   212  5597
15    5050   120   307  1561
16    4038   443  1804   753
17     872  2514  2302  1350
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_226
CC  AC: HOCOMOCO_2015_11_23_Human_m146_M01146
CC  id: HOCOMOCO_2015_11_23_Human_m146_M01146
CC  name: cluster_226
CC  version: 
CC  name: cluster_226
CC  description: gtmAATrTTKACATArs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXB1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m146_M01146
XX
//
AC  cluster_227
XX
ID  cluster_227
XX
DE  GGGtGGGyAGGGGtGGGcdk
P0       A     C     G     T
1      541   368  3364   443
2      399   376  3451   490
3      317   375  3491   533
4      784  1094   865  1973
5      306   238  3926   246
6      237   186  3815   478
7      255   129  4104   228
8      908  1931   695  1182
9     3778   187   520   231
10      84    72  4473    87
11      18    28  4596    74
12      16    66  3554  1080
13      32    41  4599    44
14     351  1037   234  3094
15      25    40  4514   137
16    1005    20  3657    34
17     833    79  3642   162
18    1127  1894   821   874
19    1244   493  1610  1369
20     990   603  1766  1357
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_227
CC  AC: HOCOMOCO_2015_11_23_Human_m554_M01554
CC  id: HOCOMOCO_2015_11_23_Human_m554_M01554
CC  name: cluster_227
CC  version: 
CC  name: cluster_227
CC  description: GGGtGGGyAGGGGtGGGcdk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TBX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m554_M01554
XX
//
AC  cluster_228
XX
ID  cluster_228
XX
DE  sgTsAGGTCA
P0       A     C     G     T
1        4    16    22    10
2        8    12    27     5
3        1     2    10    39
4        3    29    18     2
5       51     0     1     0
6        0     5    44     3
7        1     4    46     1
8       10     0     0    42
9        1    50     1     0
10      45     6     0     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_228
CC  AC: HOCOMOCO_2015_11_23_Human_m584_M01584
CC  id: HOCOMOCO_2015_11_23_Human_m584_M01584
CC  name: cluster_228
CC  version: 
CC  name: cluster_228
CC  description: sgTsAGGTCA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: THB_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m584_M01584
XX
//
AC  cluster_229
XX
ID  cluster_229
XX
DE  kskgATTssCy
P0       A     C     G     T
1        8    12    22    20
2        4    20    33     5
3        6     8    25    23
4        7    12    42     1
5       46    10     2     4
6        0     7     0    55
7        0     3     7    52
8        0    21    33     8
9        1    21    33     7
10       7    44     1    10
11       5    30     3    24
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_229
CC  AC: HOCOMOCO_2015_11_23_Human_m603_M01603
CC  id: HOCOMOCO_2015_11_23_Human_m603_M01603
CC  name: cluster_229
CC  version: 
CC  name: cluster_229
CC  description: kskgATTssCy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: YBOX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m603_M01603
XX
//
AC  cluster_23
XX
ID  cluster_23
XX
DE  dGGGrmTTyCCm
P0       A     C     G     T
1     6.75  2.75     9     7
2        6  8.75   157 23.75
3     2.25  1.25 190.75  1.25
4      3.5     1   181    10
5    82.75  4.25 101.5     7
6     81.5    67  16.5  30.5
7     6.25     3  2.75 183.5
8     0.75  9.25   5.5   180
9      0.5    71  2.25 121.75
10       5   180  2.75  7.75
11       5   178     2  10.5
12   53.25    98    22 22.25
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_23
CC  AC: cluster_23
CC  id: cluster_23
CC  name: cluster_23
CC  version: 
CC  name: cluster_23
CC  description: dGGGrmTTyCCm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gGGGgaTTtCCc
CC  consensus.strict.rc: GGGAAATCCCCC
CC  consensus.IUPAC: dGGGrmTTyCCm
CC  consensus.IUPAC.rc: KGGRAAKYCCCH
CC  consensus.regexp: [agt]GGG[ag][ac]TT[ct]CC[ac]
CC  consensus.regexp.rc: [GT]GG[AG]AA[GT][CT]CCC[ACT]
CC  merged_ID: NFKB1_B,TF65_A,RELB_C,REL_C
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m367_M01367,HOCOMOCO_2015_11_23_Human_m567_M01567,HOCOMOCO_2015_11_23_Human_m479_M01479,HOCOMOCO_2015_11_23_Human_m480_M01480
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_230
XX
ID  cluster_230
XX
DE  dtyGCGTGm
P0       A     C     G     T
1       41    18    56    39
2       11    12    35    96
3       22    44    21    67
4        3     1   146     4
5        1   150     1     2
6        3     1   149     1
7        0     3     1   150
8        0     0   154     0
9       43    67    16    28
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_230
CC  AC: HOCOMOCO_2015_11_23_Human_m1_M01001
CC  id: HOCOMOCO_2015_11_23_Human_m1_M01001
CC  name: cluster_230
CC  version: 
CC  name: cluster_230
CC  description: dtyGCGTGm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: AHR_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m1_M01001
XX
//
AC  cluster_231
XX
ID  cluster_231
XX
DE  yCwTTGT
P0       A     C     G     T
1        2    10     7    13
2        1    29     0     2
3       14     3     0    15
4        1     0     0    31
5        0     0     0    32
6        1     0    31     0
7        5     1     1    25
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_231
CC  AC: HOCOMOCO_2015_11_23_Human_m509_M01509
CC  id: HOCOMOCO_2015_11_23_Human_m509_M01509
CC  name: cluster_231
CC  version: 
CC  name: cluster_231
CC  description: yCwTTGT
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SOX10_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m509_M01509
XX
//
AC  cluster_232
XX
ID  cluster_232
XX
DE  awwATTyAtg
P0       A     C     G     T
1       35    15     4    11
2       27     1     2    35
3       25     0     8    32
4       61     1     0     3
5        1     1     1    62
6        1     1    13    50
7       11    22     0    32
8       56     0     3     6
9       13     5    10    37
10      13     6    33    13
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_232
CC  AC: HOCOMOCO_2015_11_23_Human_m448_M01448
CC  id: HOCOMOCO_2015_11_23_Human_m448_M01448
CC  name: cluster_232
CC  version: 
CC  name: cluster_232
CC  description: awwATTyAtg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PO3F2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m448_M01448
XX
//
AC  cluster_233
XX
ID  cluster_233
XX
DE  ysGTCACGCwTsAsTGm
P0       A     C     G     T
1      209   332   183   449
2      100   574   351   148
3       42    41  1039    51
4       79    57    35  1002
5        7   963     1   202
6     1078    88     4     3
7        2  1010     4   157
8        5     3  1161     4
9        6   912   252     3
10     436    20     3   714
11       5   106     4  1058
12       7   373   762    31
13     960     1   207     5
14     216   368   334   255
15      20    75    14  1064
16     108    33   819   213
17     442   375   273    83
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_233
CC  AC: HOCOMOCO_2015_11_23_Human_m418_M01418
CC  id: HOCOMOCO_2015_11_23_Human_m418_M01418
CC  name: cluster_233
CC  version: 
CC  name: cluster_233
CC  description: ysGTCACGCwTsAsTGm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PAX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m418_M01418
XX
//
AC  cluster_234
XX
ID  cluster_234
XX
DE  ghkcagTsAygmgTGAyw
P0       A     C     G     T
1       12     9    20     9
2       17    14     3    16
3        3     8    13    26
4        9    23     9     9
5       27     8    10     5
6       10     9    22     9
7        0     8     1    41
8        4    29    15     2
9       41     2     7     0
10       8    13     5    24
11      11     3    29     7
12      13    26     6     5
13      10     5    25    10
14       3    12     0    35
15       4     5    41     0
16      39     0     2     9
17       3    31     0    16
18      26     2     7    15
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_234
CC  AC: HOCOMOCO_2015_11_23_Human_m419_M01419
CC  id: HOCOMOCO_2015_11_23_Human_m419_M01419
CC  name: cluster_234
CC  version: 
CC  name: cluster_234
CC  description: ghkcagTsAygmgTGAyw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PAX2_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m419_M01419
XX
//
AC  cluster_235
XX
ID  cluster_235
XX
DE  ardgGGGGsybta
P0       A     C     G     T
1       17     1     5     2
2        7     5    12     1
3        9     1     8     7
4        6     0    16     3
5        2     2    20     1
6        4     0    21     0
7        0     0    25     0
8        0     0    25     0
9        1    14    10     0
10       6     9     1     9
11       1    10     7     7
12       3     6     5    11
13      12     6     4     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_235
CC  AC: HOCOMOCO_2015_11_23_Human_m442_M01442
CC  id: HOCOMOCO_2015_11_23_Human_m442_M01442
CC  name: cluster_235
CC  version: 
CC  name: cluster_235
CC  description: ardgGGGGsybta
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PLAG1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m442_M01442
XX
//
AC  cluster_236
XX
ID  cluster_236
XX
DE  grGGGGGGyGGa
P0       A     C     G     T
1        4     4    13     5
2       12     1     7     6
3        1     0    23     2
4        0     2    23     1
5        0     0    21     5
6        0     1    24     1
7        1     0    25     0
8        0     0    26     0
9        1    11     4    10
10       1     5    19     1
11       0     1    24     1
12      15     3     2     6
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_236
CC  AC: HOCOMOCO_2015_11_23_Human_m625_M01625
CC  id: HOCOMOCO_2015_11_23_Human_m625_M01625
CC  name: cluster_236
CC  version: 
CC  name: cluster_236
CC  description: grGGGGGGyGGa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN219_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m625_M01625
XX
//
AC  cluster_237
XX
ID  cluster_237
XX
DE  rATTAAA
P0       A     C     G     T
1       27     3    12     5
2       47     0     0     0
3        0     0     0    47
4        0     0     0    47
5       47     0     0     0
6       47     0     0     0
7       39     0     0     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_237
CC  AC: HOCOMOCO_2015_11_23_Human_m12_M01012
CC  id: HOCOMOCO_2015_11_23_Human_m12_M01012
CC  name: cluster_237
CC  version: 
CC  name: cluster_237
CC  description: rATTAAA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ARI3A_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m12_M01012
XX
//
AC  cluster_238
XX
ID  cluster_238
XX
DE  vrTkswwATGCaww
P0       A     C     G     T
1        5     5     4     1
2        9     0     4     2
3        1     0     1    13
4        1     0     6     8
5        1     5     7     2
6        6     0     0     9
7        9     0     1     5
8       14     0     0     1
9        1     0     1    13
10       1     1    12     1
11       1    12     2     0
12       9     0     3     3
13       8     1     2     4
14       6     2     3     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_238
CC  AC: HOCOMOCO_2015_11_23_Human_m447_M01447
CC  id: HOCOMOCO_2015_11_23_Human_m447_M01447
CC  name: cluster_238
CC  version: 
CC  name: cluster_238
CC  description: vrTkswwATGCaww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PO3F1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m447_M01447
XX
//
AC  cluster_239
XX
ID  cluster_239
XX
DE  smrTGAC
P0       A     C     G     T
1        8    20    29     1
2       22    36     0     0
3       29     4    25     0
4        0     4     0    54
5        0     0    58     0
6       52     1     2     3
7        7    44     1     6
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_239
CC  AC: HOCOMOCO_2015_11_23_Human_m420_M01420
CC  id: HOCOMOCO_2015_11_23_Human_m420_M01420
CC  name: cluster_239
CC  version: 
CC  name: cluster_239
CC  description: smrTGAC
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PAX2_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m420_M01420
XX
//
AC  cluster_24
XX
ID  cluster_24
XX
DE  rRGGTCA
P0       A     C     G     T
1     26.5  10.5 34.75   8.5
2    58.25  0.75 20.75   0.5
3        0     0    80  0.25
4        0  0.75 61.25 18.25
5      2.5  1.25  2.25 74.25
6     0.25    76  3.25  0.75
7     73.5  0.75   3.5   2.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_24
CC  AC: cluster_24
CC  id: cluster_24
CC  name: cluster_24
CC  version: 
CC  name: cluster_24
CC  description: rRGGTCA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gAGGTCA
CC  consensus.strict.rc: TGACCTC
CC  consensus.IUPAC: rRGGTCA
CC  consensus.IUPAC.rc: TGACCYY
CC  consensus.regexp: [ag][AG]GGTCA
CC  consensus.regexp.rc: TGACC[CT][CT]
CC  merged_ID: VDR_S,RARG_S,PPARA_S,RARA_S
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m597_M01597,HOCOMOCO_2015_11_23_Human_m477_M01477,HOCOMOCO_2015_11_23_Human_m458_M01458,HOCOMOCO_2015_11_23_Human_m474_M01474
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_240
XX
ID  cluster_240
XX
DE  cGTkACGTc
P0       A     C     G     T
1        2     3     2     2
2        1     0     7     1
3        0     0     0     9
4        0     0     5     4
5        9     0     0     0
6        0     9     0     0
7        0     0     9     0
8        1     0     0     8
9        2     5     1     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_240
CC  AC: HOCOMOCO_2015_11_23_Human_m94_M01094
CC  id: HOCOMOCO_2015_11_23_Human_m94_M01094
CC  name: cluster_240
CC  version: 
CC  name: cluster_240
CC  description: cGTkACGTc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: E4F1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m94_M01094
XX
//
AC  cluster_241
XX
ID  cluster_241
XX
DE  aaAaTTAATTAry
P0       A     C     G     T
1       39     6    13    14
2       38     3    15    16
3       57     7     7     1
4       42     2    11    17
5        1     1     2    68
6        0     0     1    71
7       72     0     0     0
8       71     0     1     0
9        0     0     0    72
10       2     1     1    68
11      67     0     2     3
12      33     7    24     8
13      12    19    11    30
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_241
CC  AC: HOCOMOCO_2015_11_23_Human_m300_M01300
CC  id: HOCOMOCO_2015_11_23_Human_m300_M01300
CC  name: cluster_241
CC  version: 
CC  name: cluster_241
CC  description: aaAaTTAATTAry
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: LHX3_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m300_M01300
XX
//
AC  cluster_242
XX
ID  cluster_242
XX
DE  gscGTGGsrsrc
P0       A     C     G     T
1        3     2    11     2
2        3     5     6     4
3        0    12     4     2
4        0     1    17     0
5        0     0     1    17
6        0     1    17     0
7        1     0    16     1
8        3     6     7     2
9       10     0     5     3
10       0     6    10     2
11       7     0     9     2
12       2    11     3     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_242
CC  AC: HOCOMOCO_2015_11_23_Human_m14_M01014
CC  id: HOCOMOCO_2015_11_23_Human_m14_M01014
CC  name: cluster_242
CC  version: 
CC  name: cluster_242
CC  description: gscGTGGsrsrc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ARNT2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m14_M01014
XX
//
AC  cluster_243
XX
ID  cluster_243
XX
DE  gmGkGGgAsr
P0       A     C     G     T
1        0     2     6     1
2        3     6     0     0
3        1     0     8     0
4        1     0     3     5
5        0     0     9     0
6        0     2     7     0
7        2     0     6     1
8        7     1     1     0
9        2     3     4     0
10       5     0     4     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_243
CC  AC: HOCOMOCO_2015_11_23_Human_m384_M01384
CC  id: HOCOMOCO_2015_11_23_Human_m384_M01384
CC  name: cluster_243
CC  version: 
CC  name: cluster_243
CC  description: gmGkGGgAsr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NR0B1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m384_M01384
XX
//
AC  cluster_244
XX
ID  cluster_244
XX
DE  ddmWGATAAGawar
P0       A     C     G     T
1      139    59   130   172
2      159    54   149   138
3      145   193   110    52
4      356     4     5   135
5        1     1   498     0
6      498     1     0     1
7        0     2     0   498
8      462     0     6    32
9      487     2     3     8
10      43    55   396     6
11     338    41   112     9
12     152    44    81   223
13     273    26   102    99
14     205    82   125    88
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_244
CC  AC: HOCOMOCO_2015_11_23_Human_m179_M01179
CC  id: HOCOMOCO_2015_11_23_Human_m179_M01179
CC  name: cluster_244
CC  version: 
CC  name: cluster_244
CC  description: ddmWGATAAGawar
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: GATA6_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m179_M01179
XX
//
AC  cluster_245
XX
ID  cluster_245
XX
DE  gkGGGyGkGksgGGgGGGg
P0       A     C     G     T
1      126   106   291   127
2       64    79   343   164
3       40    22   558    30
4        6    14   617    13
5       22     6   598    24
6       83   315     8   244
7       30     9   579    32
8       26    20   366   238
9       43     4   567    36
10     101    32   341   176
11      91   174   233   152
12     123    57   385    85
13      48    21   526    55
14      40    42   512    56
15      50    45   440   115
16      47    71   453    79
17      38    82   462    68
18      27    68   464    91
19      67    95   340   148
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_245
CC  AC: HOCOMOCO_2015_11_23_Human_m291_M01291
CC  id: HOCOMOCO_2015_11_23_Human_m291_M01291
CC  name: cluster_245
CC  version: 
CC  name: cluster_245
CC  description: gkGGGyGkGksgGGgGGGg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: KLF16_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m291_M01291
XX
//
AC  cluster_246
XX
ID  cluster_246
XX
DE  wttrgmGGGwaw
P0       A     C     G     T
1        7     4     5    12
2        5     3     5    15
3        2     3     4    19
4        9     1    15     3
5        3     0    19     6
6        9    13     0     6
7        0     1    26     1
8        0     0    27     1
9        0     0    25     3
10      19     1     1     7
11      16     4     3     5
12      12     4     4     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_246
CC  AC: HOCOMOCO_2015_11_23_Human_m93_M01093
CC  id: HOCOMOCO_2015_11_23_Human_m93_M01093
CC  name: cluster_246
CC  version: 
CC  name: cluster_246
CC  description: wttrgmGGGwaw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: E2F8_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m93_M01093
XX
//
AC  cluster_247
XX
ID  cluster_247
XX
DE  gbrTCTGGmwtt
P0       A     C     G     T
1        4     1    17     4
2        1     9     9     7
3       10     0    15     1
4        0     2     1    23
5        0    25     1     0
6        0     0     0    26
7        0     0    26     0
8        0     2    23     1
9        9    11     1     5
10      13     1     2    10
11       5     3     4    14
12       4     5     4    13
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_247
CC  AC: HOCOMOCO_2015_11_23_Human_m201_M01201
CC  id: HOCOMOCO_2015_11_23_Human_m201_M01201
CC  name: cluster_247
CC  version: 
CC  name: cluster_247
CC  description: gbrTCTGGmwtt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HAND1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m201_M01201
XX
//
AC  cluster_248
XX
ID  cluster_248
XX
DE  gtGTCTGgcy
P0       A     C     G     T
1       13    18    32    10
2       16     9     7    41
3        1     7    63     2
4        0     0     7    66
5        0    72     0     1
6        3     2     1    67
7        8     0    60     5
8        8    15    32    18
9       16    40    10     7
10       7    29    12    25
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_248
CC  AC: HOCOMOCO_2015_11_23_Human_m504_M01504
CC  id: HOCOMOCO_2015_11_23_Human_m504_M01504
CC  name: cluster_248
CC  version: 
CC  name: cluster_248
CC  description: gtGTCTGgcy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SMAD3_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m504_M01504
XX
//
AC  cluster_249
XX
ID  cluster_249
XX
DE  tTTTATkrcyyt
P0       A     C     G     T
1       72    91    66   271
2        6     4     5   485
3        7     1     1   491
4       21    11    17   451
5      485     3     5     7
6        1    15     2   482
7       70     0   216   214
8      181     6   299    14
9       12   320    64   104
10     117   149    27   207
11      68   156    71   205
12     122   111    72   195
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_249
CC  AC: HOCOMOCO_2015_11_23_Human_m135_M01135
CC  id: HOCOMOCO_2015_11_23_Human_m135_M01135
CC  name: cluster_249
CC  version: 
CC  name: cluster_249
CC  description: tTTTATkrcyyt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: EVX2_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m135_M01135
XX
//
AC  cluster_25
XX
ID  cluster_25
XX
DE  ryTAATTAGkt
P0       A     C     G     T
1    1993.25 1289.5  2973   938
2    441.75 1985.5 547.25 4219.25
3    142.75 148.5 99.25 6803.25
4    7050.75 33.75  46.5 62.75
5    7155.75   3.5    19  15.5
6      3.5  6.25  16.5 7167.5
7      157     7 1312.25 5717.5
8    5099.25   128 893.75 1072.75
9    1095.5   364 5425.75 308.5
10    1440 1516.25 2300.75 1936.75
11   876.25  1482 1104.75 3730.75
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_25
CC  AC: cluster_25
CC  id: cluster_25
CC  name: cluster_25
CC  version: 
CC  name: cluster_25
CC  description: ryTAATTAGkt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gtTAATTAGgt
CC  consensus.strict.rc: ACCTAATTAAC
CC  consensus.IUPAC: ryTAATTAGkt
CC  consensus.IUPAC.rc: AMCTAATTARY
CC  consensus.regexp: [ag][ct]TAATTAG[gt]t
CC  consensus.regexp.rc: A[AC]CTAATTA[AG][CT]
CC  merged_ID: MIXL1_D,PO6F2_D,LBX2_D,NOTO_D
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m332_M01332,HOCOMOCO_2015_11_23_Human_m456_M01456,HOCOMOCO_2015_11_23_Human_m297_M01297,HOCOMOCO_2015_11_23_Human_m383_M01383
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_250
XX
ID  cluster_250
XX
DE  tGGAAwwTTCCwt
P0       A     C     G     T
1       89   104    62   170
2        0     0   425     0
3        0     0   425     0
4      420     4     1     0
5      425     0     0     0
6      282    27     1   115
7      138    23    67   197
8        6     0     4   415
9        5    55     6   359
10      10   359     0    56
11      63   314    40     8
12     230     3    79   113
13      64    56    37   268
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_250
CC  AC: HOCOMOCO_2015_11_23_Human_m357_M01357
CC  id: HOCOMOCO_2015_11_23_Human_m357_M01357
CC  name: cluster_250
CC  version: 
CC  name: cluster_250
CC  description: tGGAAwwTTCCwt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NFAC1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m357_M01357
XX
//
AC  cluster_251
XX
ID  cluster_251
XX
DE  syGGARAAnyhCmkk
P0       A     C     G     T
1        1     2     5     0
2        0     3     0     5
3        0     0     8     0
4        0     0     8     0
5        8     0     0     0
6        6     0     2     0
7        8     0     0     0
8        7     0     1     0
9        2     2     2     2
10       1     2     0     5
11       2     4     0     2
12       0     8     0     0
13       4     3     0     1
14       1     0     2     5
15       1     1     3     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_251
CC  AC: HOCOMOCO_2015_11_23_Human_m361_M01361
CC  id: HOCOMOCO_2015_11_23_Human_m361_M01361
CC  name: cluster_251
CC  version: 
CC  name: cluster_251
CC  description: syGGARAAnyhCmkk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NFAT5_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m361_M01361
XX
//
AC  cluster_252
XX
ID  cluster_252
XX
DE  ttTGACAGstk
P0       A     C     G     T
1     2342  1721  2326  3509
2     2104  1278  2172  4344
3      103    73    91  9631
4        7    10  9876     5
5     9732     3   158     5
6        7  9876     5    10
7     9773     1   118     6
8      574   670  7585  1069
9     1106  3390  4532   870
10    1045  1422   841  6590
11    1656  1568  4075  2599
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_252
CC  AC: HOCOMOCO_2015_11_23_Human_m326_M01326
CC  id: HOCOMOCO_2015_11_23_Human_m326_M01326
CC  name: cluster_252
CC  version: 
CC  name: cluster_252
CC  description: ttTGACAGstk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MEIS3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m326_M01326
XX
//
AC  cluster_253
XX
ID  cluster_253
XX
DE  twwwwwawwtTTTwATkrc
P0       A     C     G     T
1     2108  1461  1632  3779
2     2789  1027  1069  4095
3     3773   593   621  3993
4     3563   412   439  4566
5     3854   367   824  3935
6     4238   514  1437  2791
7     3689  1472  1758  2061
8     2655  1688  1949  2688
9     2275  1742  1941  3022
10    2220  1254  1811  3695
11     837   916  1118  6109
12     203   120   850  7807
13       5   118    76  8781
14    3323   148   128  5381
15    8868    41    70     1
16     360  1545   491  6584
17    1039   825  3533  3583
18    4237   806  2848  1089
19    1505  3832  1821  1822
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_253
CC  AC: HOCOMOCO_2015_11_23_Human_m250_M01250
CC  id: HOCOMOCO_2015_11_23_Human_m250_M01250
CC  name: cluster_253
CC  version: 
CC  name: cluster_253
CC  description: twwwwwawwtTTTwATkrc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXC10_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m250_M01250
XX
//
AC  cluster_254
XX
ID  cluster_254
XX
DE  TAAymyr
P0       A     C     G     T
1        0     0     0     4
2        4     0     0     0
3        4     0     0     0
4        0     2     0     2
5        1     3     0     0
6        0     1     0     3
7        1     0     3     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_254
CC  AC: HOCOMOCO_2015_11_23_Human_m469_M01469
CC  id: HOCOMOCO_2015_11_23_Human_m469_M01469
CC  name: cluster_254
CC  version: 
CC  name: cluster_254
CC  description: TAAymyr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PRRX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m469_M01469
XX
//
AC  cluster_255
XX
ID  cluster_255
XX
DE  sAhATWyywGcybyw
P0       A     C     G     T
1        2     3     5     2
2       10     2     0     0
3        4     3     0     5
4       11     0     1     0
5        0     0     1    11
6        3     0     0     9
7        0     5     2     5
8        0     7     0     5
9        4     0     0     8
10       2     0    10     0
11       2     8     0     2
12       2     3     1     6
13       1     4     3     4
14       1     4     1     6
15       5     2     2     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_255
CC  AC: HOCOMOCO_2015_11_23_Human_m563_M01563
CC  id: HOCOMOCO_2015_11_23_Human_m563_M01563
CC  name: cluster_255
CC  version: 
CC  name: cluster_255
CC  description: sAhATWyywGcybyw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TEAD3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m563_M01563
XX
//
AC  cluster_256
XX
ID  cluster_256
XX
DE  rtCAtGTGmb
P0       A     C     G     T
1       21    11    27     9
2       15    13    11    29
3        0    68     0     0
4       67     0     1     0
5        5     9     8    46
6        1     4    63     0
7        0     0     1    67
8        0     0    68     0
9       31    22     9     6
10       1    21    21    25
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_256
CC  AC: HOCOMOCO_2015_11_23_Human_m331_M01331
CC  id: HOCOMOCO_2015_11_23_Human_m331_M01331
CC  name: cluster_256
CC  version: 
CC  name: cluster_256
CC  description: rtCAtGTGmb
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MITF_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m331_M01331
XX
//
AC  cluster_257
XX
ID  cluster_257
XX
DE  ymATTAacymaTta
P0       A     C     G     T
1      917  3919   698  2679
2     3373  2194  1877   769
3     6606   535   637   435
4        4     4     4  8201
5        1    16     0  8196
6     8196     0    11     6
7     3952   982  1305  1974
8     1073  4527  1888   725
9     1136  2079  1722  3276
10    2517  3012  1464  1220
11    4139  1553  1538   983
12    1296   751   510  5656
13    1358   920   521  5414
14    5028   634   756  1795
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_257
CC  AC: HOCOMOCO_2015_11_23_Human_m44_M01044
CC  id: HOCOMOCO_2015_11_23_Human_m44_M01044
CC  name: cluster_257
CC  version: 
CC  name: cluster_257
CC  description: ymATTAacymaTta
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BSH_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m44_M01044
XX
//
AC  cluster_258
XX
ID  cluster_258
XX
DE  cayTAATTAssytwww
P0       A     C     G     T
1        3    29     7    12
2       25    10    11     5
3        1    31     6    13
4        1     2     0    48
5       49     1     0     1
6       50     0     1     0
7        1     0     0    50
8        0     1    11    39
9       44     0     7     0
10      10    19    15     7
11       4    25    15     7
12       8    25     5    13
13      10    11     4    26
14      21     9     5    16
15      18     7     9    17
16      17    10     8    16
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_258
CC  AC: HOCOMOCO_2015_11_23_Human_m595_M01595
CC  id: HOCOMOCO_2015_11_23_Human_m595_M01595
CC  name: cluster_258
CC  version: 
CC  name: cluster_258
CC  description: cayTAATTAssytwww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: VAX2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m595_M01595
XX
//
AC  cluster_259
XX
ID  cluster_259
XX
DE  sryTTCyrGGAAats
P0       A     C     G     T
1      629  1515  1329   606
2     1605   641  1022   811
3      493  1053   492  2041
4       20    66    46  3947
5       70    53   248  3708
6      620  3312    50    97
7      207  2650   162  1060
8     1974     0  2105     0
9      595    25  3401    58
10     103    30  3691   255
11    3974    56    34    15
12    4011    27    25    16
13    1993   436   933   717
14     651   873   821  1734
15     770  1116  1663   530
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_259
CC  AC: HOCOMOCO_2015_11_23_Human_m541_M01541
CC  id: HOCOMOCO_2015_11_23_Human_m541_M01541
CC  name: cluster_259
CC  version: 
CC  name: cluster_259
CC  description: sryTTCyrGGAAats
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: STAT1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m541_M01541
XX
//
AC  cluster_26
XX
ID  cluster_26
XX
DE  tGGCACGTGyCr
P0       A     C     G     T
1    506.667 560.667 410.333  1134
2    96.3333    86 2335.67 93.6667
3      101 10.3333 2489.67 10.6667
4    3.33333  2595    11 2.33333
5    2605.67 0.333333     4 1.66667
6    0.333333 2605.33 4.66667 1.33333
7    0.333333 0.333333 2610.67 0.333333
8    0.333333 0.333333 12.6667 2598.33
9    2.66667 2.33333 2603.67     3
10      22 1481.33 121.667 986.667
11     136 2152.33 171.333   152
12   797.333   236 1285.33   293
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_26
CC  AC: cluster_26
CC  id: cluster_26
CC  name: cluster_26
CC  version: 
CC  name: cluster_26
CC  description: tGGCACGTGyCr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tGGCACGTGcCg
CC  consensus.strict.rc: CGGCACGTGCCA
CC  consensus.IUPAC: tGGCACGTGyCr
CC  consensus.IUPAC.rc: YGRCACGTGCCA
CC  consensus.regexp: tGGCACGTG[ct]C[ag]
CC  consensus.regexp.rc: [CT]G[AG]CACGTGCCA
CC  merged_ID: HEY1_D,HES5_D,HES7_D
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m208_M01208,HOCOMOCO_2015_11_23_Human_m205_M01205,HOCOMOCO_2015_11_23_Human_m206_M01206
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_260
XX
ID  cluster_260
XX
DE  haTTGTTTATty
P0       A     C     G     T
1        4     4     1     4
2        9     1     3     0
3        1     2     0    10
4        0     0     0    13
5        0     0    13     0
6        0     0     0    13
7        0     0     0    13
8        0     0     0    13
9       13     0     0     0
10       0     0     1    12
11       2     0     3     8
12       0     4     0     9
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_260
CC  AC: HOCOMOCO_2015_11_23_Human_m169_M01169
CC  id: HOCOMOCO_2015_11_23_Human_m169_M01169
CC  name: cluster_260
CC  version: 
CC  name: cluster_260
CC  description: haTTGTTTATty
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXQ1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m169_M01169
XX
//
AC  cluster_261
XX
ID  cluster_261
XX
DE  caaTGGAAAwTt
P0       A     C     G     T
1      168   289   124   129
2      342   173    89   106
3      435    36   137   102
4       94   101    15   500
5        0     0   710     0
6        0     0   710     0
7      709     0     1     0
8      710     0     0     0
9      517    53    15   125
10     340    46    70   254
11     136    28    19   527
12      68   142    58   442
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_261
CC  AC: HOCOMOCO_2015_11_23_Human_m356_M01356
CC  id: HOCOMOCO_2015_11_23_Human_m356_M01356
CC  name: cluster_261
CC  version: 
CC  name: cluster_261
CC  description: caaTGGAAAwTt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NFAC1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m356_M01356
XX
//
AC  cluster_262
XX
ID  cluster_262
XX
DE  GTTGCyagGsra
P0       A     C     G     T
1        1     1    22     0
2        2     0     0    22
3        0     0     2    22
4        3     1    19     1
5        1    23     0     0
6        0    10     4    10
7       15     2     3     4
8        2     5    12     5
9        3     1    20     0
10       0     8    12     4
11      13     3     7     1
12      15     3     5     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_262
CC  AC: HOCOMOCO_2015_11_23_Human_m482_M01482
CC  id: HOCOMOCO_2015_11_23_Human_m482_M01482
CC  name: cluster_262
CC  version: 
CC  name: cluster_262
CC  description: GTTGCyagGsra
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: RFX1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m482_M01482
XX
//
AC  cluster_263
XX
ID  cluster_263
XX
DE  tcGTGGGGGGTmk
P0       A     C     G     T
1       92    91    59   250
2       53   310    47    82
3       30    21   417    24
4       22    94    21   355
5       15    14   446    17
6       15    15   449    13
7        7     6   455    24
8       18    10   448    16
9        7     9   467     9
10       9     9   464    10
11      10     7    21   454
12     149   275    43    25
13     103    54   154   181
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_263
CC  AC: HOCOMOCO_2015_11_23_Human_m192_M01192
CC  id: HOCOMOCO_2015_11_23_Human_m192_M01192
CC  name: cluster_263
CC  version: 
CC  name: cluster_263
CC  description: tcGTGGGGGGTmk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: GLIS1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m192_M01192
XX
//
AC  cluster_264
XX
ID  cluster_264
XX
DE  grAATbyCCCt
P0       A     C     G     T
1        0     2     7     2
2        4     0     7     0
3       10     0     0     1
4        8     0     1     2
5        2     1     0     8
6        1     4     3     3
7        1     4     0     6
8        0    10     1     0
9        1    10     0     0
10       2     9     0     0
11       2     1     1     7
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_264
CC  AC: HOCOMOCO_2015_11_23_Human_m368_M01368
CC  id: HOCOMOCO_2015_11_23_Human_m368_M01368
CC  name: cluster_264
CC  version: 
CC  name: cluster_264
CC  description: grAATbyCCCt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NFKB2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m368_M01368
XX
//
AC  cluster_265
XX
ID  cluster_265
XX
DE  gGGATTAAggg
P0       A     C     G     T
1      723   400  1322   737
2      233    58  2831    60
3       51    19  3077    35
4     2992   149    18    23
5        2     3    29  3148
6        5    10     8  3159
7     3148     5     8    21
8     2170   116   579   317
9      743   785  1102   552
10     682   393  1444   663
11     574   506  1533   569
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_265
CC  AC: HOCOMOCO_2015_11_23_Human_m436_M01436
CC  id: HOCOMOCO_2015_11_23_Human_m436_M01436
CC  name: cluster_265
CC  version: 
CC  name: cluster_265
CC  description: gGGATTAAggg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PITX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m436_M01436
XX
//
AC  cluster_266
XX
ID  cluster_266
XX
DE  raaagtGAAAGtGAAAsyra
P0       A     C     G     T
1       86    49   113    46
2      167    32    67    28
3      192    19    48    35
4      191    30    51    22
5       58    68   128    40
6       66    66    66    96
7       61     4   223     6
8      273     3    14     4
9      276     7     9     2
10     282     4     4     4
11      35    50   207     2
12      31    64     7   192
13       9     4   281     0
14     252     7    32     3
15     269     9    14     2
16     280     2     9     3
17       5   103   180     6
18      19    74    16   185
19     111    39   117    27
20     139    56    73    26
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_266
CC  AC: HOCOMOCO_2015_11_23_Human_m267_M01267
CC  id: HOCOMOCO_2015_11_23_Human_m267_M01267
CC  name: cluster_266
CC  version: 
CC  name: cluster_266
CC  description: raaagtGAAAGtGAAAsyra
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: IRF1_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m267_M01267
XX
//
AC  cluster_267
XX
ID  cluster_267
XX
DE  rrgArGTGwkAr
P0       A     C     G     T
1      173    40   108    54
2       95    19   175    86
3       83    71   167    54
4      313     3    26    33
5      100     0   254    21
6        8    31   332     4
7        9     1     6   359
8        0     2   372     1
9      136    68    10   161
10      20    63   129   163
11     337     3    12    23
12     246    13    99    17
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_267
CC  AC: HOCOMOCO_2015_11_23_Human_m112_M01112
CC  id: HOCOMOCO_2015_11_23_Human_m112_M01112
CC  name: cluster_267
CC  version: 
CC  name: cluster_267
CC  description: rrgArGTGwkAr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: EOMES_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m112_M01112
XX
//
AC  cluster_268
XX
ID  cluster_268
XX
DE  rTtAwTwATTw
P0       A     C     G     T
1        4     1     2     0
2        0     0     0     7
3        1     0     1     5
4        6     0     1     0
5        4     0     0     3
6        0     0     0     7
7        4     1     0     2
8        7     0     0     0
9        0     1     0     6
10       0     0     0     7
11       5     0     0     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_268
CC  AC: HOCOMOCO_2015_11_23_Human_m615_M01615
CC  id: HOCOMOCO_2015_11_23_Human_m615_M01615
CC  name: cluster_268
CC  version: 
CC  name: cluster_268
CC  description: rTtAwTwATTw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZFHX3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m615_M01615
XX
//
AC  cluster_269
XX
ID  cluster_269
XX
DE  ArwTTwATkr
P0       A     C     G     T
1        5     1     0     0
2        3     0     3     0
3        2     1     0     3
4        1     0     0     5
5        0     0     0     6
6        2     1     1     2
7        6     0     0     0
8        0     0     0     6
9        0     0     3     3
10       2     1     2     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_269
CC  AC: HOCOMOCO_2015_11_23_Human_m263_M01263
CC  id: HOCOMOCO_2015_11_23_Human_m263_M01263
CC  name: cluster_269
CC  version: 
CC  name: cluster_269
CC  description: ArwTTwATkr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXD9_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m263_M01263
XX
//
AC  cluster_27
XX
ID  cluster_27
XX
DE  rgrCAtGyytGgrCwTGyyy
P0       A     C     G     T
1    141.667 54.6667 207.333    61
2    104.333 22.6667 259.333 78.3333
3    185.667 13.3333 242.667    23
4    6.33333 438.667     7 12.6667
5    349.333    28    33 54.3333
6      108 11.6667 52.3333 292.667
7    1.66667 2.66667 455.667 4.66667
8    43.6667 165.333    12 243.667
9    48.3333 263.667    20 132.667
10   88.6667 10.3333 92.3333 273.333
11      41 42.3333 364.333    17
12   94.3333 46.6667 261.667    62
13   227.667     4 200.333 32.6667
14   9.66667 451.667 0.666667 2.66667
15   276.667 40.3333    10 137.667
16   54.6667    31 37.6667 341.333
17   19.3333 3.33333 438.333 3.66667
18      45 197.667     9   213
19      78 231.667 35.6667 119.333
20   60.6667 104.667    45   123
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_27
CC  AC: cluster_27
CC  id: cluster_27
CC  name: cluster_27
CC  version: 
CC  name: cluster_27
CC  description: rgrCAtGyytGgrCwTGyyy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gggCAtGtctGgaCaTGtct
CC  consensus.strict.rc: AGACATGTCCAGACATGCCC
CC  consensus.IUPAC: rgrCAtGyytGgrCwTGyyy
CC  consensus.IUPAC.rc: RRRCAWGYCCARRCATGYCY
CC  consensus.regexp: [ag]g[ag]CAtG[ct][ct]tGg[ag]C[at]TG[ct][ct][ct]
CC  consensus.regexp.rc: [AG][AG][AG]CA[AT]G[CT]CCA[AG][AG]CATG[CT]C[CT]
CC  merged_ID: P63_S,P53_B,P73_S
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m414_M01414,HOCOMOCO_2015_11_23_Human_m412_M01412,HOCOMOCO_2015_11_23_Human_m416_M01416
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_270
XX
ID  cluster_270
XX
DE  CmGTTrr
P0       A     C     G     T
1        4    50     0     0
2       33    16     4     1
3        0     0    54     0
4        0     0     0    54
5        0     0     0    54
6       16     9    25     4
7       14     8    21    11
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_270
CC  AC: HOCOMOCO_2015_11_23_Human_m343_M01343
CC  id: HOCOMOCO_2015_11_23_Human_m343_M01343
CC  name: cluster_270
CC  version: 
CC  name: cluster_270
CC  description: CmGTTrr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MYB_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m343_M01343
XX
//
AC  cluster_271
XX
ID  cluster_271
XX
DE  tyAAGTG
P0       A     C     G     T
1        7     4    10    38
2        3    26     9    21
3       58     0     1     0
4       54     1     2     2
5        0     1    51     7
6        2     0     1    56
7        5     0    53     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_271
CC  AC: HOCOMOCO_2015_11_23_Human_m376_M01376
CC  id: HOCOMOCO_2015_11_23_Human_m376_M01376
CC  name: cluster_271
CC  version: 
CC  name: cluster_271
CC  description: tyAAGTG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NKX25_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m376_M01376
XX
//
AC  cluster_272
XX
ID  cluster_272
XX
DE  kwytwdtwTGTTTACTTAwm
P0       A     C     G     T
1        1     1     2     2
2        2     1     1     2
3        1     2     1     2
4        1     1     1     3
5        3     0     0     3
6        2     0     2     2
7        1     1     1     3
8        4     0     0     2
9        0     0     0     6
10       1     0     5     0
11       0     0     0     6
12       0     0     0     6
13       0     0     1     5
14       5     0     1     0
15       0     5     0     1
16       1     0     0     5
17       0     0     0     6
18       6     0     0     0
19       4     0     0     2
20       3     2     1     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_272
CC  AC: HOCOMOCO_2015_11_23_Human_m134_M01134
CC  id: HOCOMOCO_2015_11_23_Human_m134_M01134
CC  name: cluster_272
CC  version: 
CC  name: cluster_272
CC  description: kwytwdtwTGTTTACTTAwm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: EVX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m134_M01134
XX
//
AC  cluster_273
XX
ID  cluster_273
XX
DE  wawgTAAATrTTwwytTWa
P0       A     C     G     T
1      283    73    72   540
2      573    71   240    84
3      326   128   152   362
4      222    73   610    63
5       93    46     1   828
6      716   163     1    88
7      957     4     0     7
8      951     2     0    15
9        0   176     0   792
10     508     0   459     1
11       0     0     5   963
12       0     0     0   968
13     425     0    58   485
14     406     0   162   400
15       1   364    41   562
16     174    86   146   562
17      11    58     9   890
18     679     6     8   275
19     583    39   240   106
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_273
CC  AC: HOCOMOCO_2015_11_23_Human_m150_M01150
CC  id: HOCOMOCO_2015_11_23_Human_m150_M01150
CC  name: cluster_273
CC  version: 
CC  name: cluster_273
CC  description: wawgTAAATrTTwwytTWa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXD2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m150_M01150
XX
//
AC  cluster_274
XX
ID  cluster_274
XX
DE  mmgvCACGmGmcm
P0       A     C     G     T
1        9    10     6     3
2       12    13     1     2
3        2     5    19     2
4        7    12     7     2
5        0    28     0     0
6       27     0     0     1
7        4    20     4     0
8        0     3    20     5
9       13     9     3     3
10       2     0    25     1
11      10    13     5     0
12       5    16     3     4
13      10    12     4     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_274
CC  AC: HOCOMOCO_2015_11_23_Human_m204_M01204
CC  id: HOCOMOCO_2015_11_23_Human_m204_M01204
CC  name: cluster_274
CC  version: 
CC  name: cluster_274
CC  description: mmgvCACGmGmcm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HES1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m204_M01204
XX
//
AC  cluster_275
XX
ID  cluster_275
XX
DE  gGsawaCAGGAAry
P0       A     C     G     T
1        2     2     7     3
2        2     1    10     1
3        2     7     5     0
4        7     2     2     3
5        8     2     0     4
6        7     3     1     3
7        2    12     0     0
8       13     0     1     0
9        0     0    14     0
10       0     0    14     0
11      14     0     0     0
12      12     0     0     2
13       7     2     5     0
14       0     6     0     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_275
CC  AC: HOCOMOCO_2015_11_23_Human_m104_M01104
CC  id: HOCOMOCO_2015_11_23_Human_m104_M01104
CC  name: cluster_275
CC  version: 
CC  name: cluster_275
CC  description: gGsawaCAGGAAry
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ELF3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m104_M01104
XX
//
AC  cluster_276
XX
ID  cluster_276
XX
DE  arAAWsmGGAAs
P0       A     C     G     T
1       38    16    17    11
2       43     9    24     6
3       70     0     6     6
4       76     0     0     6
5       59     0     0    23
6       10    27    43     2
7       41    25    14     2
8        5     0    77     0
9        1     0    81     0
10      82     0     0     0
11      82     0     0     0
12       5    26    47     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_276
CC  AC: HOCOMOCO_2015_11_23_Human_m531_M01531
CC  id: HOCOMOCO_2015_11_23_Human_m531_M01531
CC  name: cluster_276
CC  version: 
CC  name: cluster_276
CC  description: arAAWsmGGAAs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SPIB_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m531_M01531
XX
//
AC  cluster_277
XX
ID  cluster_277
XX
DE  aAwTwrrkdTAATT
P0       A     C     G     T
1      628   120   110   243
2     1054    15    15    17
3      358    10    12   721
4       34    11    47  1009
5      518     4   207   372
6      414    77   374   236
7      284   242   365   210
8      141   241   339   380
9      322   127   365   287
10       0    92     0  1009
11    1095     0     6     0
12    1101     0     0     0
13       0     0     1  1100
14       0     0     1  1100
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_277
CC  AC: HOCOMOCO_2015_11_23_Human_m80_M01080
CC  id: HOCOMOCO_2015_11_23_Human_m80_M01080
CC  name: cluster_277
CC  version: 
CC  name: cluster_277
CC  description: aAwTwrrkdTAATT
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: DLX5_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m80_M01080
XX
//
AC  cluster_278
XX
ID  cluster_278
XX
DE  yaTTAATKAgytk
P0       A     C     G     T
1        2     8     1    16
2       14     4     6     3
3        0     5     0    22
4        0     0     0    27
5       27     0     0     0
6       27     0     0     0
7        0     0     0    27
8        0     0    20     7
9       22     0     3     2
10       0     3    18     6
11       3    12     2    10
12       4     5     4    14
13       3     2    11    11
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_278
CC  AC: HOCOMOCO_2015_11_23_Human_m452_M01452
CC  id: HOCOMOCO_2015_11_23_Human_m452_M01452
CC  name: cluster_278
CC  version: 
CC  name: cluster_278
CC  description: yaTTAATKAgytk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PO4F2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m452_M01452
XX
//
AC  cluster_279
XX
ID  cluster_279
XX
DE  rwyAyGCwwawttr
P0       A     C     G     T
1       92    37   105    66
2       87    11    20   182
3        9   103    10   178
4      295     2     2     1
5        7    91     6   196
6        4     1   279    16
7        5   254    11    30
8      186     5     1   108
9      161    13    12   114
10     180    42    60    18
11      98     3    28   171
12      40    38    71   151
13      74    36    22   168
14      78    31   144    47
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_279
CC  AC: HOCOMOCO_2015_11_23_Human_m446_M01446
CC  id: HOCOMOCO_2015_11_23_Human_m446_M01446
CC  name: cluster_279
CC  version: 
CC  name: cluster_279
CC  description: rwyAyGCwwawttr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PO2F3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m446_M01446
XX
//
AC  cluster_28
XX
ID  cluster_28
XX
DE  wGGAAAAwtw
P0       A     C     G     T
1    10.6667 3.33333 5.66667 21.3333
2        0     0 40.6667 0.333333
3        0     0    41     0
4    40.6667     0 0.333333     0
5    39.6667     0 1.33333     0
6    39.6667 0.333333     1     0
7    31.3333     4     3 2.66667
8       12 7.66667     4 17.3333
9    6.33333 9.66667 6.66667 18.3333
10       2     0 1.33333     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_28
CC  AC: cluster_28
CC  id: cluster_28
CC  name: cluster_28
CC  version: 
CC  name: cluster_28
CC  description: wGGAAAAwtw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tGGAAAAttt
CC  consensus.strict.rc: AAATTTTCCA
CC  consensus.IUPAC: wGGAAAAwtw
CC  consensus.IUPAC.rc: WAWTTTTCCW
CC  consensus.regexp: [at]GGAAAA[at]t[at]
CC  consensus.regexp.rc: [AT]A[AT]TTTTCC[AT]
CC  merged_ID: NFAC4_C,NFAC2_B,NFAC3_B
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m360_M01360,HOCOMOCO_2015_11_23_Human_m358_M01358,HOCOMOCO_2015_11_23_Human_m359_M01359
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_280
XX
ID  cluster_280
XX
DE  aTGATTTATTacTTT
P0       A     C     G     T
1        5     0     1     1
2        0     0     0     7
3        0     0     7     0
4        7     0     0     0
5        0     0     0     7
6        0     0     0     7
7        0     0     1     6
8        7     0     0     0
9        0     0     0     7
10       0     0     1     6
11       5     1     0     1
12       0     5     1     1
13       1     0     0     6
14       0     0     0     7
15       0     0     1     6
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_280
CC  AC: HOCOMOCO_2015_11_23_Human_m254_M01254
CC  id: HOCOMOCO_2015_11_23_Human_m254_M01254
CC  name: cluster_280
CC  version: 
CC  name: cluster_280
CC  description: aTGATTTATTacTTT
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXC6_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m254_M01254
XX
//
AC  cluster_281
XX
ID  cluster_281
XX
DE  aAAgTCAAAArtcA
P0       A     C     G     T
1       11     4     4     4
2       18     2     2     1
3       18     1     3     1
4        2     2    16     3
5        2     0     2    19
6        2    20     1     0
7       21     0     1     1
8       18     3     2     0
9       22     0     0     1
10      19     0     3     1
11       6     2    13     2
12       1     2     5    15
13       3    16     2     2
14      17     3     2     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_281
CC  AC: HOCOMOCO_2015_11_23_Human_m395_M01395
CC  id: HOCOMOCO_2015_11_23_Human_m395_M01395
CC  name: cluster_281
CC  version: 
CC  name: cluster_281
CC  description: aAAgTCAAAArtcA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NR2E3_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m395_M01395
XX
//
AC  cluster_282
XX
ID  cluster_282
XX
DE  mbsyGGCwmrTrrmmt
P0       A     C     G     T
1        4     2     1     1
2        0     2     3     3
3        0     4     4     0
4        1     4     0     3
5        0     0     8     0
6        1     0     6     1
7        1     7     0     0
8        4     1     0     3
9        3     5     0     0
10       2     1     4     1
11       0     1     0     7
12       3     0     4     1
13       3     1     4     0
14       2     4     1     1
15       3     3     1     1
16       1     1     1     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_282
CC  AC: HOCOMOCO_2015_11_23_Human_m207_M01207
CC  id: HOCOMOCO_2015_11_23_Human_m207_M01207
CC  name: cluster_282
CC  version: 
CC  name: cluster_282
CC  description: mbsyGGCwmrTrrmmt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HESX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m207_M01207
XX
//
AC  cluster_283
XX
ID  cluster_283
XX
DE  GbbsGCwCGTGsChybv
P0       A     C     G     T
1        0     0     7     1
2        0     2     4     2
3        0     2     3     3
4        0     2     5     1
5        1     0     7     0
6        0     7     0     1
7        4     0     1     3
8        0     7     1     0
9        0     1     7     0
10       0     1     0     7
11       0     0     8     0
12       1     2     5     0
13       1     6     1     0
14       3     2     1     2
15       0     2     1     5
16       0     2     3     3
17       3     2     2     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_283
CC  AC: HOCOMOCO_2015_11_23_Human_m209_M01209
CC  id: HOCOMOCO_2015_11_23_Human_m209_M01209
CC  name: cluster_283
CC  version: 
CC  name: cluster_283
CC  description: GbbsGCwCGTGsChybv
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HEY2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m209_M01209
XX
//
AC  cluster_284
XX
ID  cluster_284
XX
DE  TyGTrtkTTttT
P0       A     C     G     T
1        0    17     3    82
2        0    49     2    51
3        0     0   100     2
4        0     0     3    99
5       29    19    31    23
6       13    22    11    56
7        8     8    26    60
8        2     9     9    82
9        6     9     9    78
10       7    11    19    65
11      11    13    18    60
12      10     9    12    71
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_284
CC  AC: HOCOMOCO_2015_11_23_Human_m170_M01170
CC  id: HOCOMOCO_2015_11_23_Human_m170_M01170
CC  name: cluster_284
CC  version: 
CC  name: cluster_284
CC  description: TyGTrtkTTttT
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FUBP1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m170_M01170
XX
//
AC  cluster_285
XX
ID  cluster_285
XX
DE  gsssssgsgsvrggGGCGGGrv
P0       A     C     G     T
1      100   122   242    36
2       95   142   214    49
3       60   130   259    51
4      117   130   210    43
5       89   152   227    32
6       86   174   199    41
7       78    94   245    83
8       81   150   228    41
9       87   113   242    58
10      96   136   224    44
11     131   148   188    33
12     136    70   249    45
13     101    56   282    61
14     112    72   269    47
15      11    50   433     6
16       2     7   486     5
17      29   453     1    17
18       3     7   476    14
19      10     6   475     9
20      64    21   405    10
21     298    20   168    14
22     197   163   131     9
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_285
CC  AC: HOCOMOCO_2015_11_23_Human_m572_M01572
CC  id: HOCOMOCO_2015_11_23_Human_m572_M01572
CC  name: cluster_285
CC  version: 
CC  name: cluster_285
CC  description: gsssssgsgsvrggGGCGGGrv
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TFDP1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m572_M01572
XX
//
AC  cluster_286
XX
ID  cluster_286
XX
DE  ghsmGGAagwway
P0       A     C     G     T
1        3     3     8     1
2        7     4     0     4
3        2     9     4     0
4       10     4     0     1
5        2     0    13     0
6        0     1    14     0
7       13     2     0     0
8        9     2     2     2
9        1     3    10     1
10       4     2     2     7
11       9     2     0     4
12      10     2     1     2
13       1     7     1     6
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_286
CC  AC: HOCOMOCO_2015_11_23_Human_m130_M01130
CC  id: HOCOMOCO_2015_11_23_Human_m130_M01130
CC  name: cluster_286
CC  version: 
CC  name: cluster_286
CC  description: ghsmGGAagwway
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ETV5_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m130_M01130
XX
//
AC  cluster_287
XX
ID  cluster_287
XX
DE  rwcCmGGAwgwagt
P0       A     C     G     T
1      658   304   612   315
2      985   186    72   646
3      434   932   430    93
4        9  1864     8     8
5      710  1170     6     3
6        0     0  1889     0
7      189    34  1377   289
8     1887     0     0     2
9      761     1     1  1126
10     326   275  1034   254
11    1042   179   118   550
12     866   371   251   401
13     337   388   925   239
14     349   321   319   900
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_287
CC  AC: HOCOMOCO_2015_11_23_Human_m529_M01529
CC  id: HOCOMOCO_2015_11_23_Human_m529_M01529
CC  name: cluster_287
CC  version: 
CC  name: cluster_287
CC  description: rwcCmGGAwgwagt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SPDEF_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m529_M01529
XX
//
AC  cluster_288
XX
ID  cluster_288
XX
DE  trACCGTTaam
P0       A     C     G     T
1     1036  1068   408  1816
2     2027   174  1575   552
3     3291    86   631   320
4       30  4273     8    17
5      450  3426   451     1
6        0     0  4328     0
7        0     0     0  4328
8      146   167   130  3885
9     2925   245   538   620
10    2622   566   489   651
11    1752  1296   498   782
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_288
CC  AC: HOCOMOCO_2015_11_23_Human_m341_M01341
CC  id: HOCOMOCO_2015_11_23_Human_m341_M01341
CC  name: cluster_288
CC  version: 
CC  name: cluster_288
CC  description: trACCGTTaam
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MYBA_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m341_M01341
XX
//
AC  cluster_289
XX
ID  cluster_289
XX
DE  kGTAACkGw
P0       A     C     G     T
1        0     1     2     4
2        1     0     6     0
3        0     0     1     6
4        7     0     0     0
5        6     1     0     0
6        1     6     0     0
7        1     0     2     4
8        1     0     6     0
9        2     0     0     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_289
CC  AC: HOCOMOCO_2015_11_23_Human_m411_M01411
CC  id: HOCOMOCO_2015_11_23_Human_m411_M01411
CC  name: cluster_289
CC  version: 
CC  name: cluster_289
CC  description: kGTAACkGw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: OVOL1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m411_M01411
XX
//
AC  cluster_29
XX
ID  cluster_29
XX
DE  awstrGGkcAAAGGkcA
P0       A     C     G     T
1    236.5    83  49.5    83
2    242.5  19.5    48   142
3     59.5 138.5 172.5  81.5
4       54  64.5  65.5   268
5    182.5    16 211.5    42
6       64   3.5   350  34.5
7     82.5    15 327.5    27
8       83  54.5   170 144.5
9       54   243   110    45
10     428    10   8.5   5.5
11   311.5  16.5 105.5  18.5
12     377   2.5  70.5     2
13      68   2.5 370.5    11
14      39   8.5 341.5    63
15      15  59.5   121 256.5
16      62   251    76    63
17     350  26.5    35  40.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_29
CC  AC: cluster_29
CC  id: cluster_29
CC  name: cluster_29
CC  version: 
CC  name: cluster_29
CC  description: awstrGGkcAAAGGkcA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aagtgGGgcAAAGGtcA
CC  consensus.strict.rc: TGACCTTTGCCCCACTT
CC  consensus.IUPAC: awstrGGkcAAAGGkcA
CC  consensus.IUPAC.rc: TGMCCTTTGMCCYASWT
CC  consensus.regexp: a[at][cg]t[ag]GG[gt]cAAAGG[gt]cA
CC  consensus.regexp.rc: TG[AC]CCTTTG[AC]CC[CT]A[CG][AT]T
CC  merged_ID: PPARG_S,RXRA_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m460_M01460,HOCOMOCO_2015_11_23_Human_m494_M01494
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_290
XX
ID  cluster_290
XX
DE  cGcCyGrGGCgCGt
P0       A     C     G     T
1        1     2     1     1
2        0     0     5     0
3        0     3     1     1
4        0     5     0     0
5        0     3     0     2
6        0     0     5     0
7        2     1     2     0
8        0     0     5     0
9        0     0     5     0
10       0     4     1     0
11       1     1     2     1
12       0     4     1     0
13       0     1     4     0
14       1     1     0     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_290
CC  AC: HOCOMOCO_2015_11_23_Human_m10_M01010
CC  id: HOCOMOCO_2015_11_23_Human_m10_M01010
CC  name: cluster_290
CC  version: 
CC  name: cluster_290
CC  description: cGcCyGrGGCgCGt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: AP2D_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m10_M01010
XX
//
AC  cluster_291
XX
ID  cluster_291
XX
DE  GsCyGvGGgs
P0       A     C     G     T
1        1     0    22     5
2        0    18     7     3
3        0    27     0     1
4        3    16     0     9
5        2     1    25     0
6        9     9    10     0
7        2     1    24     1
8        0     0    28     0
9        5     3    19     1
10       6    12     7     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_291
CC  AC: HOCOMOCO_2015_11_23_Human_m8_M01008
CC  id: HOCOMOCO_2015_11_23_Human_m8_M01008
CC  name: cluster_291
CC  version: 
CC  name: cluster_291
CC  description: GsCyGvGGgs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: AP2B_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m8_M01008
XX
//
AC  cluster_292
XX
ID  cluster_292
XX
DE  GACAGcTGTCAtwry
P0       A     C     G     T
1        1     1    10     2
2       11     1     2     0
3        2    10     1     1
4       12     1     1     0
5        2     0    12     0
6        3     8     2     1
7        0     0     0    14
8        2     0    12     0
9        0     0     0    14
10       2    10     0     2
11      11     0     1     2
12       2     2     3     7
13       7     1     2     4
14       5     0     8     1
15       1     4     1     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_292
CC  AC: HOCOMOCO_2015_11_23_Human_m566_M01566
CC  id: HOCOMOCO_2015_11_23_Human_m566_M01566
CC  name: cluster_292
CC  version: 
CC  name: cluster_292
CC  description: GACAGcTGTCAtwry
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TF2LX_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m566_M01566
XX
//
AC  cluster_293
XX
ID  cluster_293
XX
DE  rwATTtt
P0       A     C     G     T
1       25    17    33    19
2       30     8     0    56
3       79     4     0    11
4        0     0     0    94
5        2     1     0    91
6       23     6     9    56
7       19    11    11    53
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_293
CC  AC: HOCOMOCO_2015_11_23_Human_m219_M01219
CC  id: HOCOMOCO_2015_11_23_Human_m219_M01219
CC  name: cluster_293
CC  version: 
CC  name: cluster_293
CC  description: rwATTtt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HMGA1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m219_M01219
XX
//
AC  cluster_294
XX
ID  cluster_294
XX
DE  gscCAAGGtCAsas
P0       A     C     G     T
1       75    68   284    73
2       64   156   214    66
3       26   265   108   101
4       58   357    80     5
5      364    77    56     3
6      410     4    82     4
7       12     1   480     7
8        3    10   483     4
9       35    77   105   283
10       2   471    19     8
11     406    23    62     9
12      35   241   184    40
13     204   115   120    61
14      42   145   263    50
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_294
CC  AC: HOCOMOCO_2015_11_23_Human_m116_M01116
CC  id: HOCOMOCO_2015_11_23_Human_m116_M01116
CC  name: cluster_294
CC  version: 
CC  name: cluster_294
CC  description: gscCAAGGtCAsas
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ERR1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m116_M01116
XX
//
AC  cluster_295
XX
ID  cluster_295
XX
DE  wrcTrGTTAmy
P0       A     C     G     T
1       53    20    37    47
2       53    29    50    25
3       11    89    18    39
4        1     1     3   152
5       94     0    62     1
6        0     0   157     0
7        0     0     6   151
8       18     0     2   137
9      156     0     1     0
10      56    57    26    18
11      28    47    38    44
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_295
CC  AC: HOCOMOCO_2015_11_23_Human_m216_M01216
CC  id: HOCOMOCO_2015_11_23_Human_m216_M01216
CC  name: cluster_295
CC  version: 
CC  name: cluster_295
CC  description: wrcTrGTTAmy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HMBX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m216_M01216
XX
//
AC  cluster_296
XX
ID  cluster_296
XX
DE  aArGGGTTAAw
P0       A     C     G     T
1      139    24    36    36
2      173    16    24    22
3      117    20    70    28
4       13    12   187    23
5       24     0   204     7
6        6     2   221     6
7        5     8     5   217
8       11     5    10   209
9      213     6     7     9
10     204    10    10    11
11      61    44    29   101
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_296
CC  AC: HOCOMOCO_2015_11_23_Human_m636_M01636
CC  id: HOCOMOCO_2015_11_23_Human_m636_M01636
CC  name: cluster_296
CC  version: 
CC  name: cluster_296
CC  description: aArGGGTTAAw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN652_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m636_M01636
XX
//
AC  cluster_297
XX
ID  cluster_297
XX
DE  wwGATWa
P0       A     C     G     T
1       30    27    27    30
2       57    12     8    37
3        1     0   113     0
4      112     1     0     1
5        1     0     1   112
6       80     0     1    33
7       74    12    17    11
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_297
CC  AC: HOCOMOCO_2015_11_23_Human_m180_M01180
CC  id: HOCOMOCO_2015_11_23_Human_m180_M01180
CC  name: cluster_297
CC  version: 
CC  name: cluster_297
CC  description: wwGATWa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: GATA6_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m180_M01180
XX
//
AC  cluster_298
XX
ID  cluster_298
XX
DE  TaaAGGTCAAAGGTCAAsk
P0       A     C     G     T
1        1     1     1    16
2       13     4     0     2
3       13     0     4     2
4       15     0     4     0
5        0     0    19     0
6        0     0    18     1
7        0     0     0    19
8        0    19     0     0
9       19     0     0     0
10      19     0     0     0
11      19     0     0     0
12       0     0    19     0
13       0     0    19     0
14       0     0     0    19
15       0    19     0     0
16      19     0     0     0
17      15     0     2     2
18       0    12     5     2
19       0     1    12     6
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_298
CC  AC: HOCOMOCO_2015_11_23_Human_m386_M01386
CC  id: HOCOMOCO_2015_11_23_Human_m386_M01386
CC  name: cluster_298
CC  version: 
CC  name: cluster_298
CC  description: TaaAGGTCAAAGGTCAAsk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NR1H2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m386_M01386
XX
//
AC  cluster_299
XX
ID  cluster_299
XX
DE  tTGCTGAc
P0       A     C     G     T
1       12     9    15    27
2        4     6     4    49
3        1     3    59     0
4        3    56     3     1
5        3     2     0    58
6        2     0    60     1
7       55     0     4     4
8        6    39    10     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_299
CC  AC: HOCOMOCO_2015_11_23_Human_m314_M01314
CC  id: HOCOMOCO_2015_11_23_Human_m314_M01314
CC  name: cluster_299
CC  version: 
CC  name: cluster_299
CC  description: tTGCTGAc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MAF_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m314_M01314
XX
//
AC  cluster_3
XX
ID  cluster_3
XX
DE  sykcTsATTGGyygg
P0       A     C     G     T
1    0.666667 1.33333 1.66667 0.666667
2    96.8333 303.833 90.3333 256.5
3    53.3333 149.167 220.167 329.167
4    45.1667 402.333 134.333   170
5    25.1667   181 27.8333 517.833
6    21.3333   200 484.333 46.1667
7    735.833 3.33333 3.83333 8.83333
8    0.833333 11.1667 28.3333 711.5
9      2.5 0.833333 4.66667 743.833
10   0.333333 2.16667 744.333     5
11     116     4 631.333   0.5
12    73.5 396.667 29.3333 252.333
13      70 217.167 87.1667 377.5
14   164.667 105.167 342.5 109.167
15   70.8333 55.1667 131.833 49.8333
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_3
CC  AC: cluster_3
CC  id: cluster_3
CC  name: cluster_3
CC  version: 
CC  name: cluster_3
CC  description: sykcTsATTGGyygg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gctcTgATTGGctgg
CC  consensus.strict.rc: CCAGCCAATCAGAGC
CC  consensus.IUPAC: sykcTsATTGGyygg
CC  consensus.IUPAC.rc: CCRRCCAATSAGMRS
CC  consensus.regexp: [cg][ct][gt]cT[cg]ATTGG[ct][ct]gg
CC  consensus.regexp.rc: CC[AG][AG]CCAAT[CG]AG[AC][AG][CG]
CC  merged_ID: PBX3_B,NFYC_B,NFYA_A,NFYB_A,FOXI1_B,CEBPZ_D
CC  merge_nb: 6
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m430_M01430,HOCOMOCO_2015_11_23_Human_m371_M01371,HOCOMOCO_2015_11_23_Human_m369_M01369,HOCOMOCO_2015_11_23_Human_m370_M01370,HOCOMOCO_2015_11_23_Human_m156_M01156,HOCOMOCO_2015_11_23_Human_m53_M01053
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_30
XX
ID  cluster_30
XX
DE  ktyTGTGGTTwk
P0       A     C     G     T
1    28.3333    35 50.6667 52.6667
2    44.3333    83 83.3333 130.333
3       28 147.667    22 143.333
4    9.33333 4.66667 3.33333 323.667
5    0.333333 2.33333   336 2.33333
6    7.33333 59.6667     2   272
7    1.33333     0 339.333 0.333333
8    0.333333     0 338.333 2.33333
9    0.333333 19.6667 11.3333 309.667
10      10    73 21.3333 236.667
11   116.333     8    37 179.667
12   24.6667 42.6667    63    44
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_30
CC  AC: cluster_30
CC  id: cluster_30
CC  name: cluster_30
CC  version: 
CC  name: cluster_30
CC  description: ktyTGTGGTTwk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ttcTGTGGTTtg
CC  consensus.strict.rc: CAAACCACAGAA
CC  consensus.IUPAC: ktyTGTGGTTwk
CC  consensus.IUPAC.rc: MWAACCACARAM
CC  consensus.regexp: [gt]t[ct]TGTGGTT[at][gt]
CC  consensus.regexp.rc: [AC][AT]AACCACA[AG]A[AC]
CC  merged_ID: RUNX3_C,PEBB_C,RUNX1_A
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m493_M01493,HOCOMOCO_2015_11_23_Human_m432_M01432,HOCOMOCO_2015_11_23_Human_m491_M01491
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_300
XX
ID  cluster_300
XX
DE  kwkmwrGGATTArak
P0       A     C     G     T
1        4     3    10     8
2        8     5     3     9
3        6     2     8     9
4       14     7     4     0
5       11     4     3     7
6        7     6    10     2
7        2     4    18     1
8        3     0    22     0
9       24     1     0     0
10       0     0     0    25
11       1     0     0    24
12      25     0     0     0
13      12     2     9     2
14      12     4     6     3
15       4     4     9     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_300
CC  AC: HOCOMOCO_2015_11_23_Human_m410_M01410
CC  id: HOCOMOCO_2015_11_23_Human_m410_M01410
CC  name: cluster_300
CC  version: 
CC  name: cluster_300
CC  description: kwkmwrGGATTArak
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: OTX2_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m410_M01410
XX
//
AC  cluster_301
XX
ID  cluster_301
XX
DE  tyykrTTTtyt
P0       A     C     G     T
1      174   188   174   371
2      174   270   186   277
3      138   365   148   256
4       94   148   360   305
5      291   125   491     0
6        0     0     0   907
7        0     0     0   907
8      106    43    56   702
9      175   213   132   387
10     190   238   179   300
11     216   149   221   321
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_301
CC  AC: HOCOMOCO_2015_11_23_Human_m127_M01127
CC  id: HOCOMOCO_2015_11_23_Human_m127_M01127
CC  name: cluster_301
CC  version: 
CC  name: cluster_301
CC  description: tyykrTTTtyt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ETV2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m127_M01127
XX
//
AC  cluster_302
XX
ID  cluster_302
XX
DE  gsAAAAAvycks
P0       A     C     G     T
1        5     5    26     3
2        0    16    23     0
3       39     0     0     0
4       39     0     0     0
5       39     0     0     0
6       39     0     0     0
7       39     0     0     0
8       14    10    14     1
9        5    12     6    16
10       9    16     7     7
11       4     8    17    10
12       6    13    15     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_302
CC  AC: HOCOMOCO_2015_11_23_Human_m630_M01630
CC  id: HOCOMOCO_2015_11_23_Human_m630_M01630
CC  name: cluster_302
CC  version: 
CC  name: cluster_302
CC  description: gsAAAAAvycks
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN384_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m630_M01630
XX
//
AC  cluster_303
XX
ID  cluster_303
XX
DE  tGtTrCTATwwATAGc
P0       A     C     G     T
1       17    22    18    69
2        6     5   103    12
3       25     4    19    78
4        7     3     5   111
5       55     0    65     6
6        0   125     1     0
7        0     0     0   126
8      125     0     1     0
9       29     0     0    97
10      57     0     1    68
11      68     0     0    58
12     103     0     0    23
13       0     1     0   125
14     126     0     0     0
15       7     6   107     6
16      29    59    14    24
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_303
CC  AC: HOCOMOCO_2015_11_23_Human_m321_M01321
CC  id: HOCOMOCO_2015_11_23_Human_m321_M01321
CC  name: cluster_303
CC  version: 
CC  name: cluster_303
CC  description: tGtTrCTATwwATAGc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MEF2B_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m321_M01321
XX
//
AC  cluster_304
XX
ID  cluster_304
XX
DE  GkGhrTrsGTgkG
P0       A     C     G     T
1        0     1     9     2
2        2     0     3     7
3        2     0    10     0
4        4     4     1     3
5        4     0     8     0
6        0     2     1     9
7        7     0     5     0
8        0     7     4     1
9        0     0    12     0
10       2     0     0    10
11       1     2     8     1
12       1     2     5     4
13       0     1    11     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_304
CC  AC: HOCOMOCO_2015_11_23_Human_m113_M01113
CC  id: HOCOMOCO_2015_11_23_Human_m113_M01113
CC  name: cluster_304
CC  version: 
CC  name: cluster_304
CC  description: GkGhrTrsGTgkG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: EPAS1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m113_M01113
XX
//
AC  cluster_305
XX
ID  cluster_305
XX
DE  gkkrGyrGswGgkk
P0       A     C     G     T
1       17    10    34    19
2       12    10    25    33
3        8     2    48    22
4       33     7    33     7
5       16     1    63     0
6        6    43     2    29
7       35     2    34     9
8        3     1    72     4
9        1    28    49     2
10      20     0    17    43
11       3     1    70     6
12       3    17    47    13
13       7     7    37    29
14      13     8    32    27
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_305
CC  AC: HOCOMOCO_2015_11_23_Human_m17_M01017
CC  id: HOCOMOCO_2015_11_23_Human_m17_M01017
CC  name: cluster_305
CC  version: 
CC  name: cluster_305
CC  description: gkkrGyrGswGgkk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ASCL2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m17_M01017
XX
//
AC  cluster_306
XX
ID  cluster_306
XX
DE  AGGTGtgA
P0       A     C     G     T
1       32     1     2     2
2        9     2    26     0
3        0     0    37     0
4        0     1     0    36
5        0     0    37     0
6        6     1     7    23
7        1     9    24     3
8       28     2     3     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_306
CC  AC: HOCOMOCO_2015_11_23_Human_m560_M01560
CC  id: HOCOMOCO_2015_11_23_Human_m560_M01560
CC  name: cluster_306
CC  version: 
CC  name: cluster_306
CC  description: AGGTGtgA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TBX5_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m560_M01560
XX
//
AC  cluster_307
XX
ID  cluster_307
XX
DE  acakgkkwkgkCrkGTwgk
P0       A     C     G     T
1     7908  3455  4593  3350
2     3373  8284  3950  3699
3     8152  2475  4293  4386
4     2926  3020  5889  7471
5     2510  1522 12369  2905
6     3430  1945  8388  5543
7     2689  4105  6976  5536
8     5520   948  4781  8057
9     1561   761  8641  8343
10    2681   565 12292  3768
11    4337   547  7460  6962
12    1299 14092   893  3022
13    9518    25  8850   913
14      26   114  6437 12729
15      23    20 19248    15
16     984  1086  2504 14732
17    6499  1604  4480  6723
18    4654  4166  7672  2814
19    3664  2654  7996  4992
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_307
CC  AC: HOCOMOCO_2015_11_23_Human_m275_M01275
CC  id: HOCOMOCO_2015_11_23_Human_m275_M01275
CC  name: cluster_307
CC  version: 
CC  name: cluster_307
CC  description: acakgkkwkgkCrkGTwgk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: IRX2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m275_M01275
XX
//
AC  cluster_308
XX
ID  cluster_308
XX
DE  ggwkkggwgrsrcGkGTrrr
P0       A     C     G     T
1        1     1     5     2
2        2     2     5     0
3        3     1     2     3
4        1     2     3     3
5        0     0     6     3
6        1     2     5     1
7        2     0     6     1
8        3     0     2     4
9        2     0     6     1
10       4     1     4     0
11       1     4     4     0
12       3     0     5     1
13       0     6     2     1
14       1     0     8     0
15       0     0     6     3
16       0     0     9     0
17       1     0     1     7
18       4     1     3     1
19       3     1     4     1
20       3     1     3     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_308
CC  AC: HOCOMOCO_2015_11_23_Human_m55_M01055
CC  id: HOCOMOCO_2015_11_23_Human_m55_M01055
CC  name: cluster_308
CC  version: 
CC  name: cluster_308
CC  description: ggwkkggwgrsrcGkGTrrr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: CLOCK_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m55_M01055
XX
//
AC  cluster_309
XX
ID  cluster_309
XX
DE  rTTAyGTAAykymk
P0       A     C     G     T
1       14    10    24     3
2        3     0     1    47
3        1     0     5    45
4       50     0     0     1
5        0    21     2    28
6        8     0    43     0
7        0     2     0    49
8       51     0     0     0
9       50     0     0     1
10       1    16    11    23
11       7     8    21    15
12       8    18     9    16
13      21    19     7     4
14       8    10    13    20
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_309
CC  AC: HOCOMOCO_2015_11_23_Human_m366_M01366
CC  id: HOCOMOCO_2015_11_23_Human_m366_M01366
CC  name: cluster_309
CC  version: 
CC  name: cluster_309
CC  description: rTTAyGTAAykymk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NFIL3_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m366_M01366
XX
//
AC  cluster_31
XX
ID  cluster_31
XX
DE  kGGGtGgtc
P0       A     C     G     T
1    4.33333 5.33333 13.6667 10.6667
2        3 1.33333 28.3333 1.33333
3    0.333333 1.66667 29.6667 2.33333
4    1.33333 1.66667 29.3333 1.66667
5    7.66667 1.33333 6.33333 18.6667
6    4.66667 1.66667 25.6667     2
7        3 5.66667    23 2.33333
8    1.66667 4.66667 5.66667    22
9        5 16.6667 7.66667 4.66667
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_31
CC  AC: cluster_31
CC  id: cluster_31
CC  name: cluster_31
CC  version: 
CC  name: cluster_31
CC  description: kGGGtGgtc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gGGGtGgtc
CC  consensus.strict.rc: GACCACCCC
CC  consensus.IUPAC: kGGGtGgtc
CC  consensus.IUPAC.rc: GACCACCCM
CC  consensus.regexp: [gt]GGGtGgtc
CC  consensus.regexp.rc: GACCACCC[AC]
CC  merged_ID: ZIC3_C,ZIC1_B,ZIC2_C
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m619_M01619,HOCOMOCO_2015_11_23_Human_m617_M01617,HOCOMOCO_2015_11_23_Human_m618_M01618
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_310
XX
ID  cluster_310
XX
DE  TGtTtAyrTaAmtk
P0       A     C     G     T
1        1     2     1    10
2        2     0    11     1
3        3     1     3     7
4        0     0     0    14
5        3     1     3     7
6       13     0     1     0
7        1     4     0     9
8        4     0     9     1
9        0     1     2    11
10       9     3     1     1
11      10     0     2     2
12       6     4     3     1
13       1     3     2     8
14       3     0     6     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_310
CC  AC: HOCOMOCO_2015_11_23_Human_m565_M01565
CC  id: HOCOMOCO_2015_11_23_Human_m565_M01565
CC  name: cluster_310
CC  version: 
CC  name: cluster_310
CC  description: TGtTtAyrTaAmtk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TEF_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m565_M01565
XX
//
AC  cluster_311
XX
ID  cluster_311
XX
DE  arkwdSvvamyyAGGTswkMRrha
P0       A     C     G     T
1        5     1     1     1
2        2     0     5     1
3        0     1     4     3
4        3     0     1     4
5        4     0     2     2
6        0     2     6     0
7        4     2     2     0
8        2     3     3     0
9        5     1     1     1
10       3     3     1     1
11       1     2     0     5
12       0     2     1     5
13       8     0     0     0
14       0     0     8     0
15       1     0     7     0
16       0     0     0     8
17       0     3     5     0
18       4     0     0     4
19       0     0     4     4
20       6     2     0     0
21       6     0     2     0
22       4     1     3     0
23       4     2     0     2
24       5     1     1     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_311
CC  AC: HOCOMOCO_2015_11_23_Human_m558_M01558
CC  id: HOCOMOCO_2015_11_23_Human_m558_M01558
CC  name: cluster_311
CC  version: 
CC  name: cluster_311
CC  description: arkwdSvvamyyAGGTswkMRrha
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TBX3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m558_M01558
XX
//
AC  cluster_312
XX
ID  cluster_312
XX
DE  kttgwkTGwTTwy
P0       A     C     G     T
1        2     3     4     4
2        0     2     2     9
3        3     0     1     9
4        2     1     9     1
5        6     0     0     7
6        0     2     4     7
7        0     1     0    12
8        2     1    10     0
9        4     1     1     7
10       0     0     1    12
11       0     1     2    10
12       4     2     1     6
13       1     6     1     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_312
CC  AC: HOCOMOCO_2015_11_23_Human_m162_M01162
CC  id: HOCOMOCO_2015_11_23_Human_m162_M01162
CC  name: cluster_312
CC  version: 
CC  name: cluster_312
CC  description: kttgwkTGwTTwy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXM1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m162_M01162
XX
//
AC  cluster_313
XX
ID  cluster_313
XX
DE  yrsdTkyAATwrg
P0       A     C     G     T
1        1     3     1     2
2        3     1     2     1
3        1     3     2     1
4        2     0     2     3
5        1     0     0     6
6        1     0     2     4
7        0     3     0     4
8        7     0     0     0
9        7     0     0     0
10       0     0     0     7
11       2     0     1     4
12       5     0     2     0
13       1     1     4     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_313
CC  AC: HOCOMOCO_2015_11_23_Human_m299_M01299
CC  id: HOCOMOCO_2015_11_23_Human_m299_M01299
CC  name: cluster_313
CC  version: 
CC  name: cluster_313
CC  description: yrsdTkyAATwrg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: LHX2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m299_M01299
XX
//
AC  cluster_314
XX
ID  cluster_314
XX
DE  yTTsATTrAtkssc
P0       A     C     G     T
1        1     3     1     4
2        0     0     2     7
3        0     1     0     8
4        1     3     5     0
5        8     0     0     1
6        2     0     0     7
7        2     0     0     7
8        4     0     4     1
9        9     0     0     0
10       1     1     1     6
11       2     0     4     3
12       2     4     3     0
13       1     4     3     1
14       2     3     2     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_314
CC  AC: HOCOMOCO_2015_11_23_Human_m255_M01255
CC  id: HOCOMOCO_2015_11_23_Human_m255_M01255
CC  name: cluster_314
CC  version: 
CC  name: cluster_314
CC  description: yTTsATTrAtkssc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXC8_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m255_M01255
XX
//
AC  cluster_315
XX
ID  cluster_315
XX
DE  gGGkTGCCc
P0       A     C     G     T
1        8     4    20     7
2        1     4    29     5
3        0     0    36     3
4        1     0    14    24
5        0     0     0    39
6        1     0    38     0
7        1    38     0     0
8        0    39     0     0
9        8    23     4     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_315
CC  AC: HOCOMOCO_2015_11_23_Human_m210_M01210
CC  id: HOCOMOCO_2015_11_23_Human_m210_M01210
CC  name: cluster_315
CC  version: 
CC  name: cluster_315
CC  description: gGGkTGCCc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HIC1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m210_M01210
XX
//
AC  cluster_316
XX
ID  cluster_316
XX
DE  crGGGGGCCc
P0       A     C     G     T
1        2     7     2     1
2        4     0     7     1
3        0     0    10     2
4        2     0    10     0
5        1     0    11     0
6        0     0    12     0
7        0     0    12     0
8        0    11     0     1
9        1    10     0     1
10       1     7     2     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_316
CC  AC: HOCOMOCO_2015_11_23_Human_m443_M01443
CC  id: HOCOMOCO_2015_11_23_Human_m443_M01443
CC  name: cluster_316
CC  version: 
CC  name: cluster_316
CC  description: crGGGGGCCc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PLAL1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m443_M01443
XX
//
AC  cluster_317
XX
ID  cluster_317
XX
DE  yTsGCrrGArkw
P0       A     C     G     T
1      258   900   212   640
2        5     1    20  1984
3      295  1059   564    92
4       25    38  1947     0
5        4  1995    10     1
6     1247     4   759     0
7      621     0  1347    42
8       12     7  1777   214
9     1794   214     1     1
10     823   111   690   386
11     361   242   834   573
12     732   379   358   541
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_317
CC  AC: HOCOMOCO_2015_11_23_Human_m286_M01286
CC  id: HOCOMOCO_2015_11_23_Human_m286_M01286
CC  name: cluster_317
CC  version: 
CC  name: cluster_317
CC  description: yTsGCrrGArkw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: KAISO_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m286_M01286
XX
//
AC  cluster_318
XX
ID  cluster_318
XX
DE  wsvrksyCacGTGmwgrrrs
P0       A     C     G     T
1        4     1     2     3
2        0     6     4     0
3        3     3     3     1
4        4     1     5     0
5        1     1     4     4
6        0     4     5     1
7        0     5     0     5
8        0    10     0     0
9        7     0     2     1
10       2     6     1     1
11       0     1     8     1
12       0     0     0    10
13       0     0    10     0
14       3     6     1     0
15       4     2     1     3
16       0     1     7     2
17       5     2     3     0
18       4     1     4     1
19       3     1     6     0
20       2     3     3     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_318
CC  AC: HOCOMOCO_2015_11_23_Human_m39_M01039
CC  id: HOCOMOCO_2015_11_23_Human_m39_M01039
CC  name: cluster_318
CC  version: 
CC  name: cluster_318
CC  description: wsvrksyCacGTGmwgrrrs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BHE41_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m39_M01039
XX
//
AC  cluster_319
XX
ID  cluster_319
XX
DE  ggGTCACGTGrssvsssss
P0       A     C     G     T
1      108   104   224    64
2      102    70   273    55
3       58     8   432     2
4        8    24    18   450
5        1   499     0     0
6      496     0     2     2
7        0   463    11    26
8       51     0   449     0
9        2     4     2   492
10       1     0   498     1
11     169    84   198    49
12      30   224   133   113
13      60   232   167    41
14     126   149   165    60
15      64   185   183    68
16      63   165   192    80
17      81   163   194    62
18      64   189   157    90
19      61   137   193   109
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_319
CC  AC: HOCOMOCO_2015_11_23_Human_m593_M01593
CC  id: HOCOMOCO_2015_11_23_Human_m593_M01593
CC  name: cluster_319
CC  version: 
CC  name: cluster_319
CC  description: ggGTCACGTGrssvsssss
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: USF2_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m593_M01593
XX
//
AC  cluster_32
XX
ID  cluster_32
XX
DE  yyCAAGGTCA
P0       A     C     G     T
1     0.75     3   2.5     5
2    58.25   292 158.75 451.25
3     64.5 688.5 134.75  72.5
4    896.5  5.75 52.75  5.25
5    944.75  0.75 12.25   2.5
6     5.25  0.75 948.5  5.75
7     2.25     2 952.75  3.25
8    40.25 51.25    84 784.75
9      3.5 831.75    68    57
10   879.25   9.5  61.5    10
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_32
CC  AC: cluster_32
CC  id: cluster_32
CC  name: cluster_32
CC  version: 
CC  name: cluster_32
CC  description: yyCAAGGTCA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ttCAAGGTCA
CC  consensus.strict.rc: TGACCTTGAA
CC  consensus.IUPAC: yyCAAGGTCA
CC  consensus.IUPAC.rc: TGACCTTGRR
CC  consensus.regexp: [ct][ct]CAAGGTCA
CC  consensus.regexp.rc: TGACCTTG[AG][AG]
CC  merged_ID: NR5A2_C,STF1_B,ERR2_A,ERR3_B
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m400_M01400,HOCOMOCO_2015_11_23_Human_m546_M01546,HOCOMOCO_2015_11_23_Human_m117_M01117,HOCOMOCO_2015_11_23_Human_m118_M01118
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_320
XX
ID  cluster_320
XX
DE  stcmbcwGTtgCyrgGsara
P0       A     C     G     T
1       85   146   232    32
2       54    51   116   274
3       46   310    78    61
4      228   130   103    34
5       43   153   147   152
6       26   263    93   113
7      183    25    85   202
8       21     6   450    18
9        6    41    57   391
10      71   100    33   291
11      79     8   327    81
12      12   441    15    27
13       8   179     8   300
14     253    10   220    12
15      63    22   335    75
16      16    19   432    28
17     101   164   165    65
18     231    94   100    70
19     141    31   249    74
20     251    80   117    47
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_320
CC  AC: HOCOMOCO_2015_11_23_Human_m486_M01486
CC  id: HOCOMOCO_2015_11_23_Human_m486_M01486
CC  name: cluster_320
CC  version: 
CC  name: cluster_320
CC  description: stcmbcwGTtgCyrgGsara
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: RFX5_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m486_M01486
XX
//
AC  cluster_321
XX
ID  cluster_321
XX
DE  myGCTTTCkAG
P0       A     C     G     T
1       23    18    11    14
2        3    24     1    38
3        4     0    61     1
4        0    64     0     2
5        9     0     0    57
6        0     1     0    65
7        0     0     0    66
8        0    66     0     0
9        0     0    29    37
10      64     1     1     0
11       1     0    65     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_321
CC  AC: HOCOMOCO_2015_11_23_Human_m33_M01033
CC  id: HOCOMOCO_2015_11_23_Human_m33_M01033
CC  name: cluster_321
CC  version: 
CC  name: cluster_321
CC  description: myGCTTTCkAG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BCL6B_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m33_M01033
XX
//
AC  cluster_322
XX
ID  cluster_322
XX
DE  aardgcwTTCywgGAa
P0       A     C     G     T
1       12     6     7     8
2       17     2     6     8
3       13     3    10     7
4       12     2     9    10
5        8     2    16     7
6        5    18     4     6
7        9     3     2    19
8        4     1     1    27
9        3     1     1    28
10       0    31     1     1
11       4     9     4    16
12      19     1     2    11
13       7     4    22     0
14       4     0    25     4
15      23     2     7     1
16      20     2     5     6
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_322
CC  AC: HOCOMOCO_2015_11_23_Human_m34_M01034
CC  id: HOCOMOCO_2015_11_23_Human_m34_M01034
CC  name: cluster_322
CC  version: 
CC  name: cluster_322
CC  description: aardgcwTTCywgGAa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BCL6_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m34_M01034
XX
//
AC  cluster_323
XX
ID  cluster_323
XX
DE  yTGGGArr
P0       A     C     G     T
1        3    17    14    26
2        3     5     0    52
3        3     0    57     0
4        1     2    57     0
5        0     1    59     0
6       51     0     9     0
7       19     2    32     7
8       25     2    22    11
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_323
CC  AC: HOCOMOCO_2015_11_23_Human_m265_M01265
CC  id: HOCOMOCO_2015_11_23_Human_m265_M01265
CC  name: cluster_323
CC  version: 
CC  name: cluster_323
CC  description: yTGGGArr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: IKZF1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m265_M01265
XX
//
AC  cluster_324
XX
ID  cluster_324
XX
DE  sygTGrGAAma
P0       A     C     G     T
1       12   236   186    65
2       34   212    42   211
3       72   124   283    20
4        1     0     3   495
5        2     3   493     1
6      202     1   251    45
7        0     0   498     1
8      485     1     9     4
9      417    17    63     2
10     189   156    74    80
11     147   123   120   109
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_324
CC  AC: HOCOMOCO_2015_11_23_Human_m547_M01547
CC  id: HOCOMOCO_2015_11_23_Human_m547_M01547
CC  name: cluster_324
CC  version: 
CC  name: cluster_324
CC  description: sygTGrGAAma
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SUH_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m547_M01547
XX
//
AC  cluster_325
XX
ID  cluster_325
XX
DE  skGCTGAcbmygcaryhtycv
P0       A     C     G     T
1        3    10     9     5
2        3     0     8    16
3        1     1    21     4
4        0    27     0     0
5        2     0     0    25
6        0     3    24     0
7       19     3     1     4
8        4    16     4     3
9        0    10    10     7
10       9    12     1     5
11       5    10     3     9
12       1     4    18     4
13       2    17     2     6
14      18     2     2     5
15       7     4    13     3
16       2    15     2     8
17       7    11     0     9
18       4     4     3    16
19       3     9     2    13
20       3    18     2     4
21       7     9     8     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_325
CC  AC: HOCOMOCO_2015_11_23_Human_m307_M01307
CC  id: HOCOMOCO_2015_11_23_Human_m307_M01307
CC  name: cluster_325
CC  version: 
CC  name: cluster_325
CC  description: skGCTGAcbmygcaryhtycv
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MAFA_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m307_M01307
XX
//
AC  cluster_326
XX
ID  cluster_326
XX
DE  argyCYCwgGGGAsh
P0       A     C     G     T
1      204    76   104   116
2      230    45   154    71
3       91    84   208   117
4       13   180    34   273
5        1   498     0     1
6        2   373     0   125
7        4   488     0     8
8      247     3    18   232
9       91     9   328    72
10      15     1   484     0
11      65     1   428     6
12       0     1   497     2
13     400    12    74    14
14     108   204   163    25
15     134   134    48   184
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_326
CC  AC: HOCOMOCO_2015_11_23_Human_m56_M01056
CC  id: HOCOMOCO_2015_11_23_Human_m56_M01056
CC  name: cluster_326
CC  version: 
CC  name: cluster_326
CC  description: argyCYCwgGGGAsh
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: COE1_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m56_M01056
XX
//
AC  cluster_327
XX
ID  cluster_327
XX
DE  gtTTGTTtwsyyAGa
P0       A     C     G     T
1        1     0     3     1
2        1     1     0     3
3        0     1     0     4
4        0     0     0     5
5        1     0     4     0
6        0     0     0     5
7        0     0     0     5
8        1     0     1     3
9        2     0     0     3
10       0     2     3     0
11       1     2     0     2
12       0     3     0     2
13       5     0     0     0
14       0     1     4     0
15       2     1     1     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_327
CC  AC: HOCOMOCO_2015_11_23_Human_m148_M01148
CC  id: HOCOMOCO_2015_11_23_Human_m148_M01148
CC  name: cluster_327
CC  version: 
CC  name: cluster_327
CC  description: gtTTGTTtwsyyAGa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXC2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m148_M01148
XX
//
AC  cluster_328
XX
ID  cluster_328
XX
DE  rGswCAARGktcAhykrr
P0       A     C     G     T
1        5     1     3     2
2        2     0     9     0
3        1     3     7     0
4        5     0     2     4
5        2     8     1     0
6       11     0     0     0
7       10     0     1     0
8        8     0     3     0
9        0     0    11     0
10       2     1     3     5
11       2     2     2     5
12       2     7     2     0
13      10     0     1     0
14       3     3     2     3
15       2     3     2     4
16       2     0     4     5
17       3     1     5     2
18       6     2     3     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_328
CC  AC: HOCOMOCO_2015_11_23_Human_m396_M01396
CC  id: HOCOMOCO_2015_11_23_Human_m396_M01396
CC  name: cluster_328
CC  version: 
CC  name: cluster_328
CC  description: rGswCAARGktcAhykrr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NR2F6_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m396_M01396
XX
//
AC  cluster_329
XX
ID  cluster_329
XX
DE  dwmrTtTGCACyTy
P0       A     C     G     T
1        3     1     4     4
2        5     2     2     3
3        3     6     1     2
4        5     0     5     2
5        2     0     1     9
6        2     0     2     8
7        0     1     1    10
8        2     1     9     0
9        0    10     1     1
10      10     0     2     0
11       0    10     1     1
12       0     3     2     7
13       1     0     2     9
14       0     7     2     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_329
CC  AC: HOCOMOCO_2015_11_23_Human_m128_M01128
CC  id: HOCOMOCO_2015_11_23_Human_m128_M01128
CC  name: cluster_329
CC  version: 
CC  name: cluster_329
CC  description: dwmrTtTGCACyTy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ETV3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m128_M01128
XX
//
AC  cluster_33
XX
ID  cluster_33
XX
DE  kGCGkGGGCGg
P0       A     C     G     T
1    145.5 179.5 304.5 324.5
2       42    57   843    12
3       59   786  19.5  89.5
4      3.5     1   948   1.5
5      9.5    13   466 465.5
6     71.5    18 861.5     3
7       24    62 860.5   7.5
8     11.5   9.5   933     0
9     84.5 713.5     7   149
10    20.5     7 910.5    16
11      87    62 581.5 223.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_33
CC  AC: cluster_33
CC  id: cluster_33
CC  name: cluster_33
CC  version: 
CC  name: cluster_33
CC  description: kGCGkGGGCGg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tGCGgGGGCGg
CC  consensus.strict.rc: CCGCCCCCGCA
CC  consensus.IUPAC: kGCGkGGGCGg
CC  consensus.IUPAC.rc: CCGCCCMCGCM
CC  consensus.regexp: [gt]GCG[gt]GGGCGg
CC  consensus.regexp.rc: CCGCCC[AC]CGC[AC]
CC  merged_ID: EGR1_S,EGR2_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m96_M01096,HOCOMOCO_2015_11_23_Human_m97_M01097
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_330
XX
ID  cluster_330
XX
DE  gggkATTGCAytb
P0       A     C     G     T
1        9     4    27     1
2        9     7    25     0
3        8     7    23     3
4        2     3    25    11
5       34     0     5     2
6        0     1     1    39
7        0     0     0    41
8        1     0    37     3
9        0    36     5     0
10      37     0     4     0
11       6    11     8    16
12       5    10     9    17
13       5    13    11    12
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_330
CC  AC: HOCOMOCO_2015_11_23_Human_m75_M01075
CC  id: HOCOMOCO_2015_11_23_Human_m75_M01075
CC  name: cluster_330
CC  version: 
CC  name: cluster_330
CC  description: gggkATTGCAytb
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: DDIT3_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m75_M01075
XX
//
AC  cluster_331
XX
ID  cluster_331
XX
DE  wwaawTTTWAygr
P0       A     C     G     T
1      696   180   133  1633
2     1494   241   152   755
3     1754   349   164   375
4     1467   461   269   445
5      908   436   575   723
6      297   139   189  2017
7        0     0     2  2640
8        0     1     0  2641
9      790     0     3  1849
10    2595    15    17    15
11     377   805   302  1158
12     352   346  1319   625
13    1066   360   763   453
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_331
CC  AC: HOCOMOCO_2015_11_23_Human_m236_M01236
CC  id: HOCOMOCO_2015_11_23_Human_m236_M01236
CC  name: cluster_331
CC  version: 
CC  name: cluster_331
CC  description: wwaawTTTWAygr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXA11_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m236_M01236
XX
//
AC  cluster_332
XX
ID  cluster_332
XX
DE  rrGGTCAatGaCCt
P0       A     C     G     T
1       22     2    17     4
2       21     1    23     0
3        0     1    43     1
4        1     0    39     5
5        1     3     1    40
6        1    37     6     1
7       44     0     1     0
8       17     9     9    10
9        2     6     7    30
10      10     1    33     1
11      27     6     4     8
12       3    38     1     3
13       1    40     1     3
14       4     5     5    31
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_332
CC  AC: HOCOMOCO_2015_11_23_Human_m387_M01387
CC  id: HOCOMOCO_2015_11_23_Human_m387_M01387
CC  name: cluster_332
CC  version: 
CC  name: cluster_332
CC  description: rrGGTCAatGaCCt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NR1H4_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m387_M01387
XX
//
AC  cluster_333
XX
ID  cluster_333
XX
DE  rrGTTAGrGaCCyr
P0       A     C     G     T
1      207    51   159    83
2      187    15   247    51
3       46    77   348    29
4       90    12    19   379
5        4     4    16   476
6      490     6     2     2
7       38    32   428     2
8      297     1   175    27
9       44    35   408    13
10     305    67    18   110
11     103   364    23    10
12       8   396    71    25
13      71   185    25   219
14     198    81   129    92
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_333
CC  AC: HOCOMOCO_2015_11_23_Human_m462_M01462
CC  id: HOCOMOCO_2015_11_23_Human_m462_M01462
CC  name: cluster_333
CC  version: 
CC  name: cluster_333
CC  description: rrGTTAGrGaCCyr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PRD14_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m462_M01462
XX
//
AC  cluster_334
XX
ID  cluster_334
XX
DE  ggCTGAGg
P0       A     C     G     T
1      782   327  2021    26
2      512   632  2012     0
3       22  2958    21   155
4        0     0   131  3025
5        0     0  3156     0
6     3143    13     0     0
7      597    35  2524     0
8      427   783  1369   577
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_334
CC  AC: HOCOMOCO_2015_11_23_Human_m424_M01424
CC  id: HOCOMOCO_2015_11_23_Human_m424_M01424
CC  name: cluster_334
CC  version: 
CC  name: cluster_334
CC  description: ggCTGAGg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PAX5_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m424_M01424
XX
//
AC  cluster_335
XX
ID  cluster_335
XX
DE  TTCyyaGGAAwyt
P0       A     C     G     T
1        3     3     3    94
2        3     1     0    99
3        6    96     1     0
4       16    43     9    35
5       17    43    10    33
6       41    21    19    22
7       10     7    72    14
8       22     1    72     8
9      102     0     0     1
10      94     1     4     4
11      40     7    13    43
12      15    26    11    51
13      20    24    15    44
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_335
CC  AC: HOCOMOCO_2015_11_23_Human_m545_M01545
CC  id: HOCOMOCO_2015_11_23_Human_m545_M01545
CC  name: cluster_335
CC  version: 
CC  name: cluster_335
CC  description: TTCyyaGGAAwyt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: STAT6_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m545_M01545
XX
//
AC  cluster_336
XX
ID  cluster_336
XX
DE  nTGACAg
P0       A     C     G     T
1        4     4     4     4
2        0     0     0    16
3        0     1    15     0
4       16     0     0     0
5        0    16     0     0
6       16     0     0     0
7        3     2     9     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_336
CC  AC: HOCOMOCO_2015_11_23_Human_m578_M01578
CC  id: HOCOMOCO_2015_11_23_Human_m578_M01578
CC  name: cluster_336
CC  version: 
CC  name: cluster_336
CC  description: nTGACAg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TGIF1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m578_M01578
XX
//
AC  cluster_337
XX
ID  cluster_337
XX
DE  ssrgrCmGGAAGyrrvgsssss
P0       A     C     G     T
1       58   128   117    34
2       69    85   157    26
3       95    82   126    34
4       43    72   186    36
5      119    41   143    34
6       20   275    42     0
7      143   192     2     0
8        0     1   336     0
9        0     0   337     0
10     335     1     1     0
11     327     0     0    10
12      43     2   289     3
13      26   128    53   130
14      91    53   167    26
15     100    71   140    26
16      89   112   116    20
17      66    78   144    49
18      49   104   137    47
19      47    97   127    66
20      46    85   147    59
21      52    86   153    46
22      64   116   140    17
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_337
CC  AC: HOCOMOCO_2015_11_23_Human_m126_M01126
CC  id: HOCOMOCO_2015_11_23_Human_m126_M01126
CC  name: cluster_337
CC  version: 
CC  name: cluster_337
CC  description: ssrgrCmGGAAGyrrvgsssss
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ETV1_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m126_M01126
XX
//
AC  cluster_338
XX
ID  cluster_338
XX
DE  AAGkTCAAGkTCA
P0       A     C     G     T
1       39     3     5     3
2       47     1     1     1
3        2     0    47     1
4        0     2    19    29
5        0     1     0    49
6        1    49     0     0
7       49     1     0     0
8       44     2     1     3
9        0     1    45     4
10       1     0    30    19
11       2     5     2    41
12       2    43     0     5
13      43     2     2     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_338
CC  AC: HOCOMOCO_2015_11_23_Human_m401_M01401
CC  id: HOCOMOCO_2015_11_23_Human_m401_M01401
CC  name: cluster_338
CC  version: 
CC  name: cluster_338
CC  description: AAGkTCAAGkTCA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NR6A1_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m401_M01401
XX
//
AC  cluster_339
XX
ID  cluster_339
XX
DE  gkrgrrkvAgGkGkrr
P0       A     C     G     T
1       60    24   119    52
2       48    36   107    64
3       71    15   139    30
4       62    21   122    50
5       78    11   134    32
6       80    11   104    60
7       46     2   127    80
8       97    82    75     1
9      225    12     3    15
10      43    56   145    11
11      42     0   195    18
12      49     6    90   110
13      28     8   187    32
14      61    16   113    65
15      91    31    75    58
16      80    40    96    39
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_339
CC  AC: HOCOMOCO_2015_11_23_Human_m335_M01335
CC  id: HOCOMOCO_2015_11_23_Human_m335_M01335
CC  name: cluster_339
CC  version: 
CC  name: cluster_339
CC  description: gkrgrrkvAgGkGkrr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MNT_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m335_M01335
XX
//
AC  cluster_34
XX
ID  cluster_34
XX
DE  tGaTGACGTGgca
P0       A     C     G     T
1       72   101  73.5 409.5
2     59.5  57.5   468    71
3      285 113.5   152 105.5
4     80.5  49.5    15   511
5      2.5   133   488  32.5
6    651.5     1   1.5     2
7      1.5 649.5     3     2
8        0     1   654     1
9      1.5   2.5   2.5 649.5
10       4    10   639     3
11    86.5  77.5   392   100
12      93 396.5    81  85.5
13     354  93.5 117.5    91
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_34
CC  AC: cluster_34
CC  id: cluster_34
CC  name: cluster_34
CC  version: 
CC  name: cluster_34
CC  description: tGaTGACGTGgca
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tGaTGACGTGgca
CC  consensus.strict.rc: TGCCACGTCATCA
CC  consensus.IUPAC: tGaTGACGTGgca
CC  consensus.IUPAC.rc: TGCCACGTCATCA
CC  consensus.regexp: tGaTGACGTGgca
CC  consensus.regexp.rc: TGCCACGTCATCA
CC  merged_ID: CR3L2_D,CREB3_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m63_M01063,HOCOMOCO_2015_11_23_Human_m65_M01065
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_340
XX
ID  cluster_340
XX
DE  krdrrwGgGkrrg
P0       A     C     G     T
1       51    41   103    68
2       92    47    89    35
3       69    14   104    76
4       69     8   126    60
5      131     0    72    60
6      162     3     0    98
7        0     0   263     0
8        0    22   176    65
9       44    20   186    13
10      52    24   110    77
11     102    17    87    57
12      99    21   111    32
13      53    29   140    41
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_340
CC  AC: HOCOMOCO_2015_11_23_Human_m532_M01532
CC  id: HOCOMOCO_2015_11_23_Human_m532_M01532
CC  name: cluster_340
CC  version: 
CC  name: cluster_340
CC  description: krdrrwGgGkrrg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SPIC_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m532_M01532
XX
//
AC  cluster_341
XX
ID  cluster_341
XX
DE  kCCAGATGTTysss
P0       A     C     G     T
1        1     1     5    11
2        4    14     0     0
3        0    18     0     0
4       18     0     0     0
5        0     0    18     0
6       17     1     0     0
7        0     0     0    18
8        0     0    18     0
9        0     2     0    16
10       0     0     4    14
11       1     5     4     8
12       4     8     5     1
13       2     7     8     1
14       3     7     6     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_341
CC  AC: HOCOMOCO_2015_11_23_Human_m605_M01605
CC  id: HOCOMOCO_2015_11_23_Human_m605_M01605
CC  name: cluster_341
CC  version: 
CC  name: cluster_341
CC  description: kCCAGATGTTysss
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZBT18_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m605_M01605
XX
//
AC  cluster_342
XX
ID  cluster_342
XX
DE  ytmaTGaAwAWwy
P0       A     C     G     T
1        3    17     5    13
2        9     5     2    22
3       15    17     5     1
4       25     4     0     9
5        3     0     0    35
6        1     0    37     0
7       22     6     5     5
8       35     0     0     3
9       10     0     2    26
10      33     1     1     3
11      10     0     0    28
12      20     0     6    12
13       6    12     5    15
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_342
CC  AC: HOCOMOCO_2015_11_23_Human_m435_M01435
CC  id: HOCOMOCO_2015_11_23_Human_m435_M01435
CC  name: cluster_342
CC  version: 
CC  name: cluster_342
CC  description: ytmaTGaAwAWwy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PIT1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m435_M01435
XX
//
AC  cluster_343
XX
ID  cluster_343
XX
DE  dkswtkwtATkGAyTwwwyyw
P0       A     C     G     T
1        2     0     3     2
2        1     0     2     4
3        1     3     2     1
4        2     1     1     3
5        1     1     1     4
6        1     1     3     2
7        3     0     0     4
8        0     1     1     5
9        7     0     0     0
10       0     0     0     7
11       0     1     2     4
12       1     0     6     0
13       7     0     0     0
14       0     2     0     5
15       0     0     0     7
16       2     1     0     4
17       2     0     0     5
18       2     0     1     4
19       0     3     0     4
20       0     3     0     4
21       2     1     0     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_343
CC  AC: HOCOMOCO_2015_11_23_Human_m407_M01407
CC  id: HOCOMOCO_2015_11_23_Human_m407_M01407
CC  name: cluster_343
CC  version: 
CC  name: cluster_343
CC  description: dkswtkwtATkGAyTwwwyyw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ONEC2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m407_M01407
XX
//
AC  cluster_344
XX
ID  cluster_344
XX
DE  mGkrTtrTGsRAra
P0       A     C     G     T
1       11    13     6     6
2        8     1    25     2
3        5     1    15    15
4       15     0    18     3
5        2     0     4    30
6        5     5     6    20
7       21     0    15     0
8        3     1     6    26
9        4     0    31     1
10       2     9    24     1
11      10     0    25     1
12      32     1     1     2
13      19     4    12     1
14      22     2     5     7
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_344
CC  AC: HOCOMOCO_2015_11_23_Human_m627_M01627
CC  id: HOCOMOCO_2015_11_23_Human_m627_M01627
CC  name: cluster_344
CC  version: 
CC  name: cluster_344
CC  description: mGkrTtrTGsRAra
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN282_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m627_M01627
XX
//
AC  cluster_345
XX
ID  cluster_345
XX
DE  TATTATGGGATGk
P0       A     C     G     T
1       28    45    20  1134
2     1199     9    10     9
3        6     7     6  1208
4        4     4     4  1215
5     1216     3     4     4
6        2     2     2  1221
7        1     1  1224     1
8        2     1  1223     1
9        1     1  1187    38
10    1133    68    11    15
11       5     4     3  1215
12     142    30  1025    30
13      41   168   551   467
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_345
CC  AC: HOCOMOCO_2015_11_23_Human_m631_M01631
CC  id: HOCOMOCO_2015_11_23_Human_m631_M01631
CC  name: cluster_345
CC  version: 
CC  name: cluster_345
CC  description: TATTATGGGATGk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN410_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m631_M01631
XX
//
AC  cluster_346
XX
ID  cluster_346
XX
DE  rrtATAwAwws
P0       A     C     G     T
1       33    16    42    10
2       31    22    46     2
3        9     9    17    66
4       76     5     1    19
5       11     2     9    79
6       94     1     6     0
7       65     1     2    33
8       87     1     9     4
9       41     3    10    47
10      32    11    25    33
11      20    42    29    10
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_346
CC  AC: HOCOMOCO_2015_11_23_Human_m550_M01550
CC  id: HOCOMOCO_2015_11_23_Human_m550_M01550
CC  name: cluster_346
CC  version: 
CC  name: cluster_346
CC  description: rrtATAwAwws
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TBP_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m550_M01550
XX
//
AC  cluster_347
XX
ID  cluster_347
XX
DE  trsmgGTTrw
P0       A     C     G     T
1        1     1     1     6
2        3     2     4     0
3        1     5     3     0
4        6     3     0     0
5        2     1     4     2
6        0     0     8     1
7        1     0     1     7
8        0     0     0     9
9        3     1     4     1
10       5     0     1     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_347
CC  AC: HOCOMOCO_2015_11_23_Human_m342_M01342
CC  id: HOCOMOCO_2015_11_23_Human_m342_M01342
CC  name: cluster_347
CC  version: 
CC  name: cluster_347
CC  description: trsmgGTTrw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MYBB_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m342_M01342
XX
//
AC  cluster_348
XX
ID  cluster_348
XX
DE  kktskGTTG
P0       A     C     G     T
1       11    14    21    27
2       14     0    20    39
3        3     9    11    50
4        4    25    35     9
5        2    11    33    27
6        1     0    63     9
7        0     2     0    71
8        0     0     0    73
9        5     5    60     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_348
CC  AC: HOCOMOCO_2015_11_23_Human_m43_M01043
CC  id: HOCOMOCO_2015_11_23_Human_m43_M01043
CC  name: cluster_348
CC  version: 
CC  name: cluster_348
CC  description: kktskGTTG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BRCA1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m43_M01043
XX
//
AC  cluster_349
XX
ID  cluster_349
XX
DE  tTTTTATtytTTTTttt
P0       A     C     G     T
1      295   393   161   954
2        1     1     1  1800
3        9     7     1  1786
4        1     3     1  1798
5        0     3     0  1800
6     1797     1     4     1
7        4    98     6  1695
8      231   425    51  1096
9      328   452   132   891
10     167   403    87  1146
11     146   332    62  1263
12     118   245    47  1393
13     181   213    29  1380
14     327   179    21  1276
15     404   210    42  1147
16     433   304    63  1003
17     403   379   127   894
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_349
CC  AC: HOCOMOCO_2015_11_23_Human_m61_M01061
CC  id: HOCOMOCO_2015_11_23_Human_m61_M01061
CC  name: cluster_349
CC  version: 
CC  name: cluster_349
CC  description: tTTTTATtytTTTTttt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: CPEB1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m61_M01061
XX
//
AC  cluster_35
XX
ID  cluster_35
XX
DE  ygTTAAKTGct
P0       A     C     G     T
1     5951  8815  5320 7704.5
2    6912.5  5740 9362.5 5775.5
3     1997 4785.5  2001 19007
4      739 479.5 473.5 26098.5
5    26861  14.5 841.5  73.5
6    27612.5  16.5 135.5    26
7     74.5    20 8274.5 19421.5
8     99.5  1435  1648 24608
9      942 132.5 26597   119
10   5245.5 11807.5  6840 3897.5
11   4856.5  6101 2917.5 13915.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_35
CC  AC: cluster_35
CC  id: cluster_35
CC  name: cluster_35
CC  version: 
CC  name: cluster_35
CC  description: ygTTAAKTGct
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cgTTAATTGct
CC  consensus.strict.rc: AGCAATTAACG
CC  consensus.IUPAC: ygTTAAKTGct
CC  consensus.IUPAC.rc: AGCAMTTAACR
CC  consensus.regexp: [ct]gTTAA[GT]TGct
CC  consensus.regexp.rc: AGCA[AC]TTAAC[AG]
CC  merged_ID: HMX3_D,ISL2_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m223_M01223,HOCOMOCO_2015_11_23_Human_m278_M01278
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_350
XX
ID  cluster_350
XX
DE  ssSkCCGGmgr
P0       A     C     G     T
1       17    44    44     6
2        7    43    51    10
3        0    28    83     0
4        1     0    57    53
5        0   111     0     0
6        0   111     0     0
7        0     0   111     0
8        0     0   111     0
9       59    39    12     1
10      12    12    65    22
11      36    14    53     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_350
CC  AC: HOCOMOCO_2015_11_23_Human_m317_M01317
CC  id: HOCOMOCO_2015_11_23_Human_m317_M01317
CC  name: cluster_350
CC  version: 
CC  name: cluster_350
CC  description: ssSkCCGGmgr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MBD2_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m317_M01317
XX
//
AC  cluster_351
XX
ID  cluster_351
XX
DE  sCCGGrr
P0       A     C     G     T
1        4    70    42     3
2        0   119     0     0
3        0   119     0     0
4        0     0   119     0
5        0     0   119     0
6       68     1    50     0
7       52     1    66     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_351
CC  AC: HOCOMOCO_2015_11_23_Human_m319_M01319
CC  id: HOCOMOCO_2015_11_23_Human_m319_M01319
CC  name: cluster_351
CC  version: 
CC  name: cluster_351
CC  description: sCCGGrr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MECP2_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m319_M01319
XX
//
AC  cluster_352
XX
ID  cluster_352
XX
DE  hrCywGGGTGksGmW
P0       A     C     G     T
1        3     2     0     3
2        3     1     4     0
3        0     6     1     1
4        1     3     1     3
5        3     0     1     4
6        1     0     7     0
7        0     0     7     1
8        0     0     8     0
9        0     0     0     8
10       1     0     7     0
11       0     0     3     5
12       1     2     5     0
13       0     1     6     1
14       2     4     1     1
15       2     0     0     6
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_352
CC  AC: HOCOMOCO_2015_11_23_Human_m293_M01293
CC  id: HOCOMOCO_2015_11_23_Human_m293_M01293
CC  name: cluster_352
CC  version: 
CC  name: cluster_352
CC  description: hrCywGGGTGksGmW
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: KLF3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m293_M01293
XX
//
AC  cluster_353
XX
ID  cluster_353
XX
DE  scvdrgkCAgysraSCrkRAC
P0       A     C     G     T
1       85   161   176    76
2       89   204   106    99
3      133   135   178    52
4      130    46   170   152
5      153    31   300    14
6      105   104   197    92
7       12   106   252   128
8       11   394     7    86
9      370    35    75    18
10     106   108   253    31
11       8   304    36   150
12     106   220   152    20
13     271    10   212     5
14     279    30   108    81
15       7   143   348     0
16       7   392     7    92
17     132    23   324    19
18      56    18   137   287
19     142    11   342     3
20     347    20   100    31
21      90   364    20    24
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_353
CC  AC: HOCOMOCO_2015_11_23_Human_m423_M01423
CC  id: HOCOMOCO_2015_11_23_Human_m423_M01423
CC  name: cluster_353
CC  version: 
CC  name: cluster_353
CC  description: scvdrgkCAgysraSCrkRAC
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PAX5_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m423_M01423
XX
//
AC  cluster_354
XX
ID  cluster_354
XX
DE  sbkTrCGTark
P0       A     C     G     T
1      668   995  1198   841
2      537   957  1178  1030
3      403   267  1206  1826
4      136    81    95  3390
5     2511     9  1157    25
6       27  3499   116    60
7        7     0  3673    22
8       48    40   127  3487
9     1560   864   901   377
10    1701   491  1103   407
11     674   866   959  1203
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_354
CC  AC: HOCOMOCO_2015_11_23_Human_m195_M01195
CC  id: HOCOMOCO_2015_11_23_Human_m195_M01195
CC  name: cluster_354
CC  version: 
CC  name: cluster_354
CC  description: sbkTrCGTark
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: GMEB2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m195_M01195
XX
//
AC  cluster_355
XX
ID  cluster_355
XX
DE  wmarwGATwactyrrmk
P0       A     C     G     T
1        4     1     1     7
2        5     4     3     1
3        9     2     1     1
4        4     2     4     3
5        7     0     2     4
6        0     0    13     0
7       13     0     0     0
8        0     0     0    13
9        9     0     0     4
10       9     0     2     2
11       1     6     3     3
12       2     3     2     6
13       2     4     2     5
14       6     1     4     2
15       5     0     7     1
16       5     4     1     3
17       3     2     4     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_355
CC  AC: HOCOMOCO_2015_11_23_Human_m178_M01178
CC  id: HOCOMOCO_2015_11_23_Human_m178_M01178
CC  name: cluster_355
CC  version: 
CC  name: cluster_355
CC  description: wmarwGATwactyrrmk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: GATA5_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m178_M01178
XX
//
AC  cluster_356
XX
ID  cluster_356
XX
DE  wtwcaTGTwat
P0       A     C     G     T
1      143    38    35   165
2       82    80    77   142
3      152    29    36   164
4       19   255    16    91
5      252     2    59    68
6        0     2     0   379
7        5     0   372     4
8        0     0     0   381
9      178    19     8   176
10     154    79    72    76
11      90    55    58   178
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_356
CC  AC: HOCOMOCO_2015_11_23_Human_m276_M01276
CC  id: HOCOMOCO_2015_11_23_Human_m276_M01276
CC  name: cluster_356
CC  version: 
CC  name: cluster_356
CC  description: wtwcaTGTwat
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: IRX3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m276_M01276
XX
//
AC  cluster_357
XX
ID  cluster_357
XX
DE  kktkTTGTkkks
P0       A     C     G     T
1        2     2     8     9
2        1     0     8    12
3        2     1     4    14
4        3     5     7     6
5        1     0     0    20
6        0     0     1    20
7        0     0    16     5
8        1     3     0    17
9        1     0    10    10
10       1     2     7    11
11       3     3     6     9
12       4     9     6     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_357
CC  AC: HOCOMOCO_2015_11_23_Human_m41_M01041
CC  id: HOCOMOCO_2015_11_23_Human_m41_M01041
CC  name: cluster_357
CC  version: 
CC  name: cluster_357
CC  description: kktkTTGTkkks
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BPTF_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m41_M01041
XX
//
AC  cluster_358
XX
ID  cluster_358
XX
DE  TAAwyGsysyTAA
P0       A     C     G     T
1       42     3     3  5634
2     5652     0     3    27
3     5679     0     2     1
4     2749    21   174  2738
5      201  2455   161  2865
6      370     5  5196   111
7      736  2072  1783  1091
8      352  2030   350  2950
9      956  1914  1840   972
10    1232  1932   498  2020
11     705   337   125  4515
12    4133    98    95  1356
13    4879   114   111   578
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_358
CC  AC: HOCOMOCO_2015_11_23_Human_m25_M01025
CC  id: HOCOMOCO_2015_11_23_Human_m25_M01025
CC  name: cluster_358
CC  version: 
CC  name: cluster_358
CC  description: TAAwyGsysyTAA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BARH1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m25_M01025
XX
//
AC  cluster_359
XX
ID  cluster_359
XX
DE  rkrgsdATyGRTsk
P0       A     C     G     T
1       45    14    31     7
2       12    20    36    29
3       30    14    50     3
4       19    24    44    10
5       15    28    31    23
6       26     7    34    30
7       92     1     3     1
8        1     2     0    94
9        0    66     4    27
10      13     4    74     6
11      67     0    26     4
12       1     2     0    94
13       7    25    49    16
14      12    18    38    29
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_359
CC  AC: HOCOMOCO_2015_11_23_Human_m71_M01071
CC  id: HOCOMOCO_2015_11_23_Human_m71_M01071
CC  name: cluster_359
CC  version: 
CC  name: cluster_359
CC  description: rkrgsdATyGRTsk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: CUX1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m71_M01071
XX
//
AC  cluster_36
XX
ID  cluster_36
XX
DE  dmryTAATtrr
P0       A     C     G     T
1    482.5 157.5 368.5 388.5
2    369.5 434.5 284.5 308.5
3    418.5   273   572 133.5
4      189   480 173.5 554.5
5     14.5     1     2 1379.5
6     1377     0    19     1
7     1395   0.5     1   0.5
8        9   4.5  25.5  1358
9    127.5   142   211 916.5
10   657.5   101   514 124.5
11   379.5   296   503 218.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_36
CC  AC: cluster_36
CC  id: cluster_36
CC  name: cluster_36
CC  version: 
CC  name: cluster_36
CC  description: dmryTAATtrr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: acgtTAATtag
CC  consensus.strict.rc: CTAATTAACGT
CC  consensus.IUPAC: dmryTAATtrr
CC  consensus.IUPAC.rc: YYAATTARYKH
CC  consensus.regexp: [agt][ac][ag][ct]TAATt[ag][ag]
CC  consensus.regexp.rc: [CT][CT]AATTA[AG][CT][GT][ACT]
CC  merged_ID: LHX4_D,ZBT49_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m301_M01301,HOCOMOCO_2015_11_23_Human_m606_M01606
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_360
XX
ID  cluster_360
XX
DE  katcratwwrwtTATCGATya
P0       A     C     G     T
1     6549  3359  9856  6772
2    15589  2860  3410  4677
3     3285  3016  3841 16394
4     4921 14894  3344  3377
5     8764  2185 11391  4196
6    16412  2412  2703  5009
7     5623  2115  2457 16341
8    11905  2374  3422  8835
9     8107  5529  3685  9215
10    7915  3176  9072  6373
11    9121  3034  6466  7915
12    4299  1623  5702 14912
13    2627   874  3028 20007
14   26048    77   215   196
15      69    58   127 26282
16      74 20086   248  6128
17     110    36 26344    46
18   25785   131   471   149
19     243   200   192 25901
20    3399  8429  1302 13406
21   14869  4361  3068  4238
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_360
CC  AC: HOCOMOCO_2015_11_23_Human_m72_M01072
CC  id: HOCOMOCO_2015_11_23_Human_m72_M01072
CC  name: cluster_360
CC  version: 
CC  name: cluster_360
CC  description: katcratwwrwtTATCGATya
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: CUX2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m72_M01072
XX
//
AC  cluster_361
XX
ID  cluster_361
XX
DE  gGwrkkgGkgGgcrkrG
P0       A     C     G     T
1        2     1     9     1
2        2     1    10     0
3        7     0     2     4
4        6     2     4     1
5        1     0     7     5
6        1     2     6     4
7        0     2     8     3
8        1     0    10     2
9        3     2     4     4
10       2     0     8     3
11       0     0    11     2
12       1     3     8     1
13       2     9     2     0
14       6     3     4     0
15       1     0     7     5
16       4     0     9     0
17       0     1    10     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_361
CC  AC: HOCOMOCO_2015_11_23_Human_m472_M01472
CC  id: HOCOMOCO_2015_11_23_Human_m472_M01472
CC  name: cluster_361
CC  version: 
CC  name: cluster_361
CC  description: gGwrkkgGkgGgcrkrG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PURA_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m472_M01472
XX
//
AC  cluster_362
XX
ID  cluster_362
XX
DE  yGsyAAkrwGs
P0       A     C     G     T
1        5    19     9    18
2        1     6    42     2
3        0    22    17    12
4        1    28     2    20
5       47     2     1     1
6       44     4     1     2
7        0    10    22    19
8       16     1    23    11
9       23     3     9    16
10       2     0    38    11
11       6    24    13     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_362
CC  AC: HOCOMOCO_2015_11_23_Human_m586_M01586
CC  id: HOCOMOCO_2015_11_23_Human_m586_M01586
CC  name: cluster_362
CC  version: 
CC  name: cluster_362
CC  description: yGsyAAkrwGs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TLX1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m586_M01586
XX
//
AC  cluster_363
XX
ID  cluster_363
XX
DE  ksykdgTATTskr
P0       A     C     G     T
1        3     2     5     5
2        2     4     6     3
3        0     4     3     8
4        0     2     5     8
5        5     1     5     4
6        3     1    10     1
7        0     0     0    15
8       13     0     2     0
9        0     2     0    13
10       0     1     1    13
11       2     4     8     1
12       2     1     4     8
13       4     2     6     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_363
CC  AC: HOCOMOCO_2015_11_23_Human_m13_M01013
CC  id: HOCOMOCO_2015_11_23_Human_m13_M01013
CC  name: cluster_363
CC  version: 
CC  name: cluster_363
CC  description: ksykdgTATTskr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ARI5B_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m13_M01013
XX
//
AC  cluster_364
XX
ID  cluster_364
XX
DE  TGTGKATTsgr
P0       A     C     G     T
1       48    12    18   422
2        3     0   497     0
3        4    12     3   481
4        3     0   421    76
5        4     0   362   134
6      489     6     0     5
7       14    10     4   472
8        0    23     1   476
9       56   215   171    58
10     115   118   173    94
11     182    83   190    45
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_364
CC  AC: HOCOMOCO_2015_11_23_Human_m155_M01155
CC  id: HOCOMOCO_2015_11_23_Human_m155_M01155
CC  name: cluster_364
CC  version: 
CC  name: cluster_364
CC  description: TGTGKATTsgr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXH1_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m155_M01155
XX
//
AC  cluster_365
XX
ID  cluster_365
XX
DE  wwwaATCGTTTTttw
P0       A     C     G     T
1      111    37    46    80
2      130    23    25    96
3      153    11    13    97
4      182    33    20    39
5      271     3     0     0
6        0     0     0   274
7        0   274     0     0
8        0     0   274     0
9       16     0    20   238
10      49     0     0   225
11      59     0     1   214
12      50    16    11   197
13      60    63    32   119
14      54    55    46   119
15      76    48    44   106
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_365
CC  AC: HOCOMOCO_2015_11_23_Human_m229_M01229
CC  id: HOCOMOCO_2015_11_23_Human_m229_M01229
CC  name: cluster_365
CC  version: 
CC  name: cluster_365
CC  description: wwwaATCGTTTTttw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HOMEZ_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m229_M01229
XX
//
AC  cluster_366
XX
ID  cluster_366
XX
DE  mttGGwwwwawtGGttwm
P0       A     C     G     T
1       16    11     5     9
2        8     8     6    19
3        6     6     8    21
4        2     0    36     3
5        0     0    33     8
6       13     2     1    25
7       16     4     1    20
8       15     6     6    14
9       14     3     6    18
10      21     4     6    10
11      16     2     3    20
12       9     6     3    23
13       0     1    36     4
14       0     0    40     1
15       9     4    10    18
16       3     8     8    22
17      18     1     7    15
18      17    11     4     9
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_366
CC  AC: HOCOMOCO_2015_11_23_Human_m2_M01002
CC  id: HOCOMOCO_2015_11_23_Human_m2_M01002
CC  name: cluster_366
CC  version: 
CC  name: cluster_366
CC  description: mttGGwwwwawtGGttwm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: AIRE_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m2_M01002
XX
//
AC  cluster_367
XX
ID  cluster_367
XX
DE  ggggdwTwaagGGATTAr
P0       A     C     G     T
1     1710  1223  3141  1512
2     1487   871  3935  1293
3     1496   673  4138  1279
4     1869   236  4087  1394
5     2208   257  2977  2144
6     3965   710    74  2837
7     1674   349   235  5328
8     2018  1004   529  4035
9     4546  1113  1043   884
10    3646   713  1798  1429
11    1647   906  4158   875
12     570    53  6818   145
13      59    17  7302   208
14    6718   814    12    42
15       1     2     1  7582
16      34    63     6  7483
17    7310    10    21   245
18    2848   773  2710  1255
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_367
CC  AC: HOCOMOCO_2015_11_23_Human_m438_M01438
CC  id: HOCOMOCO_2015_11_23_Human_m438_M01438
CC  name: cluster_367
CC  version: 
CC  name: cluster_367
CC  description: ggggdwTwaagGGATTAr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PITX3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m438_M01438
XX
//
AC  cluster_368
XX
ID  cluster_368
XX
DE  tryTyTwrTk
P0       A     C     G     T
1        1     1     0     5
2        2     0     5     0
3        1     3     1     2
4        1     0     0     6
5        1     2     0     4
6        0     0     0     7
7        4     0     1     2
8        4     0     2     1
9        0     0     0     7
10       0     0     2     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_368
CC  AC: HOCOMOCO_2015_11_23_Human_m256_M01256
CC  id: HOCOMOCO_2015_11_23_Human_m256_M01256
CC  name: cluster_368
CC  version: 
CC  name: cluster_368
CC  description: tryTyTwrTk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXD10_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m256_M01256
XX
//
AC  cluster_369
XX
ID  cluster_369
XX
DE  bkATwATka
P0       A     C     G     T
1        1     6     5     4
2        0     1     8     7
3       16     0     0     0
4        0     0     0    16
5       11     0     0     5
6       16     0     0     0
7        0     0     0    16
8        1     2     9     4
9        9     3     1     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_369
CC  AC: HOCOMOCO_2015_11_23_Human_m628_M01628
CC  id: HOCOMOCO_2015_11_23_Human_m628_M01628
CC  name: cluster_369
CC  version: 
CC  name: cluster_369
CC  description: bkATwATka
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN333_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m628_M01628
XX
//
AC  cluster_37
XX
ID  cluster_37
XX
DE  rcmGGAAgt
P0       A     C     G     T
1       47 13.3333    26    16
2       19 74.3333 21.3333 3.33333
3    48.3333    60     6 3.66667
4    1.66667 0.333333   112     4
5    1.66667 0.666667 114.333 1.33333
6    113.667 0.333333     2     2
7    91.3333 1.33333 2.33333    23
8    29.3333 8.66667 77.3333 2.66667
9       11 26.6667    12 68.3333
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_37
CC  AC: cluster_37
CC  id: cluster_37
CC  name: cluster_37
CC  version: 
CC  name: cluster_37
CC  description: rcmGGAAgt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: accGGAAgt
CC  consensus.strict.rc: ACTTCCGGT
CC  consensus.IUPAC: rcmGGAAgt
CC  consensus.IUPAC.rc: ACTTCCKGY
CC  consensus.regexp: [ag]c[ac]GGAAgt
CC  consensus.regexp.rc: ACTTCC[GT]G[CT]
CC  merged_ID: ETV4_B,ELK1_A,ETS1_C
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m129_M01129,HOCOMOCO_2015_11_23_Human_m106_M01106,HOCOMOCO_2015_11_23_Human_m124_M01124
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_370
XX
ID  cluster_370
XX
DE  rrGGtCAswGrGktca
P0       A     C     G     T
1      193    34   217    56
2      226     5   257    12
3       66     3   409    22
4       15    10   414    61
5       40    37   115   308
6       16   360    87    37
7      405    11    39    45
8       74   164   180    82
9      140    88   113   159
10     106    31   349    14
11     266     9   212    13
12      27     6   445    22
13      23    17   229   231
14      65    43   117   275
15      17   290   116    77
16     338    38    89    35
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_370
CC  AC: HOCOMOCO_2015_11_23_Human_m596_M01596
CC  id: HOCOMOCO_2015_11_23_Human_m596_M01596
CC  name: cluster_370
CC  version: 
CC  name: cluster_370
CC  description: rrGGtCAswGrGktca
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: VDR_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m596_M01596
XX
//
AC  cluster_371
XX
ID  cluster_371
XX
DE  hkvmhwChbAGGTGWGArvhkysmh
P0       A     C     G     T
1        2     2     1     3
2        1     1     4     2
3        2     4     2     0
4        3     3     1     1
5        4     2     0     2
6        3     1     1     3
7        0     7     0     1
8        4     2     0     2
9        1     2     3     2
10       7     0     0     1
11       1     0     7     0
12       1     0     7     0
13       1     0     0     7
14       0     0     7     1
15       2     0     0     6
16       0     1     7     0
17       7     1     0     0
18       4     0     4     0
19       4     2     2     0
20       3     2     0     3
21       1     0     5     2
22       1     4     1     2
23       0     3     4     1
24       4     3     1     0
25       2     3     1     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_371
CC  AC: HOCOMOCO_2015_11_23_Human_m557_M01557
CC  id: HOCOMOCO_2015_11_23_Human_m557_M01557
CC  name: cluster_371
CC  version: 
CC  name: cluster_371
CC  description: hkvmhwChbAGGTGWGArvhkysmh
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TBX2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m557_M01557
XX
//
AC  cluster_372
XX
ID  cluster_372
XX
DE  agGTGTgAagktGtgAa
P0       A     C     G     T
1     9356  1562  2530  2025
2     2442  1853  9187  1991
3     1024   942 12601   906
4     1229   954  1569 11721
5      759   966 12864   884
6      884  1227  1506 11856
7     1313  1399  9631  3130
8    13719   385  1027   342
9     9472   978  3314  1709
10    3295  2450  8520  1208
11    1412  1079  8940  4042
12    1537  2485  2052  9399
13    2460   889 11645   479
14    2468  1622  1800  9583
15    1653  2270  9093  2457
16   10774  1504  1668  1527
17    7861  2602  2601  2409
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_372
CC  AC: HOCOMOCO_2015_11_23_Human_m555_M01555
CC  id: HOCOMOCO_2015_11_23_Human_m555_M01555
CC  name: cluster_372
CC  version: 
CC  name: cluster_372
CC  description: agGTGTgAagktGtgAa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TBX20_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m555_M01555
XX
//
AC  cluster_373
XX
ID  cluster_373
XX
DE  wwTAAGTAtwtww
P0       A     C     G     T
1       24     1     2    16
2       22     4     2    15
3        2     3     0    38
4       42     1     0     0
5       39     2     0     2
6        4     1    33     5
7        0     0     0    43
8       36     2     5     0
9        8     7     2    26
10      23     2     4    14
11       9     8     6    20
12      20     2     8    13
13      18     6     4    15
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_373
CC  AC: HOCOMOCO_2015_11_23_Human_m378_M01378
CC  id: HOCOMOCO_2015_11_23_Human_m378_M01378
CC  name: cluster_373
CC  version: 
CC  name: cluster_373
CC  description: wwTAAGTAtwtww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NKX31_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m378_M01378
XX
//
AC  cluster_374
XX
ID  cluster_374
XX
DE  CmGkGkGTG
P0       A     C     G     T
1        0     4     0     1
2        3     2     0     0
3        0     0     4     1
4        0     0     3     2
5        0     0     5     0
6        0     0     3     2
7        0     0     5     0
8        0     1     0     4
9        0     1     4     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_374
CC  AC: HOCOMOCO_2015_11_23_Human_m296_M01296
CC  id: HOCOMOCO_2015_11_23_Human_m296_M01296
CC  name: cluster_374
CC  version: 
CC  name: cluster_374
CC  description: CmGkGkGTG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: KLF8_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m296_M01296
XX
//
AC  cluster_375
XX
ID  cluster_375
XX
DE  AGTGTGTGCAct
P0       A     C     G     T
1     1156     0     0     0
2        0     0  1156     0
3        0   141   188   827
4        0     0  1156     0
5        0     0     0  1156
6        0     0  1156     0
7        0     0     0  1156
8        0     0  1156     0
9        0  1148     0     8
10    1144     1     9     2
11     205   537   195   219
12     276   214   106   560
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_375
CC  AC: HOCOMOCO_2015_11_23_Human_m641_M01641
CC  id: HOCOMOCO_2015_11_23_Human_m641_M01641
CC  name: cluster_375
CC  version: 
CC  name: cluster_375
CC  description: AGTGTGTGCAct
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZSCA4_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m641_M01641
XX
//
AC  cluster_376
XX
ID  cluster_376
XX
DE  TsAwGCGTrAA
P0       A     C     G     T
1        4    26     6   129
2        9    89    63     4
3      151     0    13     1
4       74    11    16    64
5        5    36   123     1
6       11   124     8    22
7       30     4   131     0
8        5     3    23   134
9       61     1    92    11
10     146     3     7     9
11     120    22    10    13
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_376
CC  AC: HOCOMOCO_2015_11_23_Human_m425_M01425
CC  id: HOCOMOCO_2015_11_23_Human_m425_M01425
CC  name: cluster_376
CC  version: 
CC  name: cluster_376
CC  description: TsAwGCGTrAA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PAX6_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m425_M01425
XX
//
AC  cluster_377
XX
ID  cluster_377
XX
DE  kggkrrGRcGkskkggg
P0       A     C     G     T
1      116   125   333   199
2      125   118   353   177
3      102   113   383   175
4       61    89   291   332
5      416    24   200   133
6      380     8   370    15
7       21     4   734    14
8      203     4   542    24
9      140   429    41   163
10      28     2   696    47
11      19    63   360   331
12      13   343   386    31
13     119    35   213   406
14      64    57   229   423
15     140    97   467    69
16     120    64   477   112
17     166   103   357   147
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_377
CC  AC: HOCOMOCO_2015_11_23_Human_m468_M01468
CC  id: HOCOMOCO_2015_11_23_Human_m468_M01468
CC  name: cluster_377
CC  version: 
CC  name: cluster_377
CC  description: kggkrrGRcGkskkggg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PROX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m468_M01468
XX
//
AC  cluster_378
XX
ID  cluster_378
XX
DE  hwGdmcwggtkGwdskr
P0       A     C     G     T
1        2     2     1     2
2        5     0     0     2
3        1     0     6     0
4        3     0     2     2
5        3     3     1     0
6        0     5     1     1
7        4     0     0     3
8        1     1     4     1
9        1     1     4     1
10       1     1     0     5
11       0     1     2     4
12       1     0     6     0
13       3     0     0     4
14       2     0     2     3
15       0     2     5     0
16       1     0     2     4
17       3     1     2     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_378
CC  AC: HOCOMOCO_2015_11_23_Human_m318_M01318
CC  id: HOCOMOCO_2015_11_23_Human_m318_M01318
CC  name: cluster_378
CC  version: 
CC  name: cluster_378
CC  description: hwGdmcwggtkGwdskr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MCR_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m318_M01318
XX
//
AC  cluster_379
XX
ID  cluster_379
XX
DE  aaGAyAAGATAAkAka
P0       A     C     G     T
1       58    22    27    34
2       90     5    12    34
3        3     4   128     6
4      137     1     1     2
5        6    64     1    70
6      134     2     2     3
7      129     1     2     9
8        4     1   136     0
9      136     0     0     5
10       0    18     1   122
11     127     5     0     9
12     109     1    22     9
13      30    26    49    36
14     105    13    11    12
15      19    22    43    57
16      69    17    22    33
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_379
CC  AC: HOCOMOCO_2015_11_23_Human_m133_M01133
CC  id: HOCOMOCO_2015_11_23_Human_m133_M01133
CC  name: cluster_379
CC  version: 
CC  name: cluster_379
CC  description: aaGAyAAGATAAkAka
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: EVI1_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m133_M01133
XX
//
AC  cluster_38
XX
ID  cluster_38
XX
DE  vrGATTAr
P0       A     C     G     T
1      8.5   7.5    11     3
2      7.5   0.5  15.5   6.5
3        3   0.5    26   0.5
4       22     7     0     1
5        1     0     0    29
6        1   0.5     0  28.5
7       29   0.5   0.5     0
8       11   2.5    13   3.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_38
CC  AC: cluster_38
CC  id: cluster_38
CC  name: cluster_38
CC  version: 
CC  name: cluster_38
CC  description: vrGATTAr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ggGATTAg
CC  consensus.strict.rc: CTAATCCC
CC  consensus.IUPAC: vrGATTAr
CC  consensus.IUPAC.rc: YTAATCYB
CC  consensus.regexp: [acg][ag]GATTA[ag]
CC  consensus.regexp.rc: [CT]TAATC[CT][CGT]
CC  merged_ID: OTX1_D,CRX_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m409_M01409,HOCOMOCO_2015_11_23_Human_m68_M01068
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_380
XX
ID  cluster_380
XX
DE  rsmkGGSAGdkkGgGss
P0       A     C     G     T
1        5     0     6     0
2        0     5     5     1
3        3     5     1     2
4        1     0     5     5
5        0     1     8     2
6        0     2     9     0
7        0     3     8     0
8        9     0     2     0
9        0     0    10     1
10       3     0     5     3
11       1     0     3     7
12       1     1     6     3
13       0     1    10     0
14       1     2     7     1
15       1     0     8     2
16       1     5     5     0
17       0     5     5     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_380
CC  AC: HOCOMOCO_2015_11_23_Human_m290_M01290
CC  id: HOCOMOCO_2015_11_23_Human_m290_M01290
CC  name: cluster_380
CC  version: 
CC  name: cluster_380
CC  description: rsmkGGSAGdkkGgGss
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: KLF15_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m290_M01290
XX
//
AC  cluster_381
XX
ID  cluster_381
XX
DE  cCtAGAKGGMaGGTGGG
P0       A     C     G     T
1        2    14     2     2
2        0    19     1     0
3        4     2     2    12
4       18     2     0     0
5        0     0    17     3
6       18     2     0     0
7        0     0     5    15
8        0     1    16     3
9        0     0    19     1
10       5    15     0     0
11      13     1     2     4
12       0     0    19     1
13       0     0    17     3
14       0     4     0    16
15       1     0    19     0
16       0     0    20     0
17       2     1    16     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_381
CC  AC: HOCOMOCO_2015_11_23_Human_m609_M01609
CC  id: HOCOMOCO_2015_11_23_Human_m609_M01609
CC  name: cluster_381
CC  version: 
CC  name: cluster_381
CC  description: cCtAGAKGGMaGGTGGG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZBTB4_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m609_M01609
XX
//
AC  cluster_382
XX
ID  cluster_382
XX
DE  ywtyryTTACGk
P0       A     C     G     T
1       13    20     9    21
2       21    14     6    22
3       13    12    13    25
4        3    22     2    36
5       31     0    23     9
6        9    20     2    32
7        1     0     4    58
8        1     0    10    52
9       45    12     3     3
10       1    58     2     2
11       4     6    48     5
12      10     9    17    27
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_382
CC  AC: HOCOMOCO_2015_11_23_Human_m29_M01029
CC  id: HOCOMOCO_2015_11_23_Human_m29_M01029
CC  name: cluster_382
CC  version: 
CC  name: cluster_382
CC  description: ywtyryTTACGk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BATF3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m29_M01029
XX
//
AC  cluster_383
XX
ID  cluster_383
XX
DE  TKTmAGGGGGCr
P0       A     C     G     T
1        2     0     5    17
2        0     0    18     6
3        0     4     3    17
4        8    15     0     1
5       23     0     0     1
6        0     0    24     0
7        0     0    24     0
8        0     0    24     0
9        0     0    24     0
10       0     1    22     1
11       1    22     1     0
12      15     0     9     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_383
CC  AC: HOCOMOCO_2015_11_23_Human_m266_M01266
CC  id: HOCOMOCO_2015_11_23_Human_m266_M01266
CC  name: cluster_383
CC  version: 
CC  name: cluster_383
CC  description: TKTmAGGGGGCr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: INSM1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m266_M01266
XX
//
AC  cluster_384
XX
ID  cluster_384
XX
DE  AACCGGTTtkdC
P0       A     C     G     T
1     8554   633  1222   842
2     9334   465   579   873
3      375 10072   384   420
4      513  9391   327  1020
5      365   184 10042   660
6       24   352 10831    44
7      691   122   131 10307
8      733   737   259  9522
9     1438  1280  1005  7528
10    2512   884  3562  4293
11    4458   822  2822  3149
12     914  7906  1127  1304
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_384
CC  AC: HOCOMOCO_2015_11_23_Human_m196_M01196
CC  id: HOCOMOCO_2015_11_23_Human_m196_M01196
CC  name: cluster_384
CC  version: 
CC  name: cluster_384
CC  description: AACCGGTTtkdC
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: GRHL1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m196_M01196
XX
//
AC  cluster_385
XX
ID  cluster_385
XX
DE  mcrTGCCAGsc
P0       A     C     G     T
1       46    46    13    29
2       28    62    13    31
3       66     2    65     1
4        0     0     0   134
5        0     0   134     0
6        0   134     0     0
7        0   134     0     0
8      105    29     0     0
9        7    15   111     1
10      26    41    45    22
11      33    38    30    33
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_385
CC  AC: HOCOMOCO_2015_11_23_Human_m211_M01211
CC  id: HOCOMOCO_2015_11_23_Human_m211_M01211
CC  name: cluster_385
CC  version: 
CC  name: cluster_385
CC  description: mcrTGCCAGsc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HIC2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m211_M01211
XX
//
AC  cluster_386
XX
ID  cluster_386
XX
DE  vmTcrAtmGAtTggm
P0       A     C     G     T
1        6     5     6     2
2       10     5     1     3
3        4     1     0    14
4        3    13     1     2
5        8     3     5     3
6       14     3     0     2
7        3     3     3    10
8       10     6     0     3
9        0     1    16     2
10      15     2     2     0
11       0     2     4    13
12       1     0     1    17
13       4     3    12     0
14       4     3    10     2
15       7     5     4     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_386
CC  AC: HOCOMOCO_2015_11_23_Human_m241_M01241
CC  id: HOCOMOCO_2015_11_23_Human_m241_M01241
CC  name: cluster_386
CC  version: 
CC  name: cluster_386
CC  description: vmTcrAtmGAtTggm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXA7_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m241_M01241
XX
//
AC  cluster_387
XX
ID  cluster_387
XX
DE  gkGwktTAAyrtAwy
P0       A     C     G     T
1        2     2     5     1
2        0     2     4     4
3        2     0     8     0
4        5     1     1     3
5        0     1     3     6
6        2     0     2     6
7        1     0     0     9
8        9     0     1     0
9        8     0     0     2
10       1     6     0     3
11       6     0     3     1
12       2     1     0     7
13       8     1     1     0
14       6     0     1     3
15       1     3     2     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_387
CC  AC: HOCOMOCO_2015_11_23_Human_m45_M01045
CC  id: HOCOMOCO_2015_11_23_Human_m45_M01045
CC  name: cluster_387
CC  version: 
CC  name: cluster_387
CC  description: gkGwktTAAyrtAwy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: CDC5L_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m45_M01045
XX
//
AC  cluster_388
XX
ID  cluster_388
XX
DE  gAswAAtTaAtwTrr
P0       A     C     G     T
1        2     2     4     1
2        7     0     0     2
3        0     3     6     0
4        3     2     1     3
5        7     1     0     1
6        7     0     1     1
7        2     0     1     6
8        0     1     0     8
9        6     1     1     1
10       8     1     0     0
11       2     2     1     4
12       6     0     0     3
13       2     0     0     7
14       4     2     3     0
15       3     1     3     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_388
CC  AC: HOCOMOCO_2015_11_23_Human_m467_M01467
CC  id: HOCOMOCO_2015_11_23_Human_m467_M01467
CC  name: cluster_388
CC  version: 
CC  name: cluster_388
CC  description: gAswAAtTaAtwTrr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PROP1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m467_M01467
XX
//
AC  cluster_389
XX
ID  cluster_389
XX
DE  rsCCTsTCTGcm
P0       A     C     G     T
1        5     1     4     2
2        0     6     6     0
3        1     9     0     2
4        2     9     1     0
5        1     1     0    10
6        1     3     7     1
7        0     1     0    11
8        0    10     2     0
9        1     0     0    11
10       0     0    11     1
11       1     8     1     2
12       3     7     0     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_389
CC  AC: HOCOMOCO_2015_11_23_Human_m502_M01502
CC  id: HOCOMOCO_2015_11_23_Human_m502_M01502
CC  name: cluster_389
CC  version: 
CC  name: cluster_389
CC  description: rsCCTsTCTGcm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SMAD1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m502_M01502
XX
//
AC  cluster_39
XX
ID  cluster_39
XX
DE  ygrCCAsyAGrkGGCrsyrk
P0       A     C     G     T
1       52 172.5  67.5   203
2     84.5 101.5   203   106
3      138    48 227.5  81.5
4       40   406  23.5  25.5
5        5   481   5.5   3.5
6      366  13.5    75  40.5
7     12.5 310.5 162.5   9.5
8     61.5 229.5  29.5 174.5
9      445   6.5  25.5    18
10       4   2.5   487   1.5
11     185   3.5   303   3.5
12    16.5  11.5   287   180
13     8.5   2.5 480.5   3.5
14      18  11.5   426  39.5
15    45.5 405.5  11.5  32.5
16     201    10 272.5  11.5
17    40.5   275 155.5    24
18    48.5   183  49.5   214
19   185.5 123.5 155.5  30.5
20      31  53.5    76  84.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_39
CC  AC: cluster_39
CC  id: cluster_39
CC  name: cluster_39
CC  version: 
CC  name: cluster_39
CC  description: ygrCCAsyAGrkGGCrsyrk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tggCCAccAGggGGCgctat
CC  consensus.strict.rc: ATAGCGCCCCCTGGTGGCCA
CC  consensus.IUPAC: ygrCCAsyAGrkGGCrsyrk
CC  consensus.IUPAC.rc: MYRSYGCCMYCTRSTGGYCR
CC  consensus.regexp: [ct]g[ag]CCA[cg][ct]AG[ag][gt]GGC[ag][cg][ct][ag][gt]
CC  consensus.regexp.rc: [AC][CT][AG][CG][CT]GCC[AC][CT]CT[AG][CG]TGG[CT]C[AG]
CC  merged_ID: CTCFL_A,CTCF_A
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m69_M01069,HOCOMOCO_2015_11_23_Human_m70_M01070
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_390
XX
ID  cluster_390
XX
DE  sCAGArA
P0       A     C     G     T
1        1     2     2     0
2        0     4     0     1
3        5     0     0     0
4        0     0     4     1
5        5     0     0     0
6        2     0     2     1
7        4     1     0     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_390
CC  AC: HOCOMOCO_2015_11_23_Human_m590_M01590
CC  id: HOCOMOCO_2015_11_23_Human_m590_M01590
CC  name: cluster_390
CC  version: 
CC  name: cluster_390
CC  description: sCAGArA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: UBIP1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m590_M01590
XX
//
AC  cluster_391
XX
ID  cluster_391
XX
DE  tGksAggTGtcrsctg
P0       A     C     G     T
1        2     2     2     7
2        1     0    11     1
3        1     0     4     8
4        0     8     4     1
5       10     1     0     2
6        2     2     7     2
7        2     2     7     2
8        3     0     0    10
9        1     2    10     0
10       3     0     2     8
11       0     9     2     2
12       9     0     4     0
13       2     5     4     2
14       1     8     2     2
15       3     2     2     6
16       2     2     6     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_391
CC  AC: HOCOMOCO_2015_11_23_Human_m577_M01577
CC  id: HOCOMOCO_2015_11_23_Human_m577_M01577
CC  name: cluster_391
CC  version: 
CC  name: cluster_391
CC  description: tGksAggTGtcrsctg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TGIF1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m577_M01577
XX
//
AC  cluster_392
XX
ID  cluster_392
XX
DE  wwTCGAACGwww
P0       A     C     G     T
1     3259   648   851  1898
2     2297    71   332  3956
3      589    53    55  5959
4     1366  4860   424     6
5        0     0  6656     0
6     6655     0     0     1
7     6638     0    14     4
8       10  5008     1  1637
9      102   266  6111   177
10    2282  1082   430  2862
11    2823   335   685  2813
12    1914   543   494  3705
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_392
CC  AC: HOCOMOCO_2015_11_23_Human_m233_M01233
CC  id: HOCOMOCO_2015_11_23_Human_m233_M01233
CC  name: cluster_392
CC  version: 
CC  name: cluster_392
CC  description: wwTCGAACGwww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HSFY1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m233_M01233
XX
//
AC  cluster_393
XX
ID  cluster_393
XX
DE  rmGGTAGGTas
P0       A     C     G     T
1       64    37    99    56
2       94    88    45    29
3       13    13   223     7
4        8     5   239     4
5        1     4    16   235
6      213     5    31     7
7       30     5   216     5
8        9     1   244     2
9       13     9    23   211
10     113    46    61    36
11      51    93    73    39
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_393
CC  AC: HOCOMOCO_2015_11_23_Human_m639_M01639
CC  id: HOCOMOCO_2015_11_23_Human_m639_M01639
CC  name: cluster_393
CC  version: 
CC  name: cluster_393
CC  description: rmGGTAGGTas
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN784_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m639_M01639
XX
//
AC  cluster_394
XX
ID  cluster_394
XX
DE  aatTGACTTwTTGACTT
P0       A     C     G     T
1       25    10    14    13
2       27    13    12    10
3       11     7     8    36
4        1     1     1    59
5        1     2    58     1
6       57     1     2     2
7        0    62     0     0
8        0     1     3    58
9        0     0     0    62
10      21    10     6    25
11       5     4     4    49
12       2     3     3    54
13       4     2    54     2
14      48     6     4     4
15       5    47     1     9
16       4     6     4    48
17       4     7     8    43
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_394
CC  AC: HOCOMOCO_2015_11_23_Human_m394_M01394
CC  id: HOCOMOCO_2015_11_23_Human_m394_M01394
CC  name: cluster_394
CC  version: 
CC  name: cluster_394
CC  description: aatTGACTTwTTGACTT
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NR2E1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m394_M01394
XX
//
AC  cluster_395
XX
ID  cluster_395
XX
DE  krttGkGtssGk
P0       A     C     G     T
1     1635  1305  4384  2747
2     4653   617  3141  1660
3     1009   676  2290  6096
4     2196   365  1007  6503
5      259    98  9694    20
6        6     5  3778  6282
7        9     9  9484   569
8      638   231  2462  6740
9     1436  4330  3491   814
10     751  4386  4034   900
11     897   414  7998   762
12    1249   895  2575  5352
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_395
CC  AC: HOCOMOCO_2015_11_23_Human_m160_M01160
CC  id: HOCOMOCO_2015_11_23_Human_m160_M01160
CC  name: cluster_395
CC  version: 
CC  name: cluster_395
CC  description: krttGkGtssGk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXK1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m160_M01160
XX
//
AC  cluster_396
XX
ID  cluster_396
XX
DE  kAAAGGrAArCcAAAwswGa
P0       A     C     G     T
1        1     0     2     2
2        4     0     1     0
3        5     0     0     0
4        5     0     0     0
5        0     0     4     1
6        1     0     4     0
7        3     0     2     0
8        5     0     0     0
9        5     0     0     0
10       2     0     3     0
11       0     5     0     0
12       1     2     1     1
13       4     0     1     0
14       5     0     0     0
15       5     0     0     0
16       3     0     0     2
17       0     2     3     0
18       2     0     0     3
19       0     0     4     1
20       3     1     1     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_396
CC  AC: HOCOMOCO_2015_11_23_Human_m271_M01271
CC  id: HOCOMOCO_2015_11_23_Human_m271_M01271
CC  name: cluster_396
CC  version: 
CC  name: cluster_396
CC  description: kAAAGGrAArCcAAAwswGa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: IRF5_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m271_M01271
XX
//
AC  cluster_397
XX
ID  cluster_397
XX
DE  aATkTGwTT
P0       A     C     G     T
1        6     0     1     2
2        8     0     0     1
3        0     0     0     9
4        0     2     3     4
5        0     0     1     8
6        0     0     8     1
7        3     0     2     4
8        1     0     1     7
9        2     0     0     7
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_397
CC  AC: HOCOMOCO_2015_11_23_Human_m168_M01168
CC  id: HOCOMOCO_2015_11_23_Human_m168_M01168
CC  name: cluster_397
CC  version: 
CC  name: cluster_397
CC  description: aATkTGwTT
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: FOXP3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m168_M01168
XX
//
AC  cluster_398
XX
ID  cluster_398
XX
DE  rrkGGGGAw
P0       A     C     G     T
1       24     4    30    12
2       19     3    44     4
3       16     3    23    28
4        0     0    70     0
5        0     0    70     0
6        0     0    70     0
7        0     0    70     0
8       67     0     0     3
9       31     5    15    19
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_398
CC  AC: HOCOMOCO_2015_11_23_Human_m349_M01349
CC  id: HOCOMOCO_2015_11_23_Human_m349_M01349
CC  name: cluster_398
CC  version: 
CC  name: cluster_398
CC  description: rrkGGGGAw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MZF1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m349_M01349
XX
//
AC  cluster_399
XX
ID  cluster_399
XX
DE  ktsAyTsrmgyrkr
P0       A     C     G     T
1        4     8    15    10
2        3     8     4    22
3        9    14    12     2
4       30     3     2     2
5        5    17     0    15
6        3     6     2    26
7        3    16    17     1
8       17     3    14     3
9       16    12     5     4
10       1     8    25     3
11       5    20     0    12
12      14     4    19     0
13       0     4    17    16
14      13     2    16     6
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_399
CC  AC: HOCOMOCO_2015_11_23_Human_m427_M01427
CC  id: HOCOMOCO_2015_11_23_Human_m427_M01427
CC  name: cluster_399
CC  version: 
CC  name: cluster_399
CC  description: ktsAyTsrmgyrkr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PAX8_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m427_M01427
XX
//
AC  cluster_4
XX
ID  cluster_4
XX
DE  gyTAATTrryywwwwwwww
P0       A     C     G     T
1      182 160.167   316 98.1667
2    1065.17 4273.75 1649.42 3363.17
3    437.5 979.833 383.75 10734.8
4    12183.5 158.75   105 88.6667
5    12444.3    34 28.3333 29.25
6    11.0833 14.9167 16.0833 12493.8
7      4.5 58.75  59.5 12413.2
8     7974 24.6667 4230.33 306.917
9    3155.67 2445.92 6394.25 540.083
10   1588.08 5242.25 1708.33 3997.25
11   2812.83 3431.25 1914.25 4377.58
12   4320.5 2663.33  1128 4424.08
13   6029.58 1533.58 618.667 4354.08
14   5779.33   775 357.667 5623.92
15   5946.42 517.75 310.667 5761.08
16   5268.33 508.75 640.417 6118.42
17   3992.25 928.333 1308.5 5767.83
18   3102.42  1225 1955.42 3601.08
19   124.667 41.8333    77 123.25
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_4
CC  AC: cluster_4
CC  id: cluster_4
CC  name: cluster_4
CC  version: 
CC  name: cluster_4
CC  description: gyTAATTrryywwwwwwww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gcTAATTagcttaaattta
CC  consensus.strict.rc: TAAATTTAAGCTAATTAGC
CC  consensus.IUPAC: gyTAATTrryywwwwwwww
CC  consensus.IUPAC.rc: WWWWWWWWRRYYAATTARC
CC  consensus.regexp: g[ct]TAATT[ag][ag][ct][ct][at][at][at][at][at][at][at][at]
CC  consensus.regexp.rc: [AT][AT][AT][AT][AT][AT][AT][AT][AG][AG][CT][CT]AATTA[AG]C
CC  merged_ID: ESX1_D,HME1_D,ISX_D,LHX9_D,SHOX2_D,SHOX_D,GBX1_D,GBX2_D,VSX1_D,DLX6_D,DLX1_D,DLX4_D
CC  merge_nb: 12
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m123_M01123,HOCOMOCO_2015_11_23_Human_m217_M01217,HOCOMOCO_2015_11_23_Human_m279_M01279,HOCOMOCO_2015_11_23_Human_m304_M01304,HOCOMOCO_2015_11_23_Human_m500_M01500,HOCOMOCO_2015_11_23_Human_m501_M01501,HOCOMOCO_2015_11_23_Human_m181_M01181,HOCOMOCO_2015_11_23_Human_m182_M01182,HOCOMOCO_2015_11_23_Human_m599_M01599,HOCOMOCO_2015_11_23_Human_m81_M01081,HOCOMOCO_2015_11_23_Human_m76_M01076,HOCOMOCO_2015_11_23_Human_m79_M01079
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_40
XX
ID  cluster_40
XX
DE  yATTGTTw
P0       A     C     G     T
1        5    12     6    14
2       32     0     2     3
3        0     1   2.5  33.5
4      0.5   0.5     0    36
5        2     1    33     1
6      0.5     2     0  34.5
7        2     2     1    32
8     13.5   6.5     5    12
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_40
CC  AC: cluster_40
CC  id: cluster_40
CC  name: cluster_40
CC  version: 
CC  name: cluster_40
CC  description: yATTGTTw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tATTGTTa
CC  consensus.strict.rc: TAACAATA
CC  consensus.IUPAC: yATTGTTw
CC  consensus.IUPAC.rc: WAACAATR
CC  consensus.regexp: [ct]ATTGTT[at]
CC  consensus.regexp.rc: [AT]AACAAT[AG]
CC  merged_ID: SOX13_D,SOX5_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m511_M01511,HOCOMOCO_2015_11_23_Human_m520_M01520
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_400
XX
ID  cluster_400
XX
DE  yCmtTTCChgGAAAwcwcaTkm
P0       A     C     G     T
1       18   239    62   149
2       42   331    52    43
3      125   234    63    46
4       53    60    39   316
5        1     3     1   463
6        1     0     4   463
7       14   447     0     7
8       17   408     3    40
9      134   145    39   150
10     116    11   303    38
11      12     3   357    96
12     408    55     4     1
13     420    31    15     2
14     359    26    70    13
15     159    75    33   201
16      11   291    66   100
17     129   105    37   197
18      29   316    72    51
19     259    70    64    75
20      40    49    51   328
21      50    73   227   118
22     208   130    63    67
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_400
CC  AC: HOCOMOCO_2015_11_23_Human_m540_M01540
CC  id: HOCOMOCO_2015_11_23_Human_m540_M01540
CC  name: cluster_400
CC  version: 
CC  name: cluster_400
CC  description: yCmtTTCChgGAAAwcwcaTkm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: STAT1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m540_M01540
XX
//
AC  cluster_401
XX
ID  cluster_401
XX
DE  gGGGgCkG
P0       A     C     G     T
1        2     2     6     1
2        2     0     9     0
3        0     1     9     1
4        0     0    11     0
5        2     2     7     0
6        2     9     0     0
7        0     0     6     5
8        0     1    10     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_401
CC  AC: HOCOMOCO_2015_11_23_Human_m295_M01295
CC  id: HOCOMOCO_2015_11_23_Human_m295_M01295
CC  name: cluster_401
CC  version: 
CC  name: cluster_401
CC  description: gGGGgCkG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: KLF6_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m295_M01295
XX
//
AC  cluster_402
XX
ID  cluster_402
XX
DE  yATRTCGCGAyAk
P0       A     C     G     T
1      198  1438    93  2676
2     4296    57    40    12
3        2     1     1  4401
4     1392     0  3013     0
5        0     1     0  4404
6        0  4405     0     0
7        0     0  4405     0
8        0  4287     1   117
9        0     0  4405     0
10    4405     0     0     0
11      10  1601    10  2784
12    4329    15    14    47
13     258   546  1359  2242
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_402
CC  AC: HOCOMOCO_2015_11_23_Human_m604_M01604
CC  id: HOCOMOCO_2015_11_23_Human_m604_M01604
CC  name: cluster_402
CC  version: 
CC  name: cluster_402
CC  description: yATRTCGCGAyAk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZBED1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m604_M01604
XX
//
AC  cluster_403
XX
ID  cluster_403
XX
DE  cTTGGCw
P0       A     C     G     T
1       66   104    43    52
2       15    46     2   202
3        2     1    14   248
4        3     1   255     6
5        8     3   253     1
6       14   235    12     4
7      118    34    37    76
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_403
CC  AC: HOCOMOCO_2015_11_23_Human_m364_M01364
CC  id: HOCOMOCO_2015_11_23_Human_m364_M01364
CC  name: cluster_403
CC  version: 
CC  name: cluster_403
CC  description: cTTGGCw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: NFIA_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m364_M01364
XX
//
AC  cluster_404
XX
ID  cluster_404
XX
DE  CGkTkky
P0       A     C     G     T
1        5    43     1     9
2        0     0    55     3
3        4     6    17    31
4        0     0     9    49
5        5     1    36    16
6        3     1    28    26
7        9    31     2    16
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_404
CC  AC: HOCOMOCO_2015_11_23_Human_m73_M01073
CC  id: HOCOMOCO_2015_11_23_Human_m73_M01073
CC  name: cluster_404
CC  version: 
CC  name: cluster_404
CC  description: CGkTkky
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: CXXC1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m73_M01073
XX
//
AC  cluster_405
XX
ID  cluster_405
XX
DE  GACGTGkCmtww
P0       A     C     G     T
1        1     0  1538    38
2     1577     0     0     0
3        0  1577     0     0
4        0     0  1577     0
5        0     0     0  1577
6        1     0  1574     2
7        1     0   686   890
8      256  1193    34    94
9      711   481   185   200
10     365   346   127   739
11     531    49    35   962
12     610   176   240   551
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_405
CC  AC: HOCOMOCO_2015_11_23_Human_m602_M01602
CC  id: HOCOMOCO_2015_11_23_Human_m602_M01602
CC  name: cluster_405
CC  version: 
CC  name: cluster_405
CC  description: GACGTGkCmtww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: XBP1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m602_M01602
XX
//
AC  cluster_406
XX
ID  cluster_406
XX
DE  kgydgGGwAAsyss
P0       A     C     G     T
1        1     1    10     4
2        1     3     9     3
3        2     5     0     9
4        5     1     5     5
5        2     3     8     3
6        3     0    13     0
7        3     0    13     0
8        7     3     0     6
9       14     1     1     0
10      13     0     1     2
11       0     9     4     3
12       3     6     3     4
13       1     9     6     0
14       1     9     6     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_406
CC  AC: HOCOMOCO_2015_11_23_Human_m614_M01614
CC  id: HOCOMOCO_2015_11_23_Human_m614_M01614
CC  name: cluster_406
CC  version: 
CC  name: cluster_406
CC  description: kgydgGGwAAsyss
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZEP2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m614_M01614
XX
//
AC  cluster_407
XX
ID  cluster_407
XX
DE  cAmmAAwshhCATTGTCs
P0       A     C     G     T
1        8    39     5     5
2       45     9     3     0
3       37    18     0     2
4       22    35     0     0
5       48     9     0     0
6       42     0     3    12
7       15     0     6    36
8       12    16    16    13
9       15    18     6    18
10      15    21     0    21
11       4    40     2    11
12      57     0     0     0
13       0     0     0    57
14       0     0     0    57
15       0     0    57     0
16       0     0     0    57
17       0    40    12     5
18       9    23    16     9
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_407
CC  AC: HOCOMOCO_2015_11_23_Human_m513_M01513
CC  id: HOCOMOCO_2015_11_23_Human_m513_M01513
CC  name: cluster_407
CC  version: 
CC  name: cluster_407
CC  description: cAmmAAwshhCATTGTCs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SOX17_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m513_M01513
XX
//
AC  cluster_408
XX
ID  cluster_408
XX
DE  CaAkrGyGrTkGyGr
P0       A     C     G     T
1        1    25     6     0
2       19     5     6     2
3       28     2     2     0
4        2     2     8    20
5       20     0    12     0
6        6     0    26     0
7        0    21     0    11
8        0     0    32     0
9       12     1    12     7
10       0     1     7    24
11       6     4    12    10
12       1     0    26     5
13       0    11     6    15
14       3     1    28     0
15       9     5    13     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_408
CC  AC: HOCOMOCO_2015_11_23_Human_m610_M01610
CC  id: HOCOMOCO_2015_11_23_Human_m610_M01610
CC  name: cluster_408
CC  version: 
CC  name: cluster_408
CC  description: CaAkrGyGrTkGyGr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZBTB4_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m610_M01610
XX
//
AC  cluster_409
XX
ID  cluster_409
XX
DE  ayycATTGA
P0       A     C     G     T
1        3     1     1     0
2        0     3     0     2
3        1     2     0     2
4        0     3     1     1
5        5     0     0     0
6        0     0     0     5
7        0     0     0     5
8        0     1     4     0
9        5     0     0     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_409
CC  AC: HOCOMOCO_2015_11_23_Human_m202_M01202
CC  id: HOCOMOCO_2015_11_23_Human_m202_M01202
CC  name: cluster_409
CC  version: 
CC  name: cluster_409
CC  description: ayycATTGA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HBP1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m202_M01202
XX
//
AC  cluster_41
XX
ID  cluster_41
XX
DE  mrwAGGTCA
P0       A     C     G     T
1      178 232.5  53.5  76.5
2      326    62   136  16.5
3      247    22  78.5   193
4    458.5   0.5  80.5     1
5       15     0   523   2.5
6        5     1   500  34.5
7     12.5    30    82   416
8       11   444  66.5    19
9    501.5    11    11    17
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_41
CC  AC: cluster_41
CC  id: cluster_41
CC  name: cluster_41
CC  version: 
CC  name: cluster_41
CC  description: mrwAGGTCA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: caaAGGTCA
CC  consensus.strict.rc: TGACCTTTG
CC  consensus.IUPAC: mrwAGGTCA
CC  consensus.IUPAC.rc: TGACCTWYK
CC  consensus.regexp: [ac][ag][at]AGGTCA
CC  consensus.regexp.rc: TGACCT[AT][CT][GT]
CC  merged_ID: PPARG_S,COT1_S
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m461_M01461,HOCOMOCO_2015_11_23_Human_m57_M01057
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_410
XX
ID  cluster_410
XX
DE  gGrGssmycwrkaGGGg
P0       A     C     G     T
1        6     1    15     3
2        5     0    20     0
3       10     0    15     0
4        0     0    25     0
5        0     8    17     0
6        1    14     7     3
7       13     9     2     1
8        5     8     3     9
9        5     8     6     6
10      11     2     5     7
11       8     6    11     0
12       4     2    12     7
13      17     4     3     1
14       2     0    20     3
15       1     0    24     0
16       0     0    25     0
17       2     1    17     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_410
CC  AC: HOCOMOCO_2015_11_23_Human_m441_M01441
CC  id: HOCOMOCO_2015_11_23_Human_m441_M01441
CC  name: cluster_410
CC  version: 
CC  name: cluster_410
CC  description: gGrGssmycwrkaGGGg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PLAG1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m441_M01441
XX
//
AC  cluster_411
XX
ID  cluster_411
XX
DE  sGccGCCatstyggctscGGGC
P0       A     C     G     T
1       69   222   161    46
2       31    70   369    28
3       31   336    93    38
4       75   291    89    43
5       33    40   366    59
6        8   456    27     7
7       18   452    21     7
8      298    91    70    39
9        9   113    88   288
10      30   261   150    57
11      20    91   104   283
12      12   180    25   281
13      51    93   251   103
14      81   118   264    35
15      92   196   114    96
16      56   123    88   231
17      59   170   170    99
18      87   249    56   106
19      12    64   341    81
20      12    74   394    18
21       9    97   371    21
22      36   391    34    37
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_411
CC  AC: HOCOMOCO_2015_11_23_Human_m580_M01580
CC  id: HOCOMOCO_2015_11_23_Human_m580_M01580
CC  name: cluster_411
CC  version: 
CC  name: cluster_411
CC  description: sGccGCCatstyggctscGGGC
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: THAP1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m580_M01580
XX
//
AC  cluster_412
XX
ID  cluster_412
XX
DE  tTTAttakrGa
P0       A     C     G     T
1        3     2     2     6
2        0     0     1    12
3        1     0     2    10
4       10     1     0     2
5        1     3     0     9
6        1     0     3     9
7        8     2     2     1
8        0     1     8     4
9        6     0     6     1
10       0     0    11     2
11       8     2     1     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_412
CC  AC: HOCOMOCO_2015_11_23_Human_m259_M01259
CC  id: HOCOMOCO_2015_11_23_Human_m259_M01259
CC  name: cluster_412
CC  version: 
CC  name: cluster_412
CC  description: tTTAttakrGa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HXD13_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m259_M01259
XX
//
AC  cluster_413
XX
ID  cluster_413
XX
DE  rGrTGaTrGAGCC
P0       A     C     G     T
1       14     8    10     1
2        3     0    27     3
3       20     0    13     0
4        3     6     0    24
5        4     5    23     1
6       22     8     3     0
7        0     1     1    31
8       16     1    13     3
9        0     0    33     0
10      33     0     0     0
11       0     0    33     0
12       3    28     1     1
13       1    23     6     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_413
CC  AC: HOCOMOCO_2015_11_23_Human_m611_M01611
CC  id: HOCOMOCO_2015_11_23_Human_m611_M01611
CC  name: cluster_413
CC  version: 
CC  name: cluster_413
CC  description: rGrTGaTrGAGCC
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZBTB6_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m611_M01611
XX
//
AC  cluster_414
XX
ID  cluster_414
XX
DE  svsssscscAGGCCbsGsc
P0       A     C     G     T
1       65   177   179    59
2      145   140   146    49
3       48   121   270    41
4       77   150   214    39
5       53   134   246    47
6       57   179   163    81
7       71   225   113    71
8       43   189   163    85
9       35   279    96    70
10     476     2     1     1
11       2     2   476     0
12       0     1   479     0
13       0   477     3     0
14       1   425    42    12
15       9   157   128   186
16     108   197   144    31
17      97     4   373     6
18      67   165   238    10
19      61   252    59   108
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_414
CC  AC: HOCOMOCO_2015_11_23_Human_m616_M01616
CC  id: HOCOMOCO_2015_11_23_Human_m616_M01616
CC  name: cluster_414
CC  version: 
CC  name: cluster_414
CC  description: svsssscscAGGCCbsGsc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZFX_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m616_M01616
XX
//
AC  cluster_415
XX
ID  cluster_415
XX
DE  krrkkmtGsmAm
P0       A     C     G     T
1        2     2     3     3
2        7     0     3     0
3        3     2     3     2
4        0     0     6     4
5        1     0     6     3
6        3     7     0     0
7        2     1     0     7
8        0     1     9     0
9        0     6     4     0
10       6     4     0     0
11       9     0     0     1
12       4     4     1     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_415
CC  AC: HOCOMOCO_2015_11_23_Human_m215_M01215
CC  id: HOCOMOCO_2015_11_23_Human_m215_M01215
CC  name: cluster_415
CC  version: 
CC  name: cluster_415
CC  description: krrkkmtGsmAm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HLTF_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m215_M01215
XX
//
AC  cluster_416
XX
ID  cluster_416
XX
DE  raTTAAwyrrwATcWAAwtwww
P0       A     C     G     T
1       17     5    14     8
2       30     4     0    10
3        5     0     0    39
4        7     0     0    37
5       42     2     0     0
6       41     3     0     0
7       21     0     1    22
8        0    18    10    16
9       13     8    19     4
10      22     2    16     4
11      20     2     7    15
12      32     0     4     8
13       6     2     4    32
14       7    25     2    10
15      32     0     0    12
16      33     2     0     9
17      42     0     0     2
18      18     0     2    24
19       5     6     4    29
20      23     6     1    14
21      27     3     0    14
22      29     1     3    11
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_416
CC  AC: HOCOMOCO_2015_11_23_Human_m11_M01011
CC  id: HOCOMOCO_2015_11_23_Human_m11_M01011
CC  name: cluster_416
CC  version: 
CC  name: cluster_416
CC  description: raTTAAwyrrwATcWAAwtwww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ARI3A_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m11_M01011
XX
//
AC  cluster_417
XX
ID  cluster_417
XX
DE  mkwtTywmwthwCyhytww
P0       A     C     G     T
1        8    13     3     3
2        3     6     7    11
3       15     2     3     7
4        4     5     3    15
5        0     0     2    25
6        3     7     1    16
7        9     2     6    10
8       12     9     0     6
9       10     4     4     9
10       6     2     3    16
11      10     8     2     7
12      16     2     0     9
13       1    23     2     1
14       2    12     2    11
15       7    13     0     7
16       1     8     4    14
17       5     5     4    13
18       8     6     4     9
19      10     5     4     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_417
CC  AC: HOCOMOCO_2015_11_23_Human_m336_M01336
CC  id: HOCOMOCO_2015_11_23_Human_m336_M01336
CC  name: cluster_417
CC  version: 
CC  name: cluster_417
CC  description: mkwtTywmwthwCyhytww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MNX1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m336_M01336
XX
//
AC  cluster_418
XX
ID  cluster_418
XX
DE  tycGAGGyTAg
P0       A     C     G     T
1       14    17    34    73
2       11    46    17    64
3       10    94    10    24
4       14     9   106     9
5      125     2     7     4
6        1     3   128     6
7        1     0   136     1
8        5    91     4    38
9        6     5     2   125
10     122     3     9     4
11      27    22    60    29
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_418
CC  AC: HOCOMOCO_2015_11_23_Human_m622_M01622
CC  id: HOCOMOCO_2015_11_23_Human_m622_M01622
CC  name: cluster_418
CC  version: 
CC  name: cluster_418
CC  description: tycGAGGyTAg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZKSC3_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m622_M01622
XX
//
AC  cluster_419
XX
ID  cluster_419
XX
DE  rkskkkGkkGkTGkTTkGkGkt
P0       A     C     G     T
1        7     3    10     6
2        2     4    11     9
3        0     7    16     3
4        2     0    15     9
5        2     3    12     9
6        4     1    11    10
7        0     1    20     5
8        0     4    13     9
9        0     1    17     8
10       0     1    23     2
11       0     3    15     8
12       0     0     2    24
13       1     0    24     1
14       2     3    12     9
15       0     1     6    19
16       2     1     4    19
17       0     1     8    17
18       0     2    24     0
19       2     0    17     7
20       0     1    22     3
21       0     0    17     9
22       4     4     5    13
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_419
CC  AC: HOCOMOCO_2015_11_23_Human_m490_M01490
CC  id: HOCOMOCO_2015_11_23_Human_m490_M01490
CC  name: cluster_419
CC  version: 
CC  name: cluster_419
CC  description: rkskkkGkkGkTGkTTkGkGkt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: RREB1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m490_M01490
XX
//
AC  cluster_42
XX
ID  cluster_42
XX
DE  rsWGATAAgrw
P0       A     C     G     T
1    190.25 67.25   149 109.25
2    91.25 220.75 168.25  35.5
3    361.25  7.75     3 143.75
4     1.25   0.5   514     0
5    513.75     1  0.25  0.75
6     4.25     1     1 509.5
7    492.5  2.25     3    18
8    449.75 10.25 42.75    13
9    94.75    90 294.25 36.75
10     200 83.75 170.5 34.75
11   100.75 40.75  42.5    66
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_42
CC  AC: cluster_42
CC  id: cluster_42
CC  name: cluster_42
CC  version: 
CC  name: cluster_42
CC  description: rsWGATAAgrw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: acAGATAAgaa
CC  consensus.strict.rc: TTCTTATCTGT
CC  consensus.IUPAC: rsWGATAAgrw
CC  consensus.IUPAC.rc: WYCTTATCWSY
CC  consensus.regexp: [ag][cg][AT]GATAAg[ag][at]
CC  consensus.regexp.rc: [AT][CT]CTTATC[AT][CG][CT]
CC  merged_ID: GATA4_B,GATA1_S,GATA2_A,GATA3_C
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m177_M01177,HOCOMOCO_2015_11_23_Human_m174_M01174,HOCOMOCO_2015_11_23_Human_m175_M01175,HOCOMOCO_2015_11_23_Human_m176_M01176
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_420
XX
ID  cluster_420
XX
DE  scCtGmmcwgrCcrGA
P0       A     C     G     T
1        1     4     6     2
2        0     7     3     3
3        2    10     1     0
4        3     1     2     7
5        0     1    11     1
6        6     4     2     1
7        4     4     2     3
8        3     7     0     3
9        4     1     3     5
10       2     3     6     2
11       6     0     7     0
12       0    11     2     0
13       2     9     1     1
14       6     0     5     2
15       0     0    13     0
16      10     1     1     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_420
CC  AC: HOCOMOCO_2015_11_23_Human_m571_M01571
CC  id: HOCOMOCO_2015_11_23_Human_m571_M01571
CC  name: cluster_420
CC  version: 
CC  name: cluster_420
CC  description: scCtGmmcwgrCcrGA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TFCP2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m571_M01571
XX
//
AC  cluster_421
XX
ID  cluster_421
XX
DE  AWTwwyssssAATAT
P0       A     C     G     T
1       72     1     1     1
2       54     0     0    21
3       12     0     0    63
4       44     0     0    31
5       39     2     1    33
6       13    21     9    32
7        0    41    30     4
8        2    25    46     2
9        0    37    33     5
10       2    32    40     1
11      71     0     3     1
12      59     1     0    15
13       2     1     0    72
14      59     0     1    15
15       2     1     1    71
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_421
CC  AC: HOCOMOCO_2015_11_23_Human_m220_M01220
CC  id: HOCOMOCO_2015_11_23_Human_m220_M01220
CC  name: cluster_421
CC  version: 
CC  name: cluster_421
CC  description: AWTwwyssssAATAT
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HMGA2_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m220_M01220
XX
//
AC  cluster_422
XX
ID  cluster_422
XX
DE  gCAyGkGvcmmcsbcGTGk
P0       A     C     G     T
1        1     2     6     1
2        1     9     0     0
3        9     0     0     1
4        0     4     1     5
5        1     1     8     0
6        1     0     3     6
7        0     0    10     0
8        4     3     3     0
9        2     7     0     1
10       6     3     1     0
11       4     5     0     1
12       2     6     0     2
13       1     4     4     1
14       1     3     3     3
15       0     7     2     1
16       1     0     9     0
17       0     0     0    10
18       0     1     9     0
19       1     2     4     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_422
CC  AC: HOCOMOCO_2015_11_23_Human_m333_M01333
CC  id: HOCOMOCO_2015_11_23_Human_m333_M01333
CC  name: cluster_422
CC  version: 
CC  name: cluster_422
CC  description: gCAyGkGvcmmcsbcGTGk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MLXPL_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m333_M01333
XX
//
AC  cluster_423
XX
ID  cluster_423
XX
DE  kcrwkGcAtkmTGGGArdtvyw
P0       A     C     G     T
1        0     4    15     7
2        4    17     2     3
3       12     1     9     4
4       11     3     5     7
5        2     0    13    11
6        0     0    26     0
7        6    18     2     0
8       23     3     0     0
9        0     4     5    17
10       0     4    10    12
11      10    13     3     0
12       0     0     2    24
13       0     0    26     0
14       0     0    26     0
15       4     0    20     2
16      19     1     2     4
17      13     5     8     0
18       8     2     8     8
19       3     3     4    16
20       7     9    10     0
21       2     7     6    11
22      10     6     2     8
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_423
CC  AC: HOCOMOCO_2015_11_23_Human_m623_M01623
CC  id: HOCOMOCO_2015_11_23_Human_m623_M01623
CC  name: cluster_423
CC  version: 
CC  name: cluster_423
CC  description: kcrwkGcAtkmTGGGArdtvyw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN143_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m623_M01623
XX
//
AC  cluster_424
XX
ID  cluster_424
XX
DE  gkGGGGgGcCTTGAAA
P0       A     C     G     T
1       20    23    57    30
2       17    25    44    44
3        2     2   119     7
4        1     1   128     0
5        1     0   129     0
6        3     2   107    18
7       30     9    83     8
8       10     4   109     7
9       28    86     1    15
10       0   128     2     0
11       0     0     0   130
12       9     0     6   115
13       3     0   127     0
14     130     0     0     0
15     130     0     0     0
16     130     0     0     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_424
CC  AC: HOCOMOCO_2015_11_23_Human_m464_M01464
CC  id: HOCOMOCO_2015_11_23_Human_m464_M01464
CC  name: cluster_424
CC  version: 
CC  name: cluster_424
CC  description: gkGGGGgGcCTTGAAA
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PRDM4_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m464_M01464
XX
//
AC  cluster_425
XX
ID  cluster_425
XX
DE  tGTTAAAyGTAGATTAAgy
P0       A     C     G     T
1        8     9     8    25
2        2     2    44     2
3        2     4     2    42
4        2     7     0    41
5       49     1     0     0
6       48     0     1     1
7       45     3     0     2
8        5    21     1    23
9        2     0    48     0
10       0     0     0    50
11      42     0     5     3
12       1     0    49     0
13      50     0     0     0
14       0     4     0    46
15       0     2     2    46
16      48     1     0     1
17      44     2     2     2
18       8     6    33     3
19      10    13     8    19
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_425
CC  AC: HOCOMOCO_2015_11_23_Human_m626_M01626
CC  id: HOCOMOCO_2015_11_23_Human_m626_M01626
CC  name: cluster_425
CC  version: 
CC  name: cluster_425
CC  description: tGTTAAAyGTAGATTAAgy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN232_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m626_M01626
XX
//
AC  cluster_426
XX
ID  cluster_426
XX
DE  atTbkCCTtmYwtGGccw
P0       A     C     G     T
1      199   101    88    94
2       89    97    86   210
3       57    81    11   333
4       20   133   129   200
5       65    87   134   196
6        6   444     4    28
7        1   434     5    42
8       89    46     5   342
9       41    13   101   327
10     232   127    22   101
11      10   127     4   341
12     184    24    15   259
13      36   101    23   322
14     103     1   350    28
15      14    10   439    19
16      66   218   107    91
17      55   301    52    74
18     174    61    42   205
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_426
CC  AC: HOCOMOCO_2015_11_23_Human_m536_M01536
CC  id: HOCOMOCO_2015_11_23_Human_m536_M01536
CC  name: cluster_426
CC  version: 
CC  name: cluster_426
CC  description: atTbkCCTtmYwtGGccw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SRF_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m536_M01536
XX
//
AC  cluster_427
XX
ID  cluster_427
XX
DE  asTyyyrwwwTGAsTcAk
P0       A     C     G     T
1      246    48   119    86
2       58   179   148   114
3       21    11    67   400
4       70   172    28   229
5       48   164    31   256
6       88   237    43   131
7      139    55   209    96
8      178    55    95   171
9      147     9    51   292
10     309    49    12   129
11       0     0     0   499
12       1     8   448    42
13     482     0     5    12
14      38   260   190    11
15      17     7     2   473
16     106   308    66    19
17     363    10    48    78
18      81    89   198   131
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_427
CC  AC: HOCOMOCO_2015_11_23_Human_m30_M01030
CC  id: HOCOMOCO_2015_11_23_Human_m30_M01030
CC  name: cluster_427
CC  version: 
CC  name: cluster_427
CC  description: asTyyyrwwwTGAsTcAk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: BATF_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m30_M01030
XX
//
AC  cluster_428
XX
ID  cluster_428
XX
DE  rmsytmgCGGACGTtv
P0       A     C     G     T
1       14     3    16     9
2       14    16     6     6
3        2    17    20     3
4        8    15     7    12
5       10    10     3    19
6       20    13     7     2
7        7     5    24     6
8        3    39     0     0
9        0     0    42     0
10       2     1    39     0
11      41     1     0     0
12       0    40     2     0
13       0     3    36     3
14       1     0     0    41
15       8     1     5    28
16      16    11    11     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_428
CC  AC: HOCOMOCO_2015_11_23_Human_m213_M01213
CC  id: HOCOMOCO_2015_11_23_Human_m213_M01213
CC  name: cluster_428
CC  version: 
CC  name: cluster_428
CC  description: rmsytmgCGGACGTtv
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HINFP_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m213_M01213
XX
//
AC  cluster_429
XX
ID  cluster_429
XX
DE  GrCACCCwAGGGTGC
P0       A     C     G     T
1        0     0    22     0
2        9     0    13     0
3        0    22     0     0
4       22     0     0     0
5        0    22     0     0
6        0    22     0     0
7        0    22     0     0
8       12     0     0    10
9       21     0     0     1
10       0     0    22     0
11       0     0    22     0
12       0     0    22     0
13       0     0     0    22
14       0     0    22     0
15       4    18     0     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_429
CC  AC: HOCOMOCO_2015_11_23_Human_m632_M01632
CC  id: HOCOMOCO_2015_11_23_Human_m632_M01632
CC  name: cluster_429
CC  version: 
CC  name: cluster_429
CC  description: GrCACCCwAGGGTGC
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN423_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m632_M01632
XX
//
AC  cluster_43
XX
ID  cluster_43
XX
DE  TGTTTRCwywg
P0       A     C     G     T
1        3     1     0   256
2     43.5   0.5   215     1
3      3.5   0.5     1   255
4      2.5   0.5    19   238
5      1.5     1  29.5   228
6      191     1    68     0
7        2   217   2.5  38.5
8     95.5    34     1 129.5
9     14.5 102.5    22   121
10     136   3.5   7.5   113
11    48.5    30   139  42.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_43
CC  AC: cluster_43
CC  id: cluster_43
CC  name: cluster_43
CC  version: 
CC  name: cluster_43
CC  description: TGTTTRCwywg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: TGTTTACttag
CC  consensus.strict.rc: CTAAGTAAACA
CC  consensus.IUPAC: TGTTTRCwywg
CC  consensus.IUPAC.rc: CWRWGYAAACA
CC  consensus.regexp: TGTTT[AG]C[at][ct][at]g
CC  consensus.regexp.rc: C[AT][AG][AT]G[CT]AAACA
CC  merged_ID: FOXA2_A,FOXF2_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m144_M01144,HOCOMOCO_2015_11_23_Human_m153_M01153
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_430
XX
ID  cluster_430
XX
DE  mGkgcCGTGyGCAamrs
P0       A     C     G     T
1       15    12     4     4
2        5     2    28     0
3        3     1    13    18
4        8     6    20     1
5        8    18     2     7
6        3    29     1     2
7        0     0    35     0
8        3     1     5    26
9        0     1    34     0
10       0    11     0    24
11       0     0    35     0
12       0    34     0     1
13      35     0     0     0
14      19     4     6     6
15      21    10     2     2
16      14     4    10     7
17       4    15    11     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_430
CC  AC: HOCOMOCO_2015_11_23_Human_m339_M01339
CC  id: HOCOMOCO_2015_11_23_Human_m339_M01339
CC  name: cluster_430
CC  version: 
CC  name: cluster_430
CC  description: mGkgcCGTGyGCAamrs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MTF1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m339_M01339
XX
//
AC  cluster_431
XX
ID  cluster_431
XX
DE  mkcwskGGGtysmcyag
P0       A     C     G     T
1       15    10     7     6
2        3     2    21    12
3        0    24     9     5
4       15     7     5    11
5        3    13    22     0
6        5     1    12    20
7        1     0    33     4
8        0     0    38     0
9        0     3    27     8
10       0     9     6    23
11       7    12     7    12
12       1    15    13     9
13      13    15     6     4
14       1    24     8     5
15       3    15     8    12
16      15     7     8     8
17       6     8    19     5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_431
CC  AC: HOCOMOCO_2015_11_23_Human_m607_M01607
CC  id: HOCOMOCO_2015_11_23_Human_m607_M01607
CC  name: cluster_431
CC  version: 
CC  name: cluster_431
CC  description: mkcwskGGGtysmcyag
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZBT7A_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m607_M01607
XX
//
AC  cluster_432
XX
ID  cluster_432
XX
DE  shwhvmwTTGWybwykyyc
P0       A     C     G     T
1        1     2     4     1
2        3     3     0     2
3        4     0     1     3
4        2     3     1     2
5        2     3     2     1
6        3     4     1     0
7        5     0     0     3
8        0     0     0     8
9        0     0     0     8
10       0     0     8     0
11       2     0     0     6
12       1     2     0     5
13       1     3     2     2
14       2     1     0     5
15       1     3     1     3
16       1     1     3     3
17       0     2     1     5
18       0     5     1     2
19       1     5     1     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_432
CC  AC: HOCOMOCO_2015_11_23_Human_m514_M01514
CC  id: HOCOMOCO_2015_11_23_Human_m514_M01514
CC  name: cluster_432
CC  version: 
CC  name: cluster_432
CC  description: shwhvmwTTGWybwykyyc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SOX18_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m514_M01514
XX
//
AC  cluster_433
XX
ID  cluster_433
XX
DE  rrgGTGyTCwGTTAACAmtw
P0       A     C     G     T
1        6     4     9     4
2        9     4     7     3
3        1     3    16     3
4        3     0    20     0
5        0     1     3    19
6        1     0    22     0
7        1     8     1    13
8        0     0     1    22
9        1    20     0     2
10       6     0     2    15
11       3     0    20     0
12       0     1     2    20
13       1     1     1    20
14      19     3     1     0
15      22     0     1     0
16       0    20     1     2
17      18     0     2     3
18       6    11     2     4
19       5     4     3    11
20       9     3     4     7
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_433
CC  AC: HOCOMOCO_2015_11_23_Human_m640_M01640
CC  id: HOCOMOCO_2015_11_23_Human_m640_M01640
CC  name: cluster_433
CC  version: 
CC  name: cluster_433
CC  description: rrgGTGyTCwGTTAACAmtw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZSC16_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m640_M01640
XX
//
AC  cluster_434
XX
ID  cluster_434
XX
DE  TTCGywskAhgCGGGww
P0       A     C     G     T
1      107    91    98  1923
2      307    28    46  1838
3       27  2115    27    50
4        1     0  2213     5
5      105   631    48  1435
6      960   204   106   949
7      397   795   732   295
8      357   492   747   623
9     2198     0     9    12
10     645   612   110   852
11     486   251  1482     0
12      13  2186    20     0
13       0     1  2218     0
14      18     4  2192     5
15     165   119  1784   151
16     751   263   289   916
17     662   395   332   830
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_434
CC  AC: HOCOMOCO_2015_11_23_Human_m54_M01054
CC  id: HOCOMOCO_2015_11_23_Human_m54_M01054
CC  name: cluster_434
CC  version: 
CC  name: cluster_434
CC  description: TTCGywskAhgCGGGww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: CENPB_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m54_M01054
XX
//
AC  cluster_435
XX
ID  cluster_435
XX
DE  wkrwdmwwCsGGGmgs
P0       A     C     G     T
1       21     7     8    15
2       11     5    18    17
3       19     2    22     8
4       16     6    12    17
5       17     5    15    14
6       24    14     5     8
7       27     1     8    15
8       13     8     4    26
9        4    43     4     0
10       2    15    31     3
11       9     0    37     5
12       0     0    48     3
13       6     2    39     4
14      13    23    11     4
15      10     6    30     5
16      11    17    20     3
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_435
CC  AC: HOCOMOCO_2015_11_23_Human_m568_M01568
CC  id: HOCOMOCO_2015_11_23_Human_m568_M01568
CC  name: cluster_435
CC  version: 
CC  name: cluster_435
CC  description: wkrwdmwwCsGGGmgs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: TF7L1_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m568_M01568
XX
//
AC  cluster_436
XX
ID  cluster_436
XX
DE  kgGGkmkCAGCTGCGkCCyy
P0       A     C     G     T
1        7    13    16    19
2        6     5    35     9
3        2     2    42     9
4        5     0    47     3
5        7     7    25    16
6       27    25     3     0
7        7     8    24    16
8        3    50     2     0
9       53     1     1     0
10       0     4    48     3
11       0    55     0     0
12       3     0     0    52
13       0     0    54     1
14       1    49     3     2
15       3     1    45     6
16       0    12    16    27
17       1    49     3     2
18       5    39     4     7
19       6    23     6    20
20       7    22     4    22
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_436
CC  AC: HOCOMOCO_2015_11_23_Human_m203_M01203
CC  id: HOCOMOCO_2015_11_23_Human_m203_M01203
CC  name: cluster_436
CC  version: 
CC  name: cluster_436
CC  description: kgGGkmkCAGCTGCGkCCyy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: HEN1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m203_M01203
XX
//
AC  cluster_437
XX
ID  cluster_437
XX
DE  crgGraAmwsmaCAssTGysm
P0       A     C     G     T
1        2     5     1     2
2        4     0     4     2
3        2     1     7     0
4        0     2     8     0
5        6     0     4     0
6        7     1     2     0
7        9     0     0     1
8        3     3     2     2
9        4     2     1     3
10       1     4     4     1
11       4     3     1     2
12       6     2     2     0
13       1     9     0     0
14       8     0     0     2
15       0     3     5     2
16       2     5     3     0
17       2     0     0     8
18       0     0    10     0
19       2     4     0     4
20       0     5     5     0
21       3     4     1     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_437
CC  AC: HOCOMOCO_2015_11_23_Human_m471_M01471
CC  id: HOCOMOCO_2015_11_23_Human_m471_M01471
CC  name: cluster_437
CC  version: 
CC  name: cluster_437
CC  description: crgGraAmwsmaCAssTGysm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: PTF1A_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m471_M01471
XX
//
AC  cluster_438
XX
ID  cluster_438
XX
DE  CAGAAGACGGCATACGAb
P0       A     C     G     T
1        0    16     0     0
2       16     0     0     0
3        0     0    16     0
4       16     0     0     0
5       16     0     0     0
6        0     0    16     0
7       16     0     0     0
8        0    16     0     0
9        0     0    16     0
10       0     0    16     0
11       0    16     0     0
12      16     0     0     0
13       0     0     0    16
14      16     0     0     0
15       0    16     0     0
16       0     0    16     0
17      16     0     0     0
18       0     4     6     6
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_438
CC  AC: HOCOMOCO_2015_11_23_Human_m340_M01340
CC  id: HOCOMOCO_2015_11_23_Human_m340_M01340
CC  name: cluster_438
CC  version: 
CC  name: cluster_438
CC  description: CAGAAGACGGCATACGAb
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: MUSC_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m340_M01340
XX
//
AC  cluster_439
XX
ID  cluster_439
XX
DE  awaGaArAAdGvsAygmAa
P0       A     C     G     T
1       93    36    49    44
2       80    27    29    86
3      146    29    30    17
4       29    16   171     6
5      150    42    25     5
6      152    47    12    11
7      124     3    91     4
8      204     8     5     5
9      174    35     8     5
10      76     1    71    74
11       9     3   200    10
12      73    68    71    10
13      14   118    75    15
14     182     5     7    28
15      55    66    35    66
16      37     5   129    51
17     129    66    16    11
18     176    12    12    22
19     122    23    45    32
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_439
CC  AC: HOCOMOCO_2015_11_23_Human_m637_M01637
CC  id: HOCOMOCO_2015_11_23_Human_m637_M01637
CC  name: cluster_439
CC  version: 
CC  name: cluster_439
CC  description: awaGaArAAdGvsAygmAa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN713_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m637_M01637
XX
//
AC  cluster_44
XX
ID  cluster_44
XX
DE  ryTAATTAGyrytaWTTa
P0       A     C     G     T
1    33.3333    19    34 14.6667
2    10.6667 41.3333    20    29
3        2 2.33333 1.66667    95
4       94 1.66667 4.66667 0.666667
5      101     0     0     0
6        0     0     0   101
7        0 2.33333 1.33333 97.3333
8       97     0     4     0
9    13.3333 14.3333    69 4.33333
10   6.33333 41.6667 19.6667 33.3333
11   29.3333 15.6667 46.3333 9.66667
12      10 25.3333 15.6667    50
13      21    13     5    62
14   63.6667     3 14.6667 19.6667
15   69.3333 2.33333 3.66667 25.6667
16      21 3.66667 2.33333    74
17   12.6667     4 4.33333 59.6667
18      45 4.33333 15.3333    16
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_44
CC  AC: cluster_44
CC  id: cluster_44
CC  name: cluster_44
CC  version: 
CC  name: cluster_44
CC  description: ryTAATTAGyrytaWTTa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gcTAATTAgcgttaATTa
CC  consensus.strict.rc: TAATTAACGCTAATTAGC
CC  consensus.IUPAC: ryTAATTAgyrytaWTTa
CC  consensus.IUPAC.rc: TAAWTARYRCTAATTARY
CC  consensus.regexp: [ag][ct]TAATTAg[ct][ag][ct]ta[AT]TTa
CC  consensus.regexp.rc: TAA[AT]TA[AG][CT][AG]CTAATTA[AG][CT]
CC  merged_ID: MEOX1_D,LHX6_D,VAX1_D
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m327_M01327,HOCOMOCO_2015_11_23_Human_m302_M01302,HOCOMOCO_2015_11_23_Human_m594_M01594
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_440
XX
ID  cluster_440
XX
DE  gtTctCGCGAGrstysggs
P0       A     C     G     T
1       92    68   293    46
2       94   114    87   204
3       28    51    59   361
4      121   322    44    12
5       24    65   106   304
6        2   420    69     8
7       22     2   471     4
8        9   400    15    75
9        6    15   423    55
10     421    10    62     6
11       6     8   422    63
12     308     7   175     9
13      33   149   203   114
14      31   122   103   243
15      47   134   114   204
16     117   138   199    45
17      74   111   301    13
18      53   111   264    71
19      92   135   225    47
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_440
CC  AC: HOCOMOCO_2015_11_23_Human_m285_M01285
CC  id: HOCOMOCO_2015_11_23_Human_m285_M01285
CC  name: cluster_440
CC  version: 
CC  name: cluster_440
CC  description: gtTctCGCGAGrstysggs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: KAISO_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m285_M01285
XX
//
AC  cluster_441
XX
ID  cluster_441
XX
DE  gggtkcrmGGGTtCGAgk
P0       A     C     G     T
1       54    66   305    84
2       56    73   316    64
3       53    62   305    89
4       72    42   127   268
5       95    78   166   170
6       44   281    86    98
7      162    59   211    77
8      308   129    33    39
9        3    22   483     1
10       0     0   509     0
11       1     0   508     0
12      10     4     4   491
13      45    62    67   335
14       8   447    33    21
15      52    12   413    32
16     447     7    42    13
17      72    55   327    55
18      75    80   157   197
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_441
CC  AC: HOCOMOCO_2015_11_23_Human_m633_M01633
CC  id: HOCOMOCO_2015_11_23_Human_m633_M01633
CC  name: cluster_441
CC  version: 
CC  name: cluster_441
CC  description: gggtkcrmGGGTtCGAgk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN524_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m633_M01633
XX
//
AC  cluster_442
XX
ID  cluster_442
XX
DE  whsakkwskTwsGrbrytbc
P0       A     C     G     T
1        2     1     1     3
2        2     2     1     2
3        1     3     2     1
4        4     1     1     1
5        0     0     3     4
6        1     0     4     2
7        3     1     0     3
8        0     2     4     1
9        1     0     2     4
10       0     0     1     6
11       3     0     0     4
12       0     4     2     1
13       1     0     6     0
14       4     0     2     1
15       0     3     2     2
16       5     0     2     0
17       1     3     1     2
18       1     1     0     5
19       1     2     2     2
20       1     4     1     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_442
CC  AC: HOCOMOCO_2015_11_23_Human_m114_M01114
CC  id: HOCOMOCO_2015_11_23_Human_m114_M01114
CC  name: cluster_442
CC  version: 
CC  name: cluster_442
CC  description: whsakkwskTwsGrbrytbc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ERF_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m114_M01114
XX
//
AC  cluster_443
XX
ID  cluster_443
XX
DE  tTCAGcACCayGGACAGCkcCm
P0       A     C     G     T
1       61    66   108   264
2        9    99    34   357
3       16   435    19    29
4      443    20    22    14
5       26    15   456     2
6       44   325    85    45
7      461    13    20     5
8       11   460    22     6
9       10   446    19    24
10     283    51    76    89
11      74   152    49   224
12      10     5   480     4
13       9    11   477     2
14     440    37    14     8
15       4   409    66    20
16     475     8     9     7
17       8     6   478     7
18      45   360    45    49
19      90    19   219   171
20      45   334    93    27
21      55   367    16    61
22     137   221    61    80
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_443
CC  AC: HOCOMOCO_2015_11_23_Human_m481_M01481
CC  id: HOCOMOCO_2015_11_23_Human_m481_M01481
CC  name: cluster_443
CC  version: 
CC  name: cluster_443
CC  description: tTCAGcACCayGGACAGCkcCm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: REST_A
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m481_M01481
XX
//
AC  cluster_444
XX
ID  cluster_444
XX
DE  asGGGvvgcArdkvkyTkkkksCs
P0       A     C     G     T
1        8     2     3     2
2        3     7     5     0
3        2     0    13     0
4        1     0    13     1
5        1     0    12     2
6        5     4     5     1
7        4     6     5     0
8        3     0     9     3
9        3    10     1     1
10      12     2     0     1
11       6     0     9     0
12       4     1     6     4
13       3     3     5     4
14       5     4     5     1
15       1     1     4     9
16       0     5     0    10
17       0     1     0    14
18       0     0    10     5
19       0     2     5     8
20       1     2     6     6
21       1     2     8     4
22       1     9     4     1
23       0    13     1     1
24       2     7     4     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_444
CC  AC: HOCOMOCO_2015_11_23_Human_m629_M01629
CC  id: HOCOMOCO_2015_11_23_Human_m629_M01629
CC  name: cluster_444
CC  version: 
CC  name: cluster_444
CC  description: asGGGvvgcArdkvkyTkkkksCs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZN350_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m629_M01629
XX
//
AC  cluster_445
XX
ID  cluster_445
XX
DE  ccygGCACAyAGTAGGtsctca
P0       A     C     G     T
1       58   275   107    59
2       45   284    79    91
3       75   129    87   208
4      114    64   235    86
5       43    18   404    34
6       25   435    18    21
7      355    23   108    13
8        3   483    12     1
9      376    45    36    42
10       7   253    35   204
11     448    19    16    16
12       2    24   470     3
13       9    30    25   435
14     396    28    72     3
15      38     2   446    13
16      10    18   464     7
17      45   121    79   254
18      70   153   238    38
19      36   314    53    96
20      60    67    83   289
21      40   296    53   110
22     258    83    67    91
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_445
CC  AC: HOCOMOCO_2015_11_23_Human_m621_M01621
CC  id: HOCOMOCO_2015_11_23_Human_m621_M01621
CC  name: cluster_445
CC  version: 
CC  name: cluster_445
CC  description: ccygGCACAyAGTAGGtsctca
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: ZKSC1_C
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m621_M01621
XX
//
AC  cluster_45
XX
ID  cluster_45
XX
DE  wdTTTTAykrs
P0       A     C     G     T
1    1326.67   637   928 1035.67
2    1242.33   445 1030.33 1209.67
3    504.667   166 535.667  2721
4    45.3333 29.6667 235.333  3617
5        1     4   101 3821.33
6    101.333 6.33333 94.3333 3725.33
7    3916.67 9.33333 1.33333     0
8    569.333 1432.33 319.333 1606.33
9    504.333   448 1573.33 1401.67
10   1785.33   393 1244.67 504.333
11     571 987.667  1734 634.667
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_45
CC  AC: cluster_45
CC  id: cluster_45
CC  name: cluster_45
CC  version: 
CC  name: cluster_45
CC  description: wdTTTTAykrs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aaTTTTAtgag
CC  consensus.strict.rc: CTCATAAAATT
CC  consensus.IUPAC: wdTTTTAykrs
CC  consensus.IUPAC.rc: SYMRTAAAAHW
CC  consensus.regexp: [at][agt]TTTTA[ct][gt][ag][cg]
CC  consensus.regexp.rc: [CG][CT][AC][AG]TAAAA[ACT][AT]
CC  merged_ID: HXD12_D,HXB13_D,HXC13_D
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m258_M01258,HOCOMOCO_2015_11_23_Human_m243_M01243,HOCOMOCO_2015_11_23_Human_m253_M01253
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_46
XX
ID  cluster_46
XX
DE  cwwTGTTTACwTwrs
P0       A     C     G     T
1      3.5    12   3.5     5
2      8.5     2   5.5     8
3      7.5     4     2  10.5
4      0.5   0.5     1    22
5      0.5   2.5  20.5   0.5
6        1   0.5   0.5    22
7      0.5     0   0.5    23
8        0     0     1    23
9     22.5     0   0.5     1
10       0    19     1     4
11     6.5     4     1  12.5
12     2.5   3.5     0    18
13      13   2.5     1   7.5
14    10.5     5     6   2.5
15     3.5     6    13   1.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_46
CC  AC: cluster_46
CC  id: cluster_46
CC  name: cluster_46
CC  version: 
CC  name: cluster_46
CC  description: cwwTGTTTACwTwrs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: catTGTTTACtTaag
CC  consensus.strict.rc: CTTAAGTAAACAATG
CC  consensus.IUPAC: cwwTGTTTACwTwrs
CC  consensus.IUPAC.rc: SYWAWGTAAACAWWG
CC  consensus.regexp: c[at][at]TGTTTAC[at]T[at][ag][cg]
CC  consensus.regexp.rc: [CG][CT][AT]A[AT]GTAAACA[AT][AT]G
CC  merged_ID: FOXC1_C,FOXD1_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m147_M01147,HOCOMOCO_2015_11_23_Human_m149_M01149
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_47
XX
ID  cluster_47
XX
DE  tgrTCACGTGm
P0       A     C     G     T
1      841  1081 731.25 1728.75
2    941.25 536.5 2479.5 926.5
3     2861 296.25 1343.25 383.25
4    435.25 405.75 379.75  3663
5        0  4883  0.75     0
6    4883.5  0.25     0     0
7     1.25 4854.25  10.5 17.75
8    36.75     3  4844     0
9        3  1.75   2.5 4876.5
10    1.75   1.5  4879   1.5
11   1341.5  1342 1187.5   948
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_47
CC  AC: cluster_47
CC  id: cluster_47
CC  name: cluster_47
CC  version: 
CC  name: cluster_47
CC  description: tgrTCACGTGm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tgaTCACGTGc
CC  consensus.strict.rc: GCACGTGATCA
CC  consensus.IUPAC: tgrTCACGTGm
CC  consensus.IUPAC.rc: KCACGTGAYCA
CC  consensus.regexp: tg[ag]TCACGTG[ac]
CC  consensus.regexp.rc: [GT]CACGTGA[CT]CA
CC  merged_ID: MLX_D,TFEB_C,TFE3_C,USF1_A
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m334_M01334,HOCOMOCO_2015_11_23_Human_m576_M01576,HOCOMOCO_2015_11_23_Human_m575_M01575,HOCOMOCO_2015_11_23_Human_m592_M01592
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_48
XX
ID  cluster_48
XX
DE  tcATTAATWATKCAt
P0       A     C     G     T
1      422 329.5   399  2005
2     1636  3785 593.5   953
3    6237.5   228 243.5 258.5
4     86.5  80.5  77.5  6723
5     1120   426 293.5  5128
6    6620.5  13.5  27.5   306
7     6655    42    28 242.5
8      575    29  34.5  6329
9    1868.5  25.5     7 5066.5
10    6918   2.5   2.5  44.5
11      56   4.5     6  6901
12    24.5    13 2145.5 4784.5
13     862 4756.5 162.5 1186.5
14   6060.5   268   291   348
15   943.5   910   880  4234
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_48
CC  AC: cluster_48
CC  id: cluster_48
CC  name: cluster_48
CC  version: 
CC  name: cluster_48
CC  description: tcATTAATWATKCAt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tcATTAATTATTCAt
CC  consensus.strict.rc: ATGAATAATTAATGA
CC  consensus.IUPAC: tcATTAATWATKCAt
CC  consensus.IUPAC.rc: ATGMATWATTAATGA
CC  consensus.regexp: tcATTAAT[AT]AT[GT]CAt
CC  consensus.regexp.rc: ATG[AC]AT[AT]ATTAATGA
CC  merged_ID: PO4F1_D,PO4F3_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m451_M01451,HOCOMOCO_2015_11_23_Human_m453_M01453
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_49
XX
ID  cluster_49
XX
DE  atgsCCTGrGGsmat
P0       A     C     G     T
1      200    97   116    87
2      107    54    75   264
3       90    49   247   114
4        5 256.5   226  12.5
5        1   497     2     0
6      0.5   463   0.5    36
7       25    61    10   404
8      9.5     0   428  62.5
9    272.5    16 206.5     5
10      54     0   445     1
11       0     0 499.5   0.5
12      19 293.5 180.5     7
13   155.5   183  47.5   114
14     235    66 111.5  87.5
15    59.5  37.5    52   101
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_49
CC  AC: cluster_49
CC  id: cluster_49
CC  name: cluster_49
CC  version: 
CC  name: cluster_49
CC  description: atgsCCTGrGGsmat
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: atgcCCTGaGGccat
CC  consensus.strict.rc: ATGGCCTCAGGGCAT
CC  consensus.IUPAC: atgsCCTGrGGsmat
CC  consensus.IUPAC.rc: ATKSCCYCAGGSCAT
CC  consensus.regexp: atg[cg]CCTG[ag]GG[cg][ac]at
CC  consensus.regexp.rc: AT[GT][CG]CC[CT]CAGG[CG]CAT
CC  merged_ID: AP2A_C,AP2C_A
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m7_M01007,HOCOMOCO_2015_11_23_Human_m9_M01009
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_5
XX
ID  cluster_5
XX
DE  grTGAGTCAycs
P0       A     C     G     T
1      127 103.556 214.778 107.222
2    540.556 279.111   318 93.1111
3    25.2222 7.66667 23.8889  1174
4    9.88889    24 1133.56 63.3333
5    1155.33 31.4444 10.7778 33.2222
6    87.4444   159 959.333    25
7    19.2222 11.1111 14.4444  1186
8       49 1168.67 9.44444 3.66667
9    1206.44 6.11111 5.88889 12.3333
10      58 377.222 240.111 555.444
11   241.222 350.111   148 244.111
12   41.4444 86.1111 57.7778 36.8889
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_5
CC  AC: cluster_5
CC  id: cluster_5
CC  name: cluster_5
CC  version: 
CC  name: cluster_5
CC  description: grTGAGTCAycs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gaTGAGTCAtcc
CC  consensus.strict.rc: GGATGACTCATC
CC  consensus.IUPAC: grTGAGTCAycs
CC  consensus.IUPAC.rc: SGRTGACTCAYC
CC  consensus.regexp: g[ag]TGAGTCA[ct]c[cg]
CC  consensus.regexp.rc: [CG]G[AG]TGACTCA[CT]C
CC  merged_ID: SMRC1_D,FOSL2_A,JUNB_C,FOS_A,FOSB_C,FOSL1_A,JUND_A,JUN_A,BATF_S
CC  merge_nb: 9
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m506_M01506,HOCOMOCO_2015_11_23_Human_m141_M01141,HOCOMOCO_2015_11_23_Human_m282_M01282,HOCOMOCO_2015_11_23_Human_m142_M01142,HOCOMOCO_2015_11_23_Human_m139_M01139,HOCOMOCO_2015_11_23_Human_m140_M01140,HOCOMOCO_2015_11_23_Human_m283_M01283,HOCOMOCO_2015_11_23_Human_m284_M01284,HOCOMOCO_2015_11_23_Human_m31_M01031
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_50
XX
ID  cluster_50
XX
DE  amCATATGTTTttt
P0       A     C     G     T
1    631.667   102 172.333 98.3333
2      330   416   179 79.3333
3    8.66667 990.667 2.66667 2.33333
4    981.333 16.3333     4 2.66667
5    5.33333    14     2   983
6    984.667    11 2.66667     6
7    0.666667 25.6667 0.333333 977.667
8    3.33333    21   977     3
9        9    69 171.667 754.667
10   8.33333 42.3333 6.66667   947
11     110 107.667 58.6667   728
12   184.333 197.667    95 527.333
13   184.667 171.667 137.333 510.667
14   27.3333 36.6667 33.6667 67.3333
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_50
CC  AC: cluster_50
CC  id: cluster_50
CC  name: cluster_50
CC  version: 
CC  name: cluster_50
CC  description: amCATATGTTTttt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: acCATATGTTTttt
CC  consensus.strict.rc: AAAAAACATATGGT
CC  consensus.IUPAC: amCATATGTTTttt
CC  consensus.IUPAC.rc: AAAAAACATATGKT
CC  consensus.regexp: a[ac]CATATGTTTttt
CC  consensus.regexp.rc: AAAAAACATATG[GT]T
CC  merged_ID: OLIG1_D,BHE22_D,NGN2_D
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m404_M01404,HOCOMOCO_2015_11_23_Human_m36_M01036,HOCOMOCO_2015_11_23_Human_m372_M01372
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_51
XX
ID  cluster_51
XX
DE  swGTCTgkCACct
P0       A     C     G     T
1       39 103.5  64.5    43
2      139  49.5  26.5   285
3       10   9.5 466.5    14
4     17.5  44.5  70.5 367.5
5        7 478.5   1.5    13
6      7.5     2     1 489.5
7    101.5   116 258.5    24
8      113    55   151   181
9      9.5   457    22  11.5
10     371    30    22    77
11    27.5 378.5    66    28
12    73.5   296   9.5   121
13    61.5    93  42.5   303
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_51
CC  AC: cluster_51
CC  id: cluster_51
CC  name: cluster_51
CC  version: 
CC  name: cluster_51
CC  description: swGTCTgkCACct
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ctGTCTgtCACct
CC  consensus.strict.rc: AGGTGACAGACAG
CC  consensus.IUPAC: swGTCTgkCACct
CC  consensus.IUPAC.rc: AGGTGMCAGACWS
CC  consensus.regexp: [cg][at]GTCTg[gt]CACct
CC  consensus.regexp.rc: AGGTG[AC]CAGAC[AT][CG]
CC  merged_ID: SMAD2_C,SMAD4_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m503_M01503,HOCOMOCO_2015_11_23_Human_m505_M01505
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_52
XX
ID  cluster_52
XX
DE  grraCCGGAAGTgv
P0       A     C     G     T
1    32.6667 38.6667 82.3333 12.6667
2      628 386.667 661.667 220.333
3    791.667 387.667   503 214.333
4    1109.33 266.667 425.667    95
5    65.6667 1603.33   217 10.6667
6      186  1701     7 2.66667
7    1.66667 0.333333 1894.33 0.333333
8    2.33333     0  1894 0.333333
9    1893.67     1 0.666667 1.33333
10   1821.67 0.666667 0.333333    74
11     406 15.6667 1473.33 1.66667
12      83 284.667    58  1471
13   279.333   401 1049.67 166.667
14   482.333   567   672 175.333
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_52
CC  AC: cluster_52
CC  id: cluster_52
CC  name: cluster_52
CC  version: 
CC  name: cluster_52
CC  description: grraCCGGAAGTgv
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ggaaCCGGAAGTgg
CC  consensus.strict.rc: CCACTTCCGGTTCC
CC  consensus.IUPAC: grraCCGGAAGTgv
CC  consensus.IUPAC.rc: BCACTTCCGGTYYC
CC  consensus.regexp: g[ag][ag]aCCGGAAGTg[acg]
CC  consensus.regexp.rc: [CGT]CACTTCCGGT[CT][CT]C
CC  merged_ID: ELF1_A,ERG_B,GABPA_A
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m102_M01102,HOCOMOCO_2015_11_23_Human_m115_M01115,HOCOMOCO_2015_11_23_Human_m172_M01172
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_53
XX
ID  cluster_53
XX
DE  cywTTgwtATGcAaAtk
P0       A     C     G     T
1    48.3333 290.333 92.6667 113.333
2    27.6667   333 22.3333 161.667
3    244.333    20    14 266.333
4        6    20 6.33333 512.333
5    28.3333     2 21.3333   493
6    51.3333 125.667 343.333 24.3333
7      156 5.33333 21.3333   362
8    90.6667   132 74.6667 247.333
9    417.667 44.6667 21.6667 60.6667
10   28.6667    12    19   485
11   19.3333 31.6667   418 75.6667
12   17.6667   367 57.6667 102.333
13   380.333 17.6667    24 122.667
14   312.667 49.6667 112.667 69.6667
15   403.333 36.3333 44.3333 60.6667
16      98 80.3333 81.3333   285
17   24.3333 23.6667 58.6667    60
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_53
CC  AC: cluster_53
CC  id: cluster_53
CC  name: cluster_53
CC  version: 
CC  name: cluster_53
CC  description: cywTTgwtATGcAaAtk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cctTTgttATGcAaAtt
CC  consensus.strict.rc: AATTTGCATAACAAAGG
CC  consensus.IUPAC: cywTTgwtATGcAaAtk
CC  consensus.IUPAC.rc: MATTTGCATAWCAAWRG
CC  consensus.regexp: c[ct][at]TTg[at]tATGcAaAt[gt]
CC  consensus.regexp.rc: [AC]ATTTGCATA[AT]CAA[AT][AG]G
CC  merged_ID: NANOG_S,PO5F1_A,SOX2_B
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m350_M01350,HOCOMOCO_2015_11_23_Human_m454_M01454,HOCOMOCO_2015_11_23_Human_m517_M01517
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_54
XX
ID  cluster_54
XX
DE  kvCAGGTGwrv
P0       A     C     G     T
1     78.5  75.5  96.5   104
2    118.5 110.5    98  27.5
3       18 321.5     7     8
4      340     2     6   6.5
5       12   6.5   332     4
6      3.5    10   339     2
7       30     3  12.5   309
8       88   4.5   254     8
9      121    63    63 107.5
10      92    67   143  52.5
11      90    96  94.5    74
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_54
CC  AC: cluster_54
CC  id: cluster_54
CC  name: cluster_54
CC  version: 
CC  name: cluster_54
CC  description: kvCAGGTGwrv
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: taCAGGTGagc
CC  consensus.strict.rc: GCTCACCTGTA
CC  consensus.IUPAC: kvCAGGTGwrv
CC  consensus.IUPAC.rc: BYWCACCTGBM
CC  consensus.regexp: [gt][acg]CAGGTG[at][ag][acg]
CC  consensus.regexp.rc: [CGT][CT][AT]CACCTG[CGT][AC]
CC  merged_ID: ID4_D,ZEB1_B
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m264_M01264,HOCOMOCO_2015_11_23_Human_m612_M01612
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_55
XX
ID  cluster_55
XX
DE  ggTaATTAryr
P0       A     C     G     T
1     1766 1633.33 2378.33  1655
2    1233.33  1605 3843.67 750.667
3      280 1359.67 231.333 5561.67
4    4956.33 1807.67   310 358.667
5    7314.33 45.3333    47    26
6    27.3333 18.3333    14  7373
7    51.3333 126.667 29.6667  7225
8    7052.67   111   124   145
9     3641 448.333 2241.33  1102
10    1514  2081 1484.33 2353.33
11    2180 1145.33 2333.67 1773.67
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_55
CC  AC: cluster_55
CC  id: cluster_55
CC  name: cluster_55
CC  version: 
CC  name: cluster_55
CC  description: ggTaATTAryr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ggTaATTAatg
CC  consensus.strict.rc: CATTAATTACC
CC  consensus.IUPAC: ggTaATTAryr
CC  consensus.IUPAC.rc: YRYTAATTACC
CC  consensus.regexp: ggTaATTA[ag][ct][ag]
CC  consensus.regexp.rc: [CT][AG][CT]TAATTACC
CC  merged_ID: HXD3_D,HXA2_D,NKX62_D
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m260_M01260,HOCOMOCO_2015_11_23_Human_m239_M01239,HOCOMOCO_2015_11_23_Human_m381_M01381
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_56
XX
ID  cluster_56
XX
DE  yTGTTTAcw
P0       A     C     G     T
1    151.5 327.5 190.5   343
2     17.5    11    11   973
3      123   2.5 885.5   1.5
4      2.5  27.5     2 980.5
5        0     1  66.5   945
6        3  16.5 135.5 857.5
7    749.5   153    49    61
8      154   673  15.5   170
9    301.5 185.5   183 342.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_56
CC  AC: cluster_56
CC  id: cluster_56
CC  name: cluster_56
CC  version: 
CC  name: cluster_56
CC  description: yTGTTTAcw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tTGTTTAct
CC  consensus.strict.rc: AGTAAACAA
CC  consensus.IUPAC: yTGTTTAcw
CC  consensus.IUPAC.rc: WGTAAACAR
CC  consensus.regexp: [ct]TGTTTAc[at]
CC  consensus.regexp.rc: [AT]GTAAACA[AG]
CC  merged_ID: FOXO4_C,FOXP2_A
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m165_M01165,HOCOMOCO_2015_11_23_Human_m167_M01167
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_57
XX
ID  cluster_57
XX
DE  aTrTTTACwywgy
P0       A     C     G     T
1    103.5  48.5  47.5    62
2      2.5   1.5   1.5   256
3       94     0 167.5     0
4      5.5     0   0.5 255.5
5        2     1    19 239.5
6        0     0    36 225.5
7    190.5     6  64.5   0.5
8        0 234.5   2.5  24.5
9    113.5    18   0.5 129.5
10     6.5  82.5    28 144.5
11   115.5     1     5   140
12    59.5  40.5 103.5    58
13      27    92    53  89.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_57
CC  AC: cluster_57
CC  id: cluster_57
CC  name: cluster_57
CC  version: 
CC  name: cluster_57
CC  description: aTrTTTACwywgy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aTgTTTACtttgc
CC  consensus.strict.rc: GCAAAGTAAACAT
CC  consensus.IUPAC: aTrTTTACwywgy
CC  consensus.IUPAC.rc: RCWRWGTAAAYAT
CC  consensus.regexp: aT[ag]TTTAC[at][ct][at]g[ct]
CC  consensus.regexp.rc: [AG]C[AT][AG][AT]GTAAA[CT]AT
CC  merged_ID: FOXA1_A,FOXA3_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m143_M01143,HOCOMOCO_2015_11_23_Human_m145_M01145
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_58
XX
ID  cluster_58
XX
DE  gTGAykyma
P0       A     C     G     T
1    239.5 226.5   457   140
2      7.5   4.5     6  1045
3        2    10  1048     3
4     1031   7.5   4.5    20
5        7   694  72.5 289.5
6       27    66   602   368
7       57   516  14.5 475.5
8      354 469.5   133 106.5
9    605.5   132 164.5   161
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_58
CC  AC: cluster_58
CC  id: cluster_58
CC  name: cluster_58
CC  version: 
CC  name: cluster_58
CC  description: gTGAykyma
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gTGAcgcca
CC  consensus.strict.rc: TGGCGTCAC
CC  consensus.IUPAC: gTGAykyma
CC  consensus.IUPAC.rc: TKRMRTCAC
CC  consensus.regexp: gTGA[ct][gt][ct][ac]a
CC  consensus.regexp.rc: T[GT][AG][AC][AG]TCAC
CC  merged_ID: ATF2_B,ATF3_A
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m19_M01019,HOCOMOCO_2015_11_23_Human_m20_M01020
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_59
XX
ID  cluster_59
XX
DE  vdsCAGGTGbk
P0       A     C     G     T
1    56.3333    69 61.3333 34.6667
2       63 40.3333    61    57
3    47.3333 101.333    66 6.66667
4        5 210.667     2 3.66667
5    217.667 0.666667     2     1
6    1.33333 2.66667 215.667 1.66667
7    8.33333 46.3333   162 4.66667
8       15     2 4.66667 199.667
9    1.33333 2.33333 213.667     4
10   16.6667    56 67.6667    81
11      38    40 78.6667    63
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_59
CC  AC: cluster_59
CC  id: cluster_59
CC  name: cluster_59
CC  version: 
CC  name: cluster_59
CC  description: vdsCAGGTGbk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cacCAGGTGtg
CC  consensus.strict.rc: CACACCTGGTG
CC  consensus.IUPAC: vdsCAGGTGbk
CC  consensus.IUPAC.rc: MVCACCTGSHB
CC  consensus.regexp: [acg][agt][cg]CAGGTG[cgt][gt]
CC  consensus.regexp.rc: [AC][ACG]CACCTG[CG][ACT][CGT]
CC  merged_ID: TWST1_D,FIGLA_D,HTF4_B
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m587_M01587,HOCOMOCO_2015_11_23_Human_m137_M01137,HOCOMOCO_2015_11_23_Human_m234_M01234
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_6
XX
ID  cluster_6
XX
DE  TAATTAgytwaw
P0       A     C     G     T
1    607.333  2672 471.667 18359.3
2    18546.7 3060.33 239.667 263.667
3    21849.7   106 69.3333 85.3333
4    41.3333    25    48 21996
5       28 33.3333   126 21923
6    21938 23.3333   103    46
7    5512.67 3035.33 13339 223.333
8      890  8951 4191.67 8077.67
9    3864.67 3267.67  5043  9935
10   10621 3374.33  2413  5702
11   12408  2735  2087 4880.33
12   6813.33 2617.67  2076 10595.7
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_6
CC  AC: cluster_6
CC  id: cluster_6
CC  name: cluster_6
CC  version: 
CC  name: cluster_6
CC  description: TAATTAgytwaw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: TAATTAgctaat
CC  consensus.strict.rc: ATTAGCTAATTA
CC  consensus.IUPAC: TAATTAgytwaw
CC  consensus.IUPAC.rc: WTWARCTAATTA
CC  consensus.regexp: TAATTAg[ct]t[at]a[at]
CC  consensus.regexp.rc: [AT]T[AT]A[AG]CTAATTA
CC  merged_ID: VSX2_D,EMX1_D,EMX2_D
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m600_M01600,HOCOMOCO_2015_11_23_Human_m109_M01109,HOCOMOCO_2015_11_23_Human_m110_M01110
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_60
XX
ID  cluster_60
XX
DE  wTTTAtrd
P0       A     C     G     T
1     15.5     4   2.5    11
2      1.5     0   1.5    30
3      0.5     0     1  31.5
4      2.5     1     2  27.5
5     29.5     0     3   0.5
6      3.5     7   1.5    21
7      9.5   0.5  19.5   3.5
8      8.5   2.5  12.5   9.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_60
CC  AC: cluster_60
CC  id: cluster_60
CC  name: cluster_60
CC  version: 
CC  name: cluster_60
CC  description: wTTTAtrd
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aTTTAtgg
CC  consensus.strict.rc: CCATAAAT
CC  consensus.IUPAC: wTTTAtrd
CC  consensus.IUPAC.rc: HYATAAAW
CC  consensus.regexp: [at]TTTAt[ag][agt]
CC  consensus.regexp.rc: [ACT][CT]ATAAA[AT]
CC  merged_ID: CDX1_C,CDX2_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m46_M01046,HOCOMOCO_2015_11_23_Human_m47_M01047
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_61
XX
ID  cluster_61
XX
DE  arAGGTGTGAAaw
P0       A     C     G     T
1     3321 1468.5 1798.5  1267
2     9352 1637.5  5105 3948.5
3    17612   404  1371   656
4     1760 884.5 16251.5  1147
5       79 102.5 19756.5   105
6    129.5 321.5   220 19372
7      245 105.5 19605  87.5
8    349.5 996.5   372 18325
9      279 419.5 17681.5  1663
10   19284.5   191   310 257.5
11   13754.5  1985  1018 3285.5
12    9694 3305.5 2694.5  4349
13   6426.5  2497 2335.5  8784
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_61
CC  AC: cluster_61
CC  id: cluster_61
CC  name: cluster_61
CC  version: 
CC  name: cluster_61
CC  description: arAGGTGTGAAaw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aaAGGTGTGAAat
CC  consensus.strict.rc: ATTTCACACCTTT
CC  consensus.IUPAC: arAGGTGTGAAaw
CC  consensus.IUPAC.rc: WTTTCACACCTYT
CC  consensus.regexp: a[ag]AGGTGTGAAa[at]
CC  consensus.regexp.rc: [AT]TTTCACACCT[CT]T
CC  merged_ID: TBR1_D,TBX21_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m551_M01551,HOCOMOCO_2015_11_23_Human_m556_M01556
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_62
XX
ID  cluster_62
XX
DE  TGCTGAGTCAyssy
P0       A     C     G     T
1    81.6667    78 43.6667 471.667
2    49.3333    25   589 11.6667
3    21.6667 620.333 5.33333 27.6667
4    41.6667 54.3333    42   537
5       28 43.6667 555.667 47.6667
6    535.667    25 41.6667 72.6667
7       17 109.333 517.333 31.3333
8       21 15.3333    29 609.667
9    7.33333 623.667    39     5
10     592 45.3333 16.6667    21
11      25 213.333 63.6667   373
12   86.3333   183   247 158.667
13   67.6667 258.667   204   114
14   60.3333 132.667 93.6667   191
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_62
CC  AC: cluster_62
CC  id: cluster_62
CC  name: cluster_62
CC  version: 
CC  name: cluster_62
CC  description: TGCTGAGTCAyssy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: TGCTGAGTCAtgct
CC  consensus.strict.rc: AGCATGACTCAGCA
CC  consensus.IUPAC: TGCTGAGTCAyssy
CC  consensus.IUPAC.rc: RSSRTGACTCAGCA
CC  consensus.regexp: TGCTGAGTCA[ct][cg][cg][ct]
CC  consensus.regexp.rc: [AG][CG][CG][AG]TGACTCAGCA
CC  merged_ID: NF2L2_D,BACH1_A,NFE2_B
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m355_M01355,HOCOMOCO_2015_11_23_Human_m24_M01024,HOCOMOCO_2015_11_23_Human_m362_M01362
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_63
XX
ID  cluster_63
XX
DE  tTGATTaATgm
P0       A     C     G     T
1        6     6 1.33333    22
2        0 4.66667 0.333333 30.3333
3    6.33333     3    25     1
4    35.3333     0     0     0
5        0     0     0 35.3333
6        0     0     0 35.3333
7    23.3333     0 7.33333 4.66667
8       29 1.33333 4.33333 0.666667
9    1.33333 3.33333 0.333333 30.3333
10   6.33333 2.66667    19 7.33333
11   2.66667     3     2     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_63
CC  AC: cluster_63
CC  id: cluster_63
CC  name: cluster_63
CC  version: 
CC  name: cluster_63
CC  description: tTGATTaATgm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tTGATTaATgc
CC  consensus.strict.rc: GCATTAATCAA
CC  consensus.IUPAC: tTGATTaATgm
CC  consensus.IUPAC.rc: KCATTAATCAA
CC  consensus.regexp: tTGATTaATg[ac]
CC  consensus.regexp.rc: [GT]CATTAATCAA
CC  merged_ID: HXA5_D,HXB7_C,HXB8_C
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m240_M01240,HOCOMOCO_2015_11_23_Human_m248_M01248,HOCOMOCO_2015_11_23_Human_m249_M01249
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_64
XX
ID  cluster_64
XX
DE  sGTTGCCATGGywAc
P0       A     C     G     T
1    195.5   604 592.5 323.5
2       43  32.5  1607    33
3      6.5  15.5    11 1682.5
4    113.5    60     9  1533
5      249    13 1367.5    86
6      0.5  1348    14   353
7        0 1383.5     1   331
8    1548.5  28.5  64.5    74
9       72  24.5   126  1493
10   145.5    12  1557     1
11   155.5    47 1495.5  17.5
12     257   488  81.5   889
13   855.5    31   317   512
14   1364.5 144.5    98 108.5
15     129   923 117.5 218.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_64
CC  AC: cluster_64
CC  id: cluster_64
CC  name: cluster_64
CC  version: 
CC  name: cluster_64
CC  description: sGTTGCCATGGywAc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cGTTGCCATGGtaAc
CC  consensus.strict.rc: GTTACCATGGCAACG
CC  consensus.IUPAC: sGTTGCCATGGywAc
CC  consensus.IUPAC.rc: GTWRCCATGGCAACS
CC  consensus.regexp: [cg]GTTGCCATGG[ct][at]Ac
CC  consensus.regexp.rc: GT[AT][AG]CCATGGCAAC[CG]
CC  merged_ID: RFX3_B,RFX4_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m484_M01484,HOCOMOCO_2015_11_23_Human_m485_M01485
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_65
XX
ID  cluster_65
XX
DE  accGGAARTgv
P0       A     C     G     T
1     1694 397.5   618 478.25
2    474.5 1910.75 450.25 364.5
3    430.75 2130.5 328.25 310.5
4       52 41.25 3058.25  48.5
5    38.75 45.75 3003.5   112
6    3123.75 33.75 18.75 23.75
7    2752.25 47.25 12.25 388.25
8    861.75  83.5 2206.75    48
9    89.25 681.25   118 2311.5
10   744.25 613.25 1242.75 599.75
11   920.25 849.5 929.25   501
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_65
CC  AC: cluster_65
CC  id: cluster_65
CC  name: cluster_65
CC  version: 
CC  name: cluster_65
CC  description: accGGAARTgv
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: accGGAAGTgg
CC  consensus.strict.rc: CCACTTCCGGT
CC  consensus.IUPAC: accGGAARTgv
CC  consensus.IUPAC.rc: BCAYTTCCGGT
CC  consensus.regexp: accGGAA[AG]Tg[acg]
CC  consensus.regexp.rc: [CGT]CA[CT]TTCCGGT
CC  merged_ID: ELK4_A,ETS2_C,FEV_C,GABP1_C
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m108_M01108,HOCOMOCO_2015_11_23_Human_m125_M01125,HOCOMOCO_2015_11_23_Human_m136_M01136,HOCOMOCO_2015_11_23_Human_m171_M01171
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_66
XX
ID  cluster_66
XX
DE  yTAATkrstwwtTaAt
P0       A     C     G     T
1      447 1042.67 444.667 1244.33
2    212.333   191 101.333  2766
3    2871.33   111 155.333   133
4    3245.33     5 13.6667 6.66667
5       56 117.667 46.3333 3050.67
6      275 136.667 842.667 2016.33
7     2004 135.333   945 186.333
8    537.667   960  1368   405
9    796.667   742   482  1250
10     898   446 506.667  1420
11   934.667   359 311.333 1665.67
12   645.333 358.667   269 1997.67
13   581.667 230.333   236 2222.67
14   2044.33 281.333 383.667 561.333
15    1786    78   126 146.333
16   249.333   250   225  1412
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_66
CC  AC: cluster_66
CC  id: cluster_66
CC  name: cluster_66
CC  version: 
CC  name: cluster_66
CC  description: yTAATkrstwwtTaAt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tTAATtagtttttaAt
CC  consensus.strict.rc: ATTAAAAACTAATTAA
CC  consensus.IUPAC: yTAATkrstwwttaAt
CC  consensus.IUPAC.rc: ATTAAWWASYMATTAR
CC  consensus.regexp: [ct]TAAT[gt][ag][cg]t[at][at]ttaAt
CC  consensus.regexp.rc: ATTAA[AT][AT]A[CG][CT][AC]ATTA[AG]
CC  merged_ID: HXB3_D,GSX1_D,GSX2_D
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m246_M01246,HOCOMOCO_2015_11_23_Human_m199_M01199,HOCOMOCO_2015_11_23_Human_m200_M01200
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_67
XX
ID  cluster_67
XX
DE  wkrTTGCayAAyh
P0       A     C     G     T
1    58.3333    29 25.3333    54
2       53    35 107.667   106
3      197 24.3333 99.6667 6.33333
4        0     1     0 326.333
5        0 0.333333     0   327
6    18.6667 0.333333   277 31.3333
7    0.333333 323.667     2 1.33333
8    204.667    24    57 41.6667
9    48.6667   178 2.33333 98.3333
10   258.333 67.6667 0.333333     1
11     326 1.33333     0     0
12       4   133    32 158.333
13     118    97 27.3333    85
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_67
CC  AC: cluster_67
CC  id: cluster_67
CC  name: cluster_67
CC  version: 
CC  name: cluster_67
CC  description: wkrTTGCayAAyh
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: agaTTGCacAAta
CC  consensus.strict.rc: TATTGTGCAATCT
CC  consensus.IUPAC: wkrTTGCayAAyh
CC  consensus.IUPAC.rc: DRTTRTGCAAYMW
CC  consensus.regexp: [at][gt][ag]TTGCa[ct]AA[ct][act]
CC  consensus.regexp.rc: [AGT][AG]TT[AG]TGCAA[CT][AC][AT]
CC  merged_ID: CEBPD_B,CEBPA_A,CEBPB_A
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m50_M01050,HOCOMOCO_2015_11_23_Human_m48_M01048,HOCOMOCO_2015_11_23_Human_m49_M01049
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_68
XX
ID  cluster_68
XX
DE  TGACAGkTTwAygr
P0       A     C     G     T
1      0.5     0     0  33.5
2        1     0    33     0
3       31   0.5     1   1.5
4      1.5    31     0   1.5
5       30   2.5     1   0.5
6      0.5     5    26   2.5
7      2.5   7.5   8.5  15.5
8        0   0.5   0.5    33
9      0.5     2     5  26.5
10      11   1.5   1.5    20
11    23.5     5     2   3.5
12     6.5    12   2.5    13
13     7.5     2    19   5.5
14       8   1.5     5     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_68
CC  AC: cluster_68
CC  id: cluster_68
CC  name: cluster_68
CC  version: 
CC  name: cluster_68
CC  description: TGACAGkTTwAygr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: TGACAGtTTtatga
CC  consensus.strict.rc: TCATAAAACTGTCA
CC  consensus.IUPAC: TGACAGkTTwaygr
CC  consensus.IUPAC.rc: YCRTWAAMCTGTCA
CC  consensus.regexp: TGACAG[gt]TT[at]a[ct]g[ag]
CC  consensus.regexp.rc: [CT]C[AG]T[AT]AA[AC]CTGTCA
CC  merged_ID: HXA9_D,MEIS1_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m242_M01242,HOCOMOCO_2015_11_23_Human_m324_M01324
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_69
XX
ID  cluster_69
XX
DE  sSCGCsAAAc
P0       A     C     G     T
1      1.5   5.5     4     1
2        0   3.5   8.5     0
3        0    12     0     0
4        0     0  11.5   0.5
5        0    11     1     0
6        0     5     7     0
7       10     1     1     0
8     10.5   1.5     0     0
9     10.5     0     1   0.5
10     2.5     6   1.5     2
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_69
CC  AC: cluster_69
CC  id: cluster_69
CC  name: cluster_69
CC  version: 
CC  name: cluster_69
CC  description: sSCGCsAAAc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cgCGCgAAAc
CC  consensus.strict.rc: GTTTCGCGCG
CC  consensus.IUPAC: ssCGCsAAAc
CC  consensus.IUPAC.rc: GTTTSGCGSS
CC  consensus.regexp: [cg][cg]CGC[cg]AAAc
CC  consensus.regexp.rc: GTTT[CG]GCG[CG][CG]
CC  merged_ID: E2F3_B,E2F5_B
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m88_M01088,HOCOMOCO_2015_11_23_Human_m90_M01090
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_7
XX
ID  cluster_7
XX
DE  trGAAyrTTCTAGAa
P0       A     C     G     T
1    929.5   779 662.75  2081
2    2335.25 1062.5 1705.75   129
3    79.75 60.75 5033.25 58.75
4    5015.75 62.75    69    85
5    4901.75    71 73.25 186.5
6      751 1941.5 1090.75 1449.25
7    1687.75 1051.25 1960.5   533
8    241.25  82.5 102.5 4806.25
9    144.25 70.75 146.5  4871
10    52.5  5063  87.5  29.5
11    70.5 804.25 624.25 3733.5
12   3929.25 503.75 738.25 61.25
13   53.75    67 5066.25  45.5
14   4730.5 197.5 137.75 166.75
15   3377.75 571.25 601.25 682.25
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_7
CC  AC: cluster_7
CC  id: cluster_7
CC  name: cluster_7
CC  version: 
CC  name: cluster_7
CC  description: trGAAyrTTCTAGAa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: taGAAcgTTCTAGAa
CC  consensus.strict.rc: TTCTAGAACGTTCTA
CC  consensus.IUPAC: trGAAyrTTCTAGAa
CC  consensus.IUPAC.rc: TTCTAGAAYRTTCYA
CC  consensus.regexp: t[ag]GAA[ct][ag]TTCTAGAa
CC  consensus.regexp.rc: TTCTAGAA[CT][AG]TTC[CT]A
CC  merged_ID: HSF4_D,HXB2_D,HSF1_A,HSF2_A
CC  merge_nb: 4
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m232_M01232,HOCOMOCO_2015_11_23_Human_m245_M01245,HOCOMOCO_2015_11_23_Human_m230_M01230,HOCOMOCO_2015_11_23_Human_m231_M01231
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_70
XX
ID  cluster_70
XX
DE  rraaagrGGAAsTGArAv
P0       A     C     G     T
1       61    26    39    25
2    140.333 44.3333 97.6667 35.3333
3    280.667 35.3333 105.333    63
4    274.667    41   112 56.6667
5      279    16 101.333    88
6    91.6667 48.6667 318.333 25.6667
7    287.333 25.6667 149.667 21.6667
8       65 3.66667 400.667    15
9    57.3333    13 411.333 2.66667
10   451.333 11.6667 14.3333     7
11     428 2.33333 33.6667 20.3333
12   65.6667 131.333 279.667 7.66667
13      60 38.3333 28.6667 357.333
14   24.3333 19.6667   427 13.3333
15     336    27 107.667 13.6667
16   224.333 54.6667   171 34.3333
17   330.333    45 53.3333 55.6667
18   88.3333 92.3333 120.667    32
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_70
CC  AC: cluster_70
CC  id: cluster_70
CC  name: cluster_70
CC  version: 
CC  name: cluster_70
CC  description: rraaagrGGAAsTGArAv
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aaaaagaGGAAgTGAaAg
CC  consensus.strict.rc: CTTTCACTTCCTCTTTTT
CC  consensus.IUPAC: rraaagrGGAAsTGArAv
CC  consensus.IUPAC.rc: BTYTCASTTCCYCTTTYY
CC  consensus.regexp: [ag][ag]aaag[ag]GGAA[cg]TGA[ag]A[acg]
CC  consensus.regexp.rc: [CGT]T[CT]TCA[CG]TTCC[CT]CTTT[CT][CT]
CC  merged_ID: SPI1_A,IRF4_C,BC11A_C
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m530_M01530,HOCOMOCO_2015_11_23_Human_m270_M01270,HOCOMOCO_2015_11_23_Human_m32_M01032
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_71
XX
ID  cluster_71
XX
DE  wrrGgTCArrarAGkTCAk
P0       A     C     G     T
1      4.5     2     3     5
2        9     1  12.5     6
3     12.5     3  12.5   0.5
4      6.5     0    20     2
5        1     5    18   4.5
6        0   0.5   1.5  26.5
7        0    20     2   6.5
8       23   5.5     0     0
9       13     4   7.5     4
10       8   5.5   9.5   5.5
11      10   6.5     7     5
12    16.5     2   9.5   0.5
13      26     0   2.5     0
14     0.5     0    28     0
15       0     2   7.5    19
16       4   2.5   0.5  21.5
17       0    21     3   4.5
18      23   1.5     3     1
19     4.5   5.5    10   8.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_71
CC  AC: cluster_71
CC  id: cluster_71
CC  name: cluster_71
CC  version: 
CC  name: cluster_71
CC  description: wrrGgTCArrarAGkTCAk
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tgaGgTCAagaaAGtTCAg
CC  consensus.strict.rc: CTGAACTTTCTTGACCTCA
CC  consensus.IUPAC: wrrGgTCArrarAGkTCAk
CC  consensus.IUPAC.rc: MTGAMCTYTYYTGACCYYW
CC  consensus.regexp: [at][ag][ag]GgTCA[ag][ag]a[ag]AG[gt]TCA[gt]
CC  consensus.regexp.rc: [AC]TGA[AC]CT[CT]T[CT][CT]TGACC[CT][CT][AT]
CC  merged_ID: NR1I2_S,NR1I3_S
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m388_M01388,HOCOMOCO_2015_11_23_Human_m390_M01390
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_72
XX
ID  cluster_72
XX
DE  mrgGGATyAds
P0       A     C     G     T
1      799 629.5 577.5 398.5
2      807 337.5   903   357
3      305 409.5 1224.5 465.5
4     81.5    59 2243.5  20.5
5     23.5   4.5  2366  10.5
6     2011   330    56   7.5
7     13.5     8  11.5 2371.5
8      268   656  97.5  1383
9     1949  71.5 112.5 271.5
10     844 256.5 671.5 632.5
11   377.5 984.5   701 341.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_72
CC  AC: cluster_72
CC  id: cluster_72
CC  name: cluster_72
CC  version: 
CC  name: cluster_72
CC  description: mrgGGATyAds
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aggGGATtAac
CC  consensus.strict.rc: GTTAATCCCCT
CC  consensus.IUPAC: mrgGGATyAds
CC  consensus.IUPAC.rc: SHTRATCCCYK
CC  consensus.regexp: [ac][ag]gGGAT[ct]A[agt][cg]
CC  consensus.regexp.rc: [CG][ACT]T[AG]ATCCC[CT][GT]
CC  merged_ID: GSC_D,RHXF1_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m198_M01198,HOCOMOCO_2015_11_23_Human_m487_M01487
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_73
XX
ID  cluster_73
XX
DE  cCrCGTGs
P0       A     C     G     T
1     67.5   165    75  60.5
2     17.5 292.5     8    50
3    236.5   0.5   113    18
4        0   368     0     0
5       16   2.5   349   0.5
6        0   0.5     0 367.5
7        0   0.5   367   0.5
8     64.5 164.5   102    37
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_73
CC  AC: cluster_73
CC  id: cluster_73
CC  name: cluster_73
CC  version: 
CC  name: cluster_73
CC  description: cCrCGTGs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cCaCGTGc
CC  consensus.strict.rc: GCACGTGG
CC  consensus.IUPAC: cCrCGTGs
CC  consensus.IUPAC.rc: SCACGYGG
CC  consensus.regexp: cC[ag]CGTG[cg]
CC  consensus.regexp.rc: [CG]CACG[CT]GG
CC  merged_ID: ARNT_B,MYCN_B
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m15_M01015,HOCOMOCO_2015_11_23_Human_m344_M01344
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_74
XX
ID  cluster_74
XX
DE  rrcCACGTGst
P0       A     C     G     T
1    426.5 130.5 471.5   188
2      375   155 619.5    67
3      246 556.5 121.5 292.5
4       23  1190     2   1.5
5    1150.5    10    35    21
6      7.5  1104    21    84
7     43.5   6.5  1153  13.5
8        8    34    27 1147.5
9      1.5     2 1206.5   6.5
10    94.5 425.5   581 115.5
11     201 218.5 180.5 616.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_74
CC  AC: cluster_74
CC  id: cluster_74
CC  name: cluster_74
CC  version: 
CC  name: cluster_74
CC  description: rrcCACGTGst
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ggcCACGTGgt
CC  consensus.strict.rc: ACCACGTGGCC
CC  consensus.IUPAC: rrcCACGTGst
CC  consensus.IUPAC.rc: ASCACGTGGYY
CC  consensus.regexp: [ag][ag]cCACGTG[cg]t
CC  consensus.regexp.rc: A[CG]CACGTGG[CT][CT]
CC  merged_ID: MAX_A,MYC_A
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m315_M01315,HOCOMOCO_2015_11_23_Human_m345_M01345
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_75
XX
ID  cluster_75
XX
DE  wTTTTAykAcyt
P0       A     C     G     T
1    294.667 112.667 217.333   345
2      116 60.6667   103   690
3    18.3333 14.6667 17.6667   919
4    16.3333 4.66667 8.33333 940.333
5    166.333 3.33333     8   792
6    955.667 1.33333 6.66667     6
7    2.66667 481.667 7.66667 477.667
8    34.6667 41.3333 548.667   345
9    703.667 32.3333 179.667    54
10   62.6667   563   132   212
11   157.333   344   158 310.333
12   136.667   155 155.333 374.667
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_75
CC  AC: cluster_75
CC  id: cluster_75
CC  name: cluster_75
CC  version: 
CC  name: cluster_75
CC  description: wTTTTAykAcyt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tTTTTAcgAcct
CC  consensus.strict.rc: AGGTCGTAAAAA
CC  consensus.IUPAC: wTTTTAykAcyt
CC  consensus.IUPAC.rc: ARGTMRTAAAAW
CC  consensus.regexp: [at]TTTTA[ct][gt]Ac[ct]t
CC  consensus.regexp.rc: A[AG]GT[AC][AG]TAAAA[AT]
CC  merged_ID: HXD11_D,HXC11_D,HXC12_D
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m257_M01257,HOCOMOCO_2015_11_23_Human_m251_M01251,HOCOMOCO_2015_11_23_Human_m252_M01252
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_76
XX
ID  cluster_76
XX
DE  wgcwsTGATTt
P0       A     C     G     T
1       12   4.5     3  17.5
2       26     6  71.5   8.5
3      9.5    75  13.5    14
4     50.5    17   6.5    38
5      4.5    32    66   9.5
6     19.5     2     7  83.5
7        0     0   112     0
8    111.5   0.5     0     0
9        1   0.5     0 110.5
10     0.5     0     0 111.5
11    11.5  15.5  21.5  63.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_76
CC  AC: cluster_76
CC  id: cluster_76
CC  name: cluster_76
CC  version: 
CC  name: cluster_76
CC  description: wgcwsTGATTt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: tgcagTGATTt
CC  consensus.strict.rc: AAATCACTGCA
CC  consensus.IUPAC: wgcwsTGATTt
CC  consensus.IUPAC.rc: AAATCASWGCW
CC  consensus.regexp: [at]gc[at][cg]TGATTt
CC  consensus.regexp.rc: AAATCA[CG][AT]GC[AT]
CC  merged_ID: GFI1B_C,GFI1_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m187_M01187,HOCOMOCO_2015_11_23_Human_m188_M01188
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_77
XX
ID  cluster_77
XX
DE  wwAwsTAGGTCArw
P0       A     C     G     T
1    20.3333 3.66667 9.33333 11.3333
2       21 0.666667 4.33333 18.6667
3    40.6667     1 1.33333 1.66667
4    27.3333     0 0.333333    17
5    4.66667 16.3333    15 8.66667
6    3.66667     2 2.33333 36.6667
7    35.3333     0 9.33333     0
8        0     0 44.3333 0.333333
9        0     0 44.6667     0
10       0     0     0 44.6667
11       0 43.6667     0     1
12   44.6667     0     0     0
13       2 0.666667 3.33333 0.666667
14       2     0     1 1.33333
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_77
CC  AC: cluster_77
CC  id: cluster_77
CC  name: cluster_77
CC  version: 
CC  name: cluster_77
CC  description: wwAwsTAGGTCArw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aaAacTAGGTCAga
CC  consensus.strict.rc: TCTGACCTAGTTTT
CC  consensus.IUPAC: wwAwsTAGGTCArw
CC  consensus.IUPAC.rc: WYTGACCTASWTWW
CC  consensus.regexp: [at][at]A[at][cg]TAGGTCA[ag][at]
CC  consensus.regexp.rc: [AT][CT]TGACCTA[CG][AT]T[AT][AT]
CC  merged_ID: NR1D1_C,RORA_B,RORG_C
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m385_M01385,HOCOMOCO_2015_11_23_Human_m488_M01488,HOCOMOCO_2015_11_23_Human_m489_M01489
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_78
XX
ID  cluster_78
XX
DE  sggcwrwwmCcstkGg
P0       A     C     G     T
1      2.5   5.5   3.5   1.5
2      0.5   2.5   8.5   1.5
3        3   1.5   8.5     0
4        0     7     3     3
5        4     0     3     6
6      3.5     0   7.5     2
7        5     1     0     7
8        7     0     0     6
9        7     4     2     0
10     1.5  10.5     1     0
11       3     8     2     0
12       0   5.5   4.5     3
13       2     3     1     7
14       0     2     7     4
15       0   1.5   9.5     2
16     2.5   2.5   5.5   2.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_78
CC  AC: cluster_78
CC  id: cluster_78
CC  name: cluster_78
CC  version: 
CC  name: cluster_78
CC  description: sggcwrwwmCcstkGg
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cggctgtaaCcctgGg
CC  consensus.strict.rc: CCCAGGGTTACAGCCG
CC  consensus.IUPAC: sggcwrwwmCcstkGg
CC  consensus.IUPAC.rc: CCMASGGKWWYWGCCS
CC  consensus.regexp: [cg]ggc[at][ag][at][at][ac]Cc[cg]t[gt]Gg
CC  consensus.regexp.rc: CC[AC]A[CG]GG[GT][AT][AT][CT][AT]GCC[CG]
CC  merged_ID: SPZ1_D,ZN589_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m533_M01533,HOCOMOCO_2015_11_23_Human_m634_M01634
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_79
XX
ID  cluster_79
XX
DE  gCAGSTG
P0       A     C     G     T
1        1   1.5     3     1
2        0   6.5     0     0
3      6.5     0     0     0
4        0     0   6.5     0
5        0     2   4.5     0
6        0     0     0   6.5
7        0     0   6.5     0
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_79
CC  AC: cluster_79
CC  id: cluster_79
CC  name: cluster_79
CC  version: 
CC  name: cluster_79
CC  description: gCAGSTG
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gCAGgTG
CC  consensus.strict.rc: CACCTGC
CC  consensus.IUPAC: gCAGsTG
CC  consensus.IUPAC.rc: CASCTGC
CC  consensus.regexp: gCAG[cg]TG
CC  consensus.regexp.rc: CA[CG]CTGC
CC  merged_ID: MYF6_C,SNAI2_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m346_M01346,HOCOMOCO_2015_11_23_Human_m508_M01508
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_8
XX
ID  cluster_8
XX
DE  aaawwTGCTGAsTcAGCAwwww
P0       A     C     G     T
1    209.667    46    88 66.3333
2      210 33.6667 73.6667 92.6667
3    257.333    23 32.6667    97
4    201.667    21 44.6667 142.667
5      127    81 81.3333 120.667
6       26 11.6667     9 363.333
7        1     2 404.667 2.33333
8    40.6667   364 1.66667 3.66667
9    10.3333 2.66667 0.666667 396.333
10   6.33333 0.666667 392.333 10.6667
11   397.333     2     5 5.66667
12   19.3333   203 156.333 31.3333
13   40.6667    27 16.6667 325.667
14      51 273.667    22 63.3333
15   301.667 3.33333 56.3333 48.6667
16   27.6667     7   324 51.3333
17   9.66667 340.667 47.6667    12
18     298    33 33.6667 45.3333
19   174.667    54    77 104.333
20   193.333 26.6667 40.3333 149.667
21     165 35.6667    37 172.333
22      80    57    23 128.667
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_8
CC  AC: cluster_8
CC  id: cluster_8
CC  name: cluster_8
CC  version: 
CC  name: cluster_8
CC  description: aaawwTGCTGAsTcAGCAwwww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aaaaaTGCTGAcTcAGCAaatt
CC  consensus.strict.rc: AATTTGCTGAGTCAGCATTTTT
CC  consensus.IUPAC: aaawwTGCTGAsTcAGCAwwww
CC  consensus.IUPAC.rc: WWWWTGCTGASTCAGCAWWTTT
CC  consensus.regexp: aaa[at][at]TGCTGA[cg]TcAGCA[at][at][at][at]
CC  consensus.regexp.rc: [AT][AT][AT][AT]TGCTGA[CG]TCAGCA[AT][AT]TTT
CC  merged_ID: MAFG_S,MAFF_A,MAFK_S
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m310_M01310,HOCOMOCO_2015_11_23_Human_m309_M01309,HOCOMOCO_2015_11_23_Human_m312_M01312
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_80
XX
ID  cluster_80
XX
DE  rGTTAATrwTTaacm
P0       A     C     G     T
1     38.5   4.5  56.5    16
2     12.5   2.5  93.5     7
3      1.5  13.5     1  99.5
4        8   6.5   0.5 100.5
5      111   1.5     1     2
6     87.5  10.5     7  10.5
7       12     1   1.5   101
8     46.5  20.5  32.5    16
9     62.5     4   9.5  39.5
10      10     2   5.5    98
11       7    18   1.5    89
12    64.5     9  20.5  21.5
13      66    26     8  15.5
14      20  68.5     5    22
15      31    32    13    11
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_80
CC  AC: cluster_80
CC  id: cluster_80
CC  name: cluster_80
CC  version: 
CC  name: cluster_80
CC  description: rGTTAATrwTTaacm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gGTTAATaaTTaacc
CC  consensus.strict.rc: GGTTAATTATTAACC
CC  consensus.IUPAC: rGTTAATrwTTaacm
CC  consensus.IUPAC.rc: KGTTAAWYATTAACY
CC  consensus.regexp: [ag]GTTAAT[ag][at]TTaac[ac]
CC  consensus.regexp.rc: [GT]GTTAA[AT][CT]ATTAAC[CT]
CC  merged_ID: HNF1A_A,HNF1B_B
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m224_M01224,HOCOMOCO_2015_11_23_Human_m225_M01225
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_81
XX
ID  cluster_81
XX
DE  ggggrgGGGGcGGGGcsrgsgs
P0       A     C     G     T
1    198.667 226.333 614.333 104.667
2    273.667 242.667   605   161
3      210 214.667 730.333 127.333
4    311.333   279 565.333 126.667
5    436.333 241.333   429 175.667
6    261.333 167.667 755.667 97.6667
7    145.333    41 919.333 176.667
8    227.333    14 1031.33 9.66667
9    9.66667 6.66667 1254.33 11.6667
10   9.66667 17.3333  1252 3.33333
11   218.333 868.333 29.6667   166
12   28.3333    17  1192    45
13   17.6667    20 1184.33 60.3333
14   203.667 68.3333   958 52.3333
15     133   117 978.667 53.6667
16     177   714 256.333   135
17     117   558 338.667 268.667
18   332.333 203.667 490.667 255.667
19   251.667 282.667   623   125
20      53 79.6667 140.333 31.6667
21   50.3333 64.3333 160.333 29.6667
22   20.3333 54.3333 82.3333 9.33333
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_81
CC  AC: cluster_81
CC  id: cluster_81
CC  name: cluster_81
CC  version: 
CC  name: cluster_81
CC  description: ggggrgGGGGcGGGGcsrgsgs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ggggagGGGGcGGGGccggggg
CC  consensus.strict.rc: CCCCCGGCCCCGCCCCCTCCCC
CC  consensus.IUPAC: ggggrgGGGGcGGGGcsrgsgs
CC  consensus.IUPAC.rc: SCSCYSGCCCCGCCCCCYCCCC
CC  consensus.regexp: gggg[ag]gGGGGcGGGGc[cg][ag]g[cg]g[cg]
CC  consensus.regexp.rc: [CG]C[CG]C[CT][CG]GCCCCGCCCCC[CT]CCCC
CC  merged_ID: SP1_S,SP2_C,SP3_B
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m524_M01524,HOCOMOCO_2015_11_23_Human_m526_M01526,HOCOMOCO_2015_11_23_Human_m527_M01527
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_82
XX
ID  cluster_82
XX
DE  hTrTTTrTtTw
P0       A     C     G     T
1      1.5     1   0.5     1
2        0   0.5     0    24
3        7     0  16.5     1
4        0     0   0.5    24
5        0     0     0  24.5
6        0     0     0  24.5
7     13.5   0.5  10.5     0
8        0   4.5     0    20
9        3     5   0.5    16
10       1     1     5  17.5
11      11   2.5   2.5   8.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_82
CC  AC: cluster_82
CC  id: cluster_82
CC  name: cluster_82
CC  version: 
CC  name: cluster_82
CC  description: hTrTTTrTtTw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aTgTTTaTtTa
CC  consensus.strict.rc: TAAATAAACAT
CC  consensus.IUPAC: hTrTTTrTtTw
CC  consensus.IUPAC.rc: WAAAYAAAYAD
CC  consensus.regexp: [act]T[ag]TTT[ag]TtT[at]
CC  consensus.regexp.rc: [AT]AAA[CT]AAA[CT]A[AGT]
CC  merged_ID: FOXF1_D,FOXJ2_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m152_M01152,HOCOMOCO_2015_11_23_Human_m157_M01157
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_83
XX
ID  cluster_83
XX
DE  mrGGtCAsksTGACCtggs
P0       A     C     G     T
1       47    60 37.6667    22
2      251 37.3333   164 50.3333
3       65    15   369 53.6667
4    51.3333 15.6667 408.667    27
5    55.6667 47.3333   107 292.667
6        5 428.667    26    43
7    374.333 21.6667 37.3333 69.3333
8    60.3333 160.333 183.667 98.3333
9    54.3333 83.3333 229.333 135.667
10   60.6667 134.333   236 71.6667
11   24.3333 40.3333 16.6667 421.333
12   27.6667 53.3333 417.333 4.33333
13   343.333 84.6667 39.3333 35.3333
14      15   445 20.3333 22.3333
15   55.3333 397.333     8    42
16   36.3333   122 17.6667 326.667
17   72.3333 93.6667   212 124.667
18   120.333 75.3333 211.667 95.3333
19   30.3333    57 63.6667    24
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_83
CC  AC: cluster_83
CC  id: cluster_83
CC  name: cluster_83
CC  version: 
CC  name: cluster_83
CC  description: mrGGtCAsksTGACCtggs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: caGGtCAgggTGACCtggg
CC  consensus.strict.rc: CCCAGGTCACCCTGACCTG
CC  consensus.IUPAC: mrGGtCAsksTGACCtggs
CC  consensus.IUPAC.rc: SCCAGGTCASMSTGACCYK
CC  consensus.regexp: [ac][ag]GGtCA[cg][gt][cg]TGACCtgg[cg]
CC  consensus.regexp.rc: [CG]CCAGGTCA[CG][AC][CG]TGACC[CT][GT]
CC  merged_ID: ESR1_S,ESR1_S,ESR2_S
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m120_M01120,HOCOMOCO_2015_11_23_Human_m119_M01119,HOCOMOCO_2015_11_23_Human_m121_M01121
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_84
XX
ID  cluster_84
XX
DE  ggrrAAmyGAAAstga
P0       A     C     G     T
1        2     2 6.33333     2
2    169.333 95.6667   297 153.333
3    194.667 46.6667   458    16
4    448.667 10.3333   219 37.3333
5      689 6.66667 13.6667     6
6    665.667    13 21.6667    15
7    262.667 195.333 170.333    87
8    152.333   194   169   200
9    97.6667 12.6667   595    10
10     617 12.3333 79.6667 6.33333
11     644 44.6667    20 6.66667
12   626.333 7.66667    27 54.3333
13   16.3333 401.333 256.333 41.3333
14   92.3333 127.333    47 448.667
15     170   101 314.667 129.667
16     380   148 66.3333    81
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_84
CC  AC: cluster_84
CC  id: cluster_84
CC  name: cluster_84
CC  version: 
CC  name: cluster_84
CC  description: ggrrAAmyGAAAstga
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gggaAAatGAAActga
CC  consensus.strict.rc: TCAGTTTCATTTTCCC
CC  consensus.IUPAC: ggrrAAmyGAAAstga
CC  consensus.IUPAC.rc: TCASTTTCRKTTYYCC
CC  consensus.regexp: gg[ag][ag]AA[ac][ct]GAAA[cg]tga
CC  consensus.regexp.rc: TCA[CG]TTTC[AG][GT]TT[CT][CT]CC
CC  merged_ID: STAT2_B,IRF2_C,IRF8_D
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m542_M01542,HOCOMOCO_2015_11_23_Human_m268_M01268,HOCOMOCO_2015_11_23_Human_m273_M01273
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_85
XX
ID  cluster_85
XX
DE  sssscsGTTGCCAkGGmrACsgs
P0       A     C     G     T
1     20.5    64    85    25
2     19.5  77.5    65  32.5
3     40.5   176 116.5    67
4       40 154.5 148.5    57
5     25.5 229.5    85    60
6       38 150.5 149.5    62
7        2     0   398     0
8        0  11.5     1 387.5
9      7.5    48     3 341.5
10    35.5     4   337  23.5
11       0 375.5     0  24.5
12     0.5 313.5   0.5  85.5
13     301  28.5    40  30.5
14    62.5  17.5 103.5 216.5
15    16.5   1.5 381.5   0.5
16      35    26 335.5   3.5
17   112.5   154  72.5    61
18   181.5    15 178.5    25
19   291.5  65.5  32.5  10.5
20      21   327    22    30
21    71.5   127   170  31.5
22      70  88.5 217.5    24
23    26.5  68.5    88  22.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_85
CC  AC: cluster_85
CC  id: cluster_85
CC  name: cluster_85
CC  version: 
CC  name: cluster_85
CC  description: sssscsGTTGCCAkGGmrACsgs
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gcccccGTTGCCAtGGcaACggg
CC  consensus.strict.rc: CCCGTTGCCATGGCAACGGGGGC
CC  consensus.IUPAC: sssscsGTTGCCAkGGmrACsgs
CC  consensus.IUPAC.rc: SCSGTYKCCMTGGCAACSGSSSS
CC  consensus.regexp: [cg][cg][cg][cg]c[cg]GTTGCCA[gt]GG[ac][ag]AC[cg]g[cg]
CC  consensus.regexp.rc: [CG]C[CG]GT[CT][GT]CC[AC]TGGCAAC[CG]G[CG][CG][CG][CG]
CC  merged_ID: RFX2_C,ZBT7B_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m483_M01483,HOCOMOCO_2015_11_23_Human_m608_M01608
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_86
XX
ID  cluster_86
XX
DE  wwswTGTTTACrt
P0       A     C     G     T
1      763   208 284.5 467.5
2      618   294   266   545
3    200.5 640.5 615.5 266.5
4      655  41.5   155 871.5
5      2.5   0.5     2  1718
6      8.5     0 1714.5     0
7        0   1.5   0.5  1721
8      2.5     1    66 1653.5
9      0.5     1 176.5  1545
10   1604.5  36.5    15    67
11      10  1509  21.5 182.5
12     471   211   786   255
13     370   274 184.5 894.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_86
CC  AC: cluster_86
CC  id: cluster_86
CC  name: cluster_86
CC  version: 
CC  name: cluster_86
CC  description: wwswTGTTTACrt
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aactTGTTTACgt
CC  consensus.strict.rc: ACGTAAACAAGTT
CC  consensus.IUPAC: wwswTGTTTACrt
CC  consensus.IUPAC.rc: AYGTAAACAWSWW
CC  consensus.regexp: [at][at][cg][at]TGTTTAC[ag]t
CC  consensus.regexp.rc: A[CT]GTAAACA[AT][CG][AT][AT]
CC  merged_ID: FOXO3_B,FOXO6_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m164_M01164,HOCOMOCO_2015_11_23_Human_m166_M01166
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_87
XX
ID  cluster_87
XX
DE  gGTCAAAGGTCAh
P0       A     C     G     T
1     54.5    56   202    55
2      4.5     3   306    54
3        7    14    35 311.5
4        6 320.5   9.5  31.5
5    357.5     3     2     5
6      322   3.5  34.5   7.5
7      346   0.5  20.5   0.5
8      0.5     0 366.5   0.5
9      1.5     2 324.5  39.5
10       6     9   9.5   343
11     2.5   316    27    22
12     336     6  23.5     2
13    95.5 107.5  33.5    90
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_87
CC  AC: cluster_87
CC  id: cluster_87
CC  name: cluster_87
CC  version: 
CC  name: cluster_87
CC  description: gGTCAAAGGTCAh
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gGTCAAAGGTCAc
CC  consensus.strict.rc: GTGACCTTTGACC
CC  consensus.IUPAC: gGTCAAAGGTCAh
CC  consensus.IUPAC.rc: DTGACCTTTGACC
CC  consensus.regexp: gGTCAAAGGTCA[act]
CC  consensus.regexp.rc: [AGT]TGACCTTTGACC
CC  merged_ID: RXRG_B,COT1_S
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m496_M01496,HOCOMOCO_2015_11_23_Human_m58_M01058
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_88
XX
ID  cluster_88
XX
DE  gamCAGATGkyc
P0       A     C     G     T
1        3     5  10.5     2
2     52.5    11    16    11
3       36  36.5  17.5   0.5
4        0  90.5     0     0
5     90.5     0     0     0
6      0.5     2    88     0
7       72   4.5  13.5   0.5
8      0.5     8   0.5  81.5
9      2.5     5  81.5   1.5
10       3    17    41  29.5
11     6.5  23.5     3  57.5
12    10.5  45.5    15  19.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_88
CC  AC: cluster_88
CC  id: cluster_88
CC  name: cluster_88
CC  version: 
CC  name: cluster_88
CC  description: gamCAGATGkyc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gacCAGATGgtc
CC  consensus.strict.rc: GACCATCTGGTC
CC  consensus.IUPAC: gamCAGATGkyc
CC  consensus.IUPAC.rc: GRMCATCTGKTC
CC  consensus.regexp: ga[ac]CAGATG[gt][ct]c
CC  consensus.regexp.rc: G[AG][AC]CATCTG[GT]TC
CC  merged_ID: TAL1_S,TFE2_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m549_M01549,HOCOMOCO_2015_11_23_Human_m574_M01574
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_89
XX
ID  cluster_89
XX
DE  gcTAATTrsy
P0       A     C     G     T
1        2 1.33333 7.33333 0.666667
2    12.3333 30.3333 5.66667 11.3333
3        0 0.333333     0 59.3333
4    58.6667     0 0.333333 0.666667
5       59 0.666667     0     0
6    0.666667 0.666667 0.666667 57.6667
7    0.666667     2    10    47
8    38.3333 0.666667 19.3333 1.33333
9    6.33333 30.6667 19.3333 3.33333
10   10.3333 17.6667 8.66667    23
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_89
CC  AC: cluster_89
CC  id: cluster_89
CC  name: cluster_89
CC  version: 
CC  name: cluster_89
CC  description: gcTAATTrsy
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gcTAATTact
CC  consensus.strict.rc: AGTAATTAGC
CC  consensus.IUPAC: gcTAATTrsy
CC  consensus.IUPAC.rc: RSYAATTAGC
CC  consensus.regexp: gcTAATT[ag][cg][ct]
CC  consensus.regexp.rc: [AG][CG][CT]AATTAGC
CC  merged_ID: DLX3_C,NOBOX_C,PDX1_C
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m78_M01078,HOCOMOCO_2015_11_23_Human_m382_M01382,HOCOMOCO_2015_11_23_Human_m431_M01431
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_9
XX
ID  cluster_9
XX
DE  ATGACGTCAycr
P0       A     C     G     T
1    2431.33 162.5   332 54.3333
2    36.6667 36.1667    22 2972.67
3    24.1667  24.5  2382 636.833
4     2985  14.5 45.1667 22.8333
5    24.8333  3021 3.33333 18.3333
6    49.1667 21.8333 2987.83 8.66667
7        1 12.3333     1 3053.17
8    43.3333  3020     1 3.16667
9    3048.5 11.1667     3 4.83333
10    32.5  1137 40.3333 1857.67
11     659  1457 300.833 650.667
12     222 133.667 155.5    82
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_9
CC  AC: cluster_9
CC  id: cluster_9
CC  name: cluster_9
CC  version: 
CC  name: cluster_9
CC  description: ATGACGTCAycr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ATGACGTCAtca
CC  consensus.strict.rc: TGATGACGTCAT
CC  consensus.IUPAC: ATGACGTCAycr
CC  consensus.IUPAC.rc: YGRTGACGTCAT
CC  consensus.regexp: ATGACGTCA[ct]c[ag]
CC  consensus.regexp.rc: [CT]G[AG]TGACGTCAT
CC  merged_ID: CREB1_A,CREM_C,JDP2_D,ATF1_B,ATF7_D,CREB5_D
CC  merge_nb: 6
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m64_M01064,HOCOMOCO_2015_11_23_Human_m67_M01067,HOCOMOCO_2015_11_23_Human_m281_M01281,HOCOMOCO_2015_11_23_Human_m18_M01018,HOCOMOCO_2015_11_23_Human_m22_M01022,HOCOMOCO_2015_11_23_Human_m66_M01066
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_90
XX
ID  cluster_90
XX
DE  rrArsmGGAARtrrrrr
P0       A     C     G     T
1       89    63   142  46.5
2    130.5    52   130    28
3      237    19    52  32.5
4      103    84 109.5    44
5     74.5   140   120     6
6    226.5  99.5  11.5     3
7      1.5   1.5   335   2.5
8       12   2.5   326     0
9      326   0.5  10.5   3.5
10     327     2   3.5     8
11    90.5  11.5 235.5     3
12      52    77  62.5   149
13   117.5    36   140    47
14      95    48   159  38.5
15   114.5  63.5 118.5    44
16     109  54.5   133    44
17     120    76 113.5    31
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_90
CC  AC: cluster_90
CC  id: cluster_90
CC  name: cluster_90
CC  version: 
CC  name: cluster_90
CC  description: rrArsmGGAARtrrrrr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: gaAgcaGGAAGtgggga
CC  consensus.strict.rc: TCCCCACTTCCTGCTTC
CC  consensus.IUPAC: rrArsmGGAARtrrrrr
CC  consensus.IUPAC.rc: YYYYYAYTTCCKSYTYY
CC  consensus.regexp: [ag][ag]A[ag][cg][ac]GGAA[AG]t[ag][ag][ag][ag][ag]
CC  consensus.regexp.rc: [CT][CT][CT][CT][CT]A[CT]TTCC[GT][CG][CT]T[CT][CT]
CC  merged_ID: EHF_S,ELF2_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m100_M01100,HOCOMOCO_2015_11_23_Human_m103_M01103
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_91
XX
ID  cluster_91
XX
DE  CTTTGww
P0       A     C     G     T
1      0.5    36     1     0
2      2.5     1     0    34
3        0     0     0  37.5
4        0     0     0  37.5
5        0     0  37.5     0
6     17.5     2   0.5  17.5
7     12.5     2     1    22
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_91
CC  AC: cluster_91
CC  id: cluster_91
CC  name: cluster_91
CC  version: 
CC  name: cluster_91
CC  description: CTTTGww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: CTTTGat
CC  consensus.strict.rc: ATCAAAG
CC  consensus.IUPAC: CTTTGww
CC  consensus.IUPAC.rc: WWCAAAG
CC  consensus.regexp: CTTTG[at][at]
CC  consensus.regexp.rc: [AT][AT]CAAAG
CC  merged_ID: LEF1_C,SOX15_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m298_M01298,HOCOMOCO_2015_11_23_Human_m512_M01512
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_92
XX
ID  cluster_92
XX
DE  TAATTAAtwrhktwwwwww
P0       A     C     G     T
1        2     4   1.5    19
2       23     1     1   1.5
3     23.5   0.5     0   2.5
4      0.5     2     0    24
5        0   1.5     0    25
6     26.5     0     0     0
7       21     3   2.5     0
8      5.5     6     1    14
9      7.5   0.5     2  16.5
10     9.5   4.5     7   5.5
11       7   7.5     5     7
12       4   5.5     7    10
13     4.5     4     1    17
14    10.5   3.5     2  10.5
15    16.5   1.5   0.5     8
16    10.5   0.5     0  15.5
17       8     1   0.5    17
18    10.5     2     3    11
19       5     1   1.5     4
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_92
CC  AC: cluster_92
CC  id: cluster_92
CC  name: cluster_92
CC  version: 
CC  name: cluster_92
CC  description: TAATTAAtwrhktwwwwww
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: TAATTAAttacttaattta
CC  consensus.strict.rc: TAAATTAAGTAATTAATTA
CC  consensus.IUPAC: TAATTAAtwrhktwwwwww
CC  consensus.IUPAC.rc: WWWWWWAMDYWATTAATTA
CC  consensus.regexp: TAATTAAt[at][ag][act][gt]t[at][at][at][at][at][at]
CC  consensus.regexp.rc: [AT][AT][AT][AT][AT][AT]A[AC][AGT][CT][AT]ATTAATTA
CC  merged_ID: LMX1A_D,LMX1B_D
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m305_M01305,HOCOMOCO_2015_11_23_Human_m306_M01306
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_93
XX
ID  cluster_93
XX
DE  gGSGCGmAwC
P0       A     C     G     T
1        1     1     5     1
2        0     0     8     0
3        0     6     2     0
4        0     0     8     0
5        0     6     1     1
6        0     0     8     0
7        5     2     1     0
8        7     1     0     0
9        5     0     1     2
10       1     6     0     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_93
CC  AC: HOCOMOCO_2015_11_23_Human_m87_M01087
CC  id: HOCOMOCO_2015_11_23_Human_m87_M01087
CC  name: cluster_93
CC  version: 
CC  name: cluster_93
CC  description: gGSGCGmAwC
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: E2F2_B
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m87_M01087
XX
//
AC  cluster_94
XX
ID  cluster_94
XX
DE  wTAATTrw
P0       A     C     G     T
1    1.66667 0.333333 0.666667 1.66667
2        0     0 0.333333 7.66667
3        7 0.333333 0.666667     0
4        7     0     1     0
5        0     0     0     8
6    0.666667     0     0 7.33333
7        2     1 3.33333 1.66667
8        2     1 1.66667 3.33333
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_94
CC  AC: cluster_94
CC  id: cluster_94
CC  name: cluster_94
CC  version: 
CC  name: cluster_94
CC  description: wTAATTrw
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: aTAATTgt
CC  consensus.strict.rc: ACAATTAT
CC  consensus.IUPAC: wTAATTrw
CC  consensus.IUPAC.rc: WYAATTAW
CC  consensus.regexp: [at]TAATT[ag][at]
CC  consensus.regexp.rc: [AT][CT]AATTA[AT]
CC  merged_ID: DLX2_D,HXD4_D,MSX2_D
CC  merge_nb: 3
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m77_M01077,HOCOMOCO_2015_11_23_Human_m261_M01261,HOCOMOCO_2015_11_23_Human_m338_M01338
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_95
XX
ID  cluster_95
XX
DE  cdGGGyGkGGc
P0       A     C     G     T
1        1   2.5     1     1
2    619.5    47 519.5 630.5
3    132.5     3 1678.5   2.5
4     38.5   3.5  1770   4.5
5       29   9.5 1765.5  12.5
6      259   773  11.5   773
7       69     5 1733.5     9
8       58  41.5   933   784
9    114.5  21.5 1644.5    36
10      55  67.5 1586.5 107.5
11   120.5 1116.5 201.5   378
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_95
CC  AC: cluster_95
CC  id: cluster_95
CC  name: cluster_95
CC  version: 
CC  name: cluster_95
CC  description: cdGGGyGkGGc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: ctGGGcGgGGc
CC  consensus.strict.rc: GCCCCGCCCAG
CC  consensus.IUPAC: cdGGGyGkGGc
CC  consensus.IUPAC.rc: GCCMCRCCCHG
CC  consensus.regexp: c[agt]GGG[ct]G[gt]GGc
CC  consensus.regexp.rc: GCC[AC]C[AG]CCC[ACT]G
CC  merged_ID: KLF1_C,KLF4_A
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m292_M01292,HOCOMOCO_2015_11_23_Human_m294_M01294
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_96
XX
ID  cluster_96
XX
DE  vCAGGTGyr
P0       A     C     G     T
1       76  78.5    74     1
2        0 229.5     0     0
3    229.5     0     0     0
4        0     0 229.5     0
5        0     0 229.5     0
6        0     0   0.5   229
7        0     0 229.5     0
8        6   150   1.5    72
9     84.5  18.5  78.5  45.5
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_96
CC  AC: cluster_96
CC  id: cluster_96
CC  name: cluster_96
CC  version: 
CC  name: cluster_96
CC  description: vCAGGTGyr
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  consensus.strict: cCAGGTGca
CC  consensus.strict.rc: TGCACCTGG
CC  consensus.IUPAC: vCAGGTGyr
CC  consensus.IUPAC.rc: YRCACCTGB
CC  consensus.regexp: [acg]CAGGTG[ct][ag]
CC  consensus.regexp.rc: [CT][AG]CACCTG[CGT]
CC  merged_ID: ITF2_B,SNAI1_C
CC  merge_nb: 2
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m280_M01280,HOCOMOCO_2015_11_23_Human_m507_M01507
CC  merge_type: mean
CC  type: merge
XX
//
AC  cluster_97
XX
ID  cluster_97
XX
DE  gGGGGcGGGGc
P0       A     C     G     T
1      683   443  2076   287
2      300   104  2614   471
3      591    43  2845    10
4        7    45  3433     4
5       91    19  3374     5
6      809  2247    62   371
7      121    42  3242    84
8       14    36  3332   107
9      558    90  2695   146
10     264   255  2797   173
11     519  1875   655   440
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_97
CC  AC: HOCOMOCO_2015_11_23_Human_m525_M01525
CC  id: HOCOMOCO_2015_11_23_Human_m525_M01525
CC  name: cluster_97
CC  version: 
CC  name: cluster_97
CC  description: gGGGGcGGGGc
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: SP1_S
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m525_M01525
XX
//
AC  cluster_98
XX
ID  cluster_98
XX
DE  GGsGGyrGGGm
P0       A     C     G     T
1        0     0     6     0
2        0     0     6     0
3        0     3     2     1
4        0     0     6     0
5        0     0     6     0
6        0     3     1     2
7        3     0     3     0
8        0     0     5     1
9        0     1     5     0
10       0     0     6     0
11       2     2     1     1
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_98
CC  AC: HOCOMOCO_2015_11_23_Human_m99_M01099
CC  id: HOCOMOCO_2015_11_23_Human_m99_M01099
CC  name: cluster_98
CC  version: 
CC  name: cluster_98
CC  description: GGsGGyrGGGm
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: EGR4_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m99_M01099
XX
//
AC  cluster_99
XX
ID  cluster_99
XX
DE  tTAATTrrywa
P0       A     C     G     T
1       58    72    33   172
2       13     1     5   316
3      332     1     1     1
4      333     1     1     0
5        0     1     0   334
6        0     1     0   334
7      185     0   144     6
8      125    52   146    12
9       17   162    14   142
10     142    70    23   100
11     161    51    50    73
XX
CC  program: transfac
CC  matrix.nb: 1
CC  accession: cluster_99
CC  AC: HOCOMOCO_2015_11_23_Human_m497_M01497
CC  id: HOCOMOCO_2015_11_23_Human_m497_M01497
CC  name: cluster_99
CC  version: 
CC  name: cluster_99
CC  description: tTAATTrrywa
CC  transfac_consensus: 
CC  matrix.nb: 1
CC  merged_ID: RX_D
CC  merge_nb: 1
CC  merge_type: mean
CC  type: merge
CC  merged_AC: HOCOMOCO_2015_11_23_Human_m497_M01497
XX
//
