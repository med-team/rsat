AC  abd-A
XX
ID  abd-A
XX
DE  aTAAwT
P0       A     C     G     T
1    27.439 5.4878 4.39024 7.68293
2        0 3.29268 1.09756 40.6098
3    40.6098 1.09756 3.29268     0
4    40.6098     0     0 4.39024
5    14.2683     0     0 30.7317
6        0 3.29268 6.58537 35.122
XX
CC  program: clusterbuster
CC  matrix.nb: 1
CC  consensus.strict: aTAAtT
CC  consensus.strict.rc: AATTAT
CC  consensus.IUPAC: aTAAwT
CC  consensus.IUPAC.rc: AWTTAT
CC  consensus.regexp: aTAA[at]T
CC  consensus.regexp.rc: A[AT]TTAT
XX
//
AC  Antp
XX
ID  Antp
XX
DE  aATAAw
P0       A     C     G     T
1     10.5   4.5     3     0
2       18     0     0     0
3        0     0     0    18
4     16.5     0   1.5     0
5       18     0     0     0
6      7.5     0     0  10.5
XX
CC  program: clusterbuster
CC  matrix.nb: 2
CC  consensus.strict: aATAAt
CC  consensus.strict.rc: ATTATT
CC  consensus.IUPAC: aATAAw
CC  consensus.IUPAC.rc: WTTATT
CC  consensus.regexp: aATAA[at]
CC  consensus.regexp.rc: [AT]TTATT
XX
//
AC  ap
XX
ID  ap
XX
DE  AAThAt
P0       A     C     G     T
1       14     0     0     0
2       14     0     0     0
3        0     1     1    12
4        5     4     1     4
5       13     0     1     0
6        2     1     3     8
XX
CC  program: clusterbuster
CC  matrix.nb: 3
CC  consensus.strict: AATaAt
CC  consensus.strict.rc: ATTATT
CC  consensus.IUPAC: AAThAt
CC  consensus.IUPAC.rc: ATDATT
CC  consensus.regexp: AAT[act]At
CC  consensus.regexp.rc: AT[AGT]ATT
XX
//
AC  bcd
XX
ID  bcd
XX
DE  vrGmTTAd
P0       A     C     G     T
1    13.2708 18.375 12.25 5.10417
2    15.3125 2.04167 29.6042 2.04167
3    5.10417     0 42.875 1.02083
4    33.6875 15.3125     0     0
5        0     0     0    49
6    1.02083 2.04167     0 45.9375
7    36.75 1.02083 3.0625 8.16667
8    16.3333 2.04167 18.375 12.25
XX
CC  program: clusterbuster
CC  matrix.nb: 4
CC  consensus.strict: cgGaTTAg
CC  consensus.strict.rc: CTAATCCG
CC  consensus.IUPAC: vrGmTTAd
CC  consensus.IUPAC.rc: HTAAKCYB
CC  consensus.regexp: [acg][ag]G[ac]TTA[agt]
CC  consensus.regexp.rc: [ACT]TAA[GT]C[CT][CGT]
XX
//
AC  brk
XX
ID  brk
XX
DE  rGCGCCr
P0       A     C     G     T
1      4.4     0   6.6     0
2      2.2     0   8.8     0
3        0   9.9     0   1.1
4        0     0   9.9   1.1
5        0    11     0     0
6        0    11     0     0
7      5.5     0   5.5     0
XX
CC  program: clusterbuster
CC  matrix.nb: 5
CC  consensus.strict: gGCGCCa
CC  consensus.strict.rc: TGGCGCC
CC  consensus.IUPAC: rGCGCCr
CC  consensus.IUPAC.rc: YGGCGCY
CC  consensus.regexp: [ag]GCGCC[ag]
CC  consensus.regexp.rc: [CT]GGCGC[CT]
XX
//
AC  br-Z1
XX
ID  br-Z1
XX
DE  raTaCwAA
P0       A     C     G     T
1    10.5882     0 7.41176     0
2    11.6471     0 3.17647 3.17647
3    2.11765     0 1.05882 14.8235
4    11.6471 3.17647 3.17647     0
5    3.17647 12.7059 2.11765     0
6    9.52941 1.05882     0 7.41176
7    15.8824 2.11765     0     0
8       18     0     0     0
XX
CC  program: clusterbuster
CC  matrix.nb: 6
CC  consensus.strict: aaTaCaAA
CC  consensus.strict.rc: TTTGTATT
CC  consensus.IUPAC: raTaCwAA
CC  consensus.IUPAC.rc: TTWGTATY
CC  consensus.regexp: [ag]aTaC[at]AA
CC  consensus.regexp.rc: TT[AT]GTAT[CT]
XX
//
AC  br-Z2
XX
ID  br-Z2
XX
DE  CTAttwa
P0       A     C     G     T
1        0    20     0     1
2        0     0     0    21
3       21     0     0     0
4        4     3     3    11
5        5     1     3    12
6        8     3     1     9
7       12     0     4     5
XX
CC  program: clusterbuster
CC  matrix.nb: 7
CC  consensus.strict: CTAttta
CC  consensus.strict.rc: TAAATAG
CC  consensus.IUPAC: CTAttwa
CC  consensus.IUPAC.rc: TWAATAG
CC  consensus.regexp: CTAtt[at]a
CC  consensus.regexp.rc: T[AT]AATAG
XX
//
AC  br-Z3
XX
ID  br-Z3
XX
DE  wyhWhTAkTwT
P0       A     C     G     T
1        8     0     0     8
2        1     6     2     7
3        7     4     0     5
4       12     0     0     4
5        5     6     0     5
6        2     0     1    13
7       13     1     1     1
8        1     0    11     4
9        0     2     1    13
10       8     0     0     8
11       1     1     0    14
XX
CC  program: clusterbuster
CC  matrix.nb: 8
CC  consensus.strict: ataAcTAgTaT
CC  consensus.strict.rc: ATACTAGTTAT
CC  consensus.IUPAC: wyhWhTAkTwT
CC  consensus.IUPAC.rc: AWAMTADWDRW
CC  consensus.regexp: [at][ct][act][AT][act]TA[gt]T[at]T
CC  consensus.regexp.rc: A[AT]A[AC]TA[AGT][AT][AGT][AG][AT]
XX
//
AC  br-Z4
XX
ID  br-Z4
XX
DE  AhTRdTTA
P0       A     C     G     T
1        7     0     0     1
2        3     2     0     3
3        0     0     0     8
4        6     0     2     0
5        3     0     3     2
6        0     0     0     8
7        1     0     0     7
8        7     0     0     1
XX
CC  program: clusterbuster
CC  matrix.nb: 9
CC  consensus.strict: AaTAaTTA
CC  consensus.strict.rc: TAATTATT
CC  consensus.IUPAC: AhTRdTTA
CC  consensus.IUPAC.rc: TAAHYADT
CC  consensus.regexp: A[act]T[AG][agt]TTA
CC  consensus.regexp.rc: TAA[ACT][CT]A[AGT]T
XX
//
AC  byn
XX
ID  byn
XX
DE  TyGCAChT
P0       A     C     G     T
1    1.11111     0     0 8.88889
2    2.22222 4.44444     0 3.33333
3    1.11111     0 7.77778 1.11111
4        0    10     0     0
5    8.88889     0     0 1.11111
6        0    10     0     0
7    3.33333 3.33333     0 3.33333
8    1.11111 1.11111     0 7.77778
XX
CC  program: clusterbuster
CC  matrix.nb: 10
CC  consensus.strict: TcGCACaT
CC  consensus.strict.rc: ATGTGCGA
CC  consensus.IUPAC: TyGCAChT
CC  consensus.IUPAC.rc: ADGTGCRA
CC  consensus.regexp: T[ct]GCAC[act]T
CC  consensus.regexp.rc: A[AGT]GTGC[AG]A
XX
//
AC  cad
XX
ID  cad
XX
DE  TTTwyrayw
P0       A     C     G     T
1        2     0     0    11
2        0     0     2    11
3        0     1     0    12
4        6     1     2     4
5        0     5     1     7
6        4     0     9     0
7        6     3     3     1
8        0     4     1     8
9        4     0     0     9
XX
CC  program: clusterbuster
CC  matrix.nb: 11
CC  consensus.strict: TTTatgatt
CC  consensus.strict.rc: AATCATAAA
CC  consensus.IUPAC: TTTwyrayw
CC  consensus.IUPAC.rc: WRTYRWAAA
CC  consensus.regexp: TTT[at][ct][ag]a[ct][at]
CC  consensus.regexp.rc: [AT][AG]T[CT][AG][AT]AAA
XX
//
AC  Deaf1
XX
ID  Deaf1
XX
DE  mCGAmkvrC
P0       A     C     G     T
1        3     7     0     0
2        0    10     0     0
3        0     0    10     0
4        9     1     0     0
5        7     3     0     0
6        2     0     4     4
7        3     4     3     0
8        4     0     6     0
9        0     8     2     0
XX
CC  program: clusterbuster
CC  matrix.nb: 12
CC  consensus.strict: cCGAagcgC
CC  consensus.strict.rc: GCGCTTCGG
CC  consensus.IUPAC: mCGAmkvrC
CC  consensus.IUPAC.rc: GYBMKTCGK
CC  consensus.regexp: [ac]CGA[ac][gt][acg][ag]C
CC  consensus.regexp.rc: G[CT][CGT][AC][GT]TCG[GT]
XX
//
AC  Dfd
XX
ID  Dfd
XX
DE  tTAATkry
P0       A     C     G     T
1        1     2     2    11
2        1     0     0    15
3       15     1     0     0
4       15     1     0     0
5        0     0     0    16
6        1     0     5    10
7        8     0     6     2
8        0     5     2     9
XX
CC  program: clusterbuster
CC  matrix.nb: 13
CC  consensus.strict: tTAATtat
CC  consensus.strict.rc: ATAATTAA
CC  consensus.IUPAC: tTAATkry
CC  consensus.IUPAC.rc: RYMATTAA
CC  consensus.regexp: tTAAT[gt][ag][ct]
CC  consensus.regexp.rc: [AG][CT][AC]ATTAA
XX
//
AC  dl
XX
ID  dl
XX
DE  kkrwTTTyCym
P0       A     C     G     T
1        0     1    22    15
2        2     3    21    12
3       20     0    18     0
4       13     4     3    18
5        2     0     2    34
6        0     1     3    34
7        2     4     1    31
8        0    21     0    17
9        1    36     0     1
10       0    16     9    13
11      12    17     8     1
XX
CC  program: clusterbuster
CC  matrix.nb: 14
CC  consensus.strict: ggatTTTcCcc
CC  consensus.strict.rc: GGGGAAAATCC
CC  consensus.IUPAC: kkrwTTTyCym
CC  consensus.IUPAC.rc: KRGRAAAWYMM
CC  consensus.regexp: [gt][gt][ag][at]TTT[ct]C[ct][ac]
CC  consensus.regexp.rc: [GT][AG]G[AG]AAA[AT][CT][AC][AC]
XX
//
AC  Dref
XX
ID  Dref
XX
DE  aTAwCGATAr
P0       A     C     G     T
1    6.66667 1.11111     0 2.22222
2    1.11111     0 1.11111 7.77778
3    7.77778 1.11111     0 1.11111
4    3.33333     0     0 6.66667
5        0    10     0     0
6    1.11111     0 8.88889     0
7       10     0     0     0
8        0     0     0    10
9    7.77778 1.11111 1.11111     0
10   3.33333     0 6.66667     0
XX
CC  program: clusterbuster
CC  matrix.nb: 15
CC  consensus.strict: aTAtCGATAg
CC  consensus.strict.rc: CTATCGATAT
CC  consensus.IUPAC: aTAwCGATAr
CC  consensus.IUPAC.rc: YTATCGWTAT
CC  consensus.regexp: aTA[at]CGATA[ag]
CC  consensus.regexp.rc: [CT]TATCG[AT]TAT
XX
//
AC  Eip74EF
XX
ID  Eip74EF
XX
DE  mMGGAAkT
P0       A     C     G     T
1        3     5     0     0
2        6     2     0     0
3        1     0     7     0
4        0     1     7     0
5        8     0     0     0
6        8     0     0     0
7        1     0     5     2
8        1     1     0     6
XX
CC  program: clusterbuster
CC  matrix.nb: 16
CC  consensus.strict: cAGGAAgT
CC  consensus.strict.rc: ACTTCCTG
CC  consensus.IUPAC: mMGGAAkT
CC  consensus.IUPAC.rc: AMTTCCKK
CC  consensus.regexp: [ac][AC]GGAA[gt]T
CC  consensus.regexp.rc: A[AC]TTCC[GT][GT]
XX
//
AC  en
XX
ID  en
XX
DE  dtTaATTr
P0       A     C     G     T
1    23.625     0 21.375    18
2    7.875 12.375 11.25  31.5
3        0  6.75   4.5 51.75
4       36  6.75 14.625 5.625
5    60.75 1.125     0 1.125
6     2.25 1.125  2.25 57.375
7    1.125     0 3.375  58.5
8    20.25 1.125  31.5 10.125
XX
CC  program: clusterbuster
CC  matrix.nb: 17
CC  consensus.strict: atTaATTg
CC  consensus.strict.rc: CAATTAAT
CC  consensus.IUPAC: dtTaATTr
CC  consensus.IUPAC.rc: YAATTAAH
CC  consensus.regexp: [agt]tTaATT[ag]
CC  consensus.regexp.rc: [CT]AATTAA[ACT]
XX
//
AC  eve
XX
ID  eve
XX
DE  mACaaTwA
P0       A     C     G     T
1        8     9     3     0
2       18     0     2     0
3        1    16     0     3
4       13     3     4     0
5       13     0     3     4
6        0     1     0    19
7        9     1     1     9
8       19     0     0     1
XX
CC  program: clusterbuster
CC  matrix.nb: 18
CC  consensus.strict: cACaaTaA
CC  consensus.strict.rc: TTATTGTG
CC  consensus.IUPAC: mACaaTwA
CC  consensus.IUPAC.rc: TWATTGTK
CC  consensus.regexp: [ac]ACaaT[at]A
CC  consensus.regexp.rc: T[AT]ATTGT[GT]
XX
//
AC  ftz
XX
ID  ftz
XX
DE  mmATtArm
P0       A     C     G     T
1       17    32     4    13
2       30    29     0     7
3       63     1     0     2
4        0     1     1    64
5       15     4     3    44
6       62     3     1     0
7       35     7    18     6
8       28    17    14     7
XX
CC  program: clusterbuster
CC  matrix.nb: 19
CC  consensus.strict: caATtAaa
CC  consensus.strict.rc: TTTAATTG
CC  consensus.IUPAC: mmATtArm
CC  consensus.IUPAC.rc: KYTAATKK
CC  consensus.regexp: [ac][ac]ATtA[ag][ac]
CC  consensus.regexp.rc: [GT][CT]TAAT[GT][GT]
XX
//
AC  grh
XX
ID  grh
XX
DE  AACmAG
P0       A     C     G     T
1        9     0     0     0
2        7     0     2     0
3        1     7     0     1
4        3     6     0     0
5        7     0     2     0
6        0     0     7     2
XX
CC  program: clusterbuster
CC  matrix.nb: 20
CC  consensus.strict: AACcAG
CC  consensus.strict.rc: CTGGTT
CC  consensus.IUPAC: AACmAG
CC  consensus.IUPAC.rc: CTKGTT
CC  consensus.regexp: AAC[ac]AG
CC  consensus.regexp.rc: CT[GT]GTT
XX
//
AC  gt
XX
ID  gt
XX
DE  GTmAwAW
P0       A     C     G     T
1        0     1     7     0
2        1     1     0     6
3        5     3     0     0
4        8     0     0     0
5        3     0     0     5
6        8     0     0     0
7        6     0     0     2
XX
CC  program: clusterbuster
CC  matrix.nb: 21
CC  consensus.strict: GTaAtAA
CC  consensus.strict.rc: TTATTAC
CC  consensus.IUPAC: GTmAwAW
CC  consensus.IUPAC.rc: WTWTKAC
CC  consensus.regexp: GT[ac]A[at]A[AT]
CC  consensus.regexp.rc: [AT]T[AT]T[GT]AC
XX
//
AC  hb
XX
ID  hb
XX
DE  mayAAAAAa
P0       A     C     G     T
1    30.2913 49.4757 21.2039 3.02913
2    47.4563 16.1553 17.165 23.2233
3    1.00971 29.2816 11.1068 62.6019
4    96.932 1.00971 4.03883 2.01942
5    101.981 1.00971     0 1.00971
6      104     0     0     0
7    97.9417 1.00971 4.03883 1.00971
8    102.99 1.00971     0     0
9    68.6602 8.07767 12.1165 15.1456
XX
CC  program: clusterbuster
CC  matrix.nb: 22
CC  consensus.strict: catAAAAAa
CC  consensus.strict.rc: TTTTTTATG
CC  consensus.IUPAC: mayAAAAAa
CC  consensus.IUPAC.rc: TTTTTTRTK
CC  consensus.regexp: [ac]a[ct]AAAAAa
CC  consensus.regexp.rc: TTTTTT[AG]T[GT]
XX
//
AC  kni
XX
ID  kni
XX
DE  aaAayTrGAaCra
P0       A     C     G     T
1       19     2     7     5
2       19     3     5     6
3       32     0     0     1
4       21     4     0     8
5        7    12     4    10
6        0     1     8    24
7       14     3    15     1
8        5     2    26     0
9       28     2     3     0
10      11     8     8     6
11       4    24     1     4
12      16     3    11     3
13      21     8     4     0
XX
CC  program: clusterbuster
CC  matrix.nb: 23
CC  consensus.strict: aaAacTgGAaCaa
CC  consensus.strict.rc: TTGTTCCAGTTTT
CC  consensus.IUPAC: aaAayTrGAaCra
CC  consensus.IUPAC.rc: TYGTTCYARTTTT
CC  consensus.regexp: aaAa[ct]T[ag]GAaC[ag]a
CC  consensus.regexp.rc: T[CT]GTTC[CT]A[AG]TTTT
XX
//
AC  Kr
XX
ID  Kr
XX
DE  aAAaGGGTta
P0       A     C     G     T
1       24     7     9     5
2       38     3     3     1
3       33     8     2     2
4       23    10     7     5
5        2     0    41     2
6        5     0    39     1
7        6     4    34     1
8        3     0     2    40
9        6     5     4    30
10      27     6     7     5
XX
CC  program: clusterbuster
CC  matrix.nb: 24
CC  consensus.strict: aAAaGGGTta
CC  consensus.strict.rc: TAACCCTTTT
CC  consensus.IUPAC: aAAaGGGTta
CC  consensus.IUPAC.rc: TAACCCTTTT
CC  consensus.regexp: aAAaGGGTta
CC  consensus.regexp.rc: TAACCCTTTT
XX
//
AC  Mad
XX
ID  Mad
XX
DE  CGmsrCgmCrG
P0       A     C     G     T
1        3    15     1     0
2        1     0    17     1
3        7    11     1     0
4        1     7     8     3
5        6     3    10     0
6        2    14     0     3
7        3     1    12     3
8        6     8     2     3
9        0    19     0     0
10      10     3     6     0
11       1     0    17     1
XX
CC  program: clusterbuster
CC  matrix.nb: 25
CC  consensus.strict: CGcggCgcCaG
CC  consensus.strict.rc: CTGGCGCCGCG
CC  consensus.IUPAC: CGmsrCgmCrG
CC  consensus.IUPAC.rc: CYGKCGYSKCG
CC  consensus.regexp: CG[ac][cg][ag]Cg[ac]C[ag]G
CC  consensus.regexp.rc: C[CT]G[GT]CG[CT][CG][GT]CG
XX
//
AC  Med
XX
ID  Med
XX
DE  sCmssCsmmry
P0       A     C     G     T
1        1     2     5     0
2        0     8     0     0
3        5     3     0     0
4        0     3     4     1
5        0     4     4     0
6        0     7     1     0
7        0     3     5     0
8        3     5     0     0
9        4     4     0     0
10       2     0     5     1
11       1     2     0     5
XX
CC  program: clusterbuster
CC  matrix.nb: 26
CC  consensus.strict: gCagcCgcagt
CC  consensus.strict.rc: ACTGCGGCTGC
CC  consensus.IUPAC: sCmssCsmmry
CC  consensus.IUPAC.rc: RYKKSGSSKGS
CC  consensus.regexp: [cg]C[ac][cg][cg]C[cg][ac][ac][ag][ct]
CC  consensus.regexp.rc: [AG][CT][GT][GT][CG]G[CG][CG][GT]G[CG]
XX
//
AC  ovo
XX
ID  ovo
XX
DE  CyGTTA
P0       A     C     G     T
1        0    13     0     0
2    2.16667   6.5     0 4.33333
3        0     0    13     0
4    0.541667 0.541667     0 11.9167
5        0     0     0    13
6    11.9167     0 1.08333     0
XX
CC  program: clusterbuster
CC  matrix.nb: 27
CC  consensus.strict: CcGTTA
CC  consensus.strict.rc: TAACGG
CC  consensus.IUPAC: CyGTTA
CC  consensus.IUPAC.rc: TAACRG
CC  consensus.regexp: C[ct]GTTA
CC  consensus.regexp.rc: TAAC[AG]G
XX
//
AC  pan
XX
ID  pan
XX
DE  WtCAAA
P0       A     C     G     T
1    18.72     0     0  7.28
2      5.2  4.16     0 16.64
3     3.12  20.8  1.04  1.04
4     20.8     0     0   5.2
5    23.92  1.04     0  1.04
6    23.92     0     0  2.08
XX
CC  program: clusterbuster
CC  matrix.nb: 28
CC  consensus.strict: AtCAAA
CC  consensus.strict.rc: TTTGAT
CC  consensus.IUPAC: WtCAAA
CC  consensus.IUPAC.rc: TTTGAW
CC  consensus.regexp: [AT]tCAAA
CC  consensus.regexp.rc: TTTGA[AT]
XX
//
AC  prd
XX
ID  prd
XX
DE  GyGayAAw
P0       A     C     G     T
1        0     0    10     1
2        0     5     0     6
3        0     1     9     1
4        6     2     1     2
5        1     3     0     7
6       10     1     0     0
7       11     0     0     0
8        3     2     0     6
XX
CC  program: clusterbuster
CC  matrix.nb: 29
CC  consensus.strict: GtGatAAt
CC  consensus.strict.rc: ATTATCAC
CC  consensus.IUPAC: GyGayAAw
CC  consensus.IUPAC.rc: WTTRTCRC
CC  consensus.regexp: G[ct]Ga[ct]AA[at]
CC  consensus.regexp.rc: [AT]TT[AG]TC[AG]C
XX
//
AC  sd
XX
ID  sd
XX
DE  sraATthCwTTk
P0       A     C     G     T
1        3    10     6     0
2        8     4     7     0
3       11     4     0     4
4       16     1     1     1
5        1     3     0    15
6        3     0     3    13
7        5     5     0     9
8        1    14     4     0
9       10     2     0     7
10       4     1     0    14
11       3     0     0    16
12       0     4     7     8
XX
CC  program: clusterbuster
CC  matrix.nb: 30
CC  consensus.strict: caaATttCaTTt
CC  consensus.strict.rc: AAATGAAATTTG
CC  consensus.IUPAC: sraATthCwTTk
CC  consensus.IUPAC.rc: MAAWGDAATTYS
CC  consensus.regexp: [cg][ag]aATt[act]C[at]TT[gt]
CC  consensus.regexp.rc: [AC]AA[AT]G[AGT]AATT[CT][CG]
XX
//
AC  slbo
XX
ID  slbo
XX
DE  TkTGbmAwyA
P0       A     C     G     T
1        1     0     1    10
2        1     0     5     6
3        1     2     0     9
4        0     0    11     1
5        0     5     3     4
6        6     6     0     0
7       12     0     0     0
8        3     1     1     7
9        0     7     2     3
10      10     0     0     2
XX
CC  program: clusterbuster
CC  matrix.nb: 31
CC  consensus.strict: TtTGcaAtcA
CC  consensus.strict.rc: TGATTGCAAA
CC  consensus.IUPAC: TkTGbmAwyA
CC  consensus.IUPAC.rc: TRWTKVCAMA
CC  consensus.regexp: T[gt]TG[cgt][ac]A[at][ct]A
CC  consensus.regexp.rc: T[AG][AT]T[GT][ACG]CA[AC]A
XX
//
AC  sna
XX
ID  sna
XX
DE  AdCArGTdr
P0       A     C     G     T
1        9     2     0     0
2        4     0     4     3
3        0    11     0     0
4       10     0     1     0
5        4     0     6     1
6        0     0    11     0
7        0     0     2     9
8        3     0     5     3
9        3     2     6     0
XX
CC  program: clusterbuster
CC  matrix.nb: 32
CC  consensus.strict: AaCAgGTgg
CC  consensus.strict.rc: CCACCTGTT
CC  consensus.IUPAC: AdCArGTdr
CC  consensus.IUPAC.rc: YHACYTGHT
CC  consensus.regexp: A[agt]CA[ag]GT[agt][ag]
CC  consensus.regexp.rc: [CT][ACT]AC[CT]TG[ACT]T
XX
//
AC  tin
XX
ID  tin
XX
DE  CACTTGA
P0       A     C     G     T
1        0    11     0     0
2       10     1     0     0
3        0    11     0     0
4        0     0     0    11
5        0     0     0    11
6        2     1     8     0
7       10     0     1     0
XX
CC  program: clusterbuster
CC  matrix.nb: 33
CC  consensus.strict: CACTTGA
CC  consensus.strict.rc: TCAAGTG
CC  consensus.IUPAC: CACTTGA
CC  consensus.IUPAC.rc: TCAAGTG
CC  consensus.regexp: CACTTGA
CC  consensus.regexp.rc: TCAAGTG
XX
//
AC  tll
XX
ID  tll
XX
DE  AAAGTyAa
P0       A     C     G     T
1       26     1     6     4
2       27     4     5     1
3       27     0     9     1
4        8     0    28     1
5        2     2     1    32
6        1    19     1    16
7       30     1     6     0
8       25     3     5     4
XX
CC  program: clusterbuster
CC  matrix.nb: 34
CC  consensus.strict: AAAGTcAa
CC  consensus.strict.rc: TTGACTTT
CC  consensus.IUPAC: AAAGTyAa
CC  consensus.IUPAC.rc: TTRACTTT
CC  consensus.regexp: AAAGT[ct]Aa
CC  consensus.regexp.rc: TT[AG]ACTTT
XX
//
AC  Trl
XX
ID  Trl
XX
DE  TtkykCTCtCtct
P0       A     C     G     T
1       10    10     0    57
2       15     8    13    41
3        4    19    24    30
4        0    41    10    26
5        7     3    27    40
6        0    72     5     0
7        4     0    11    62
8        2    56     8    11
9        6    12     9    50
10      13    56     1     7
11       9    16     6    46
12       4    47     9    17
13      18     3    14    42
XX
CC  program: clusterbuster
CC  matrix.nb: 35
CC  consensus.strict: TttctCTCtCtct
CC  consensus.strict.rc: AGAGAGAGAGAAA
CC  consensus.IUPAC: TtkykCTCtCtct
CC  consensus.IUPAC.rc: AGAGAGAGMRMAA
CC  consensus.regexp: Tt[gt][ct][gt]CTCtCtct
CC  consensus.regexp.rc: AGAGAGAG[AC][AG][AC]AA
XX
//
AC  ttk
XX
ID  ttk
XX
DE  krTCCTk
P0       A     C     G     T
1        0     0 5.625 3.375
2    3.375     0 5.625     0
3        0     0     0     9
4        0     9     0     0
5        0     9     0     0
6        0 1.125     0 7.875
7        0     0 5.625 3.375
XX
CC  program: clusterbuster
CC  matrix.nb: 36
CC  consensus.strict: ggTCCTg
CC  consensus.strict.rc: CAGGACC
CC  consensus.IUPAC: krTCCTk
CC  consensus.IUPAC.rc: MAGGAYM
CC  consensus.regexp: [gt][ag]TCCT[gt]
CC  consensus.regexp.rc: [AC]AGGA[CT][AC]
XX
//
AC  twi
XX
ID  twi
XX
DE  mACAyaTG
P0       A     C     G     T
1    10.6667 5.33333     0     0
2     12.8   3.2     0     0
3        0    16     0     0
4       16     0     0     0
5    1.06667 7.46667 1.06667   6.4
6    10.6667   3.2 2.13333     0
7        0   3.2 1.06667 11.7333
8    1.06667     0 13.8667 1.06667
XX
CC  program: clusterbuster
CC  matrix.nb: 37
CC  consensus.strict: aACAcaTG
CC  consensus.strict.rc: CATGTGTT
CC  consensus.IUPAC: mACAyaTG
CC  consensus.IUPAC.rc: CATRTGTK
CC  consensus.regexp: [ac]ACA[ct]aTG
CC  consensus.regexp.rc: CAT[AG]TGT[GT]
XX
//
AC  Ubx
XX
ID  Ubx
XX
DE  mmATTA
P0       A     C     G     T
1       21 36.1667   3.5 9.33333
2       42  17.5   3.5     7
3    67.6667 2.33333     0     0
4      3.5     0     0  66.5
5    1.16667 4.66667     0 64.1667
6    58.3333 2.33333     0 9.33333
XX
CC  program: clusterbuster
CC  matrix.nb: 38
CC  consensus.strict: caATTA
CC  consensus.strict.rc: TAATTG
CC  consensus.IUPAC: mmATTA
CC  consensus.IUPAC.rc: TAATKK
CC  consensus.regexp: [ac][ac]ATTA
CC  consensus.regexp.rc: TAAT[GT][GT]
XX
//
AC  vvl
XX
ID  vvl
XX
DE  TAwkcA
P0       A     C     G     T
1        0     0     0    13
2    11.8182 1.18182     0     0
3    7.09091     0     0 5.90909
4        0     0 4.72727 8.27273
5    1.18182 8.27273 1.18182 2.36364
6       13     0     0     0
XX
CC  program: clusterbuster
CC  matrix.nb: 39
CC  consensus.strict: TAatcA
CC  consensus.strict.rc: TGATTA
CC  consensus.IUPAC: TAwkcA
CC  consensus.IUPAC.rc: TGMWTA
CC  consensus.regexp: TA[at][gt]cA
CC  consensus.regexp.rc: TG[AC][AT]TA
XX
//
AC  zen
XX
ID  zen
XX
DE  AATwAA
P0       A     C     G     T
1    11.9167 0.541667     0 0.541667
2    11.9167     0     0 1.08333
3        0 1.08333     0 11.9167
4    8.66667     0     0 4.33333
5       13     0     0     0
6       13     0     0     0
XX
CC  program: clusterbuster
CC  matrix.nb: 40
CC  consensus.strict: AATaAA
CC  consensus.strict.rc: TTTATT
CC  consensus.IUPAC: AATwAA
CC  consensus.IUPAC.rc: TTWATT
CC  consensus.regexp: AAT[at]AA
CC  consensus.regexp.rc: TT[AT]ATT
XX
//
AC  z
XX
ID  z
XX
DE  TGAGtG
P0       A     C     G     T
1    2.04878 7.17073 3.07317 29.7073
2        0     0    42     0
3       42     0     0     0
4        0     0    42     0
5    7.17073 10.2439     0 24.5854
6    1.02439 5.12195 30.7317 5.12195
XX
CC  program: clusterbuster
CC  matrix.nb: 41
CC  consensus.strict: TGAGtG
CC  consensus.strict.rc: CACTCA
CC  consensus.IUPAC: TGAGtG
CC  consensus.IUPAC.rc: CACTCA
CC  consensus.regexp: TGAGtG
CC  consensus.regexp.rc: CACTCA
XX
//
