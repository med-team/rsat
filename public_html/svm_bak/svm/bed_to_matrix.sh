## Input
#positive_seqs_bed=${workingdir}/CRM_heart_13-16_orthologues.bed
positive_seqs_bed=$1
genome=$2
shuffling=$3
workingdir=$4
output_matrix=${workingdir}/out_matrix.csv

#echo $workingdir

## Other inputs and variables
background_seqs="data/dmel-all-intergenic_and_introns-promoters+-500bp_r5.57.bed"
genomeInfo="data/dm3ChromInfo.txt"
#genome='dm3'
negative_seqs_bed=$workingdir/"background_"$(basename ${positive_seqs_bed}) # negative set
#ls -lh $negative_seqs_bed
#
background_freqs="data/2nt_intergenic_Drosophila_melanogaster.freq"
matrix_file="data/259_matrices.tf"

this_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Create negative set by shuffling background seqs with positive bed statistics ------------
#echo ${negative_seqs_bed}
rm -f ${negative_seqs_bed}
for ((i = 1; i <= $shuffling; i += 1))
	do
	shuffleBed -seed $i -incl $this_dir/${background_seqs} -chrom -i ${positive_seqs_bed} -g $this_dir/$genomeInfo >> ${negative_seqs_bed}
done


# Positive ---------------------------------------------------
seqs_bed=${positive_seqs_bed}
seqs_fasta=${workingdir}/crms.bed.fasta
seqs_ft=${workingdir}/crms.bed.ft
data_csv_pos=${workingdir}/crms.bed.csv
class=1

# returns $data_csv_pos=${workingdir}/crms.bed.csv
bash $this_dir/create_matrix.sh $seqs_bed $seqs_fasta $seqs_ft $background_freqs $matrix_file $data_csv_pos $class $workingdir &
#ls -lh $data_csv_pos

# Negative ---------------------------------------------------
seqs_bed=${negative_seqs_bed}
seqs_fasta=${workingdir}/background.bed.fasta
seqs_ft=${workingdir}/background.bed.ft
data_csv_neg=${workingdir}/background.bed.csv
class=-1

# returns $data_csv_pos=${workingdir}/crms.bed.csv
bash $this_dir/create_matrix.sh $seqs_bed $seqs_fasta $seqs_ft $background_freqs $matrix_file $data_csv_neg $class $workingdir &
#ls -lh $data_csv_neg

wait

# Merge positive and negative seqs in one table with one header--------------------------------
cp $data_csv_pos ${output_matrix}
tail -n+2 $data_csv_neg >> ${output_matrix}

