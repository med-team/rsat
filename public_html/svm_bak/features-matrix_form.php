<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>RSAT - Features matrix</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="main.css" media="screen,projection,print"/>
    <link rel="stylesheet" type="text/css" href="tabs.css" media="screen,projection,print"/>
    <script src="RSAT_menu.js" type="text/javascript"></script>
    <script src="RSAT_tabs.js" type="text/javascript"></script>
    <script type="text/javascript">
			function add_demo() {
				document.getElementById('genome').value = 'dm3';
				document.forms[0].bed.value="";
				document.getElementById('bedfile').value='/data/rsat/public_html/data/svm_rsat/CRM_13-16.bed'; 				
				document.forms[0].header[0].checked=true;			
				document.forms[0].output[0].checked=true;
				document.getElementById('xtime').value="5";
				document.getElementById('expName').value='HeartCRM_13-16h';									
 				document.forms[0].sequence_url.value = '/data/rsat/public_html/data/svm_rsat/data/CRM_13-16.bed';
}
		</script>
  </head>

  <body class="form">
    <div>
      <h3 align='center'><a href="http://pedagogix-tagc.univ-mrs.fr/rsat/">RSAT</a> - Features matrix</h3>
      <br/>
 
      <form method='post' action='features-matrix-ag.php' enctype='multipart/form-data'>
 	
        <fieldset>  
         <legend><b>General arguments</b></legend>    
         <b>Genome </b> <font style='color:orange'>(mandatory)</font>&nbsp;&nbsp;&nbsp;
          <select name='genome' id='genome'>
           <option value ='none'> ---UCSC genome--- </option>

<option value = 'dm3'>dm3 - D. melanogaster Apr. 2006 (BDGP R5/dm3)</option>

        </select><br/><br/> 
        <b>Give experience name &nbsp;&nbsp;</b><font style='color:orange'>(mandatory)</font><input type="text" id="expName" name="expName" size="10" placeholder="ExpName" /><br/>   </fieldset>       
    <p>
    <fieldset>
    <legend><b>Genomic coordinates</b></legend>  
	  <b>Genomic coordinates of positive training set</b> <font style='color:orange'>(mandatory)</font>
	  <br/>should be provided as a short bed file (3 columns) (<a target='_blank'
	  href='http://genome.ucsc.edu/FAQ/FAQformat.html#format1'>bed
	  format</a>), in any of the three following ways:

	  <ul type='square'>
	    <li>Paste coordinates<br/>
	      
              <textarea name='bed' rows='6' cols='45'></textarea></li>
	   <li>Specify the URL of bed file on remote server (e.g. Galaxy)<br/>
              <input type="text" name="sequence_url" size="62" /><br/></li>
            <li>Upload a file from your computer<br/>
	      <input type='file' name='bedfile' id='bedfile' size='40' /></li>
	  </ul>
	  </p>
	    
          <p><b><a href='help.fetch-sequences.html#header'>Header Format</a></b>
            <input type="radio" name="header" value="galaxy" checked="checked"/>Galaxy
	    <input type="radio" name="header" value="ucsc"/>UCSC
      <br/><br/>
<p>

</fieldset>
        <fieldset>
        <span><b>Genomic coordiantes of negative training set</b><font style='color:orange'>(mandatory)</font></span><br/>    
      
      	  <input type="radio" name="Negset" value="userNegset" /><b>Give a personnel bed file as negative training set<font style='color:green'> (optionnal, could be very slow depending of number of regions)</font></b> 
	  <br/>could be provided as a short bed file (3 columns) (<a target='_blank'
	  href='http://genome.ucsc.edu/FAQ/FAQformat.html#format1'>bed
	  format</a>), in any of the three following ways:

	  <ul type='square'>
	    <li>Paste coordinates<br/>
	      
              <textarea name='bed' rows='6' cols='45'></textarea></li>
	<li>Specify the URL of bed file on remote server (e.g. Galaxy)<br/>
              <input type="text" name="sequence_url" size="62" /><br/></li>
            <li>Upload a file from your computer<br/>
	      <input type='file' name='bedfile' id='bedfile' size='40' /></li>
	  </ul>
	  </p>
	    
          <p><b><a href='help.fetch-sequences.html#header'>Header Format</a></b>
            <input type="radio" name="header" value="galaxy" checked="checked"/>Galaxy
	    <input type="radio" name="header" value="ucsc"/>UCSC
<br/><br/> <br/>  
	
	
	<input type="radio" name="Negset" value="RandNegset"checked="checked"/> <b>Generate a negative random training set</b> 
	Specify the size of the desired random training set (X times positive set)<input type="text" name="xtime" size="5" placeholder="2"/><br/>         
      </fieldset><br/>
      
</div>
<p>
<fieldset>
<legend><b>Matrices</b><font style='color:orange'>(mandatory)</font></legend><b>Paste matrix (or matrices) in transfac format. (<a href='help.convert-matrix.html#io_format'>How to convert my matrices?</a>)</b>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!-- <A HREF='help.convert-matrix.html#io_format'><B>Format</B></a>&nbsp;<select name="matrix_format" >
<option value="alignace">alignace</option>
<option value="assembly">assembly</option>
<option value="cis-bp">cis-bp</option>
<option value="clustal">clustal</option>
<option value="cluster-buster">cluster-buster</option>
<option value="consensus">consensus</option>
<option value="encode">encode</option>
<option value="feature">feature</option>
<option value="gibbs">gibbs</option>
<option value="infogibbs">infogibbs</option>
<option value="jaspar">jaspar</option>
<option value="meme">meme</option>
<option value="motifsampler">motifsampler</option>
<option value="mscan">mscan</option>
<option value="sequences">sequences</option>
<option value="stamp">stamp</option>
<option value="stamp-transfac">stamp-transfac</option>
<option value="tab">tab</option>
<option selected="selected" value="transfac">transfac</option>
<option value="uniprobe">uniprobe</option>
</select> --><BR>
<div><div style='float:left;'><textarea name="matrix"  rows="4" cols="60"></textarea></div><div class="menu">
<div class="menu_heading_closed"
onclick="toggleMenu('98')" id="heading98"> Where to find matrices ?</div>
<div id="menu98" class="menu_collapsible">
	<a class="menu_item" href="http://www.pazar.info/" target="_blank">PAZAR</a>
	<a class="menu_item" href="http://the_brain.bwh.harvard.edu/uniprobe/" target="_blank">UniProbe</a>
	<a class="menu_item" href="http://www.gene-regulation.com/pub/databases.html" target="_blank">Transfac</a>
	<a class="menu_item" href="http://jaspar.cgb.ki.se/" target="_blank">Jaspar</a>
	<a class="menu_item" href="http://regulondb.ccg.unam.mx/download/Data_Sets.jsp" target="_blank">RegulonDB</a>

</div>
</div>
</div>

<div style="clear:both;"></div>

</fieldset><br/>
<!-- <p>
<fieldset>  
         <legend><b>ChIP-seq data</b></legend>    
         <b>Wig file </b> <font style='color:green'>(optionnal/coming soon!)</font>&nbsp;&nbsp;&nbsp;
	<ul type='square'>
	 Upload a wig file (Histones marks, TF...) from your computer<br/>
	<li>
	      <input type='file' name='wigfile' id='wigfile' size='40' /></li>
	<li>Specify the URL of wig file on remote server (e.g. Galaxy)<br/>
              <input type="text" name="sequence_urlWig" size="62" /><br/></li>
	</u>
	<p><b>Choose calculation method:</a></b>
            <input type="radio" name="intervalStatMethod" value="mean" checked="checked"/>Mean
	    <input type="radio" name="intervalStatMethod" value="cov"/>Coverage

</fieldset><br/>
</div> -->
            
      <b>Output</b>&nbsp;<input type="radio" name="output" value="display"  checked="checked"/>display <input type="radio" name="output" value="email"/>email <input type="text" name="user_email"  size="30" />

      <ul><table class='formbutton'>
        <tr valign=middle>
          <td><input type="submit" name="submit" value="GO" /></td>
          <td><input type="reset"  name="reset" /></td> 
          <td><input type="button" name="demo" value="Demo" onclick="add_demo()"/></td>  
          <td><b><a href='help.fetch-sequences.html'>[MANUAL]</a></b></td>
          <td><b><a href='http://www.bigre.ulb.ac.be/forums/' target='_top'>[ASK A QUESTION]</a></b></td>
        </tr></table>
      </ul>
    </div>
  </body>
</html>

